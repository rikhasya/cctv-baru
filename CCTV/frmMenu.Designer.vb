﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataBantuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahBarangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListBarangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MutasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanKoreksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanQtyNegativeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DaftarCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahSalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DaftarSalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabahSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DaftarSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahTeknisiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DaftarTeknisiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPembelianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembelianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPembelianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturPembelianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanReturPembelianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPOPembelianToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataPenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SOJualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanSOJualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturPenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanReturPenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanSuratJalanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanReturSuratJalanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataAkuntingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListHutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataHutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembayaranHutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PiutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListPiutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataHutangToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenerimaanPiutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataKasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InputBiayaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetoranAntarAccToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UtilityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TambahAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InputComplainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanComplainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JadwalKunjunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanHasilKunjunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPerbaikanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanHasilPerbaikanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanServiceSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanHasilServiceSupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.user = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.DataBantuToolStripMenuItem, Me.DataPembelianToolStripMenuItem, Me.DataPenjualanToolStripMenuItem, Me.DataAkuntingToolStripMenuItem, Me.UtilityToolStripMenuItem, Me.ServiceToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1004, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'DataBantuToolStripMenuItem
        '
        Me.DataBantuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahBarangToolStripMenuItem, Me.ListBarangToolStripMenuItem, Me.MutasiToolStripMenuItem, Me.LaporanKoreksiToolStripMenuItem, Me.LaporanQtyNegativeToolStripMenuItem, Me.ToolStripMenuItem2, Me.TambahCustomerToolStripMenuItem, Me.DaftarCustomerToolStripMenuItem, Me.TambahSalesToolStripMenuItem, Me.DaftarSalesToolStripMenuItem, Me.TabahSupplierToolStripMenuItem, Me.DaftarSupplierToolStripMenuItem, Me.TambahTeknisiToolStripMenuItem, Me.DaftarTeknisiToolStripMenuItem})
        Me.DataBantuToolStripMenuItem.Name = "DataBantuToolStripMenuItem"
        Me.DataBantuToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.DataBantuToolStripMenuItem.Text = "Data Bantu"
        '
        'TambahBarangToolStripMenuItem
        '
        Me.TambahBarangToolStripMenuItem.Name = "TambahBarangToolStripMenuItem"
        Me.TambahBarangToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.TambahBarangToolStripMenuItem.Text = "Tambah Barang"
        '
        'ListBarangToolStripMenuItem
        '
        Me.ListBarangToolStripMenuItem.Name = "ListBarangToolStripMenuItem"
        Me.ListBarangToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ListBarangToolStripMenuItem.Text = "Daftar Barang"
        '
        'MutasiToolStripMenuItem
        '
        Me.MutasiToolStripMenuItem.Name = "MutasiToolStripMenuItem"
        Me.MutasiToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.MutasiToolStripMenuItem.Text = "Mutasi"
        '
        'LaporanKoreksiToolStripMenuItem
        '
        Me.LaporanKoreksiToolStripMenuItem.Name = "LaporanKoreksiToolStripMenuItem"
        Me.LaporanKoreksiToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.LaporanKoreksiToolStripMenuItem.Text = "Laporan Koreksi"
        '
        'LaporanQtyNegativeToolStripMenuItem
        '
        Me.LaporanQtyNegativeToolStripMenuItem.Name = "LaporanQtyNegativeToolStripMenuItem"
        Me.LaporanQtyNegativeToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.LaporanQtyNegativeToolStripMenuItem.Text = "Laporan Qty Negative"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(189, 22)
        Me.ToolStripMenuItem2.Text = "________________"
        '
        'TambahCustomerToolStripMenuItem
        '
        Me.TambahCustomerToolStripMenuItem.Name = "TambahCustomerToolStripMenuItem"
        Me.TambahCustomerToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.TambahCustomerToolStripMenuItem.Text = "Tambah Customer"
        '
        'DaftarCustomerToolStripMenuItem
        '
        Me.DaftarCustomerToolStripMenuItem.Name = "DaftarCustomerToolStripMenuItem"
        Me.DaftarCustomerToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DaftarCustomerToolStripMenuItem.Text = "Daftar Customer"
        '
        'TambahSalesToolStripMenuItem
        '
        Me.TambahSalesToolStripMenuItem.Name = "TambahSalesToolStripMenuItem"
        Me.TambahSalesToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.TambahSalesToolStripMenuItem.Text = "Tambah Sales"
        '
        'DaftarSalesToolStripMenuItem
        '
        Me.DaftarSalesToolStripMenuItem.Name = "DaftarSalesToolStripMenuItem"
        Me.DaftarSalesToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DaftarSalesToolStripMenuItem.Text = "Daftar Sales"
        '
        'TabahSupplierToolStripMenuItem
        '
        Me.TabahSupplierToolStripMenuItem.Name = "TabahSupplierToolStripMenuItem"
        Me.TabahSupplierToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.TabahSupplierToolStripMenuItem.Text = "Tambah Supplier"
        '
        'DaftarSupplierToolStripMenuItem
        '
        Me.DaftarSupplierToolStripMenuItem.Name = "DaftarSupplierToolStripMenuItem"
        Me.DaftarSupplierToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DaftarSupplierToolStripMenuItem.Text = "Daftar Supplier"
        '
        'TambahTeknisiToolStripMenuItem
        '
        Me.TambahTeknisiToolStripMenuItem.Name = "TambahTeknisiToolStripMenuItem"
        Me.TambahTeknisiToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.TambahTeknisiToolStripMenuItem.Text = "Tambah Teknisi"
        '
        'DaftarTeknisiToolStripMenuItem
        '
        Me.DaftarTeknisiToolStripMenuItem.Name = "DaftarTeknisiToolStripMenuItem"
        Me.DaftarTeknisiToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DaftarTeknisiToolStripMenuItem.Text = "Daftar Teknisi"
        '
        'DataPembelianToolStripMenuItem
        '
        Me.DataPembelianToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PembelianToolStripMenuItem, Me.LaporanPOPembelianToolStripMenuItem, Me.LaporanPembelianToolStripMenuItem, Me.ReturPembelianToolStripMenuItem, Me.LaporanReturPembelianToolStripMenuItem})
        Me.DataPembelianToolStripMenuItem.Name = "DataPembelianToolStripMenuItem"
        Me.DataPembelianToolStripMenuItem.Size = New System.Drawing.Size(102, 20)
        Me.DataPembelianToolStripMenuItem.Text = "Data Pembelian"
        '
        'PembelianToolStripMenuItem
        '
        Me.PembelianToolStripMenuItem.Name = "PembelianToolStripMenuItem"
        Me.PembelianToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PembelianToolStripMenuItem.Text = "PO Pembelian"
        '
        'LaporanPembelianToolStripMenuItem
        '
        Me.LaporanPembelianToolStripMenuItem.Name = "LaporanPembelianToolStripMenuItem"
        Me.LaporanPembelianToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanPembelianToolStripMenuItem.Text = "Laporan Pembelian"
        '
        'ReturPembelianToolStripMenuItem
        '
        Me.ReturPembelianToolStripMenuItem.Name = "ReturPembelianToolStripMenuItem"
        Me.ReturPembelianToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ReturPembelianToolStripMenuItem.Text = "Retur Pembelian"
        '
        'LaporanReturPembelianToolStripMenuItem
        '
        Me.LaporanReturPembelianToolStripMenuItem.Name = "LaporanReturPembelianToolStripMenuItem"
        Me.LaporanReturPembelianToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanReturPembelianToolStripMenuItem.Text = "Laporan Retur Pembelian"
        '
        'LaporanPOPembelianToolStripMenuItem
        '
        Me.LaporanPOPembelianToolStripMenuItem.Name = "LaporanPOPembelianToolStripMenuItem"
        Me.LaporanPOPembelianToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanPOPembelianToolStripMenuItem.Text = "Laporan PO Pembelian"
        '
        'DataPenjualanToolStripMenuItem
        '
        Me.DataPenjualanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SOJualToolStripMenuItem, Me.LaporanSOJualToolStripMenuItem, Me.LaporanPenjualanToolStripMenuItem, Me.ReturPenjualanToolStripMenuItem, Me.LaporanReturPenjualanToolStripMenuItem, Me.LaporanSuratJalanToolStripMenuItem, Me.LaporanReturSuratJalanToolStripMenuItem})
        Me.DataPenjualanToolStripMenuItem.Name = "DataPenjualanToolStripMenuItem"
        Me.DataPenjualanToolStripMenuItem.Size = New System.Drawing.Size(98, 20)
        Me.DataPenjualanToolStripMenuItem.Text = "Data Penjualan"
        '
        'SOJualToolStripMenuItem
        '
        Me.SOJualToolStripMenuItem.Name = "SOJualToolStripMenuItem"
        Me.SOJualToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.SOJualToolStripMenuItem.Text = "SO Jual"
        '
        'LaporanSOJualToolStripMenuItem
        '
        Me.LaporanSOJualToolStripMenuItem.Name = "LaporanSOJualToolStripMenuItem"
        Me.LaporanSOJualToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanSOJualToolStripMenuItem.Text = "Laporan SO Jual"
        '
        'LaporanPenjualanToolStripMenuItem
        '
        Me.LaporanPenjualanToolStripMenuItem.Name = "LaporanPenjualanToolStripMenuItem"
        Me.LaporanPenjualanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanPenjualanToolStripMenuItem.Text = "Laporan Penjualan"
        '
        'ReturPenjualanToolStripMenuItem
        '
        Me.ReturPenjualanToolStripMenuItem.Name = "ReturPenjualanToolStripMenuItem"
        Me.ReturPenjualanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ReturPenjualanToolStripMenuItem.Text = "Retur Penjualan"
        '
        'LaporanReturPenjualanToolStripMenuItem
        '
        Me.LaporanReturPenjualanToolStripMenuItem.Name = "LaporanReturPenjualanToolStripMenuItem"
        Me.LaporanReturPenjualanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanReturPenjualanToolStripMenuItem.Text = "Laporan Retur Penjualan"
        '
        'LaporanSuratJalanToolStripMenuItem
        '
        Me.LaporanSuratJalanToolStripMenuItem.Name = "LaporanSuratJalanToolStripMenuItem"
        Me.LaporanSuratJalanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanSuratJalanToolStripMenuItem.Text = "Laporan Surat Jalan"
        '
        'LaporanReturSuratJalanToolStripMenuItem
        '
        Me.LaporanReturSuratJalanToolStripMenuItem.Name = "LaporanReturSuratJalanToolStripMenuItem"
        Me.LaporanReturSuratJalanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.LaporanReturSuratJalanToolStripMenuItem.Text = "Laporan Retur Surat Jalan"
        '
        'DataAkuntingToolStripMenuItem
        '
        Me.DataAkuntingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HutangToolStripMenuItem, Me.PiutangToolStripMenuItem, Me.DataKasToolStripMenuItem, Me.InputBiayaToolStripMenuItem, Me.SetoranAntarAccToolStripMenuItem})
        Me.DataAkuntingToolStripMenuItem.Name = "DataAkuntingToolStripMenuItem"
        Me.DataAkuntingToolStripMenuItem.Size = New System.Drawing.Size(95, 20)
        Me.DataAkuntingToolStripMenuItem.Text = "Data Akunting"
        '
        'HutangToolStripMenuItem
        '
        Me.HutangToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListHutangToolStripMenuItem, Me.DataHutangToolStripMenuItem, Me.PembayaranHutangToolStripMenuItem})
        Me.HutangToolStripMenuItem.Name = "HutangToolStripMenuItem"
        Me.HutangToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.HutangToolStripMenuItem.Text = "Hutang"
        '
        'ListHutangToolStripMenuItem
        '
        Me.ListHutangToolStripMenuItem.Name = "ListHutangToolStripMenuItem"
        Me.ListHutangToolStripMenuItem.Size = New System.Drawing.Size(183, 22)
        Me.ListHutangToolStripMenuItem.Text = "List Hutang"
        '
        'DataHutangToolStripMenuItem
        '
        Me.DataHutangToolStripMenuItem.Name = "DataHutangToolStripMenuItem"
        Me.DataHutangToolStripMenuItem.Size = New System.Drawing.Size(183, 22)
        Me.DataHutangToolStripMenuItem.Text = "Data Hutang"
        '
        'PembayaranHutangToolStripMenuItem
        '
        Me.PembayaranHutangToolStripMenuItem.Name = "PembayaranHutangToolStripMenuItem"
        Me.PembayaranHutangToolStripMenuItem.Size = New System.Drawing.Size(183, 22)
        Me.PembayaranHutangToolStripMenuItem.Text = "Pembayaran Hutang"
        '
        'PiutangToolStripMenuItem
        '
        Me.PiutangToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListPiutangToolStripMenuItem, Me.DataHutangToolStripMenuItem1, Me.PenerimaanPiutangToolStripMenuItem})
        Me.PiutangToolStripMenuItem.Name = "PiutangToolStripMenuItem"
        Me.PiutangToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.PiutangToolStripMenuItem.Text = "Piutang"
        '
        'ListPiutangToolStripMenuItem
        '
        Me.ListPiutangToolStripMenuItem.Name = "ListPiutangToolStripMenuItem"
        Me.ListPiutangToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.ListPiutangToolStripMenuItem.Text = "List Piutang"
        '
        'DataHutangToolStripMenuItem1
        '
        Me.DataHutangToolStripMenuItem1.Name = "DataHutangToolStripMenuItem1"
        Me.DataHutangToolStripMenuItem1.Size = New System.Drawing.Size(181, 22)
        Me.DataHutangToolStripMenuItem1.Text = "Data Piutang"
        '
        'PenerimaanPiutangToolStripMenuItem
        '
        Me.PenerimaanPiutangToolStripMenuItem.Name = "PenerimaanPiutangToolStripMenuItem"
        Me.PenerimaanPiutangToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.PenerimaanPiutangToolStripMenuItem.Text = "Penerimaan Piutang"
        '
        'DataKasToolStripMenuItem
        '
        Me.DataKasToolStripMenuItem.Name = "DataKasToolStripMenuItem"
        Me.DataKasToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.DataKasToolStripMenuItem.Text = "Data Kas"
        '
        'InputBiayaToolStripMenuItem
        '
        Me.InputBiayaToolStripMenuItem.Name = "InputBiayaToolStripMenuItem"
        Me.InputBiayaToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.InputBiayaToolStripMenuItem.Text = "Input Biaya"
        '
        'SetoranAntarAccToolStripMenuItem
        '
        Me.SetoranAntarAccToolStripMenuItem.Name = "SetoranAntarAccToolStripMenuItem"
        Me.SetoranAntarAccToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.SetoranAntarAccToolStripMenuItem.Text = "Setoran Antar Acc"
        '
        'UtilityToolStripMenuItem
        '
        Me.UtilityToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahUserToolStripMenuItem, Me.DataUserToolStripMenuItem, Me.AccountToolStripMenuItem})
        Me.UtilityToolStripMenuItem.Name = "UtilityToolStripMenuItem"
        Me.UtilityToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.UtilityToolStripMenuItem.Text = "Utility"
        '
        'TambahUserToolStripMenuItem
        '
        Me.TambahUserToolStripMenuItem.Name = "TambahUserToolStripMenuItem"
        Me.TambahUserToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.TambahUserToolStripMenuItem.Text = "Tambah User"
        '
        'DataUserToolStripMenuItem
        '
        Me.DataUserToolStripMenuItem.Name = "DataUserToolStripMenuItem"
        Me.DataUserToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DataUserToolStripMenuItem.Text = "Data User"
        '
        'AccountToolStripMenuItem
        '
        Me.AccountToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahAccountToolStripMenuItem, Me.DataAccountToolStripMenuItem})
        Me.AccountToolStripMenuItem.Name = "AccountToolStripMenuItem"
        Me.AccountToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.AccountToolStripMenuItem.Text = "Account"
        '
        'TambahAccountToolStripMenuItem
        '
        Me.TambahAccountToolStripMenuItem.Name = "TambahAccountToolStripMenuItem"
        Me.TambahAccountToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.TambahAccountToolStripMenuItem.Text = "Tambah Account"
        '
        'DataAccountToolStripMenuItem
        '
        Me.DataAccountToolStripMenuItem.Name = "DataAccountToolStripMenuItem"
        Me.DataAccountToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.DataAccountToolStripMenuItem.Text = "Data Account"
        '
        'ServiceToolStripMenuItem
        '
        Me.ServiceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InputComplainToolStripMenuItem, Me.LaporanComplainToolStripMenuItem, Me.JadwalKunjunganToolStripMenuItem, Me.LaporanHasilKunjunganToolStripMenuItem, Me.LaporanPerbaikanToolStripMenuItem, Me.LaporanHasilPerbaikanToolStripMenuItem, Me.LaporanServiceSupplierToolStripMenuItem, Me.LaporanHasilServiceSupplierToolStripMenuItem})
        Me.ServiceToolStripMenuItem.Name = "ServiceToolStripMenuItem"
        Me.ServiceToolStripMenuItem.Size = New System.Drawing.Size(56, 20)
        Me.ServiceToolStripMenuItem.Text = "Service"
        '
        'InputComplainToolStripMenuItem
        '
        Me.InputComplainToolStripMenuItem.Name = "InputComplainToolStripMenuItem"
        Me.InputComplainToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.InputComplainToolStripMenuItem.Text = "Input Complain"
        '
        'LaporanComplainToolStripMenuItem
        '
        Me.LaporanComplainToolStripMenuItem.Name = "LaporanComplainToolStripMenuItem"
        Me.LaporanComplainToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.LaporanComplainToolStripMenuItem.Text = "Laporan Complain"
        '
        'JadwalKunjunganToolStripMenuItem
        '
        Me.JadwalKunjunganToolStripMenuItem.Name = "JadwalKunjunganToolStripMenuItem"
        Me.JadwalKunjunganToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.JadwalKunjunganToolStripMenuItem.Text = "Jadwal Kunjungan"
        '
        'LaporanHasilKunjunganToolStripMenuItem
        '
        Me.LaporanHasilKunjunganToolStripMenuItem.Name = "LaporanHasilKunjunganToolStripMenuItem"
        Me.LaporanHasilKunjunganToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.LaporanHasilKunjunganToolStripMenuItem.Text = "Laporan Hasil Kunjungan"
        '
        'LaporanPerbaikanToolStripMenuItem
        '
        Me.LaporanPerbaikanToolStripMenuItem.Name = "LaporanPerbaikanToolStripMenuItem"
        Me.LaporanPerbaikanToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.LaporanPerbaikanToolStripMenuItem.Text = "Laporan Perbaikan"
        '
        'LaporanHasilPerbaikanToolStripMenuItem
        '
        Me.LaporanHasilPerbaikanToolStripMenuItem.Name = "LaporanHasilPerbaikanToolStripMenuItem"
        Me.LaporanHasilPerbaikanToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.LaporanHasilPerbaikanToolStripMenuItem.Text = "Laporan Hasil Perbaikan"
        '
        'LaporanServiceSupplierToolStripMenuItem
        '
        Me.LaporanServiceSupplierToolStripMenuItem.Name = "LaporanServiceSupplierToolStripMenuItem"
        Me.LaporanServiceSupplierToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.LaporanServiceSupplierToolStripMenuItem.Text = "Laporan Service Supplier"
        '
        'LaporanHasilServiceSupplierToolStripMenuItem
        '
        Me.LaporanHasilServiceSupplierToolStripMenuItem.Name = "LaporanHasilServiceSupplierToolStripMenuItem"
        Me.LaporanHasilServiceSupplierToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.LaporanHasilServiceSupplierToolStripMenuItem.Text = "Laporan Hasil Service Supplier"
        '
        'user
        '
        Me.user.AutoSize = True
        Me.user.BackColor = System.Drawing.Color.LightBlue
        Me.user.Location = New System.Drawing.Point(813, 9)
        Me.user.Name = "user"
        Me.user.Size = New System.Drawing.Size(27, 13)
        Me.user.TabIndex = 2
        Me.user.Text = "user"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.LightBlue
        Me.Label2.Location = New System.Drawing.Point(872, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "is working"
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 690)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.user)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataBantuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahBarangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListBarangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DaftarCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahSalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DaftarSalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabahSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DaftarSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPembelianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataAkuntingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembelianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPembelianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturPembelianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanReturPembelianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanReturPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UtilityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SOJualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanSOJualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanSuratJalanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListHutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataHutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembayaranHutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PiutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListPiutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataHutangToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenerimaanPiutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataKasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents user As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MutasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanReturSuratJalanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanKoreksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanQtyNegativeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InputBiayaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InputComplainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanComplainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JadwalKunjunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPerbaikanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanServiceSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanHasilServiceSupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanHasilPerbaikanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TambahTeknisiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DaftarTeknisiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanHasilKunjunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetoranAntarAccToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPOPembelianToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
