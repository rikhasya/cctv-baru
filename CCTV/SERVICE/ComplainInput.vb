﻿Public Class ComplainInput

    Public garansi As Integer

    Private Sub ComplainInput_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F2 Then
            Cari_faktur.ksup.Text = CBlgn.SelectedValue
            Cari_faktur.dari.Text = "CMPN"
            Cari_faktur.Show()

        ElseIf e.KeyCode = Keys.F1 Then
            Cari_Brg.dari.Text = "CMPL"
            Dim s As String = noJual.Text
            If noJual.Text <> "" Then
                s = noJual.Text
                s = s.Substring(0, 2)
            End If
            Cari_Brg.no.Text = s
            Cari_Brg.Show()
        End If
    End Sub

    Private Sub ComplainInput_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub ComplainInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim tgl As System.DateTime = Tgl1.Text
        Tgl2.Text = tgl.AddDays(3)
        Console.WriteLine(Tgl2.Text)

        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With

    End Sub

    Private Sub CBlgn_KeyDown(sender As Object, e As KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            JTgaransi.Focus()
        End If
    End Sub

    Private Sub CBlgn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBlgn.SelectedIndexChanged

    End Sub

    Private Sub batal_Click(sender As Object, e As EventArgs) Handles batal.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Input Complain ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Me.Close()
        Exit Sub
    End Sub

    Private Sub simpan_Click(sender As Object, e As EventArgs) Handles simpan.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Complain ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsComplain
        Dim t As String
        t = a.no()

        Dim table2 As New DataTable("DETJADKUNJUNGAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")
        table2.Columns.Add("KET2")

        For i = 0 To DGVbrg.Rows.Count - 1
            table2.Rows.Add(t, DGVbrg.Item("K_BRG", i).Value.ToString, DGVbrg.Item("N_BRG", i).Value.ToString, DGVbrg.Item("QTY1", i).Value.ToString, DGVbrg.Item("SAT", i).Value.ToString, DGVbrg.Item("KET2", i).Value.ToString)
        Next

        Dim b As Boolean
        b = a.input(t, CDate(Tgl1.Text), kett.Text, CBlgn.SelectedValue, CDate(Tgl2.Text), noJual.Text, tisi.Text, table2)
        If b = True Then
            MsgBox("Berhasil Input Complain")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Input Complain")
            Exit Sub
        End If

    End Sub

    Private Sub INPUTbrg_Click(sender As Object, e As EventArgs) Handles INPUTbrg.Click
       
        If qty.Text = "" Then
            MsgBox("CEK QTY")
            Exit Sub
        End If

        Dim y As Integer = 0
        For i = 0 To DGVbrg.Rows.Count - 1
            If DGVbrg.Item("K_BRG", i).Value.ToString = kbrg.Text Then
                DGVbrg.Item("qty1", i).Value = DGVbrg.Item("qty1", i).Value + CDbl(qty.Text)
                y = 1
            End If
        Next

        If y = 0 Then
            With DGVbrg
                .Rows.Add(1)
                .Rows(DGVbrg.Rows.Count() - 1).Cells("k_brg").Value = kbrg.Text
                .Rows(DGVbrg.Rows.Count() - 1).Cells("n_brg").Value = nbrg.Text
                .Rows(DGVbrg.Rows.Count() - 1).Cells("qty1").Value = CDbl(qty.Text)
                .Rows(DGVbrg.Rows.Count() - 1).Cells("sat").Value = satuan.Text
                .Rows(DGVbrg.Rows.Count() - 1).Cells("KET2").Value = ketRusak.Text
            End With
        End If

        If serial2.Text <> "" Then
            tisi.Text = tisi.Text & " kode : " & kbrg.Text & " , serial : " & serial2.Text & " . "
        End If

        kbrg.Clear()
        nbrg.Clear()
        qty.Text = 0
        satuan.Clear()
        ketRusak.Clear()
        Panel1.Enabled = False
    End Sub

    Private Sub DGVbrg_DoubleClick(sender As Object, e As EventArgs) Handles DGVbrg.DoubleClick
        Dim baris As Integer
        baris = DGVbrg.CurrentRow.Index.ToString

        kbrg.Text = DGVbrg.Item("K_BRG", baris).Value.ToString
        nbrg.Text = DGVbrg.Item("N_BRG", baris).Value.ToString
        qty.Text = DGVbrg.Item("QTY1", baris).Value.ToString
        satuan.Text = DGVbrg.Item("SAT", baris).Value.ToString
       
        DGVbrg.Rows.RemoveAt(baris)

    End Sub

    Private Sub dateFak_ValueChanged(sender As Object, e As EventArgs) Handles dateFak.ValueChanged
        Dim s As System.DateTime = CDate(dateFak.Text)
        JTgaransi.Text = s.AddYears(1)
    End Sub

    Private Sub serial2_Leave(sender As Object, e As EventArgs) Handles serial2.Leave
        Dim a As New clsCCTV.clsSJ
        Dim b As DataTable
        b = a.tableSerial(serial2.Text)

        If b.Rows.Count = 1 Then
            If b.Rows(0)("K_BRG") <> kbrg.Text Then
                MsgBox("kode barang yang di pilih salah")
                Exit Sub
            Else
                noJual.Text = b.Rows(0)("NO_FAK")
                dateFak.Text = FormatDateTime(b.Rows(0)("TGL_FAK"), DateFormat.ShortDate)
            End If
        End If
    End Sub

End Class