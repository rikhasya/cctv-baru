﻿Imports System.Drawing.Printing
Imports System.IO

Public Class ServiceLaporanHasil

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1
    Private Sub ServiceLaporanHasil_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub ServiceLaporanHasil_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_SUP") = "ALL"
        row("K_SUP") = "0"
        b.Rows.Add(row)
        With CBsup
            .DisplayMember = "N_SUP"
            .ValueMember = "K_SUP"
            .DataSource = b
        End With
        CBsup.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim sup As String
        If CBsup.Text = "ALL" Then
            sup = "%"
        Else
            sup = CBsup.SelectedValue
        End If

        Dim a As New clsCCTV.clsReService
        Dim tb As DataSet
        tb = a.table(date1.Text, date2.Text, sup)

        Dim i As Integer
        For i = 0 To tb.Tables("DATRESERVICE").Rows.Count - 1
            Console.WriteLine("dat" & tb.Tables("DATRESERVICE").Rows(i)(0).ToString)
        Next

        For i = 0 To tb.Tables("DETRESERVICE").Rows.Count - 1
            Console.WriteLine("det" & tb.Tables("DETRESERVICE").Rows(i)(0).ToString)
        Next

        Dim parentColumn As DataColumn = tb.Tables("DATRESERVICE").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETRESERVICE").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETRESERVICE", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATRESERVICE"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETRESERVICE"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVheader.Focus()
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SVCHASIL_EDITHAPUS")
        If b = True Then

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ServiceEditHapusHasil
            frm = New ServiceEditHapusHasil
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Hasil Service"
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.noFak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.date1.Text = FormatDateTime(DGVheader.Item("TANGGAL", row).Value.ToString, DateFormat.ShortDate)
            frm.supplier.Text = DGVheader.Item("N_SUP", row).Value.ToString
            frm.kSup.Text = DGVheader.Item("K_SUP", row).Value.ToString
            frm.noService.Text = DGVheader.Item("NO_SERVICE", row).Value.ToString
            frm.ket2.Text = DGVheader.Item("KET", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub
    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("PRINTULANGLAPORAN")
            If b = True Then
                PRINT()
            Else
                MsgBox("anda tdk di otorisasi")
            End If
        End If
    End Sub
    Sub PRINT()
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Ulang Faktur Hasil Service?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsReService
            ServiceInputHasil.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With ServiceInputHasil
                .tmpnofak = .tb.Tables("DATRESERVICE").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = ServiceInputHasil.tb.Tables("DETRESERVICE").Rows.Count - 1

            ReDim ServiceInputHasil.tmpKode(jumlah)
            ReDim ServiceInputHasil.tmpNama(jumlah)
            ReDim ServiceInputHasil.tmpQty(jumlah)
            ReDim ServiceInputHasil.tmpSat(jumlah)
            ReDim ServiceInputHasil.tmpHrg(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With ServiceInputHasil
                    .tmpKode(ipo) = .tb.Tables("DETRESERVICE").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETRESERVICE").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETRESERVICE").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETRESERVICE").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETRESERVICE").Rows(ipo)("KET").ToString
                End With
            Next

            ServiceInputHasil.arrke = 0

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf ServiceInputHasil.faktur
            PrintDoc.Print()
            Exit Sub
        End If
    End Sub

    Private Sub CBsup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBsup.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class