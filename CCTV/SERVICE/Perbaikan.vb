﻿Public Class Perbaikan

    Private Sub Perbaikan_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.dari.Text = "PBKN"
            Cari_Brg.Show()
        End If
    End Sub

    Private Sub Perbaikan_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Perbaikan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsTehnisi
        Dim b As DataTable
        b = a.table("%", "%")
        With cbTek
            .DisplayMember = "N_TEH"
            .ValueMember = "K_TEH"
            .DataSource = b
        End With

        Dim c As New clsCCTV.clsKunjungan
        Dim d As DataSet
        d = c.table(noVisit.Text)
        DGV1.DataSource = d.Tables("DETJADKUNJUNGAN2")

    End Sub

    Private Sub tqty_Leave(sender As Object, e As EventArgs)
        If IsNumeric(tqty.Text) Then
            tqty.Text = FormatNumber(tqty.Text, 2)
        End If
    End Sub

    Private Sub tqty1_Leave(sender As Object, e As EventArgs) Handles tqty1.Leave
        If IsNumeric(tqty1.Text) Then
            tqty1.Text = FormatNumber(tqty1.Text, 2)
        End If
    End Sub

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
        Dim row As Integer
        row = DGV1.CurrentRow.Index.ToString
        kbrg.Text = DGV1.Item("K_BRG", row).Value.ToString
        nbrg.Text = DGV1.Item("N_BRG", row).Value.ToString
        tsat.Text = DGV1.Item("SAT", row).Value.ToString
    End Sub

    Private Sub DGV2_CellClick(sender As Object, e As DataGridViewCellEventArgs)
        Dim row As Integer
        row = DGV2.CurrentRow.Index.ToString
        'For i = 0 To DGV3.Rows.Count - 1
        '    If DGV3.Item("K_BRG3", i).Value.ToString = DGV2.Item("K_BRG1", row).Value.ToString Then
        '        MsgBox("Sudah input sparepart untuk barang tersebut")
        '        Exit Sub
        '    End If
        'Next
        'kodebrg.Text = DGV2.Item("K_BRG1", row).Value.ToString
        'namabrg.Text = DGV2.Item("N_BRG1", row).Value.ToString
        ' DGV3.Rows.RemoveAt(row)
    End Sub

    Private Sub DGV2_DoubleClick(sender As Object, e As EventArgs)
        Dim baris As Integer
        baris = DGV2.CurrentRow.Index.ToString
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & DGV2.Item("K_BRG1", baris).Value.ToString & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim i As Integer
        For i = 0 To DGV3.RowCount - 1
            If DGV3.Item("K_BRG3", i).Value = DGV2.Item("K_BRG1", baris).Value.ToString Then
                MsgBox("Sudah input sparepart untuk barang tersebut.")
                'namabrg.Text = "-"
                'kodebrg.Text = "-"
                Exit Sub
            End If
        Next

    End Sub

    Private Sub input1_Click(sender As Object, e As EventArgs)
        If kbrg.Text = "" Then
            MsgBox("Kode barang kosong")
            Exit Sub
        ElseIf tqty.Text = "" Then
            MsgBox("cek qty")
            Exit Sub
        ElseIf Not IsNumeric(tqty.Text) Or tqty.Text = 0 Then
            MsgBox("cek qty")
            Exit Sub
        End If
        Dim a As Boolean = True
        For b = 0 To DGV2.RowCount - 1
            Console.WriteLine(kbrg.Text & ":" & DGV2.Item("K_BRG1", b).Value)
            If kbrg.Text = DGV2.Item("K_BRG1", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        With DGV2
            .Rows.Add(1)
            .Rows(DGV2.Rows.Count() - 1).Cells("k_brg1").Value = kbrg.Text
            .Rows(DGV2.Rows.Count() - 1).Cells("n_brg1").Value = nbrg.Text
            .Rows(DGV2.Rows.Count() - 1).Cells("qty1").Value = CDbl(tqty.Text)
            .Rows(DGV2.Rows.Count() - 1).Cells("sat1").Value = tsat.Text
        End With

        kbrg.Clear()
        nbrg.Clear()
        tqty.Text = 0
        tsat.Clear()
    End Sub

    Private Sub input2_Click(sender As Object, e As EventArgs) Handles input2.Click
        If kbrg1.Text = "" Then
            MsgBox("Kode barang kosong")
            Exit Sub
        ElseIf tqty1.Text = "" Then
            MsgBox("cek qty")
            Exit Sub
        ElseIf Not IsNumeric(tqty1.Text) Or tqty1.Text = 0 Then
            MsgBox("cek qty")
            Exit Sub
        End If

        Dim a As Boolean = True
        For b = 0 To DGV3.RowCount - 1
            Console.WriteLine(kbrg1.Text & ":" & DGV3.Item("K_BRG2", b).Value)
            If kbrg1.Text = DGV3.Item("K_BRG2", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        With DGV3
            .Rows.Add(1)
            .Rows(DGV3.Rows.Count() - 1).Cells("k_brg2").Value = kbrg1.Text
            .Rows(DGV3.Rows.Count() - 1).Cells("n_brg2").Value = nbrg1.Text
            .Rows(DGV3.Rows.Count() - 1).Cells("qty2").Value = CDbl(tqty1.Text)
            .Rows(DGV3.Rows.Count() - 1).Cells("sat2").Value = tsat1.Text
        End With

        kbrg1.Clear()
        nbrg1.Clear()
        tqty1.Text = 0
        tsat1.Clear()
    End Sub

    Private Sub DGV2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub DGV3_DoubleClick(sender As Object, e As EventArgs) Handles DGV3.DoubleClick
        Dim baris As Integer
        baris = DGV3.CurrentRow.Index.ToString
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & DGV3.Item("K_BRG2", baris).Value.ToString & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        DGV3.Rows.RemoveAt(baris)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Input Perbaikan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Me.Close()
        Exit Sub
    End Sub

    Private Sub SIMPAN_Click(sender As Object, e As EventArgs) Handles SIMPAN.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Perbaikan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsPerbaikan
        Dim t As String
        t = a.no()

        Dim table1 As New DataTable("DETPERBAIKAN")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim table2 As New DataTable("DETPERBAIKAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG_RUSAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")

        For i = 0 To DGV2.Rows.Count - 1
            table1.Rows.Add(t, DGV2.Item("K_BRG1", i).Value.ToString, DGV2.Item("N_BRG1", i).Value.ToString, DGV2.Item("QTY1", i).Value.ToString, DGV2.Item("SAT1", i).Value.ToString)
        Next
        For i = 0 To DGV3.Rows.Count - 1
            table2.Rows.Add(t, DGV3.Item("K_BRG3", i).Value.ToString, DGV3.Item("K_BRG2", i).Value.ToString, DGV3.Item("N_BRG2", i).Value.ToString, DGV3.Item("QTY2", i).Value.ToString, DGV3.Item("SAT2", i).Value.ToString)
        Next

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim b As Boolean
        b = a.input(t, CDate(date1.Text), keterangan.Text, noVisit.Text, CDate(date2.Text), cbTek.SelectedValue, set1)

        If b = True Then
            MsgBox("Berhasil Input Perbaikan")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Input Perbaikan")
            Exit Sub
        End If

    End Sub

    Private Sub DGV3_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV3.CellContentClick

    End Sub

End Class