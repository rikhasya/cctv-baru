﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JadwalInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbTeknisi = New System.Windows.Forms.ComboBox()
        Me.INPUTtek = New System.Windows.Forms.Button()
        Me.DGVtek = New System.Windows.Forms.DataGridView()
        Me.Kode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.simpan = New System.Windows.Forms.Button()
        Me.batal = New System.Windows.Forms.Button()
        Me.DGVbrg = New System.Windows.Forms.DataGridView()
        Me.k_brg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.n_brg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.qty1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.satuan = New System.Windows.Forms.TextBox()
        Me.nbrg = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.INPUTbrg = New System.Windows.Forms.Button()
        Me.qty = New System.Windows.Forms.TextBox()
        Me.kbrg = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.noComp = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.nlgn = New System.Windows.Forms.Label()
        Me.klgn = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ket = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVtek, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVbrg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(612, 123)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(91, 20)
        Me.date1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(609, 107)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Tanggal Kunjungan :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(117, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Teknisi yang bertugas :"
        '
        'cbTeknisi
        '
        Me.cbTeknisi.FormattingEnabled = True
        Me.cbTeknisi.Location = New System.Drawing.Point(10, 35)
        Me.cbTeknisi.Name = "cbTeknisi"
        Me.cbTeknisi.Size = New System.Drawing.Size(200, 21)
        Me.cbTeknisi.TabIndex = 3
        '
        'INPUTtek
        '
        Me.INPUTtek.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INPUTtek.Location = New System.Drawing.Point(138, 62)
        Me.INPUTtek.Name = "INPUTtek"
        Me.INPUTtek.Size = New System.Drawing.Size(75, 23)
        Me.INPUTtek.TabIndex = 4
        Me.INPUTtek.Text = "INPUT"
        Me.INPUTtek.UseVisualStyleBackColor = True
        '
        'DGVtek
        '
        Me.DGVtek.AllowUserToDeleteRows = False
        Me.DGVtek.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVtek.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVtek.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Kode, Me.nama})
        Me.DGVtek.Location = New System.Drawing.Point(12, 102)
        Me.DGVtek.Name = "DGVtek"
        Me.DGVtek.RowHeadersVisible = False
        Me.DGVtek.Size = New System.Drawing.Size(234, 191)
        Me.DGVtek.TabIndex = 5
        '
        'Kode
        '
        Me.Kode.HeaderText = "Kode"
        Me.Kode.Name = "Kode"
        Me.Kode.Visible = False
        '
        'nama
        '
        Me.nama.HeaderText = "Nama Teknisi"
        Me.nama.Name = "nama"
        '
        'simpan
        '
        Me.simpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.simpan.Location = New System.Drawing.Point(784, 619)
        Me.simpan.Name = "simpan"
        Me.simpan.Size = New System.Drawing.Size(90, 37)
        Me.simpan.TabIndex = 6
        Me.simpan.Text = "SIMPAN"
        Me.simpan.UseVisualStyleBackColor = True
        '
        'batal
        '
        Me.batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.batal.Location = New System.Drawing.Point(880, 619)
        Me.batal.Name = "batal"
        Me.batal.Size = New System.Drawing.Size(90, 37)
        Me.batal.TabIndex = 7
        Me.batal.Text = "BATAL"
        Me.batal.UseVisualStyleBackColor = True
        '
        'DGVbrg
        '
        Me.DGVbrg.AllowUserToDeleteRows = False
        Me.DGVbrg.BackgroundColor = System.Drawing.Color.LightSalmon
        Me.DGVbrg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVbrg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.k_brg, Me.n_brg, Me.qty1, Me.SAT})
        Me.DGVbrg.Location = New System.Drawing.Point(294, 149)
        Me.DGVbrg.Name = "DGVbrg"
        Me.DGVbrg.RowHeadersVisible = False
        Me.DGVbrg.Size = New System.Drawing.Size(663, 245)
        Me.DGVbrg.TabIndex = 24
        '
        'k_brg
        '
        Me.k_brg.HeaderText = "Kode Barang"
        Me.k_brg.Name = "k_brg"
        '
        'n_brg
        '
        Me.n_brg.HeaderText = "Nama Barang"
        Me.n_brg.Name = "n_brg"
        Me.n_brg.Width = 150
        '
        'qty1
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.qty1.DefaultCellStyle = DataGridViewCellStyle1
        Me.qty1.HeaderText = "Qty"
        Me.qty1.Name = "qty1"
        Me.qty1.Width = 50
        '
        'SAT
        '
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.Width = 60
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.PeachPuff
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.satuan)
        Me.GroupBox1.Controls.Add(Me.nbrg)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.INPUTbrg)
        Me.GroupBox1.Controls.Add(Me.qty)
        Me.GroupBox1.Controls.Add(Me.kbrg)
        Me.GroupBox1.Location = New System.Drawing.Point(36, 123)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(252, 157)
        Me.GroupBox1.TabIndex = 23
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GANTI BARANG"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(74, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 38
        Me.Label4.Text = "Sat"
        '
        'satuan
        '
        Me.satuan.Enabled = False
        Me.satuan.Location = New System.Drawing.Point(77, 119)
        Me.satuan.Name = "satuan"
        Me.satuan.Size = New System.Drawing.Size(58, 20)
        Me.satuan.TabIndex = 37
        '
        'nbrg
        '
        Me.nbrg.Location = New System.Drawing.Point(13, 76)
        Me.nbrg.Name = "nbrg"
        Me.nbrg.Size = New System.Drawing.Size(223, 20)
        Me.nbrg.TabIndex = 35
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 60)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 13)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "Nama Barang"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 103)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(23, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Qty"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Kode Barang *F1"
        '
        'INPUTbrg
        '
        Me.INPUTbrg.Location = New System.Drawing.Point(148, 116)
        Me.INPUTbrg.Name = "INPUTbrg"
        Me.INPUTbrg.Size = New System.Drawing.Size(75, 23)
        Me.INPUTbrg.TabIndex = 23
        Me.INPUTbrg.Text = "INPUT"
        Me.INPUTbrg.UseVisualStyleBackColor = True
        '
        'qty
        '
        Me.qty.Location = New System.Drawing.Point(13, 119)
        Me.qty.Name = "qty"
        Me.qty.Size = New System.Drawing.Size(58, 20)
        Me.qty.TabIndex = 22
        '
        'kbrg
        '
        Me.kbrg.Location = New System.Drawing.Point(13, 35)
        Me.kbrg.Name = "kbrg"
        Me.kbrg.Size = New System.Drawing.Size(152, 20)
        Me.kbrg.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(343, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(293, 22)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "INPUT JADWAL KUNJUNGAN" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(611, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 13)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "No Complain :"
        '
        'noComp
        '
        Me.noComp.Enabled = False
        Me.noComp.Location = New System.Drawing.Point(614, 84)
        Me.noComp.Name = "noComp"
        Me.noComp.Size = New System.Drawing.Size(100, 20)
        Me.noComp.TabIndex = 37
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(275, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "Customer : "
        '
        'nlgn
        '
        Me.nlgn.AutoSize = True
        Me.nlgn.Location = New System.Drawing.Point(341, 68)
        Me.nlgn.Name = "nlgn"
        Me.nlgn.Size = New System.Drawing.Size(10, 13)
        Me.nlgn.TabIndex = 39
        Me.nlgn.Text = "-"
        '
        'klgn
        '
        Me.klgn.AutoSize = True
        Me.klgn.Location = New System.Drawing.Point(341, 84)
        Me.klgn.Name = "klgn"
        Me.klgn.Size = New System.Drawing.Size(10, 13)
        Me.klgn.TabIndex = 40
        Me.klgn.Text = "-"
        Me.klgn.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(488, 597)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 13)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Keterangan :"
        '
        'ket
        '
        Me.ket.AcceptsReturn = True
        Me.ket.Location = New System.Drawing.Point(562, 591)
        Me.ket.Multiline = True
        Me.ket.Name = "ket"
        Me.ket.Size = New System.Drawing.Size(198, 65)
        Me.ket.TabIndex = 41
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DGVtek)
        Me.GroupBox2.Controls.Add(Me.INPUTtek)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbTeknisi)
        Me.GroupBox2.Location = New System.Drawing.Point(36, 286)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(252, 299)
        Me.GroupBox2.TabIndex = 43
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.TextBox2)
        Me.GroupBox3.Controls.Add(Me.DataGridView1)
        Me.GroupBox3.Location = New System.Drawing.Point(307, 400)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(663, 185)
        Me.GroupBox3.TabIndex = 44
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Serial"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(17, 107)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "INPUT"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(17, 81)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(152, 20)
        Me.TextBox1.TabIndex = 39
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Nomor Serial"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 24)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 13)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "Kode Barang"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(17, 40)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(152, 20)
        Me.TextBox2.TabIndex = 37
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.Bisque
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG1, Me.Column1})
        Me.DataGridView1.Location = New System.Drawing.Point(195, 17)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(414, 162)
        Me.DataGridView1.TabIndex = 0
        '
        'K_BRG1
        '
        Me.K_BRG1.HeaderText = "Kode Barang"
        Me.K_BRG1.Name = "K_BRG1"
        '
        'Column1
        '
        Me.Column1.HeaderText = "Nomor Serial"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 150
        '
        'JadwalInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.ket)
        Me.Controls.Add(Me.klgn)
        Me.Controls.Add(Me.nlgn)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.noComp)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.DGVbrg)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.batal)
        Me.Controls.Add(Me.simpan)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.date1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "JadwalInput"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "INPUT JADWAL KUNJUNGAN"
        CType(Me.DGVtek, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVbrg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbTeknisi As System.Windows.Forms.ComboBox
    Friend WithEvents INPUTtek As System.Windows.Forms.Button
    Friend WithEvents DGVtek As System.Windows.Forms.DataGridView
    Friend WithEvents simpan As System.Windows.Forms.Button
    Friend WithEvents batal As System.Windows.Forms.Button
    Friend WithEvents DGVbrg As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents INPUTbrg As System.Windows.Forms.Button
    Friend WithEvents qty As System.Windows.Forms.TextBox
    Friend WithEvents kbrg As System.Windows.Forms.TextBox
    Friend WithEvents nbrg As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents noComp As System.Windows.Forms.TextBox
    Friend WithEvents Kode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents satuan As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents nlgn As System.Windows.Forms.Label
    Friend WithEvents klgn As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ket As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents k_brg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents n_brg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents qty1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
End Class
