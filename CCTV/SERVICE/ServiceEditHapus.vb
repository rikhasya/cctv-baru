﻿Imports System.Drawing.Printing
Imports System.IO

Public Class ServiceEditHapus

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpKet()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub ServiceEditHapus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.dari.Text = "ESVC"
            Cari_Brg.no.Text = noRetur.Text
        End If
    End Sub

    Private Sub ServiceEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub ServiceEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadd()
        Dim c As New clsCCTV.clsKunjungan
        Dim d As DataSet
        d = c.table(noRetur.Text)
        DGV3.DataSource = d.Tables("DETJADKUNJUNGAN2")
    End Sub

    Sub loadd()
        Dim a As New clsCCTV.clsService
        Dim b As DataSet
        b = a.table(noFak.Text)
        DGVsvc.DataSource = b.Tables("DETSERVICE")
    End Sub

    Private Sub butInput_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butInput.Click
        Dim a As Boolean = True
        Dim b As Integer
        For b = 0 To DGVsvc.RowCount - 1
            Console.WriteLine(kode1.Text & ":" & DGVsvc.Item("K_BRG", b).Value)
            If kode1.Text = DGVsvc.Item("K_BRG", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("KET")

        table1.Rows.Add(noFak.Text, kode1.Text, nama.Text, CDbl(qty1.Text), satuan.Text, Ket1.Text)

        Dim c As New clsCCTV.clsService
        Dim d As Boolean
        d = c.input1brg(noFak.Text, table1)

        If d = True Then
            MsgBox("Berhasil input 1 barang baru")
            kode1.Clear()
            nama.Clear()
            qty1.Clear()
            satuan.Clear()
            Ket1.Clear()
            loadd()
        Else
            MsgBox("Gagal input")
        End If
    End Sub

    Private Sub butHAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAP.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Faktur?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsService
        Dim b As Boolean
        b = a.hapus(noFak.Text)

        If b = True Then
            MsgBox("Berhasil hapus faktur service " & noFak.Text)
            Me.Close()
        Else
            MsgBox("Gagal hapus faktur")
        End If
    End Sub

    Private Sub DGVsvc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVsvc.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVsvc.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsService
        Dim b As Boolean
        b = a.hapus1brg(noFak.Text, DGVsvc.Item("K_BRG", baris).Value.ToString)

        If b = True Then
            loadd()
        Else
            MsgBox("Gagal hapus")
        End If
    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        Dim a As New clsCCTV.clsService
        Dim b As Boolean
        b = a.setvalid(noFak.Text, ket2.Text)

        If b = True Then
            MsgBox("Berhasil simpan")
            PRINT()
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal simpan")
            Exit Sub
        End If
    End Sub

    Sub PRINT()
        
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Ulang Faktur Service?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsService
            ServiceInput.tb = otc.table(noFak.Text)

            With ServiceInput
                .tmpnofak = .tb.Tables("DATSERVICE").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = ServiceInput.tb.Tables("DETSERVICE").Rows.Count - 1

            ReDim ServiceInput.tmpKode(jumlah)
            ReDim ServiceInput.tmpNama(jumlah)
            ReDim ServiceInput.tmpQty(jumlah)
            ReDim ServiceInput.tmpSat(jumlah)
            ReDim ServiceInput.tmpKet(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With ServiceInput
                    .tmpKode(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("SAT").ToString
                    .tmpKet(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("KET").ToString
                End With
            Next
            With ServiceInput
                arrke = 0
                printFont = New Font("Times New Roman", 10)
            End With

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf ServiceInput.faktur
            PrintDoc.Print()
            Exit Sub
        End If
    End Sub

    Private Sub DGV3_DoubleClick(sender As Object, e As EventArgs) Handles DGV3.DoubleClick
        Dim row As Integer
        row = DGV3.CurrentRow.Index.ToString

        kode1.Text = DGV3.Item("K_BRG3", row).Value.ToString
        nama.Text = DGV3.Item("N_BRG3", row).Value.ToString
        satuan.Text = DGV3.Item("SAT3", row).Value.ToString
        qty1.Text = 0
    End Sub

End Class