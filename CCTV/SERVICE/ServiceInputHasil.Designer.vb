﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceInputHasil
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ket2 = New System.Windows.Forms.TextBox()
        Me.satuan = New System.Windows.Forms.TextBox()
        Me.noService = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.butInput = New System.Windows.Forms.Button()
        Me.biaya = New System.Windows.Forms.TextBox()
        Me.qty1 = New System.Windows.Forms.TextBox()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.kode1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.kode = New System.Windows.Forms.Panel()
        Me.kSup = New System.Windows.Forms.Label()
        Me.supplier = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGV3 = New System.Windows.Forms.DataGridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.K_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.totb = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DGVsvc = New System.Windows.Forms.DataGridView()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.biaya1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2.SuspendLayout()
        Me.kode.SuspendLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(868, 588)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(75, 37)
        Me.butBAT.TabIndex = 4
        Me.butBAT.Text = "BATAL"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(605, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "No. Service"
        '
        'butSIM
        '
        Me.butSIM.Enabled = False
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(766, 588)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(75, 37)
        Me.butSIM.TabIndex = 3
        Me.butSIM.Text = "SIMPAN"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(158, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tanggal"
        '
        'ket2
        '
        Me.ket2.Location = New System.Drawing.Point(110, 578)
        Me.ket2.Multiline = True
        Me.ket2.Name = "ket2"
        Me.ket2.Size = New System.Drawing.Size(287, 57)
        Me.ket2.TabIndex = 2
        '
        'satuan
        '
        Me.satuan.Enabled = False
        Me.satuan.Location = New System.Drawing.Point(81, 73)
        Me.satuan.Name = "satuan"
        Me.satuan.Size = New System.Drawing.Size(57, 20)
        Me.satuan.TabIndex = 14
        Me.satuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'noService
        '
        Me.noService.Enabled = False
        Me.noService.Location = New System.Drawing.Point(674, 14)
        Me.noService.Name = "noService"
        Me.noService.Size = New System.Drawing.Size(122, 20)
        Me.noService.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(36, 578)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Keterangan :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel2.Controls.Add(Me.satuan)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.butInput)
        Me.Panel2.Controls.Add(Me.biaya)
        Me.Panel2.Controls.Add(Me.qty1)
        Me.Panel2.Controls.Add(Me.nama)
        Me.Panel2.Controls.Add(Me.kode1)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(621, 146)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(350, 132)
        Me.Panel2.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(78, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(23, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Sat"
        '
        'butInput
        '
        Me.butInput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butInput.Location = New System.Drawing.Point(260, 106)
        Me.butInput.Name = "butInput"
        Me.butInput.Size = New System.Drawing.Size(75, 23)
        Me.butInput.TabIndex = 2
        Me.butInput.Text = "INPUT"
        Me.butInput.UseVisualStyleBackColor = True
        '
        'biaya
        '
        Me.biaya.Location = New System.Drawing.Point(153, 73)
        Me.biaya.Name = "biaya"
        Me.biaya.Size = New System.Drawing.Size(178, 20)
        Me.biaya.TabIndex = 1
        Me.biaya.Text = "0"
        Me.biaya.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'qty1
        '
        Me.qty1.Location = New System.Drawing.Point(16, 74)
        Me.qty1.Name = "qty1"
        Me.qty1.Size = New System.Drawing.Size(48, 20)
        Me.qty1.TabIndex = 0
        Me.qty1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nama
        '
        Me.nama.Enabled = False
        Me.nama.Location = New System.Drawing.Point(153, 24)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(178, 20)
        Me.nama.TabIndex = 9
        '
        'kode1
        '
        Me.kode1.Enabled = False
        Me.kode1.Location = New System.Drawing.Point(16, 24)
        Me.kode1.Name = "kode1"
        Me.kode1.Size = New System.Drawing.Size(122, 20)
        Me.kode1.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(150, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Biaya"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Qty"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(150, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Kode"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(210, 13)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 4
        '
        'kode
        '
        Me.kode.BackColor = System.Drawing.Color.LightSteelBlue
        Me.kode.Controls.Add(Me.kSup)
        Me.kode.Controls.Add(Me.supplier)
        Me.kode.Controls.Add(Me.Label9)
        Me.kode.Controls.Add(Me.date1)
        Me.kode.Controls.Add(Me.noService)
        Me.kode.Controls.Add(Me.Label2)
        Me.kode.Controls.Add(Me.Label1)
        Me.kode.Location = New System.Drawing.Point(0, 55)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(1003, 41)
        Me.kode.TabIndex = 0
        '
        'kSup
        '
        Me.kSup.AutoSize = True
        Me.kSup.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.kSup.Location = New System.Drawing.Point(366, 2)
        Me.kSup.Name = "kSup"
        Me.kSup.Size = New System.Drawing.Size(21, 9)
        Me.kSup.TabIndex = 8
        Me.kSup.Text = "kode"
        Me.kSup.Visible = False
        '
        'supplier
        '
        Me.supplier.Location = New System.Drawing.Point(368, 14)
        Me.supplier.Name = "supplier"
        Me.supplier.Size = New System.Drawing.Size(222, 20)
        Me.supplier.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(317, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Supplier"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(334, 23)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(310, 22)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "INPUT HASIL SERVICE SUPPLIER"
        '
        'DGV3
        '
        Me.DGV3.AllowUserToAddRows = False
        Me.DGV3.AllowUserToDeleteRows = False
        Me.DGV3.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.DGV3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG3, Me.NO_FAK3, Me.N_BRG3, Me.QTY3, Me.SAT3, Me.KET})
        Me.DGV3.Location = New System.Drawing.Point(25, 108)
        Me.DGV3.Name = "DGV3"
        Me.DGV3.ReadOnly = True
        Me.DGV3.RowHeadersVisible = False
        Me.DGV3.Size = New System.Drawing.Size(590, 170)
        Me.DGV3.TabIndex = 23
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(621, 130)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(157, 13)
        Me.Label12.TabIndex = 26
        Me.Label12.Text = "* Doubleclick untuk pilih barang"
        '
        'K_BRG3
        '
        Me.K_BRG3.DataPropertyName = "K_BRG"
        Me.K_BRG3.HeaderText = "Kode Barang"
        Me.K_BRG3.Name = "K_BRG3"
        Me.K_BRG3.ReadOnly = True
        '
        'NO_FAK3
        '
        Me.NO_FAK3.DataPropertyName = "NO_FAK"
        Me.NO_FAK3.HeaderText = "NO FAK 3"
        Me.NO_FAK3.Name = "NO_FAK3"
        Me.NO_FAK3.ReadOnly = True
        Me.NO_FAK3.Visible = False
        '
        'N_BRG3
        '
        Me.N_BRG3.DataPropertyName = "N_BRG"
        Me.N_BRG3.HeaderText = "Nama Barang"
        Me.N_BRG3.Name = "N_BRG3"
        Me.N_BRG3.ReadOnly = True
        Me.N_BRG3.Width = 150
        '
        'QTY3
        '
        Me.QTY3.DataPropertyName = "QTY"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.QTY3.DefaultCellStyle = DataGridViewCellStyle5
        Me.QTY3.HeaderText = "Qty"
        Me.QTY3.Name = "QTY3"
        Me.QTY3.ReadOnly = True
        Me.QTY3.Width = 50
        '
        'SAT3
        '
        Me.SAT3.DataPropertyName = "SAT"
        Me.SAT3.HeaderText = "Sat"
        Me.SAT3.Name = "SAT3"
        Me.SAT3.ReadOnly = True
        Me.SAT3.Width = 60
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        Me.KET.Width = 200
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.totb)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.DGVsvc)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 289)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(957, 269)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        '
        'totb
        '
        Me.totb.Enabled = False
        Me.totb.Location = New System.Drawing.Point(397, 235)
        Me.totb.Name = "totb"
        Me.totb.Size = New System.Drawing.Size(192, 20)
        Me.totb.TabIndex = 28
        Me.totb.Text = "0"
        Me.totb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(329, 236)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(66, 13)
        Me.Label11.TabIndex = 27
        Me.Label11.Text = "Total Biaya :"
        '
        'DGVsvc
        '
        Me.DGVsvc.AllowUserToDeleteRows = False
        Me.DGVsvc.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.DGVsvc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsvc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.biaya1})
        Me.DGVsvc.Location = New System.Drawing.Point(6, 19)
        Me.DGVsvc.Name = "DGVsvc"
        Me.DGVsvc.RowHeadersVisible = False
        Me.DGVsvc.Size = New System.Drawing.Size(584, 210)
        Me.DGVsvc.TabIndex = 26
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode"
        Me.K_BRG.Name = "K_BRG"
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.Width = 150
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.Width = 50
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.Width = 60
        '
        'biaya1
        '
        Me.biaya1.DataPropertyName = "BIAYA"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        Me.biaya1.DefaultCellStyle = DataGridViewCellStyle6
        Me.biaya1.HeaderText = "Biaya"
        Me.biaya1.Name = "biaya1"
        Me.biaya1.Width = 120
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(593, 34)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(157, 13)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "* Doubleclick untuk pilih barang"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(602, 66)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(169, 20)
        Me.TextBox3.TabIndex = 77
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(599, 50)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(69, 13)
        Me.Label27.TabIndex = 78
        Me.Label27.Text = "Kode Barang"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(777, 66)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(169, 20)
        Me.TextBox4.TabIndex = 79
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(871, 92)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 81
        Me.Button2.Text = "INPUT"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(774, 50)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(67, 13)
        Me.Label28.TabIndex = 80
        Me.Label28.Text = "Nomor Serial"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.Bisque
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.DataGridView1.Location = New System.Drawing.Point(596, 121)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(350, 134)
        Me.DataGridView1.TabIndex = 82
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Kode Barang"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nomor Serial"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'ServiceInputHasil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.DGV3)
        Me.Controls.Add(Me.butBAT)
        Me.Controls.Add(Me.butSIM)
        Me.Controls.Add(Me.ket2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.kode)
        Me.Controls.Add(Me.Label8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "ServiceInputHasil"
        Me.Text = "ServiceInputHasil"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.kode.ResumeLayout(False)
        Me.kode.PerformLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ket2 As System.Windows.Forms.TextBox
    Friend WithEvents satuan As System.Windows.Forms.TextBox
    Friend WithEvents noService As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents butInput As System.Windows.Forms.Button
    Friend WithEvents biaya As System.Windows.Forms.TextBox
    Friend WithEvents qty1 As System.Windows.Forms.TextBox
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents kode1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents kode As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents supplier As System.Windows.Forms.TextBox
    Friend WithEvents kSup As System.Windows.Forms.Label
    Friend WithEvents DGV3 As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents K_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents totb As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DGVsvc As System.Windows.Forms.DataGridView
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents biaya1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
