﻿Public Class PerbaikanEditHapus

    Private Sub Perbaikan_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.dari.Text = "EPBK"
            Cari_Brg.Show()
        End If
    End Sub

    Private Sub Perbaikan_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Perbaikan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load1()
        load2()
        load3()
    End Sub

    Sub load1()
        Dim c As New clsCCTV.clsKunjungan
        Dim d As DataSet
        d = c.table(noVisit.Text)
        DGV1.DataSource = d.Tables("DETJADKUNJUNGAN2")

    End Sub

    Sub load2()
        Dim c As New clsCCTV.clsPerbaikan
        Dim d As DataSet
        d = c.table(nofak.Text)
        DGV2.DataSource = d.Tables("DETPERBAIKAN")
    End Sub

    Sub load3()
        Dim c As New clsCCTV.clsPerbaikan
        Dim d As DataSet
        d = c.table(nofak.Text)
        DGV3.DataSource = d.Tables("DETPERBAIKAN2")
    End Sub

    Private Sub tqty_Leave(sender As Object, e As EventArgs) Handles tqty.Leave
        If IsNumeric(tqty.Text) Then
            tqty.Text = FormatNumber(tqty.Text, 2)
        End If
    End Sub

    Private Sub tqty1_Leave(sender As Object, e As EventArgs) Handles tqty1.Leave
        If IsNumeric(tqty1.Text) Then
            tqty1.Text = FormatNumber(tqty1.Text, 2)
        End If
    End Sub

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
        Dim row As Integer
        row = DGV1.CurrentRow.Index.ToString
        kbrg.Text = DGV1.Item("K_BRG", row).Value.ToString
        nbrg.Text = DGV1.Item("N_BRG", row).Value.ToString
        tsat.Text = DGV1.Item("SAT", row).Value.ToString
    End Sub

    Private Sub DGV2_DoubleClick(sender As Object, e As EventArgs) Handles DGV2.DoubleClick
        Dim baris As Integer
        baris = DGV2.CurrentRow.Index.ToString
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & DGV2.Item("K_BRG1", baris).Value.ToString & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        For i = 0 To DGV3.Rows.Count - 1
            If DGV3.Item("K_BRG3", i).Value.ToString = DGV2.Item("K_BRG1", baris).Value.ToString Then
                MsgBox("Sudah input sparepart untuk barang tersebut")
                Exit Sub
            End If
        Next

        Dim a As New clsCCTV.clsPerbaikan
        Dim b As Boolean
        b = a.hapus1brg(nofak.Text, DGV2.Item("K_BRG1", baris).Value.ToString)

        If b = True Then
            load2()
            load3()
            namabrg.Text = "-"
            kodebrg.Text = "-"
            frmMenu.MenuStrip1.Enabled = False
            Exit Sub
        Else
            MsgBox("Gagal Hapus 1 barang")
            Exit Sub
        End If
        

    End Sub

    Private Sub input1_Click(sender As Object, e As EventArgs) Handles input1.Click
        If kbrg.Text = "" Then
            MsgBox("Kode barang kosong")
            Exit Sub
        ElseIf tqty.Text = "" Then
            MsgBox("cek qty")
            Exit Sub
        ElseIf Not IsNumeric(tqty.Text) Or tqty.Text = 0 Then
            MsgBox("cek qty")
            Exit Sub
        End If
        Dim a As Boolean = True
        For b = 0 To DGV2.RowCount - 1
            Console.WriteLine(kbrg.Text & ":" & DGV2.Item("K_BRG1", b).Value)
            If kbrg.Text = DGV2.Item("K_BRG1", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If
        Dim table1 As New DataTable("DETPERBAIKAN")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim table2 As New DataTable("DETPERBAIKAN2")

        table1.Rows.Add(nofak.Text, kbrg.Text, nbrg.Text, CDbl(tqty.Text), tsat.Text)

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        frmMenu.MenuStrip1.Enabled = False

        Dim c As New clsCCTV.clsPerbaikan
        Dim d As Boolean
        d = c.input1brg(nofak.Text, set1)
        If d = True Then
            load2()
            kbrg.Clear()
            nbrg.Clear()
            tqty.Text = 0
            tsat.Clear()
            Exit Sub
        Else
            MsgBox("Gagal Input Barang Rusak")
            Exit Sub
        End If

       
    End Sub

    Private Sub input2_Click(sender As Object, e As EventArgs) Handles input2.Click
        If kodebrg.Text = "-" Then
            MsgBox("pilih barang yang akan di service")
            Exit Sub
        ElseIf kbrg1.Text = "" Then
            MsgBox("Kode barang kosong")
            Exit Sub
        ElseIf tqty1.Text = "" Then
            MsgBox("cek qty")
            Exit Sub
        ElseIf Not IsNumeric(tqty1.Text) Or tqty1.Text = 0 Then
            MsgBox("cek qty")
            Exit Sub
        End If

        Dim a As Boolean = True
        For b = 0 To DGV3.RowCount - 1
            Console.WriteLine(kbrg1.Text & ":" & DGV3.Item("K_BRG2", b).Value)
            If kbrg1.Text = DGV3.Item("K_BRG2", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        Dim table1 As New DataTable("DETPERBAIKAN")

        Dim table2 As New DataTable("DETPERBAIKAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG_RUSAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")

            table2.Rows.Add(nofak.Text, kodebrg.Text, kbrg1.Text, nbrg1.Text, CDbl(tqty1.Text), tsat1.Text)

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        frmMenu.MenuStrip1.Enabled = False

        Dim c As New clsCCTV.clsPerbaikan
        Dim d As Boolean
        d = c.input1sparepart(nofak.Text, set1)

        If d = True Then
            load3()
            kbrg1.Clear()
            nbrg1.Clear()
            tqty1.Text = 0
            tsat1.Clear()
            Exit Sub
        Else
            MsgBox("Gagal Input Sparepart")
            Exit Sub
        End If

       
    End Sub

    Private Sub DGV2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV2.CellContentClick
        Dim row As Integer
        row = DGV2.CurrentRow.Index.ToString
        For i = 0 To DGV3.Rows.Count - 1
            If DGV3.Item("K_BRG3", i).Value.ToString = DGV2.Item("K_BRG1", row).Value.ToString Then
                MsgBox("Sudah input sparepart untuk barang tersebut")
                Exit Sub
            End If
        Next
        Dim a As New clsCCTV.clsPerbaikan
        Dim b As Boolean
        b = a.hapus1brg(nofak.Text, DGV2.Item("K_BRG1", row).Value.ToString)

        If b = True Then
            kodebrg.Text = DGV2.Item("K_BRG1", row).Value.ToString
            namabrg.Text = DGV2.Item("N_BRG1", row).Value.ToString
            load2()
        Else
            MsgBox("Gagal Hapus 1 barang")
            Exit Sub
        End If

    End Sub

    Private Sub DGV3_DoubleClick(sender As Object, e As EventArgs) Handles DGV3.DoubleClick
        Dim baris As Integer
        baris = DGV3.CurrentRow.Index.ToString
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & DGV3.Item("K_BRG2", baris).Value.ToString & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Dim a As New clsCCTV.clsPerbaikan
        Dim b As Boolean
        b = a.hapus1brg(nofak.Text, DGV3.Item("K_BRG3", baris).Value.ToString, DGV3.Item("K_BRG2", baris).Value.ToString)

        If b = True Then
            load3()
            frmMenu.MenuStrip1.Enabled = False
            Exit Sub
        Else
            MsgBox("Gagal Hapus 1 barang")
            Exit Sub
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles HAPUS.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Laporan Perbaikan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsPerbaikan
        Dim b As Boolean
        b = a.hapus(nofak.Text)
        If b = True Then
            MsgBox("Berhasil Hapus Laporan Perbaikan")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Hapus Laporan Perbaikan")
            Exit Sub
        End If
    End Sub

    Private Sub SIMPAN_Click(sender As Object, e As EventArgs) Handles SIMPAN.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Perbaikan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

            MsgBox("Berhasil Simpan Perbaikan")
            Me.Close()
            Exit Sub
       
    End Sub

    Private Sub DGV3_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV3.CellContentClick

    End Sub
End Class