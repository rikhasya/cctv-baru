﻿Public Class ComplainLaporan

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DGVcomp.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("COMPEDITHAPUS")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If

        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim row As Integer
        row = DGVcomp.CurrentRow.Index.ToString
        Dim frm As ComplainEdithapus
        frm = New ComplainEdithapus
        frm.MdiParent = frmMenu
        frm.Text = "Edit Hapus Complain"
        'frm.nofak.Text = DGVcomp.Item("NO_FAK", row).Value.ToString
        'frm.noJual.Text = DGVcomp.Item("NO_JUAL", row).Value.ToString
        'frm.nlgn.Text = DGVcomp.Item("K_LGN", row).Value.ToString
        'frm.Tgl1.Text = CDate(DGVcomp.Item("TGL_FAK", row).Value.ToString)
        'frm.Tgl2.Text = CDate(DGVcomp.Item("TGL_JT", row).Value.ToString)
        'frm.kett.Text = DGVcomp.Item("KET", row).Value.ToString
        'frm.tisi.Text = DGVcomp.Item("TXT_ISI", row).Value.ToString
        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()
    End Sub

    Private Sub DataGridView1_KeyDown(sender As Object, e As KeyEventArgs) Handles DGVcomp.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("JADWALINPUT")
            If b = False Then
                MsgBox("anda tdk di otorisasi")
                Exit Sub
            End If

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim row As Integer
            row = DGVcomp.CurrentRow.Index.ToString
            Dim frm As JadwalInput
            frm = New JadwalInput
            frm.MdiParent = frmMenu
            frm.Text = "Input Jadwal"
            'frm.noComp.Text = DGVcomp.Item("NO_FAK", row).Value.ToString
            'frm.klgn.Text = DGVcomp.Item("K_LGN", row).Value.ToString
            'frm.nlgn.Text = DGVcomp.Item("N_LGN", row).Value.ToString
            Dim list As Integer = 0
            For i = 0 To DGVcomp2.Rows.Count - 1
                list = list + 1
            Next
            If list <> 0 Then
                frm.BARIS = "Y"
            End If
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        End If
    End Sub

    Private Sub ComplainLaporan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With cbLgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        cbLgn.SelectedValue = 0

        CBstts.Text = "ALL"
    End Sub

    Dim lns As String

    Private Sub CARI_Click(sender As Object, e As EventArgs) Handles CARI.Click
        DGVcomp.AutoGenerateColumns = False 'before databinding

        Dim stts As String
        If CBstts.Text = "ALL" Then
            stts = "%"
        Else
            stts = CBstts.Text
        End If

        If cbSelesai.CheckState = CheckState.Checked Then
            lns = "Y"
        ElseIf cbSelesai.CheckState = CheckState.Indeterminate Then
            lns = "%"
        ElseIf cbSelesai.CheckState = CheckState.Unchecked Then
            lns = "N"
        End If

        Dim lgn As String
        If cbLgn.Text = "ALL" Then
            lgn = "%"
        Else
            lgn = cbLgn.SelectedValue
        End If

        Dim a As New clsCCTV.clsComplain
        Dim b As DataSet
        b = a.table(CDate(date1.Text), CDate(date2.Text), lgn, lns, stts)
        Dim parentColumn As DataColumn = b.Tables("DATCOMPLAIN").Columns("NO_FAK")
        Dim childColumn As DataColumn = b.Tables("DETCOMPLAIN").Columns("NO_FAK")
       
        Dim relDATbELIDETbELI As DataRelation
        relDATbELIDETbELI = New DataRelation("DETCOMPLAIN", parentColumn, childColumn, False)
        b.Relations.Add(relDATbELIDETbELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource
       
        masterbindingSource.DataSource = b
        masterbindingSource.DataMember = "DATCOMPLAIN"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETCOMPLAIN"

        DGVcomp.DataSource = masterbindingSource
        DGVcomp2.DataSource = detailbindingSource
        DGVcomp.Focus()
        b.Dispose()
    End Sub
End Class