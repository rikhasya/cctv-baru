﻿Public Class HasilVisitInputBerhasil

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub HasilVisitInputBerhasil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsKunjungan
        Dim b As DataSet
        b = a.table(noVisit.Text)
        DGVbrg.DataSource = b.Tables("DETJADKUNJUNGAN2")
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Input Hasil ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Me.Close()
        Exit Sub
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Hasil ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsKunjungan
        Dim b As Boolean
        b = a.inputHasilKunj(noVisit.Text, CDate(date1.Text), cbHasil.Text, keterangan.Text)
        If b = True Then
            MsgBox("Berhasil Simpan Hasil Visit")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Simpan Hasil Visit")
            Exit Sub
        End If
    End Sub
End Class