﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PerbaikanEditHapus
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DGV1 = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.kbrg = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.nbrg = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tqty = New System.Windows.Forms.TextBox()
        Me.input1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tsat = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.keterangan = New System.Windows.Forms.TextBox()
        Me.SIMPAN = New System.Windows.Forms.Button()
        Me.HAPUS = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.nofak = New System.Windows.Forms.TextBox()
        Me.noVisit = New System.Windows.Forms.TextBox()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.kodebrg = New System.Windows.Forms.Label()
        Me.namabrg = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.tsat1 = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.kbrg1 = New System.Windows.Forms.TextBox()
        Me.input2 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tqty1 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.nbrg1 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.DGV3 = New System.Windows.Forms.DataGridView()
        Me.K_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.DGV2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.teknisi = New System.Windows.Forms.TextBox()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(416, 38)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(197, 22)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "INPUT PERBAIKAN "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(86, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Tanggal"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(58, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "No. Perbaikan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(525, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "No. Visit"
        '
        'DGV1
        '
        Me.DGV1.AllowUserToAddRows = False
        Me.DGV1.AllowUserToDeleteRows = False
        Me.DGV1.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT})
        Me.DGV1.Location = New System.Drawing.Point(61, 145)
        Me.DGV1.Name = "DGV1"
        Me.DGV1.ReadOnly = True
        Me.DGV1.RowHeadersVisible = False
        Me.DGV1.Size = New System.Drawing.Size(426, 123)
        Me.DGV1.TabIndex = 27
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "NO_FAK"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode "
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle1
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(58, 129)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(276, 13)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Barang Yang akan di perbaiki *Doubleclik di baris barang"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(58, 391)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 13)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Detail Barang"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 13)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Kode Barang"
        '
        'kbrg
        '
        Me.kbrg.Location = New System.Drawing.Point(11, 31)
        Me.kbrg.Name = "kbrg"
        Me.kbrg.Size = New System.Drawing.Size(152, 20)
        Me.kbrg.TabIndex = 31
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(166, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 13)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "Nama Barang"
        '
        'nbrg
        '
        Me.nbrg.Location = New System.Drawing.Point(169, 31)
        Me.nbrg.Name = "nbrg"
        Me.nbrg.Size = New System.Drawing.Size(215, 20)
        Me.nbrg.TabIndex = 33
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(91, 55)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(23, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Qty"
        '
        'tqty
        '
        Me.tqty.Location = New System.Drawing.Point(94, 71)
        Me.tqty.Name = "tqty"
        Me.tqty.Size = New System.Drawing.Size(69, 20)
        Me.tqty.TabIndex = 35
        '
        'input1
        '
        Me.input1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.input1.Location = New System.Drawing.Point(267, 66)
        Me.input1.Name = "input1"
        Me.input1.Size = New System.Drawing.Size(107, 29)
        Me.input1.TabIndex = 37
        Me.input1.Text = "INPUT"
        Me.input1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tsat)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.kbrg)
        Me.GroupBox1.Controls.Add(Me.input1)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.tqty)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.nbrg)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(61, 274)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(426, 108)
        Me.GroupBox1.TabIndex = 38
        Me.GroupBox1.TabStop = False
        '
        'tsat
        '
        Me.tsat.Location = New System.Drawing.Point(169, 71)
        Me.tsat.Name = "tsat"
        Me.tsat.Size = New System.Drawing.Size(65, 20)
        Me.tsat.TabIndex = 38
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(166, 55)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(23, 13)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "Sat"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(517, 537)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 13)
        Me.Label11.TabIndex = 39
        Me.Label11.Text = "Keterangan :"
        '
        'keterangan
        '
        Me.keterangan.AcceptsReturn = True
        Me.keterangan.Location = New System.Drawing.Point(515, 553)
        Me.keterangan.Multiline = True
        Me.keterangan.Name = "keterangan"
        Me.keterangan.Size = New System.Drawing.Size(317, 60)
        Me.keterangan.TabIndex = 40
        '
        'SIMPAN
        '
        Me.SIMPAN.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SIMPAN.Location = New System.Drawing.Point(843, 551)
        Me.SIMPAN.Name = "SIMPAN"
        Me.SIMPAN.Size = New System.Drawing.Size(107, 29)
        Me.SIMPAN.TabIndex = 39
        Me.SIMPAN.Text = "SIMPAN"
        Me.SIMPAN.UseVisualStyleBackColor = True
        '
        'HAPUS
        '
        Me.HAPUS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HAPUS.Location = New System.Drawing.Point(843, 582)
        Me.HAPUS.Name = "HAPUS"
        Me.HAPUS.Size = New System.Drawing.Size(107, 29)
        Me.HAPUS.TabIndex = 41
        Me.HAPUS.Text = "HAPUS"
        Me.HAPUS.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(139, 66)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(10, 13)
        Me.Label12.TabIndex = 42
        Me.Label12.Text = ":"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(138, 92)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(10, 13)
        Me.Label13.TabIndex = 43
        Me.Label13.Text = ":"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(573, 92)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(10, 13)
        Me.Label14.TabIndex = 44
        Me.Label14.Text = ":"
        '
        'nofak
        '
        Me.nofak.Enabled = False
        Me.nofak.Location = New System.Drawing.Point(155, 63)
        Me.nofak.Name = "nofak"
        Me.nofak.Size = New System.Drawing.Size(106, 20)
        Me.nofak.TabIndex = 45
        '
        'noVisit
        '
        Me.noVisit.Enabled = False
        Me.noVisit.Location = New System.Drawing.Point(589, 89)
        Me.noVisit.Name = "noVisit"
        Me.noVisit.Size = New System.Drawing.Size(106, 20)
        Me.noVisit.TabIndex = 46
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(155, 89)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(92, 20)
        Me.date1.TabIndex = 47
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.kodebrg)
        Me.GroupBox2.Controls.Add(Me.namabrg)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.DGV3)
        Me.GroupBox2.Location = New System.Drawing.Point(505, 129)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(465, 401)
        Me.GroupBox2.TabIndex = 48
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sparepart"
        '
        'kodebrg
        '
        Me.kodebrg.AutoSize = True
        Me.kodebrg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.kodebrg.Location = New System.Drawing.Point(309, 29)
        Me.kodebrg.Name = "kodebrg"
        Me.kodebrg.Size = New System.Drawing.Size(12, 15)
        Me.kodebrg.TabIndex = 48
        Me.kodebrg.Text = "-"
        Me.kodebrg.Visible = False
        '
        'namabrg
        '
        Me.namabrg.AutoSize = True
        Me.namabrg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namabrg.Location = New System.Drawing.Point(35, 29)
        Me.namabrg.Name = "namabrg"
        Me.namabrg.Size = New System.Drawing.Size(12, 15)
        Me.namabrg.TabIndex = 47
        Me.namabrg.Text = "-"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.tsat1)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.kbrg1)
        Me.GroupBox3.Controls.Add(Me.input2)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.tqty1)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.nbrg1)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 53)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(448, 108)
        Me.GroupBox3.TabIndex = 46
        Me.GroupBox3.TabStop = False
        '
        'tsat1
        '
        Me.tsat1.Location = New System.Drawing.Point(248, 71)
        Me.tsat1.Name = "tsat1"
        Me.tsat1.Size = New System.Drawing.Size(65, 20)
        Me.tsat1.TabIndex = 40
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(245, 55)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(23, 13)
        Me.Label22.TabIndex = 41
        Me.Label22.Text = "Sat"
        '
        'kbrg1
        '
        Me.kbrg1.Location = New System.Drawing.Point(11, 31)
        Me.kbrg1.Name = "kbrg1"
        Me.kbrg1.Size = New System.Drawing.Size(152, 20)
        Me.kbrg1.TabIndex = 31
        '
        'input2
        '
        Me.input2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.input2.Location = New System.Drawing.Point(339, 66)
        Me.input2.Name = "input2"
        Me.input2.Size = New System.Drawing.Size(84, 29)
        Me.input2.TabIndex = 37
        Me.input2.Text = "INPUT"
        Me.input2.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 13)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "*F1 untuk cari sparepart"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 15)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 13)
        Me.Label15.TabIndex = 32
        Me.Label15.Text = "Kode Barang"
        '
        'tqty1
        '
        Me.tqty1.Location = New System.Drawing.Point(172, 71)
        Me.tqty1.Name = "tqty1"
        Me.tqty1.Size = New System.Drawing.Size(69, 20)
        Me.tqty1.TabIndex = 35
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(169, 55)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(23, 13)
        Me.Label16.TabIndex = 36
        Me.Label16.Text = "Qty"
        '
        'nbrg1
        '
        Me.nbrg1.Location = New System.Drawing.Point(169, 31)
        Me.nbrg1.Name = "nbrg1"
        Me.nbrg1.Size = New System.Drawing.Size(215, 20)
        Me.nbrg1.TabIndex = 33
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(166, 15)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 13)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Nama Barang"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(5, 176)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(71, 13)
        Me.Label18.TabIndex = 45
        Me.Label18.Text = "Detail Barang"
        '
        'DGV3
        '
        Me.DGV3.AllowUserToAddRows = False
        Me.DGV3.AllowUserToDeleteRows = False
        Me.DGV3.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.DGV3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG3, Me.NO_FAK2, Me.K_BRG2, Me.N_BRG2, Me.QTY2, Me.SAT2})
        Me.DGV3.Location = New System.Drawing.Point(8, 192)
        Me.DGV3.Name = "DGV3"
        Me.DGV3.ReadOnly = True
        Me.DGV3.RowHeadersVisible = False
        Me.DGV3.Size = New System.Drawing.Size(448, 194)
        Me.DGV3.TabIndex = 44
        '
        'K_BRG3
        '
        Me.K_BRG3.DataPropertyName = "K_BRG_RUSAK"
        Me.K_BRG3.HeaderText = "Kode Barang Service"
        Me.K_BRG3.Name = "K_BRG3"
        Me.K_BRG3.ReadOnly = True
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No FAk"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG2
        '
        Me.K_BRG2.DataPropertyName = "K_BRG"
        Me.K_BRG2.HeaderText = "Kode Sparepart"
        Me.K_BRG2.Name = "K_BRG2"
        Me.K_BRG2.ReadOnly = True
        '
        'N_BRG2
        '
        Me.N_BRG2.DataPropertyName = "N_BRG"
        Me.N_BRG2.HeaderText = "Nama"
        Me.N_BRG2.Name = "N_BRG2"
        Me.N_BRG2.ReadOnly = True
        '
        'QTY2
        '
        Me.QTY2.DataPropertyName = "QTY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.QTY2.DefaultCellStyle = DataGridViewCellStyle2
        Me.QTY2.HeaderText = "Qty"
        Me.QTY2.Name = "QTY2"
        Me.QTY2.ReadOnly = True
        '
        'SAT2
        '
        Me.SAT2.DataPropertyName = "SAT"
        Me.SAT2.HeaderText = "Sat"
        Me.SAT2.Name = "SAT2"
        Me.SAT2.ReadOnly = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(766, 92)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(10, 13)
        Me.Label19.TabIndex = 61
        Me.Label19.Text = ":"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(719, 92)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(41, 13)
        Me.Label20.TabIndex = 60
        Me.Label20.Text = "Teknisi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'DGV2
        '
        Me.DGV2.AllowUserToAddRows = False
        Me.DGV2.AllowUserToDeleteRows = False
        Me.DGV2.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.DGV2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.K_BRG1, Me.N_BRG1, Me.QTY1, Me.SAT1})
        Me.DGV2.Location = New System.Drawing.Point(61, 407)
        Me.DGV2.Name = "DGV2"
        Me.DGV2.ReadOnly = True
        Me.DGV2.RowHeadersVisible = False
        Me.DGV2.Size = New System.Drawing.Size(426, 123)
        Me.DGV2.TabIndex = 63
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "NO_FAK"
        Me.DataGridViewTextBoxColumn1.HeaderText = "NO_FAK"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'K_BRG1
        '
        Me.K_BRG1.DataPropertyName = "K_BRG"
        Me.K_BRG1.HeaderText = "Kode "
        Me.K_BRG1.Name = "K_BRG1"
        Me.K_BRG1.ReadOnly = True
        '
        'N_BRG1
        '
        Me.N_BRG1.DataPropertyName = "N_BRG"
        Me.N_BRG1.HeaderText = "Nama"
        Me.N_BRG1.Name = "N_BRG1"
        Me.N_BRG1.ReadOnly = True
        '
        'QTY1
        '
        Me.QTY1.DataPropertyName = "QTY"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.QTY1.DefaultCellStyle = DataGridViewCellStyle3
        Me.QTY1.HeaderText = "Qty"
        Me.QTY1.Name = "QTY1"
        Me.QTY1.ReadOnly = True
        '
        'SAT1
        '
        Me.SAT1.DataPropertyName = "SAT"
        Me.SAT1.HeaderText = "Sat"
        Me.SAT1.Name = "SAT1"
        Me.SAT1.ReadOnly = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(58, 533)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(223, 26)
        Me.Label23.TabIndex = 64
        Me.Label23.Text = "Klik 1x untuk pilih barang untuk pilih sparepart" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Klik 2x untuk hapus barang "
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(395, 89)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(92, 20)
        Me.date2.TabIndex = 67
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(378, 92)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(10, 13)
        Me.Label24.TabIndex = 66
        Me.Label24.Text = ":"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(288, 92)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 13)
        Me.Label25.TabIndex = 65
        Me.Label25.Text = "Tanggal Selesai"
        '
        'teknisi
        '
        Me.teknisi.Enabled = False
        Me.teknisi.Location = New System.Drawing.Point(782, 89)
        Me.teknisi.Name = "teknisi"
        Me.teknisi.Size = New System.Drawing.Size(179, 20)
        Me.teknisi.TabIndex = 68
        '
        'PerbaikanEditHapus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.teknisi)
        Me.Controls.Add(Me.date2)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.DGV2)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.date1)
        Me.Controls.Add(Me.noVisit)
        Me.Controls.Add(Me.nofak)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.HAPUS)
        Me.Controls.Add(Me.SIMPAN)
        Me.Controls.Add(Me.keterangan)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DGV1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "PerbaikanEditHapus"
        Me.Text = "Perbaikan"
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DGV1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents kbrg As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents nbrg As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tqty As System.Windows.Forms.TextBox
    Friend WithEvents input1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents keterangan As System.Windows.Forms.TextBox
    Friend WithEvents SIMPAN As System.Windows.Forms.Button
    Friend WithEvents HAPUS As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents nofak As System.Windows.Forms.TextBox
    Friend WithEvents noVisit As System.Windows.Forms.TextBox
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents kbrg1 As System.Windows.Forms.TextBox
    Friend WithEvents input2 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents tqty1 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents nbrg1 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents DGV3 As System.Windows.Forms.DataGridView
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents DGV2 As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tsat As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents tsat1 As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents namabrg As System.Windows.Forms.Label
    Friend WithEvents kodebrg As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents teknisi As System.Windows.Forms.TextBox
    Friend WithEvents K_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
