﻿Public Class PerbaikanHasil

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim i As Integer
        Dim tot1 As Integer = 0
        For i = 0 To DGV2.RowCount - 1
            tot1 = tot1 + DGV2.Item("QTY", i).Value
        Next

        Dim tot2 As Integer = 0
        For i = 0 To DGV1.RowCount - 1
            tot2 = tot2 + DGV1.Item("QTY1", i).Value
        Next

        Dim tot3 As Integer = 0
        For i = 0 To DGV3.RowCount - 1
            tot3 = tot3 + DGV3.Item("QTY2", i).Value
        Next

        If (tot2 + tot3) <> tot1 Then
            MsgBox("Jumlah Barang Pada Saat Input = Jumlah Barang Setelah Perbaikan")
            Exit Sub
        End If

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Perbaikan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsPerbaikan
        Dim t As String
        t = a.no()

        Dim table1 As New DataTable("DETHASILPERBAIKAN")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim table2 As New DataTable("DETHASILPERBAIKAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")

        For i = 0 To DGV1.Rows.Count - 1
           
                table1.Rows.Add(noPerbaikan.Text, DGV1.Item("K_BRG1", i).Value.ToString & "-S", DGV1.Item("N_BRG1", i).Value.ToString, DGV1.Item("QTY1", i).Value.ToString, DGV1.Item("SAT1", i).Value.ToString)
        Next

        For i = 0 To DGV3.Rows.Count - 1
            table2.Rows.Add(noPerbaikan.Text, DGV3.Item("K_BRG2", i).Value.ToString, DGV3.Item("N_BRG2", i).Value.ToString, DGV3.Item("QTY2", i).Value.ToString, DGV3.Item("SAT2", i).Value.ToString)
        Next

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim bb As New clsCCTV.clsHasilPerbaikan
        Dim b As Boolean
        b = bb.input(noPerbaikan.Text, CDate(date1.Text), ket.Text, CDate(date1.Text), ktek.Text, set1)

        If b = True Then
            MsgBox("Berhasil Input Hasil Perbaikan")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Input Hasil Perbaikan")
            Exit Sub
        End If

    End Sub

    Private Sub PerbaikanHasil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load2()
    End Sub

    Sub load2()
        Dim c As New clsCCTV.clsPerbaikan
        Dim d As DataSet
        d = c.table(noPerbaikan.Text)
        DGV2.DataSource = d.Tables("DETPERBAIKAN")
    End Sub

    Private Sub DGV2_DoubleClick(sender As Object, e As EventArgs) Handles DGV2.DoubleClick
        Dim row As Integer
        row = DGV2.CurrentRow.Index.ToString
        kbrg.Text = DGV2.Item("K_BRG", row).Value.ToString
        nbrg.Text = DGV2.Item("N_BRG", row).Value.ToString
        tsat.Text = DGV2.Item("SAT", row).Value.ToString
    End Sub

    Private Sub GAGAL_Click(sender As Object, e As EventArgs) Handles GAGAL.Click
        If tqty.Text = "" Then
            Exit Sub
        ElseIf Not IsNumeric(tqty.Text) Or tqty.Text = 0 Then
            MsgBox("Cek qty")
            Exit Sub
        End If
        With DGV3
            .Rows.Add(1)
            .Rows(DGV3.Rows.Count() - 1).Cells("k_brg2").Value = kbrg.Text
            .Rows(DGV3.Rows.Count() - 1).Cells("n_brg2").Value = nbrg.Text
            .Rows(DGV3.Rows.Count() - 1).Cells("qty2").Value = CDbl(tqty.Text)
            .Rows(DGV3.Rows.Count() - 1).Cells("sat2").Value = tsat.Text
        End With
        kbrg.Clear()
        nbrg.Clear()
        tqty.Text = 0
        tsat.Clear()
    End Sub

    Private Sub BERHASIL_Click(sender As Object, e As EventArgs) Handles BERHASIL.Click
        If tqty.Text = "" Then
            Exit Sub
        ElseIf Not IsNumeric(tqty.Text) Or tqty.Text = 0 Then
            MsgBox("Cek qty")
            Exit Sub
        End If

        Dim s As String
        Dim ss As String
        s = Microsoft.VisualBasic.Right(kbrg.Text, 2)
        If s = "-S" Then
            ss = kbrg.Text
        Else
            ss = kbrg.Text & "-S"
        End If
       
        Dim a As New clsCCTV.clsBarang
        Dim b As DataTable
        b = a.table(ss)
        If b.Rows.Count <> 1 Then
            MsgBox("Belum ada kode barang baru untuk barang hasil perbaikan")
            Exit Sub
            'Dim gmn As DialogResult
            'gmn = MessageBox.Show("Belum ada kode barang " & ss & " untuk barang hasil perbaikan. Buat barang baru ?", "Konfirmasi", MessageBoxButtons.YesNo)
            'If gmn = Windows.Forms.DialogResult.No Then
            '    Exit Sub
            'End If

            'Dim c As Boolean
            'c = a.input(ss, nbrg.Text, "B", 0, ket.Text, tsat.Text, 0)

            'If c = True Then
            '    MsgBox("Berhasil buat kode barang baru")
            'Else
            '    MsgBox("Gagal buat kode barang baru")
            '    Exit Sub
            'End If
        End If

            With DGV1
                .Rows.Add(1)
                .Rows(DGV1.Rows.Count() - 1).Cells("k_brg1").Value = kbrg.Text
                .Rows(DGV1.Rows.Count() - 1).Cells("n_brg1").Value = nbrg.Text
                .Rows(DGV1.Rows.Count() - 1).Cells("qty1").Value = CDbl(tqty.Text)
                .Rows(DGV1.Rows.Count() - 1).Cells("sat1").Value = tsat.Text
            End With
            kbrg.Clear()
            nbrg.Clear()
            tqty.Text = 0
            tsat.Clear()
    End Sub

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
       
        Dim baris As Integer
        baris = DGV1.CurrentRow.Index.ToString

        DGV1.Rows.RemoveAt(baris)

    End Sub

    Private Sub DGV3_DoubleClick(sender As Object, e As EventArgs) Handles DGV3.DoubleClick
        Dim baris As Integer
        baris = DGV3.CurrentRow.Index.ToString

        DGV3.Rows.RemoveAt(baris)
    End Sub

    Private Sub DGV1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV1.CellContentClick

    End Sub
End Class