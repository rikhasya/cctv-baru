﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceLaporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.CBsup = New System.Windows.Forms.ComboBox()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DGVheader = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_RETUR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TANGGAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(323, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "s/d"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 399)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Detail Laporan :"
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.KET1})
        Me.DGVdetail.Location = New System.Drawing.Point(12, 415)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(575, 223)
        Me.DGVdetail.TabIndex = 30
        '
        'CBsup
        '
        Me.CBsup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBsup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBsup.FormattingEnabled = True
        Me.CBsup.Location = New System.Drawing.Point(527, 10)
        Me.CBsup.Name = "CBsup"
        Me.CBsup.Size = New System.Drawing.Size(180, 21)
        Me.CBsup.TabIndex = 2
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(347, 11)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(88, 20)
        Me.date2.TabIndex = 1
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(233, 11)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(181, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Tanggal"
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(713, 10)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 3
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(445, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Supplier"
        '
        'DGVheader
        '
        Me.DGVheader.AllowUserToDeleteRows = False
        Me.DGVheader.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.DGVheader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVheader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.NO_RETUR, Me.TANGGAL, Me.N_SUP, Me.ALAMAT, Me.TELP, Me.KET, Me.K_SUP, Me.USERID, Me.VALID, Me.DATESTAMP, Me.LUNAS})
        Me.DGVheader.Location = New System.Drawing.Point(12, 104)
        Me.DGVheader.Name = "DGVheader"
        Me.DGVheader.RowHeadersVisible = False
        Me.DGVheader.Size = New System.Drawing.Size(978, 282)
        Me.DGVheader.TabIndex = 29
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'NO_RETUR
        '
        Me.NO_RETUR.DataPropertyName = "NO_JADWAL"
        Me.NO_RETUR.HeaderText = "No Visit"
        Me.NO_RETUR.Name = "NO_RETUR"
        Me.NO_RETUR.ReadOnly = True
        '
        'TANGGAL
        '
        Me.TANGGAL.DataPropertyName = "TGL_FAK"
        Me.TANGGAL.HeaderText = "Tanggal"
        Me.TANGGAL.Name = "TANGGAL"
        Me.TANGGAL.ReadOnly = True
        '
        'N_SUP
        '
        Me.N_SUP.DataPropertyName = "N_SUP"
        Me.N_SUP.HeaderText = "Supplier"
        Me.N_SUP.Name = "N_SUP"
        Me.N_SUP.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        Me.TELP.Visible = False
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'K_SUP
        '
        Me.K_SUP.DataPropertyName = "K_SUP"
        Me.K_SUP.HeaderText = "Kode Supplier"
        Me.K_SUP.Name = "K_SUP"
        Me.K_SUP.ReadOnly = True
        Me.K_SUP.Visible = False
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "Datestamp"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "* Doubleclick untuk edit/hapus laporan"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.CBsup)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.date2)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-3, 35)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1011, 43)
        Me.Panel1.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(412, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(162, 24)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Laporan Service"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(217, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(151, 13)
        Me.Label7.TabIndex = 39
        Me.Label7.Text = "** F2 untuk input Hasil Service"
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        Me.N_BRG.Width = 150
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle9
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.Width = 50
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        Me.SAT.Width = 60
        '
        'KET1
        '
        Me.KET1.DataPropertyName = "KET"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.KET1.DefaultCellStyle = DataGridViewCellStyle10
        Me.KET1.HeaderText = "Keterangan"
        Me.KET1.Name = "KET1"
        Me.KET1.Width = 200
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.Bisque
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.Column1})
        Me.DataGridView2.Location = New System.Drawing.Point(593, 415)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.Size = New System.Drawing.Size(397, 223)
        Me.DataGridView2.TabIndex = 73
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Kode Barang"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Column1
        '
        Me.Column1.HeaderText = "Nomor Serial"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 150
        '
        'ServiceLaporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.DGVheader)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "ServiceLaporan"
        Me.Text = "ServiceLaporan"
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents CBsup As System.Windows.Forms.ComboBox
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DGVheader As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_RETUR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TANGGAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
