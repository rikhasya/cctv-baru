﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVsvc = New System.Windows.Forms.DataGridView()
        Me.kode = New System.Windows.Forms.Panel()
        Me.CBsup = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.noRetur = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.satuan = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.butInput = New System.Windows.Forms.Button()
        Me.Ket1 = New System.Windows.Forms.TextBox()
        Me.qty1 = New System.Windows.Forms.TextBox()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.kode1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ket2 = New System.Windows.Forms.TextBox()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGV3 = New System.Windows.Forms.DataGridView()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.K_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.kode.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVsvc
        '
        Me.DGVsvc.AllowUserToDeleteRows = False
        Me.DGVsvc.BackgroundColor = System.Drawing.Color.LemonChiffon
        Me.DGVsvc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsvc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.KET})
        Me.DGVsvc.Location = New System.Drawing.Point(36, 290)
        Me.DGVsvc.Name = "DGVsvc"
        Me.DGVsvc.RowHeadersVisible = False
        Me.DGVsvc.Size = New System.Drawing.Size(920, 173)
        Me.DGVsvc.TabIndex = 0
        '
        'kode
        '
        Me.kode.BackColor = System.Drawing.Color.Wheat
        Me.kode.Controls.Add(Me.CBsup)
        Me.kode.Controls.Add(Me.Label9)
        Me.kode.Controls.Add(Me.date1)
        Me.kode.Controls.Add(Me.noRetur)
        Me.kode.Controls.Add(Me.Label2)
        Me.kode.Controls.Add(Me.Label1)
        Me.kode.Location = New System.Drawing.Point(0, 46)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(1003, 41)
        Me.kode.TabIndex = 0
        '
        'CBsup
        '
        Me.CBsup.FormattingEnabled = True
        Me.CBsup.Location = New System.Drawing.Point(620, 11)
        Me.CBsup.Name = "CBsup"
        Me.CBsup.Size = New System.Drawing.Size(211, 21)
        Me.CBsup.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(560, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Supplier"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(253, 11)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 4
        '
        'noRetur
        '
        Me.noRetur.Enabled = False
        Me.noRetur.Location = New System.Drawing.Point(423, 11)
        Me.noRetur.Name = "noRetur"
        Me.noRetur.Size = New System.Drawing.Size(122, 20)
        Me.noRetur.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(368, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "No. Visit"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(201, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tanggal"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Beige
        Me.Panel2.Controls.Add(Me.satuan)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.butInput)
        Me.Panel2.Controls.Add(Me.Ket1)
        Me.Panel2.Controls.Add(Me.qty1)
        Me.Panel2.Controls.Add(Me.nama)
        Me.Panel2.Controls.Add(Me.kode1)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(533, 121)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(447, 162)
        Me.Panel2.TabIndex = 1
        '
        'satuan
        '
        Me.satuan.Enabled = False
        Me.satuan.Location = New System.Drawing.Point(82, 61)
        Me.satuan.Name = "satuan"
        Me.satuan.Size = New System.Drawing.Size(48, 20)
        Me.satuan.TabIndex = 14
        Me.satuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(79, 46)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(23, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Sat"
        '
        'butInput
        '
        Me.butInput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butInput.Location = New System.Drawing.Point(342, 122)
        Me.butInput.Name = "butInput"
        Me.butInput.Size = New System.Drawing.Size(75, 23)
        Me.butInput.TabIndex = 2
        Me.butInput.Text = "INPUT"
        Me.butInput.UseVisualStyleBackColor = True
        '
        'Ket1
        '
        Me.Ket1.AcceptsReturn = True
        Me.Ket1.Location = New System.Drawing.Point(17, 101)
        Me.Ket1.Multiline = True
        Me.Ket1.Name = "Ket1"
        Me.Ket1.Size = New System.Drawing.Size(314, 44)
        Me.Ket1.TabIndex = 1
        '
        'qty1
        '
        Me.qty1.Location = New System.Drawing.Point(17, 62)
        Me.qty1.Name = "qty1"
        Me.qty1.Size = New System.Drawing.Size(48, 20)
        Me.qty1.TabIndex = 0
        Me.qty1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nama
        '
        Me.nama.Enabled = False
        Me.nama.Location = New System.Drawing.Point(153, 24)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(178, 20)
        Me.nama.TabIndex = 9
        '
        'kode1
        '
        Me.kode1.Enabled = False
        Me.kode1.Location = New System.Drawing.Point(16, 24)
        Me.kode1.Name = "kode1"
        Me.kode1.Size = New System.Drawing.Size(122, 20)
        Me.kode1.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 86)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Keterangan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Qty"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(150, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Kode"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(324, 610)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Keterangan :"
        '
        'ket2
        '
        Me.ket2.Location = New System.Drawing.Point(398, 610)
        Me.ket2.Multiline = True
        Me.ket2.Name = "ket2"
        Me.ket2.Size = New System.Drawing.Size(396, 44)
        Me.ket2.TabIndex = 2
        '
        'butSIM
        '
        Me.butSIM.Enabled = False
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(800, 615)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(75, 37)
        Me.butSIM.TabIndex = 3
        Me.butSIM.Text = "SIMPAN"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(881, 615)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(75, 37)
        Me.butBAT.TabIndex = 4
        Me.butBAT.Text = "BATAL"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(367, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(247, 22)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "INPUT SERVICE SUPPLIER" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'DGV3
        '
        Me.DGV3.AllowUserToAddRows = False
        Me.DGV3.AllowUserToDeleteRows = False
        Me.DGV3.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGV3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG3, Me.NO_FAK3, Me.N_BRG3, Me.QTY3, Me.SAT3})
        Me.DGV3.Location = New System.Drawing.Point(12, 102)
        Me.DGV3.Name = "DGV3"
        Me.DGV3.ReadOnly = True
        Me.DGV3.RowHeadersVisible = False
        Me.DGV3.Size = New System.Drawing.Size(515, 182)
        Me.DGV3.TabIndex = 22
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(533, 102)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(157, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "* Doubleclick untuk pilih barang"
        '
        'K_BRG3
        '
        Me.K_BRG3.DataPropertyName = "K_BRG"
        Me.K_BRG3.HeaderText = "Kode Barang"
        Me.K_BRG3.Name = "K_BRG3"
        Me.K_BRG3.ReadOnly = True
        '
        'NO_FAK3
        '
        Me.NO_FAK3.DataPropertyName = "NO_FAK"
        Me.NO_FAK3.HeaderText = "NO FAK 3"
        Me.NO_FAK3.Name = "NO_FAK3"
        Me.NO_FAK3.ReadOnly = True
        Me.NO_FAK3.Visible = False
        '
        'N_BRG3
        '
        Me.N_BRG3.DataPropertyName = "N_BRG"
        Me.N_BRG3.HeaderText = "Nama Barang"
        Me.N_BRG3.Name = "N_BRG3"
        Me.N_BRG3.ReadOnly = True
        Me.N_BRG3.Width = 150
        '
        'QTY3
        '
        Me.QTY3.DataPropertyName = "QTY"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.QTY3.DefaultCellStyle = DataGridViewCellStyle7
        Me.QTY3.HeaderText = "Qty"
        Me.QTY3.Name = "QTY3"
        Me.QTY3.ReadOnly = True
        Me.QTY3.Width = 50
        '
        'SAT3
        '
        Me.SAT3.DataPropertyName = "SAT"
        Me.SAT3.HeaderText = "Sat"
        Me.SAT3.Name = "SAT3"
        Me.SAT3.ReadOnly = True
        Me.SAT3.Width = 60
        '
        'K_BRG
        '
        Me.K_BRG.HeaderText = "Kode"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.HeaderText = "Nama"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        Me.N_BRG.Width = 150
        '
        'QTY
        '
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        Me.QTY.Width = 50
        '
        'SAT
        '
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        Me.SAT.Width = 60
        '
        'KET
        '
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        Me.KET.Width = 200
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Location = New System.Drawing.Point(327, 465)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(629, 139)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.Bisque
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.DataGridView1.Location = New System.Drawing.Point(197, 23)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(426, 108)
        Me.DataGridView1.TabIndex = 76
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Kode Barang"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nomor Serial"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(105, 108)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 76
        Me.Button2.Text = "INPUT"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(11, 82)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(169, 20)
        Me.TextBox4.TabIndex = 74
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(11, 43)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(169, 20)
        Me.TextBox3.TabIndex = 72
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(8, 66)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(67, 13)
        Me.Label28.TabIndex = 75
        Me.Label28.Text = "Nomor Serial"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(8, 27)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(69, 13)
        Me.Label27.TabIndex = 73
        Me.Label27.Text = "Kode Barang"
        '
        'ServiceInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.DGV3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.butBAT)
        Me.Controls.Add(Me.butSIM)
        Me.Controls.Add(Me.ket2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.kode)
        Me.Controls.Add(Me.DGVsvc)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "ServiceInput"
        Me.Text = "ServiceInput"
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.kode.ResumeLayout(False)
        Me.kode.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVsvc As System.Windows.Forms.DataGridView
    Friend WithEvents kode As System.Windows.Forms.Panel
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents noRetur As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents butInput As System.Windows.Forms.Button
    Friend WithEvents Ket1 As System.Windows.Forms.TextBox
    Friend WithEvents qty1 As System.Windows.Forms.TextBox
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents kode1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ket2 As System.Windows.Forms.TextBox
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents satuan As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CBsup As System.Windows.Forms.ComboBox
    Friend WithEvents DGV3 As System.Windows.Forms.DataGridView
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
End Class
