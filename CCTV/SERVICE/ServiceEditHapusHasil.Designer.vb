﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceEditHapusHasil
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.butHAP = New System.Windows.Forms.Button()
        Me.satuan = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.butInput = New System.Windows.Forms.Button()
        Me.biaya = New System.Windows.Forms.TextBox()
        Me.qty1 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.kode = New System.Windows.Forms.Panel()
        Me.kSup = New System.Windows.Forms.Label()
        Me.supplier = New System.Windows.Forms.TextBox()
        Me.noFak = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.noService = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.kode1 = New System.Windows.Forms.TextBox()
        Me.DGVsvc = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.biaya1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.ket2 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGV3 = New System.Windows.Forms.DataGridView()
        Me.K_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totb = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.kode.SuspendLayout()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butHAP
        '
        Me.butHAP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butHAP.Location = New System.Drawing.Point(866, 575)
        Me.butHAP.Name = "butHAP"
        Me.butHAP.Size = New System.Drawing.Size(75, 37)
        Me.butHAP.TabIndex = 22
        Me.butHAP.Text = "HAPUS"
        Me.butHAP.UseVisualStyleBackColor = True
        '
        'satuan
        '
        Me.satuan.Enabled = False
        Me.satuan.Location = New System.Drawing.Point(411, 23)
        Me.satuan.Name = "satuan"
        Me.satuan.Size = New System.Drawing.Size(48, 20)
        Me.satuan.TabIndex = 14
        Me.satuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(408, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(23, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Sat"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(331, 9)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 4
        '
        'butInput
        '
        Me.butInput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butInput.Location = New System.Drawing.Point(642, 21)
        Me.butInput.Name = "butInput"
        Me.butInput.Size = New System.Drawing.Size(75, 23)
        Me.butInput.TabIndex = 2
        Me.butInput.Text = "INPUT"
        Me.butInput.UseVisualStyleBackColor = True
        '
        'biaya
        '
        Me.biaya.Location = New System.Drawing.Point(476, 23)
        Me.biaya.Name = "biaya"
        Me.biaya.Size = New System.Drawing.Size(149, 20)
        Me.biaya.TabIndex = 1
        Me.biaya.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'qty1
        '
        Me.qty1.Location = New System.Drawing.Point(346, 24)
        Me.qty1.Name = "qty1"
        Me.qty1.Size = New System.Drawing.Size(48, 20)
        Me.qty1.TabIndex = 0
        Me.qty1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(438, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Supplier"
        '
        'kode
        '
        Me.kode.BackColor = System.Drawing.Color.LightSteelBlue
        Me.kode.Controls.Add(Me.kSup)
        Me.kode.Controls.Add(Me.supplier)
        Me.kode.Controls.Add(Me.noFak)
        Me.kode.Controls.Add(Me.Label11)
        Me.kode.Controls.Add(Me.Label9)
        Me.kode.Controls.Add(Me.date1)
        Me.kode.Controls.Add(Me.noService)
        Me.kode.Controls.Add(Me.Label2)
        Me.kode.Controls.Add(Me.Label1)
        Me.kode.Enabled = False
        Me.kode.Location = New System.Drawing.Point(0, 55)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(1003, 41)
        Me.kode.TabIndex = 18
        '
        'kSup
        '
        Me.kSup.AutoSize = True
        Me.kSup.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.kSup.Location = New System.Drawing.Point(487, -2)
        Me.kSup.Name = "kSup"
        Me.kSup.Size = New System.Drawing.Size(21, 9)
        Me.kSup.TabIndex = 10
        Me.kSup.Text = "kode"
        Me.kSup.Visible = False
        '
        'supplier
        '
        Me.supplier.Location = New System.Drawing.Point(489, 10)
        Me.supplier.Name = "supplier"
        Me.supplier.Size = New System.Drawing.Size(222, 20)
        Me.supplier.TabIndex = 9
        '
        'noFak
        '
        Me.noFak.Enabled = False
        Me.noFak.Location = New System.Drawing.Point(139, 10)
        Me.noFak.Name = "noFak"
        Me.noFak.Size = New System.Drawing.Size(122, 20)
        Me.noFak.TabIndex = 7
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(74, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(57, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "No. Faktur"
        '
        'noService
        '
        Me.noService.Enabled = False
        Me.noService.Location = New System.Drawing.Point(786, 10)
        Me.noService.Name = "noService"
        Me.noService.Size = New System.Drawing.Size(122, 20)
        Me.noService.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(717, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "No. Service"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(279, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tanggal"
        '
        'nama
        '
        Me.nama.Enabled = False
        Me.nama.Location = New System.Drawing.Point(151, 24)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(178, 20)
        Me.nama.TabIndex = 9
        '
        'kode1
        '
        Me.kode1.Enabled = False
        Me.kode1.Location = New System.Drawing.Point(14, 24)
        Me.kode1.Name = "kode1"
        Me.kode1.Size = New System.Drawing.Size(122, 20)
        Me.kode1.TabIndex = 5
        '
        'DGVsvc
        '
        Me.DGVsvc.AllowUserToAddRows = False
        Me.DGVsvc.AllowUserToDeleteRows = False
        Me.DGVsvc.BackgroundColor = System.Drawing.Color.Lavender
        Me.DGVsvc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsvc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.biaya1})
        Me.DGVsvc.Location = New System.Drawing.Point(37, 355)
        Me.DGVsvc.Name = "DGVsvc"
        Me.DGVsvc.ReadOnly = True
        Me.DGVsvc.RowHeadersVisible = False
        Me.DGVsvc.Size = New System.Drawing.Size(920, 210)
        Me.DGVsvc.TabIndex = 23
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Fak"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Satuan"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'biaya1
        '
        Me.biaya1.DataPropertyName = "KET"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.biaya1.DefaultCellStyle = DataGridViewCellStyle1
        Me.biaya1.HeaderText = "Biaya"
        Me.biaya1.Name = "biaya1"
        Me.biaya1.ReadOnly = True
        '
        'butSIM
        '
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(764, 575)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(75, 37)
        Me.butSIM.TabIndex = 21
        Me.butSIM.Text = "SIMPAN"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'ket2
        '
        Me.ket2.Location = New System.Drawing.Point(108, 575)
        Me.ket2.Multiline = True
        Me.ket2.Name = "ket2"
        Me.ket2.Size = New System.Drawing.Size(287, 56)
        Me.ket2.TabIndex = 20
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(34, 575)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Keterangan :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Kode"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.PowderBlue
        Me.Panel2.Controls.Add(Me.satuan)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.butInput)
        Me.Panel2.Controls.Add(Me.biaya)
        Me.Panel2.Controls.Add(Me.qty1)
        Me.Panel2.Controls.Add(Me.nama)
        Me.Panel2.Controls.Add(Me.kode1)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(37, 293)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(920, 55)
        Me.Panel2.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(473, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Biaya"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(343, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Qty"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(148, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Nama"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(369, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(270, 22)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "EDIT/HAPUS HASIL SERVICE"
        '
        'DGV3
        '
        Me.DGV3.AllowUserToAddRows = False
        Me.DGV3.AllowUserToDeleteRows = False
        Me.DGV3.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.DGV3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG3, Me.NO_FAK3, Me.N_BRG3, Me.QTY3, Me.SAT3, Me.KET})
        Me.DGV3.Location = New System.Drawing.Point(173, 103)
        Me.DGV3.Name = "DGV3"
        Me.DGV3.ReadOnly = True
        Me.DGV3.RowHeadersVisible = False
        Me.DGV3.Size = New System.Drawing.Size(643, 170)
        Me.DGV3.TabIndex = 26
        '
        'K_BRG3
        '
        Me.K_BRG3.DataPropertyName = "K_BRG"
        Me.K_BRG3.HeaderText = "Kode Barang"
        Me.K_BRG3.Name = "K_BRG3"
        Me.K_BRG3.ReadOnly = True
        '
        'NO_FAK3
        '
        Me.NO_FAK3.DataPropertyName = "NO_FAK"
        Me.NO_FAK3.HeaderText = "NO FAK 3"
        Me.NO_FAK3.Name = "NO_FAK3"
        Me.NO_FAK3.ReadOnly = True
        Me.NO_FAK3.Visible = False
        '
        'N_BRG3
        '
        Me.N_BRG3.DataPropertyName = "N_BRG"
        Me.N_BRG3.HeaderText = "Nama Barang"
        Me.N_BRG3.Name = "N_BRG3"
        Me.N_BRG3.ReadOnly = True
        '
        'QTY3
        '
        Me.QTY3.DataPropertyName = "QTY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.QTY3.DefaultCellStyle = DataGridViewCellStyle2
        Me.QTY3.HeaderText = "Qty"
        Me.QTY3.Name = "QTY3"
        Me.QTY3.ReadOnly = True
        '
        'SAT3
        '
        Me.SAT3.DataPropertyName = "SAT"
        Me.SAT3.HeaderText = "Sat"
        Me.SAT3.Name = "SAT3"
        Me.SAT3.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'totb
        '
        Me.totb.Enabled = False
        Me.totb.Location = New System.Drawing.Point(502, 572)
        Me.totb.Name = "totb"
        Me.totb.Size = New System.Drawing.Size(192, 20)
        Me.totb.TabIndex = 28
        Me.totb.Text = "0"
        Me.totb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(434, 573)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(66, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Total Biaya :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(170, 275)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(157, 13)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "* Doubleclick untuk pilih barang"
        '
        'ServiceEditHapusHasil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.totb)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.DGV3)
        Me.Controls.Add(Me.butHAP)
        Me.Controls.Add(Me.kode)
        Me.Controls.Add(Me.DGVsvc)
        Me.Controls.Add(Me.butSIM)
        Me.Controls.Add(Me.ket2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "ServiceEditHapusHasil"
        Me.Text = "ServiceEditHapusHasil"
        Me.kode.ResumeLayout(False)
        Me.kode.PerformLayout()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butHAP As System.Windows.Forms.Button
    Friend WithEvents satuan As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents butInput As System.Windows.Forms.Button
    Friend WithEvents biaya As System.Windows.Forms.TextBox
    Friend WithEvents qty1 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents kode As System.Windows.Forms.Panel
    Friend WithEvents noService As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents kode1 As System.Windows.Forms.TextBox
    Friend WithEvents DGVsvc As System.Windows.Forms.DataGridView
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents ket2 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents noFak As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents kSup As System.Windows.Forms.Label
    Friend WithEvents supplier As System.Windows.Forms.TextBox
    Friend WithEvents DGV3 As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents biaya1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents totb As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
