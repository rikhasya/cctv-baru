﻿Public Class JadwalEdithapus

    Public BARIS As String = "N"

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles batal.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Jadwal ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim b As New clsCCTV.clsKunjungan
        Dim c As Boolean
        c = b.hapus(NOFAK.Text)
        If c = True Then
            MsgBox("Berhasil Hapus Jadwal")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Hapus Jadwal")
            Exit Sub
        End If
    End Sub

    Private Sub simpan_Click(sender As Object, e As EventArgs) Handles simpan.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Perubahan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Me.Close()
        Exit Sub

    End Sub

    Private Sub JadwalInput_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.dari.Text = "EJDW"
            Cari_Brg.Show()
        End If
    End Sub

    Private Sub JadwalInput_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub JadwalInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsTehnisi
        Dim b As DataTable
        b = a.table("%", "%")
        With cbTeknisi
            .DisplayMember = "N_TEH"
            .ValueMember = "K_TEH"
            .DataSource = b
        End With

        loadTek()
        loadbrg()
    End Sub

    Sub loadTek()
        Dim a As New clsCCTV.clsKunjungan
        Dim b As DataSet
        b = a.table(NOFAK.Text)
        DGVtek.DataSource = b.Tables("DETJADKUNJUNGAN1")
    End Sub

    Private Sub INPUTtek_Click(sender As Object, e As EventArgs) Handles INPUTtek.Click
        Dim table1 As New DataTable("DETJADKUNJUNGAN1")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_TEH")
        table1.Rows.Add(NOFAK, cbTeknisi.SelectedValue)

        Dim table2 As New DataTable("DETJADKUNJUNGAN2")
        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim a As New clsCCTV.clsKunjungan
        Dim b As Boolean
        b = a.input1TEH(NOFAK.Text, set1)

        If b = True Then
            loadTek()
        End If
    End Sub

    Sub loadbrg()
        Dim a As New clsCCTV.clsKunjungan
        Dim b As DataSet
        b = a.table(NOFAK.Text)
        DGVbrg.DataSource = b.Tables("DETJADKUNJUNGAN2")
    End Sub

    Private Sub INPUTbrg_Click(sender As Object, e As EventArgs) Handles INPUTbrg.Click

        Dim table1 As New DataTable("DETJADKUNJUNGAN1")
        Dim table2 As New DataTable("DETJADKUNJUNGAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")
        table2.Rows.Add(NOFAK.Text, kbrg.Text, nbrg.Text, CDbl(qty.Text), satuan.Text)

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim a As New clsCCTV.clsKunjungan
        Dim b As Boolean
        b = a.input1brg(NOFAK.Text, set1)

        If b = True Then
            loadbrg()
            kbrg.Clear()
            nbrg.Clear()
            qty.Text = 0
            satuan.Clear()
        End If

    End Sub

    Private Sub DGVtek_DoubleClick(sender As Object, e As EventArgs) Handles DGVtek.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus teknisi ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGVtek.CurrentRow.Index.ToString
        Dim a As New clsCCTV.clsKunjungan
        Dim b As Boolean
        b = a.hapus1teh(NOFAK.Text, DGVtek.Item("Kode", row).Value.ToString)

        If b = True Then
            loadTek()
        Else
            MsgBox("gagal hapus teknisi")
            Exit Sub
        End If
    End Sub

    Private Sub DGVbrg_DoubleClick(sender As Object, e As EventArgs) Handles DGVbrg.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus barang ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGVbrg.CurrentRow.Index.ToString
        Dim a As New clsCCTV.clsKunjungan
        Dim b As Boolean
        b = a.hapus1brg(NOFAK.Text, DGVbrg.Item("K_BRG", row).Value.ToString)

        If b = True Then
            loadbrg()
        Else
            MsgBox("gagal hapus barang")
            Exit Sub
        End If
    End Sub

    Private Sub DGVbrg_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVbrg.CellContentClick

    End Sub
End Class