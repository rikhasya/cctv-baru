﻿Public Class JadwalLaporan

    Private Sub JadwalLaporan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With cbLgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        cbLgn.SelectedValue = "0"

        cbStatus.Text = "ALL"
    End Sub

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("JADWALEDITHAPUS")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If

        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As JadwalEdithapus
        frm = New JadwalEdithapus
        frm.MdiParent = frmMenu
        frm.Text = "Edit / Hapus Departemen"
        Dim row As Integer
        row = DGV1.CurrentRow.Index.ToString
        frm.NOFAK.Text = DGV1.Item("NO_FAK", row).Value.ToString
        frm.nlgn.Text = DGV1.Item("N_LGN", row).Value.ToString
        frm.klgn.Text = DGV1.Item("K_LGN", row).Value.ToString
        frm.ket.Text = DGV1.Item("KET", row).Value.ToString
        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()

    End Sub

    Private Sub DataGridView1_KeyDown(sender As Object, e As KeyEventArgs) Handles DGV1.KeyDown

        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("JADWALHASIL")
            If b = False Then
                MsgBox("anda tdk di otorisasi")
                Exit Sub
            End If

            '    Dim row As Integer
            '    row = DGV1.CurrentRow.Index.ToString
            '    'If DGV1.Item("HASIL_KUNJ", row).Value.ToString <> "JADWAL" Then
            '    MsgBox("Sudah pernah input status.")
            '    Exit Sub
            'End If
            'HasilVisitInputBerhasil.noVisit.Text = DGV1.Item("NO_FAK", row).Value.ToString
            'HasilVisitInputBerhasil.nlgn.Text = DGV1.Item("N_LGN", row).Value.ToString
            HasilVisitInputBerhasil.Show()

        End If
    End Sub

    Private Sub CARI_Click(sender As Object, e As EventArgs) Handles CARI.Click
        DGV1.AutoGenerateColumns = False 'before databinding

        Dim lgn As String
        If cbLgn.Text = "ALL" Then
            lgn = "%"
        Else
            lgn = cbLgn.SelectedValue
        End If

        Dim status As String
        If cbStatus.Text = "ALL" Then
            status = "%"
        Else
            status = cbStatus.Text
        End If

        Dim a As New clsCCTV.clsKunjungan
        Dim b As DataSet
        b = a.table(CDate(date1.Text), CDate(date2.Text), lgn, status)

        Dim parentColumn As DataColumn = b.Tables("DATJADKUNJUNGAN").Columns("NO_FAK")
        Dim childColumn As DataColumn = b.Tables("DETJADKUNJUNGAN1").Columns("NO_FAK")
        Dim childColumn2 As DataColumn = b.Tables("DETJADKUNJUNGAN2").Columns("NO_FAK")

        Dim relDATbELIDETbELI As DataRelation
        relDATbELIDETbELI = New DataRelation("DETJADKUNJUNGAN1", parentColumn, childColumn, False)
        b.Relations.Add(relDATbELIDETbELI)

        Dim relDATbELIDETbELI2 As DataRelation
        relDATbELIDETbELI2 = New DataRelation("DETJADKUNJUNGAN2", parentColumn, childColumn2, False)
        b.Relations.Add(relDATbELIDETbELI2)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource
        Dim detailbindingSource2 As New BindingSource

        masterbindingSource.DataSource = b
        masterbindingSource.DataMember = "DATJADKUNJUNGAN"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETJADKUNJUNGAN1"

        detailbindingSource2.DataSource = masterbindingSource
        detailbindingSource2.DataMember = "DETJADKUNJUNGAN2"

        DGV1.DataSource = masterbindingSource
        DGV3.DataSource = detailbindingSource2
        DGV2.DataSource = detailbindingSource
        DGV1.Focus()

    End Sub
End Class