﻿Public Class HasilVisitLaporan

    Private Sub PerbaikanHasilLaporan_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub PerbaikanHasilLaporan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBsup
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBsup.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(sender As Object, e As EventArgs) Handles butCARI.Click
        Dim lgn As String
        If CBsup.Text = "ALL" Then
            lgn = "%"
        Else
            lgn = CBsup.SelectedValue
        End If

        Dim a As New clsCCTV.clsHasilPerbaikan
        Dim b As DataSet
        b = a.table(CDate(date1.Text), CDate(date2.Text), lgn)

        Dim parentColumn As DataColumn = b.Tables("DATHASILPERBAIKAN").Columns("NO_FAK")
        Dim childColumn As DataColumn = b.Tables("DETHASILPERBAIKAN").Columns("NO_FAK")
        Dim childColumn2 As DataColumn = b.Tables("DETHASILPERBAIKAN2").Columns("NO_FAK")

        Dim relDATbELIDETbELI As DataRelation
        relDATbELIDETbELI = New DataRelation("DETHASILPERBAIKAN", parentColumn, childColumn, False)
        b.Relations.Add(relDATbELIDETbELI)

        Dim relDATbELIDETbELI2 As DataRelation
        relDATbELIDETbELI2 = New DataRelation("DETHASILPERBAIKAN2", parentColumn, childColumn2, False)
        b.Relations.Add(relDATbELIDETbELI2)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource
        Dim detailbindingSource2 As New BindingSource

        masterbindingSource.DataSource = b
        masterbindingSource.DataMember = "DATHASILPERBAIKAN"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETHASILPERBAIKAN"

        detailbindingSource2.DataSource = masterbindingSource
        detailbindingSource2.DataMember = "DETHASILPERBAIKAN2"

        DGV1.DataSource = masterbindingSource
        'DGV3.DataSource = detailbindingSource2
        'DGV2.DataSource = detailbindingSource
        DGV1.Focus()

    End Sub

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PERBAIKANHASILEDITHAPUS")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If

        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As PerbaikanHasilEditHapus
        frm = New PerbaikanHasilEditHapus
        frm.MdiParent = frmMenu
        frmMenu.Text = "Edit / Hapus Hasil Perbaikan"
        'Dim row As Integer
        'row = DGV1.CurrentRow.Index.ToString
        'frm.nofak.Text = DGV1.Item("NO_JADWAL", row).Value.ToString
        'frm.date1.Text = CDate(DGV1.Item("TGL_FAK", row).Value.ToString)
        'frm.teknisi.Text = DGV1.Item("N_TEK", row).Value.ToString
        'frm.ktek.Text = DGV1.Item("K_TEK", row).Value.ToString
        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()
    End Sub

    Private Sub DGV1_KeyDown(sender As Object, e As KeyEventArgs) Handles DGV1.KeyDown
        Try
            If e.KeyCode = Keys.F1 Then
                Dim a As New clsCCTV.clsPengguna
                Dim b As Boolean
                b = a.cek("PERBAIKANINPUT")
                If b = False Then
                    MsgBox("anda tdk di otorisasi")
                    Exit Sub
                End If
                Dim row As Integer
                row = DGV1.CurrentRow.Index.ToString

                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As Perbaikan
                frm = New Perbaikan
                frm.MdiParent = frmMenu
                frm.Text = "Input Perbaikan"
                'frm.noVisit.Text = DGV1.Item("NO_FAK", row).Value.ToString
                frm.Show()
                frm.Dock = DockStyle.Fill
                Me.Close()


            ElseIf e.KeyCode = Keys.F2 Then
                Dim a As New clsCCTV.clsPengguna
                Dim b As Boolean
                b = a.cek("SERVICE_INPUT")
                If b = False Then
                    MsgBox("anda tdk di otorisasi")
                    Exit Sub
                End If
                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As ServiceInput
                frm = New ServiceInput
                frm.MdiParent = frmMenu
                frmMenu.Text = "Input Service Supplier"
                Dim row As Integer
                row = DGV1.CurrentRow.Index.ToString
                'frm.noRetur.Text = DGV1.Item("NO_FAK", row).Value.ToString
                frm.Show()
                frm.Dock = DockStyle.Fill
                Me.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class