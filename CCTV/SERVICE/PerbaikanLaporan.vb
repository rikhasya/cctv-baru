﻿Public Class PerbaikanLaporan

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PERBAIKANEDITHAPUS")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As PerbaikanEditHapus
        frm = New PerbaikanEditHapus
        frm.MdiParent = frmMenu
        frmMenu.Text = "Edit / Hapus Perbaikan"
        Dim row As Integer
        row = DGV1.CurrentRow.Index.ToString
        frm.nofak.Text = DGV1.Item("NO_FAK", row).Value.ToString
        frm.date1.Text = CDate(DGV1.Item("TANGGAL", row).Value.ToString)
        frm.date2.Text = CDate(DGV1.Item("TGL_JT", row).Value.ToString)
        frm.teknisi.Text = DGV1.Item("N_TEK", row).Value.ToString
        frm.noVisit.Text = DGV1.Item("NO_JADWAL", row).Value.ToString
        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()

    End Sub

    Private Sub DGVheader_KeyDown(sender As Object, e As KeyEventArgs) Handles DGV1.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("PERBAIKANHASIL")
            If b = False Then
                MsgBox("anda tdk di otorisasi")
                Exit Sub
            End If
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As PerbaikanHasil
            frm = New PerbaikanHasil
            frm.MdiParent = frmMenu
            frm.Text = "Input Hasil Perbaikan"
            Dim row As Integer
            row = DGV1.CurrentRow.Index.ToString
            'frm.noPerbaikan.Text = DGV1.Item("NO_FAK", row).Value.ToString
            'frm.teknisi.Text = DGV1.Item("N_TEK", row).Value.ToString
            'frm.ktek.Text = DGV1.Item("K_TEK", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()

        End If
    End Sub

    Dim STTS As String

    Private Sub butCARI_Click(sender As Object, e As EventArgs) Handles butCARI.Click
        Dim lgn As String
        If CBsup.Text = "ALL" Then
            lgn = "%"
        Else
            lgn = CBsup.SelectedValue
        End If

        If CBstts.CheckState = CheckState.Checked Then
            STTS = "Y"
        ElseIf CBstts.CheckState = CheckState.Indeterminate Then
            STTS = "%"
        ElseIf CBstts.CheckState = CheckState.Unchecked Then
            STTS = "N"
        End If
        Dim a As New clsCCTV.clsPerbaikan
        Dim b As DataSet
        b = a.table(CDate(date1.Text), CDate(date2.Text), lgn, STTS)

        Dim parentColumn As DataColumn = b.Tables("DATPERBAIKAN").Columns("NO_FAK")
        Dim childColumn As DataColumn = b.Tables("DETPERBAIKAN").Columns("NO_FAK")
        Dim childColumn2 As DataColumn = b.Tables("DETPERBAIKAN2").Columns("NO_FAK")

        Dim relDATbELIDETbELI As DataRelation
        relDATbELIDETbELI = New DataRelation("DETPERBAIKAN", parentColumn, childColumn, False)
        b.Relations.Add(relDATbELIDETbELI)

        Dim relDATbELIDETbELI2 As DataRelation
        relDATbELIDETbELI2 = New DataRelation("DETPERBAIKAN2", parentColumn, childColumn2, False)
        b.Relations.Add(relDATbELIDETbELI2)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource
        Dim detailbindingSource2 As New BindingSource

        masterbindingSource.DataSource = b
        masterbindingSource.DataMember = "DATPERBAIKAN"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETPERBAIKAN"

        detailbindingSource2.DataSource = masterbindingSource
        detailbindingSource2.DataMember = "DETPERBAIKAN2"

        DGV1.DataSource = masterbindingSource
        'DGV3.DataSource = detailbindingSource2
        DGV2.DataSource = detailbindingSource
        DGV1.Focus()

    End Sub

    Private Sub PerbaikanLaporan_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub PerbaikanLaporan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsTehnisi
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_TEH") = "ALL"
        row("K_TEH") = "0"
        b.Rows.Add(row)
        With CBsup
            .DisplayMember = "N_TEH"
            .ValueMember = "K_TEH"
            .DataSource = b
        End With
        CBsup.SelectedValue = "0"
    End Sub

    Private Sub DGV1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV1.CellContentClick

    End Sub
End Class