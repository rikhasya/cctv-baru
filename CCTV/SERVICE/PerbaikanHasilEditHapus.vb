﻿Public Class PerbaikanHasilEditHapus

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim i As Integer
        Dim tot1 As Integer = 0
        For i = 0 To DGV2.RowCount - 1
            tot1 = tot1 + DGV2.Item("QTY", i).Value
        Next

        Dim tot2 As Integer = 0
        For i = 0 To DGV1.RowCount - 1
            tot2 = tot2 + DGV1.Item("QTY1", i).Value
        Next

        Dim tot3 As Integer = 0
        For i = 0 To DGV3.RowCount - 1
            tot3 = tot3 + DGV3.Item("QTY2", i).Value
        Next

        If (tot2 + tot3) <> tot1 Then
            MsgBox("Jumlah Barang Pada Saat Input = Jumlah Barang Setelah Perbaikan")
            Exit Sub
        End If

        MsgBox("Berhasil Simpan Perubahan")
        Exit Sub


    End Sub

    Private Sub PerbaikanHasil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load1()
        load2()
        load3()
    End Sub

    Sub load2()
        Dim c As New clsCCTV.clsPerbaikan
        Dim d As DataSet
        d = c.table(nofak.Text)
        DGV2.DataSource = d.Tables("DETPERBAIKAN")
    End Sub

    Sub load1()
        Dim c As New clsCCTV.clsHasilPerbaikan
        Dim d As DataSet
        d = c.table(nofak.Text)
        DGV1.DataSource = d.Tables("DETHASILPERBAIKAN")
    End Sub

    Sub load3()
        Dim c As New clsCCTV.clsHasilPerbaikan
        Dim d As DataSet
        d = c.table(nofak.Text)
        DGV3.DataSource = d.Tables("DETHASILPERBAIKAN2")
    End Sub

    Private Sub DGV2_DoubleClick(sender As Object, e As EventArgs) Handles DGV2.DoubleClick
        Dim row As Integer
        row = DGV2.CurrentRow.Index.ToString
        kbrg.Text = DGV2.Item("K_BRG", row).Value.ToString
        nbrg.Text = DGV2.Item("N_BRG", row).Value.ToString
        tsat.Text = DGV2.Item("SAT", row).Value.ToString
    End Sub

    Private Sub GAGAL_Click(sender As Object, e As EventArgs) Handles GAGAL.Click
        If tqty.Text = "" Then
            Exit Sub
        ElseIf Not IsNumeric(tqty.Text) Or tqty.Text = 0 Then
            MsgBox("Cek qty")
            Exit Sub
        End If
        Dim table1 As New DataTable("DETHASILPERBAIKAN")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim table2 As New DataTable("DETHASILPERBAIKAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")

        table2.Rows.Add(nofak.Text, kbrg.Text, nbrg.Text, CDbl(tqty.Text), tsat.Text)

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim a As New clsCCTV.clsHasilPerbaikan
        Dim b As Boolean
        b = a.input1_2(nofak.Text, set1)

        If b = True Then
            load3()
            kbrg.Clear()
            nbrg.Clear()
            tqty.Text = 0
            tsat.Clear()
        Else
            MsgBox("gagal input barang")
        End If
    End Sub

    Private Sub BERHASIL_Click(sender As Object, e As EventArgs) Handles BERHASIL.Click
        If tqty.Text = "" Then
            Exit Sub
        ElseIf Not IsNumeric(tqty.Text) Or tqty.Text = 0 Then
            MsgBox("Cek qty")
            Exit Sub
        End If

        Dim table1 As New DataTable("DETHASILPERBAIKAN")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim table2 As New DataTable("DETHASILPERBAIKAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")

        table1.Rows.Add(nofak.Text, kbrg.Text, nbrg.Text, CDbl(tqty.Text), tsat.Text)

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim a As New clsCCTV.clsHasilPerbaikan
        Dim b As Boolean
        b = a.input1_1(nofak.Text, set1)

        If b = True Then
            load1()
            kbrg.Clear()
            nbrg.Clear()
            tqty.Text = 0
            tsat.Clear()
        Else
            MsgBox("gagal input barang")
        End If
    End Sub

    Private Sub HAPUS_Click(sender As Object, e As EventArgs) Handles HAPUS.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Hasil Perbaikan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsHasilPerbaikan
        Dim b As Boolean
        b = a.hapus(nofak.Text)

        If b = True Then
            MsgBox("Berhasil Hapus Hasil Perbaikan")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Hapus Hasil Perbaikan")
            Exit Sub
        End If

    End Sub

    Private Sub DGV1_DoubleClick(sender As Object, e As EventArgs) Handles DGV1.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Barang ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGV1.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsHasilPerbaikan
        Dim b As Boolean
        b = a.hapus1BRG1(nofak.Text, DGV1.Item("K_BRG1", row).Value.ToString)
        If b = True Then
            load1()
        Else
            MsgBox("Gagal hapus barang")
        End If
    End Sub

    Private Sub DGV3_DoubleClick(sender As Object, e As EventArgs) Handles DGV3.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Barang ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGV3.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsHasilPerbaikan
        Dim b As Boolean
        b = a.hapus1BRG2(nofak.Text, DGV3.Item("K_BRG2", row).Value.ToString)
        If b = True Then
            load3()
        Else
            MsgBox("Gagal hapus barang")
        End If
    End Sub
End Class