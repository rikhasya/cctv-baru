﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JadwalLaporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGV1 = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_COMPLAIN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_KUNJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HASIL_KUNJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGV2 = New System.Windows.Forms.DataGridView()
        Me.K_TEH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_TEH2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_TEH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbStatus = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CARI = New System.Windows.Forms.Button()
        Me.cbLgn = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGV3 = New System.Windows.Forms.DataGridView()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV1
        '
        Me.DGV1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff
        Me.DGV1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV1.BackgroundColor = System.Drawing.Color.White
        Me.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.NO_COMPLAIN, Me.TGL_FAK, Me.N_LGN, Me.K_LGN, Me.TGL_KUNJ, Me.HASIL_KUNJ, Me.KET, Me.KET2, Me.USERID, Me.DATESTAMP})
        Me.DGV1.Location = New System.Drawing.Point(27, 106)
        Me.DGV1.Name = "DGV1"
        Me.DGV1.RowHeadersVisible = False
        Me.DGV1.Size = New System.Drawing.Size(938, 285)
        Me.DGV1.TabIndex = 0
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No.  Visit"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.Width = 80
        '
        'NO_COMPLAIN
        '
        Me.NO_COMPLAIN.DataPropertyName = "NO_COMPLAIN"
        Me.NO_COMPLAIN.HeaderText = "No. Complain"
        Me.NO_COMPLAIN.Name = "NO_COMPLAIN"
        Me.NO_COMPLAIN.Width = 80
        '
        'TGL_FAK
        '
        Me.TGL_FAK.DataPropertyName = "TGL_FAK"
        Me.TGL_FAK.HeaderText = "Tanggal"
        Me.TGL_FAK.Name = "TGL_FAK"
        Me.TGL_FAK.Width = 70
        '
        'N_LGN
        '
        Me.N_LGN.DataPropertyName = "N_LGN"
        Me.N_LGN.HeaderText = "Nama Pelanggan"
        Me.N_LGN.Name = "N_LGN"
        '
        'K_LGN
        '
        Me.K_LGN.DataPropertyName = "K_LGN"
        Me.K_LGN.HeaderText = "Kode"
        Me.K_LGN.Name = "K_LGN"
        Me.K_LGN.Visible = False
        '
        'TGL_KUNJ
        '
        Me.TGL_KUNJ.DataPropertyName = "TGL_KUNJ"
        Me.TGL_KUNJ.HeaderText = "Tanggal Visit"
        Me.TGL_KUNJ.Name = "TGL_KUNJ"
        Me.TGL_KUNJ.Width = 70
        '
        'HASIL_KUNJ
        '
        Me.HASIL_KUNJ.DataPropertyName = "HASIL_KUNJ"
        Me.HASIL_KUNJ.HeaderText = "Status"
        Me.HASIL_KUNJ.Name = "HASIL_KUNJ"
        Me.HASIL_KUNJ.Width = 70
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        '
        'KET2
        '
        Me.KET2.DataPropertyName = "KET2"
        Me.KET2.HeaderText = "Keterangan Hasil"
        Me.KET2.Name = "KET2"
        Me.KET2.Width = 70
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.Width = 70
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "Datestamp"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DATESTAMP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DGV2
        '
        Me.DGV2.AllowUserToDeleteRows = False
        Me.DGV2.BackgroundColor = System.Drawing.Color.OldLace
        Me.DGV2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_TEH, Me.K_TEH2, Me.N_TEH, Me.NO_FAK2})
        Me.DGV2.Location = New System.Drawing.Point(835, 419)
        Me.DGV2.Name = "DGV2"
        Me.DGV2.RowHeadersVisible = False
        Me.DGV2.Size = New System.Drawing.Size(130, 214)
        Me.DGV2.TabIndex = 1
        '
        'K_TEH
        '
        Me.K_TEH.DataPropertyName = "K_TEH1"
        Me.K_TEH.HeaderText = "Kode Teknisi "
        Me.K_TEH.Name = "K_TEH"
        Me.K_TEH.Visible = False
        '
        'K_TEH2
        '
        Me.K_TEH2.DataPropertyName = "K_TEH"
        Me.K_TEH2.HeaderText = "Column1"
        Me.K_TEH2.Name = "K_TEH2"
        Me.K_TEH2.Visible = False
        '
        'N_TEH
        '
        Me.N_TEH.DataPropertyName = "N_TEH"
        Me.N_TEH.HeaderText = "Nama Tehnisi"
        Me.N_TEH.Name = "N_TEH"
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "NO FAK 2"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.cbStatus)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.CARI)
        Me.Panel1.Controls.Add(Me.cbLgn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.date2)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(0, 44)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1002, 56)
        Me.Panel1.TabIndex = 2
        '
        'cbStatus
        '
        Me.cbStatus.FormattingEnabled = True
        Me.cbStatus.Items.AddRange(New Object() {"ALL", "JADWAL", "GAGAL", "BERHASIL"})
        Me.cbStatus.Location = New System.Drawing.Point(617, 26)
        Me.cbStatus.Name = "cbStatus"
        Me.cbStatus.Size = New System.Drawing.Size(93, 21)
        Me.cbStatus.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(614, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Status"
        '
        'CARI
        '
        Me.CARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CARI.Location = New System.Drawing.Point(726, 24)
        Me.CARI.Name = "CARI"
        Me.CARI.Size = New System.Drawing.Size(75, 23)
        Me.CARI.TabIndex = 13
        Me.CARI.Text = "CARI"
        Me.CARI.UseVisualStyleBackColor = True
        '
        'cbLgn
        '
        Me.cbLgn.FormattingEnabled = True
        Me.cbLgn.Location = New System.Drawing.Point(386, 25)
        Me.cbLgn.Name = "cbLgn"
        Me.cbLgn.Size = New System.Drawing.Size(211, 21)
        Me.cbLgn.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(251, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(23, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "s/d"
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(276, 26)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(90, 20)
        Me.date2.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(383, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Pelanggan"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(159, 26)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(90, 20)
        Me.date1.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(216, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Tanggal Complain"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(344, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(337, 22)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "JADWAL KUNJUNGAN COMPLAIN"
        '
        'DGV3
        '
        Me.DGV3.AllowUserToDeleteRows = False
        Me.DGV3.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGV3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG, Me.NO_FAK3, Me.N_BRG, Me.QTY, Me.SAT})
        Me.DGV3.Location = New System.Drawing.Point(27, 419)
        Me.DGV3.Name = "DGV3"
        Me.DGV3.RowHeadersVisible = False
        Me.DGV3.Size = New System.Drawing.Size(459, 214)
        Me.DGV3.TabIndex = 21
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        '
        'NO_FAK3
        '
        Me.NO_FAK3.DataPropertyName = "NO_FAK"
        Me.NO_FAK3.HeaderText = "NO FAK 3"
        Me.NO_FAK3.Name = "NO_FAK3"
        Me.NO_FAK3.Visible = False
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.Width = 150
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.Width = 50
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.Width = 60
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(24, 394)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(194, 13)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "* F1 untuk Input Hasil Service Complain"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.Linen
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG1, Me.Column1})
        Me.DataGridView1.Location = New System.Drawing.Point(492, 419)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(337, 214)
        Me.DataGridView1.TabIndex = 24
        '
        'K_BRG1
        '
        Me.K_BRG1.HeaderText = "Kode Barang"
        Me.K_BRG1.Name = "K_BRG1"
        '
        'Column1
        '
        Me.Column1.HeaderText = "Nomor Serial"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 150
        '
        'JadwalLaporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.DGV3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.DGV2)
        Me.Controls.Add(Me.DGV1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "JadwalLaporan"
        Me.Text = "JadwalLaporan"
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV1 As System.Windows.Forms.DataGridView
    Friend WithEvents DGV2 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CARI As System.Windows.Forms.Button
    Friend WithEvents cbLgn As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DGV3 As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_COMPLAIN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_KUNJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HASIL_KUNJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_TEH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_TEH2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_TEH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
