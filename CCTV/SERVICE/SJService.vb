﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SJService

    Private Sub SuratJalan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SuratJalan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
     
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub noSO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles noSO.TextChanged
        Dim a As New clsCCTV.clsService
        Dim b As DataSet
        b = a.table(noSJ.Text)
        DGVsvc.DataSource = b.Tables("DETSERVICE")
    End Sub

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        'yg QTY = 0 tidak ikut masuk ke database
        'kalau ngisi qty dengan huruf ada protect
        Dim ask As DialogResult
        ask = MessageBox.Show("Input Print ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim b As New clsCCTV.clsSJ
        Dim t As String
        t = b.no()

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim a As New clsCCTV.clsSJ
        Dim p As Boolean
        p = a.input(t, Tanggal.Text, ksup.Text, Keterangan.Text, noSO.Text, table1)


        If p = True Then
            MsgBox("Berhasil Input Surat Jalan " & t)

            Dim otc As New clsCCTV.clsSJ
            tmpnofak = t
            tb = otc.table(tmpnofak)

            Dim PrintDoc As New PrintDocument

            ask = MessageBox.Show("Print Surat Jalan?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.Yes Then

                Dim jumlah As Integer = tb.Tables("DETSJ").Rows.Count - 1

                ReDim tmpKode(jumlah)
                ReDim tmpNama(jumlah)
                ReDim tmpQty(jumlah)
                ReDim tmpSat(jumlah)

                Dim ipo As Integer
                For ipo = 0 To jumlah

                    tmpKode(ipo) = tb.Tables("DETSJ").Rows(ipo)("K_BRG").ToString
                    tmpNama(ipo) = tb.Tables("DETSJ").Rows(ipo)("N_BRG").ToString
                    tmpQty(ipo) = tb.Tables("DETSJ").Rows(ipo)("QTY").ToString
                    tmpSat(ipo) = tb.Tables("DETSJ").Rows(ipo)("SAT").ToString
                Next

                arrke = 0
                printFont = New Font("Times New Roman", 10)

                AddHandler PrintDoc.PrintPage, AddressOf Me.SJ
                PrintDoc.Print()
            End If

            ask = MessageBox.Show("Print Alamat Kirim ?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.No Then
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
                Exit Sub
            End If
            pilihPrinter.Show()
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Input Surat Jalan")
        End If

    End Sub

    Public Sub SJ(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim count As Integer = 0
        Dim leftMargin As Single = ev.MarginBounds.Left
        Dim topMargin As Single = ev.MarginBounds.Top
        Dim line As String = Nothing

        'hitung line per halaman 
        'linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics)

        Dim ukuran As New Font("Times New Roman", 11, FontStyle.Regular)
        Dim ukJD As New Font("Times New Roman", 10, FontStyle.Bold)
        Dim ukMn As New Font("Times New Roman", 10, FontStyle.Regular)

        ev.Graphics.DrawString("SURAT JALAN", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 35, 15)
        ev.Graphics.DrawString("No : " & tmpnofak, New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 33, 35)
        ev.Graphics.DrawImage(My.Resources.logo_sm, 15, 65, 165, 50)

        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("TGL_FAK"), New Font(FontFamily.GenericSerif, 9, FontStyle.Regular), Brushes.Black, 70, 60)

        ev.Graphics.DrawString("Kepada : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 500, 35)
        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("N_LGN"), New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 560, 35)

        Dim displayRectangleJ As New Rectangle(New Point(560, 55), New Size(350, 45))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleJ)
        Dim formatK As New StringFormat(StringFormatFlags.NoClip)
        formatK.LineAlignment = StringAlignment.Near ' = top
        formatK.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("ALAMAT"), ukMn, Brushes.Black, RectangleF.op_Implicit(displayRectangleJ), formatK)

        ev.Graphics.DrawString("Dengan rincian barang sebagai berikut : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 30, 130)

        ev.Graphics.DrawLine(Pens.Black, 25, 170, 600, 170)
        ev.Graphics.DrawLine(Pens.Black, 25, 150, 600, 150)
        ev.Graphics.DrawLine(Pens.Black, 25, 150, 25, 400) 'vertikal 
        ev.Graphics.DrawLine(Pens.Black, 45, 150, 45, 400) 'ok
        ev.Graphics.DrawString("Kode Barang", ukJD, Brushes.Black, 60, 150)
        ev.Graphics.DrawLine(Pens.Black, 170, 150, 170, 400)
        ev.Graphics.DrawString("Nama Barang", ukJD, Brushes.Black, 230, 150)
        ev.Graphics.DrawLine(Pens.Black, 490, 150, 490, 400)
        ev.Graphics.DrawString("Qty", ukJD, Brushes.Black, 500, 150)
        ev.Graphics.DrawLine(Pens.Black, 540, 150, 540, 400)
        ev.Graphics.DrawString("Satuan", ukJD, Brushes.Black, 545, 150)
        ev.Graphics.DrawLine(Pens.Black, 600, 150, 600, 400)

        While count < 12
            If arrke > UBound(tmpKode) Then
                line = Nothing
            Else
                line = "aa"
                ev.Graphics.DrawString(arrke + 1, ukMn, Brushes.Black, 30, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpKode(arrke), ukMn, Brushes.Black, 52, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpNama(arrke), ukMn, Brushes.Black, 175, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpQty(arrke), ukMn, Brushes.Black, 535, 170 + ((count Mod 12) * 12), New StringFormat(LeftRightAlignment.Right))
                ev.Graphics.DrawString(tmpSat(arrke), ukMn, Brushes.Black, 550, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawLine(Pens.Black, 25, 400, 600, 400)
            End If

            If line Is Nothing Then
                Exit While
            End If
            count += 1
            arrke += 1
        End While

        Dim displayRectangleL As New Rectangle(New Point(610, 320), New Size(200, 70))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleL)
        Dim formatL As New StringFormat(StringFormatFlags.NoClip)
        formatL.LineAlignment = StringAlignment.Near ' = top
        formatL.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ev.Graphics.DrawString("Ket : ", ukMn, Brushes.Black, 610, 300)
        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("KET"), ukMn, Brushes.Black, RectangleF.op_Implicit(displayRectangleL), formatL)

        ev.Graphics.DrawString("Penerima", ukMn, Brushes.Black, 60, 445)
        ev.Graphics.DrawString("Pengirim", ukMn, Brushes.Black, 460, 445)
        ev.Graphics.DrawString("SEJAHTERA MANDIRI", ukMn, Brushes.Black, 420, 460)
        ev.Graphics.DrawString("(................................)", ukMn, Brushes.Black, 20, 505)
        ev.Graphics.DrawString("(................................)", ukMn, Brushes.Black, 430, 505)

        ev.Graphics.DrawString("***Perhatian !!", New Font("Courier", 7, FontStyle.Bold), Brushes.Black, 250, 480)
        ev.Graphics.DrawString("Telitilah barang-barang yang akan diterima.***", New Font("Courier", 7, FontStyle.Italic), Brushes.Black, 180, 500)

        If line = "aa" And arrke <= UBound(tmpKode) Then
            hal = hal + 1
            ev.HasMorePages = True
        Else
            ev.HasMorePages = False
        End If
    End Sub

    Public Sub alkirim(ByVal sender As Object, ByVal ev As PrintPageEventArgs)

        ev.Graphics.DrawString("PENGIRIM :", New Font(FontFamily.GenericSerif, 14, FontStyle.Bold), Brushes.Black, 100, 150)
        ev.Graphics.DrawImage(My.Resources.logo_sm, 100, 170, 165, 50)
        ev.Graphics.DrawString("LTC - Glodok Lt UG C1 No. 3", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 100, 215)
        ev.Graphics.DrawString("JAKARTA - KOTA", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 100, 235)
        ev.Graphics.DrawString("Telp 021 - 62320481", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 100, 255)
        ev.Graphics.DrawString("No. SJ " & tmpnofak, New Font(FontFamily.GenericSerif, 9, FontStyle.Regular), Brushes.Black, 100, 275)

        ev.Graphics.DrawString("KEPADA:", New Font(FontFamily.GenericSerif, 14, FontStyle.Bold), Brushes.Black, 500, 500)        'tb.Tables("DATSJ").Rows(0)("N_LGN")
        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("N_LGN"), New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 500, 520)
        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("TELP"), New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 500, 540)
        ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("ALAMAT"), New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 500, 560)

    End Sub
End Class