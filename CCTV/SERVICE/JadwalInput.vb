﻿Public Class JadwalInput

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles batal.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Input Jadwal ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Me.Close()
    End Sub

    Private Sub simpan_Click(sender As Object, e As EventArgs) Handles simpan.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Jadwal ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim b As New clsCCTV.clsKunjungan
        Dim t As String
        t = b.no("K")

        Dim table1 As New DataTable("DETJADKUNJUNGAN1")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_TEH")

        Dim table2 As New DataTable("DETJADKUNJUNGAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")

        Dim i As Integer
        For i = 0 To DGVtek.Rows.Count - 1
            table1.Rows.Add(t, DGVtek.Item("Kode", i).Value.ToString)
        Next

        For i = 0 To DGVbrg.Rows.Count - 1
            table2.Rows.Add(t, DGVbrg.Item("K_BRG", i).Value.ToString, DGVbrg.Item("N_BRG", i).Value.ToString, DGVbrg.Item("QTY1", i).Value.ToString, DGVbrg.Item("SAT", i).Value.ToString)
        Next

        Dim set1 As New DataSet
        set1.Tables.Add(table1)
        set1.Tables.Add(table2)

        Dim c As Boolean
        c = b.input(t, CDate(date1.Text), klgn.Text, ket.Text, noComp.Text, set1)
        If c = True Then
            MsgBox("Berhasil Input Jadwal")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Input Jadwal")
            Exit Sub
        End If

        gmn = MessageBox.Show("Print Jadwal  ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

    End Sub

    Private Sub JadwalInput_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.dari.Text = "JDWL"
            If BARIS = "Y" Then
                Cari_Brg.DET = "Y"
                Cari_Brg.nocm = noComp.Text
            End If
            Cari_Brg.Show()
        End If
    End Sub

    Private Sub JadwalInput_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Public BARIS As String = "N"

    Private Sub JadwalInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsTehnisi
        Dim b As DataTable
        b = a.table("%", "%")
        With cbTeknisi
            .DisplayMember = "N_TEH"
            .ValueMember = "K_TEH"
            .DataSource = b
        End With
    End Sub

    Private Sub INPUTtek_Click(sender As Object, e As EventArgs) Handles INPUTtek.Click
        With DGVtek
            .Rows.Add(1)
            .Rows(DGVtek.Rows.Count() - 1).Cells("Kode").Value = cbTeknisi.SelectedValue
            .Rows(DGVtek.Rows.Count() - 1).Cells("nama").Value = cbTeknisi.Text
        End With
    End Sub

    Private Sub INPUTbrg_Click(sender As Object, e As EventArgs) Handles INPUTbrg.Click
        Dim a As Boolean = True
        For b = 0 To DGVbrg.RowCount - 1
            Console.WriteLine(kbrg.Text & ":" & DGVbrg.Item("K_BRG", b).Value)
            If kbrg.Text = DGVbrg.Item("K_BRG", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        If qty.Text = "" Then
            MsgBox("CEK QTY")
            Exit Sub
        End If

        With DGVbrg
            .Rows.Add(1)
            .Rows(DGVbrg.Rows.Count() - 1).Cells("k_brg").Value = kbrg.Text
            .Rows(DGVbrg.Rows.Count() - 1).Cells("n_brg").Value = nbrg.Text
            .Rows(DGVbrg.Rows.Count() - 1).Cells("qty1").Value = CDbl(qty.Text)
            .Rows(DGVbrg.Rows.Count() - 1).Cells("sat").Value = satuan.Text
        End With

        kbrg.Clear()
        nbrg.Clear()
        qty.Text = 0
        satuan.Clear()
    End Sub

    Private Sub qty_Leave(sender As Object, e As EventArgs) Handles qty.Leave
        If IsNumeric(qty.Text) Then
            qty.Text = FormatNumber(qty.Text, 2)
        End If
    End Sub

    Private Sub DGVbrg_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVbrg.CellContentClick

    End Sub

    Private Sub DGVbrg_DoubleClick(sender As Object, e As EventArgs) Handles DGVbrg.DoubleClick
        Dim baris As Integer
        baris = DGVbrg.CurrentRow.Index.ToString

        kbrg.Text = DGVbrg.Item("K_BRG", baris).Value.ToString
        nbrg.Text = DGVbrg.Item("N_BRG", baris).Value.ToString
        qty.Text = DGVbrg.Item("QTY1", baris).Value.ToString
        satuan.Text = DGVbrg.Item("SAT", baris).Value.ToString

        DGVbrg.Rows.RemoveAt(baris)
    End Sub
End Class