﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SJService
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.telp = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.alamat = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.noSO = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Tanggal = New System.Windows.Forms.DateTimePicker()
        Me.noSJ = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.butINPUT = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Keterangan = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.nsup = New System.Windows.Forms.TextBox()
        Me.ksup = New System.Windows.Forms.Label()
        Me.DGVsvc = New System.Windows.Forms.DataGridView()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Wheat
        Me.Panel1.Controls.Add(Me.ksup)
        Me.Panel1.Controls.Add(Me.nsup)
        Me.Panel1.Controls.Add(Me.telp)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.alamat)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.noSO)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Tanggal)
        Me.Panel1.Controls.Add(Me.noSJ)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(12, 43)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(897, 102)
        Me.Panel1.TabIndex = 0
        '
        'telp
        '
        Me.telp.Enabled = False
        Me.telp.Location = New System.Drawing.Point(288, 40)
        Me.telp.Name = "telp"
        Me.telp.Size = New System.Drawing.Size(198, 20)
        Me.telp.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(208, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Telp"
        '
        'alamat
        '
        Me.alamat.Enabled = False
        Me.alamat.Location = New System.Drawing.Point(573, 11)
        Me.alamat.Multiline = True
        Me.alamat.Name = "alamat"
        Me.alamat.Size = New System.Drawing.Size(305, 78)
        Me.alamat.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(515, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Alamat"
        '
        'noSO
        '
        Me.noSO.Enabled = False
        Me.noSO.Location = New System.Drawing.Point(69, 40)
        Me.noSO.Name = "noSO"
        Me.noSO.Size = New System.Drawing.Size(116, 20)
        Me.noSO.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "No. Retur"
        '
        'Tanggal
        '
        Me.Tanggal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Tanggal.Location = New System.Drawing.Point(69, 68)
        Me.Tanggal.Name = "Tanggal"
        Me.Tanggal.Size = New System.Drawing.Size(92, 20)
        Me.Tanggal.TabIndex = 7
        '
        'noSJ
        '
        Me.noSJ.Enabled = False
        Me.noSJ.Location = New System.Drawing.Point(69, 11)
        Me.noSJ.Name = "noSJ"
        Me.noSJ.Size = New System.Drawing.Size(116, 20)
        Me.noSJ.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(208, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Supplier"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tanggal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. SJ "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(202, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Input Surat Jalan  Service"
        '
        'butINPUT
        '
        Me.butINPUT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINPUT.Location = New System.Drawing.Point(742, 412)
        Me.butINPUT.Name = "butINPUT"
        Me.butINPUT.Size = New System.Drawing.Size(75, 33)
        Me.butINPUT.TabIndex = 32
        Me.butINPUT.Text = "INPUT SJ"
        Me.butINPUT.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(834, 412)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 33)
        Me.Button2.TabIndex = 33
        Me.Button2.Text = "BATAL"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Keterangan
        '
        Me.Keterangan.Location = New System.Drawing.Point(81, 400)
        Me.Keterangan.Multiline = True
        Me.Keterangan.Name = "Keterangan"
        Me.Keterangan.Size = New System.Drawing.Size(381, 109)
        Me.Keterangan.TabIndex = 89
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(13, 400)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 90
        Me.Label15.Text = "Keterangan"
        '
        'nsup
        '
        Me.nsup.Enabled = False
        Me.nsup.Location = New System.Drawing.Point(288, 11)
        Me.nsup.Name = "nsup"
        Me.nsup.Size = New System.Drawing.Size(198, 20)
        Me.nsup.TabIndex = 14
        '
        'ksup
        '
        Me.ksup.AutoSize = True
        Me.ksup.Location = New System.Drawing.Point(285, 68)
        Me.ksup.Name = "ksup"
        Me.ksup.Size = New System.Drawing.Size(39, 13)
        Me.ksup.TabIndex = 15
        Me.ksup.Text = "Alamat"
        Me.ksup.Visible = False
        '
        'DGVsvc
        '
        Me.DGVsvc.AllowUserToAddRows = False
        Me.DGVsvc.AllowUserToDeleteRows = False
        Me.DGVsvc.BackgroundColor = System.Drawing.Color.BurlyWood
        Me.DGVsvc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsvc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.KET, Me.NO_FAK})
        Me.DGVsvc.Location = New System.Drawing.Point(12, 151)
        Me.DGVsvc.Name = "DGVsvc"
        Me.DGVsvc.ReadOnly = True
        Me.DGVsvc.RowHeadersVisible = False
        Me.DGVsvc.Size = New System.Drawing.Size(897, 243)
        Me.DGVsvc.TabIndex = 91
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Satuan"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "Nofak"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'SJService
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(921, 521)
        Me.Controls.Add(Me.DGVsvc)
        Me.Controls.Add(Me.Keterangan)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.butINPUT)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "SJService"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Surat Jalan"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents noSJ As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents butINPUT As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents noSO As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents telp As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents alamat As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents nsup As System.Windows.Forms.TextBox
    Friend WithEvents ksup As System.Windows.Forms.Label
    Friend WithEvents DGVsvc As System.Windows.Forms.DataGridView
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
