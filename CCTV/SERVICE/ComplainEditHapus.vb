﻿Public Class ComplainEdithapus

    Public garansi As Integer

    Private Sub ComplainInput_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.dari.Text = "ECMP"
            Cari_Brg.no.Text = noJual.Text
            Cari_Brg.Show()
        End If
    End Sub

    Private Sub ComplainInput_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub ComplainInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim tgl As System.DateTime = Tgl1.Text
        Tgl2.Text = tgl.AddDays(3)
        Console.WriteLine(Tgl2.Text)
        loadd()
    End Sub

    Sub loadd()
        Dim a As New clsCCTV.clsComplain
        Dim b As DataSet
        b = a.table(nofak.Text)
        DGVbrg.DataSource = b.Tables("DETCOMPLAIN")
        b.Dispose()
    End Sub

    Private Sub batal_Click(sender As Object, e As EventArgs) Handles batal.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Complain ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsComplain
        Dim b As Boolean
        b = a.delete(nofak.Text)

        If b = True Then
            MsgBox("Berhasil Hapus Complain No. " & nofak.Text)
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Hapus Complain")
            Exit Sub
        End If
    End Sub

    Private Sub simpan_Click(sender As Object, e As EventArgs) Handles simpan.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Perubahan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsComplain

        Dim b As Boolean
        b = a.update(nofak.Text, kett.Text, CDate(Tgl2.Text), noJual.Text, tisi.Text)
        If b = True Then
            MsgBox("Berhasil Simpan Perubahan")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Simpan Perubahan")
            Exit Sub
        End If

    End Sub

    Private Sub INPUTbrg_Click(sender As Object, e As EventArgs) Handles INPUTbrg.Click
        Dim a As Boolean = True
        For i = 0 To DGVbrg.RowCount - 1
            Console.WriteLine(kbrg.Text & ":" & DGVbrg.Item("K_BRG", i).Value)
            If kbrg.Text = DGVbrg.Item("K_BRG", i).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        If qty.Text = "" Then
            MsgBox("CEK QTY")
            Exit Sub
        End If

        Dim table2 As New DataTable("DETJADKUNJUNGAN2")
        table2.Columns.Add("NO_FAK")
        table2.Columns.Add("K_BRG")
        table2.Columns.Add("N_BRG")
        table2.Columns.Add("QTY")
        table2.Columns.Add("SAT")
        table2.Columns.Add("KET2")

        table2.Rows.Add(nofak.Text, kbrg.Text, nbrg.Text, CDbl(qty.Text), satuan.Text, ketRusak.Text)

        Dim c As New clsCCTV.clsComplain
        Dim b As Boolean
        b = c.input1brg(nofak.Text, table2)

        If b = True Then
            loadd()
            kbrg.Clear()
            nbrg.Clear()
            qty.Text = 0
            satuan.Clear()

        Else
            MsgBox("Gagal input barang baru")
            Exit Sub
        End If
        Panel1.Enabled = False
    End Sub

    Private Sub DGVbrg_DoubleClick(sender As Object, e As EventArgs) Handles DGVbrg.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Barang ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGVbrg.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsComplain
        Dim b As Boolean
        b = a.delete(nofak.Text, DGVbrg.Item("K_BRG", row).Value.ToString)

        If b = True Then
            loadd()
        Else
            MsgBox("Gagal hapus barang")
            Exit Sub
        End If

    End Sub

    Private Sub DGVbrg_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVbrg.CellContentClick

    End Sub
End Class