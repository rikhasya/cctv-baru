﻿Imports System.Drawing.Printing
Imports System.IO

Public Class ServiceLaporan

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpKet()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub ServiceLaporan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub ServiceLaporan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_SUP") = "ALL"
        row("K_SUP") = "0"
        b.Rows.Add(row)
        With CBsup
            .DisplayMember = "N_SUP"
            .ValueMember = "K_SUP"
            .DataSource = b
        End With
        CBsup.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsService

        Dim sup As String
        If CBsup.Text = "ALL" Then
            sup = "%"
        Else
            sup = CBsup.SelectedValue
        End If

        Dim tb As DataSet
        tb = a.table(date1.Text, date2.Text, sup)

        Dim i As Integer
        For i = 0 To tb.Tables("DATSERVICE").Rows.Count - 1
            Console.WriteLine("dat" & tb.Tables("DATSERVICE").Rows(i)(0).ToString)
        Next

        For i = 0 To tb.Tables("DETSERVICE").Rows.Count - 1
            Console.WriteLine("det" & tb.Tables("DETSERVICE").Rows(i)(0).ToString)
        Next

        Dim parentColumn As DataColumn = tb.Tables("DATSERVICE").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETSERVICE").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETSERVICE", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATSERVICE"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETSERVICE"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVheader.Focus()
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SERVICE_EDITHAPUS")
        If b = True Then

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ServiceEditHapus
            frm = New ServiceEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Service"
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.noFak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.date1.Text = FormatDateTime(DGVheader.Item("TANGGAL", row).Value.ToString, DateFormat.ShortDate)
            frm.supplier.Text = DGVheader.Item("N_SUP", row).Value.ToString
            frm.kSup.Text = DGVheader.Item("K_SUP", row).Value.ToString
            frm.noRetur.Text = DGVheader.Item("NO_RETUR", row).Value.ToString
            frm.ket2.Text = DGVheader.Item("KET", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        If e.KeyCode = Keys.F2 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("SVCHASIL_INPUT")
            If b = True Then
                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As ServiceInputHasil
                frm = New ServiceInputHasil
                frm.MdiParent = frmMenu
                frm.Text = "Input Hasil Service"
                Dim row As Integer
                row = DGVheader.CurrentRow.Index.ToString
                'frm.noService.Text = DGVheader.Item("NO_FAK", row).Value.ToString
                'frm.supplier.Text = DGVheader.Item("N_SUP", row).Value.ToString
                'frm.kSup.Text = DGVheader.Item("K_SUP", row).Value.ToString

                frm.Show()
                frm.Dock = DockStyle.Fill
                Me.Close()
            Else
                MsgBox("anda tdk di otorisasi")
            End If

        End If
    End Sub

    Sub PRINT()
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Ulang Faktur Service?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsService
            ServiceInput.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With ServiceInput
                .tmpnofak = .tb.Tables("DATSERVICE").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = ServiceInput.tb.Tables("DETSERVICE").Rows.Count - 1

            ReDim ServiceInput.tmpKode(jumlah)
            ReDim ServiceInput.tmpNama(jumlah)
            ReDim ServiceInput.tmpQty(jumlah)
            ReDim ServiceInput.tmpSat(jumlah)
            ReDim ServiceInput.tmpKet(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With ServiceInput
                    .tmpKode(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("SAT").ToString
                    .tmpKet(ipo) = .tb.Tables("DETSERVICE").Rows(ipo)("KET").ToString
                End With
            Next
            'With ServiceInput
            '    arrke = 0
            '    printFont = New Font("Times New Roman", 10)
            'End With

            ServiceInput.arrke = 0

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf ServiceInput.faktur
            PrintDoc.Print()
            Exit Sub
        End If
    End Sub

    Private Sub CBsup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBsup.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

    Private Sub DGVheader_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVheader.CellContentClick

    End Sub
End Class