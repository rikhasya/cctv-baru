﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ComplainInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CBlgn = New System.Windows.Forms.ComboBox()
        Me.Tgl2 = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nlgn = New System.Windows.Forms.Label()
        Me.Tgl1 = New System.Windows.Forms.DateTimePicker()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.serial2 = New System.Windows.Forms.TextBox()
        Me.JTgaransi = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.noJual = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dateFak = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tisi = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.kett = New System.Windows.Forms.TextBox()
        Me.simpan = New System.Windows.Forms.Button()
        Me.batal = New System.Windows.Forms.Button()
        Me.DGVbrg = New System.Windows.Forms.DataGridView()
        Me.k_brg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.n_brg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.qty1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ketRusak = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.satuan = New System.Windows.Forms.TextBox()
        Me.nbrg = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.INPUTbrg = New System.Windows.Forms.Button()
        Me.qty = New System.Windows.Forms.TextBox()
        Me.kbrg = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVbrg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(417, 26)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(182, 22)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "INPUT COMPLAIN"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.CBlgn)
        Me.Panel1.Controls.Add(Me.Tgl2)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.nlgn)
        Me.Panel1.Controls.Add(Me.Tgl1)
        Me.Panel1.Location = New System.Drawing.Point(26, 60)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(331, 78)
        Me.Panel1.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(84, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(10, 13)
        Me.Label10.TabIndex = 101
        Me.Label10.Text = ":"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(46, 13)
        Me.Label14.TabIndex = 100
        Me.Label14.Text = "Tanggal"
        '
        'CBlgn
        '
        Me.CBlgn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBlgn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBlgn.FormattingEnabled = True
        Me.CBlgn.Location = New System.Drawing.Point(105, 29)
        Me.CBlgn.Name = "CBlgn"
        Me.CBlgn.Size = New System.Drawing.Size(187, 21)
        Me.CBlgn.TabIndex = 0
        '
        'Tgl2
        '
        Me.Tgl2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Tgl2.Location = New System.Drawing.Point(105, 54)
        Me.Tgl2.Name = "Tgl2"
        Me.Tgl2.Size = New System.Drawing.Size(97, 20)
        Me.Tgl2.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(84, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(10, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = ":"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(84, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(10, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = ":"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "JT Complain"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Pelanggan"
        '
        'nlgn
        '
        Me.nlgn.AutoSize = True
        Me.nlgn.Location = New System.Drawing.Point(105, 32)
        Me.nlgn.Name = "nlgn"
        Me.nlgn.Size = New System.Drawing.Size(10, 13)
        Me.nlgn.TabIndex = 99
        Me.nlgn.Text = "-"
        Me.nlgn.Visible = False
        '
        'Tgl1
        '
        Me.Tgl1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Tgl1.Location = New System.Drawing.Point(105, 4)
        Me.Tgl1.Name = "Tgl1"
        Me.Tgl1.Size = New System.Drawing.Size(97, 20)
        Me.Tgl1.TabIndex = 20
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(154, 10)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(33, 13)
        Me.Label21.TabIndex = 101
        Me.Label21.Text = "Serial"
        '
        'serial2
        '
        Me.serial2.BackColor = System.Drawing.Color.White
        Me.serial2.Location = New System.Drawing.Point(157, 25)
        Me.serial2.Name = "serial2"
        Me.serial2.Size = New System.Drawing.Size(203, 20)
        Me.serial2.TabIndex = 100
        '
        'JTgaransi
        '
        Me.JTgaransi.Enabled = False
        Me.JTgaransi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.JTgaransi.Location = New System.Drawing.Point(493, 50)
        Me.JTgaransi.Name = "JTgaransi"
        Me.JTgaransi.Size = New System.Drawing.Size(87, 20)
        Me.JTgaransi.TabIndex = 2
        Me.JTgaransi.Value = New Date(2000, 1, 1, 8, 15, 0, 0)
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(412, 54)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 13)
        Me.Label12.TabIndex = 26
        Me.Label12.Text = "Exp Garansi"
        '
        'noJual
        '
        Me.noJual.BackColor = System.Drawing.Color.White
        Me.noJual.Location = New System.Drawing.Point(366, 25)
        Me.noJual.Name = "noJual"
        Me.noJual.Size = New System.Drawing.Size(107, 20)
        Me.noJual.TabIndex = 25
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(366, 10)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "No. Penjualan"
        '
        'dateFak
        '
        Me.dateFak.Enabled = False
        Me.dateFak.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateFak.Location = New System.Drawing.Point(493, 24)
        Me.dateFak.Name = "dateFak"
        Me.dateFak.Size = New System.Drawing.Size(87, 20)
        Me.dateFak.TabIndex = 34
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(477, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(10, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = ":"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(490, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tanggal Jual"
        '
        'tisi
        '
        Me.tisi.AcceptsReturn = True
        Me.tisi.BackColor = System.Drawing.Color.Khaki
        Me.tisi.Location = New System.Drawing.Point(363, 60)
        Me.tisi.Multiline = True
        Me.tisi.Name = "tisi"
        Me.tisi.Size = New System.Drawing.Size(608, 78)
        Me.tisi.TabIndex = 1
        Me.tisi.Text = "*isi komplen*"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(418, 615)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Keterangan :"
        '
        'kett
        '
        Me.kett.Location = New System.Drawing.Point(492, 612)
        Me.kett.Multiline = True
        Me.kett.Name = "kett"
        Me.kett.Size = New System.Drawing.Size(299, 44)
        Me.kett.TabIndex = 3
        '
        'simpan
        '
        Me.simpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.simpan.Location = New System.Drawing.Point(815, 621)
        Me.simpan.Name = "simpan"
        Me.simpan.Size = New System.Drawing.Size(75, 26)
        Me.simpan.TabIndex = 4
        Me.simpan.Text = "SIMPAN"
        Me.simpan.UseVisualStyleBackColor = True
        '
        'batal
        '
        Me.batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.batal.Location = New System.Drawing.Point(896, 621)
        Me.batal.Name = "batal"
        Me.batal.Size = New System.Drawing.Size(75, 26)
        Me.batal.TabIndex = 5
        Me.batal.Text = "BATAL"
        Me.batal.UseVisualStyleBackColor = True
        '
        'DGVbrg
        '
        Me.DGVbrg.AllowUserToDeleteRows = False
        Me.DGVbrg.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVbrg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVbrg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.k_brg, Me.n_brg, Me.qty1, Me.SAT, Me.KET2})
        Me.DGVbrg.Location = New System.Drawing.Point(23, 207)
        Me.DGVbrg.Name = "DGVbrg"
        Me.DGVbrg.RowHeadersVisible = False
        Me.DGVbrg.Size = New System.Drawing.Size(951, 166)
        Me.DGVbrg.TabIndex = 32
        '
        'k_brg
        '
        Me.k_brg.HeaderText = "Kode "
        Me.k_brg.Name = "k_brg"
        '
        'n_brg
        '
        Me.n_brg.HeaderText = "Nama"
        Me.n_brg.Name = "n_brg"
        '
        'qty1
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.qty1.DefaultCellStyle = DataGridViewCellStyle3
        Me.qty1.HeaderText = "Qty"
        Me.qty1.Name = "qty1"
        '
        'SAT
        '
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        '
        'KET2
        '
        Me.KET2.HeaderText = "Keterangan Rusak"
        Me.KET2.Name = "KET2"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Bisque
        Me.GroupBox1.Controls.Add(Me.ketRusak)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.satuan)
        Me.GroupBox1.Controls.Add(Me.nbrg)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.INPUTbrg)
        Me.GroupBox1.Controls.Add(Me.qty)
        Me.GroupBox1.Controls.Add(Me.kbrg)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 144)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(951, 79)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "BARANG RUSAK"
        '
        'ketRusak
        '
        Me.ketRusak.AcceptsReturn = True
        Me.ketRusak.Location = New System.Drawing.Point(553, 16)
        Me.ketRusak.MaxLength = 255
        Me.ketRusak.Multiline = True
        Me.ketRusak.Name = "ketRusak"
        Me.ketRusak.Size = New System.Drawing.Size(304, 39)
        Me.ketRusak.TabIndex = 2
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(490, 35)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(57, 13)
        Me.Label20.TabIndex = 39
        Me.Label20.Text = "Ket Rusak"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(427, 19)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(23, 13)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "Sat"
        '
        'satuan
        '
        Me.satuan.Enabled = False
        Me.satuan.Location = New System.Drawing.Point(426, 35)
        Me.satuan.Name = "satuan"
        Me.satuan.Size = New System.Drawing.Size(58, 20)
        Me.satuan.TabIndex = 37
        '
        'nbrg
        '
        Me.nbrg.BackColor = System.Drawing.Color.White
        Me.nbrg.Enabled = False
        Me.nbrg.Location = New System.Drawing.Point(157, 35)
        Me.nbrg.Name = "nbrg"
        Me.nbrg.Size = New System.Drawing.Size(198, 20)
        Me.nbrg.TabIndex = 35
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(154, 19)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(72, 13)
        Me.Label16.TabIndex = 36
        Me.Label16.Text = "Nama Barang"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(359, 19)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(23, 13)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "Qty"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(10, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(88, 13)
        Me.Label18.TabIndex = 25
        Me.Label18.Text = "Kode Barang *F1"
        '
        'INPUTbrg
        '
        Me.INPUTbrg.Location = New System.Drawing.Point(863, 30)
        Me.INPUTbrg.Name = "INPUTbrg"
        Me.INPUTbrg.Size = New System.Drawing.Size(75, 23)
        Me.INPUTbrg.TabIndex = 3
        Me.INPUTbrg.Text = "INPUT"
        Me.INPUTbrg.UseVisualStyleBackColor = True
        '
        'qty
        '
        Me.qty.Location = New System.Drawing.Point(362, 35)
        Me.qty.Name = "qty"
        Me.qty.Size = New System.Drawing.Size(58, 20)
        Me.qty.TabIndex = 1
        '
        'kbrg
        '
        Me.kbrg.Location = New System.Drawing.Point(13, 35)
        Me.kbrg.Name = "kbrg"
        Me.kbrg.Size = New System.Drawing.Size(138, 20)
        Me.kbrg.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Bisque
        Me.GroupBox2.Controls.Add(Me.JTgaransi)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.dateFak)
        Me.GroupBox2.Controls.Add(Me.serial2)
        Me.GroupBox2.Controls.Add(Me.TextBox1)
        Me.GroupBox2.Controls.Add(Me.noJual)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(23, 377)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(951, 75)
        Me.GroupBox2.TabIndex = 33
        Me.GroupBox2.TabStop = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(873, 44)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 23)
        Me.Button1.TabIndex = 40
        Me.Button1.Text = "INPUT"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.AcceptsReturn = True
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.Location = New System.Drawing.Point(600, 26)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(267, 41)
        Me.TextBox2.TabIndex = 104
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(597, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 103
        Me.Label7.Text = "Ket Rusak"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(10, 9)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 13)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "Kode Barang "
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(13, 25)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(138, 20)
        Me.TextBox1.TabIndex = 26
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column1, Me.DataGridViewTextBoxColumn5})
        Me.DataGridView1.Location = New System.Drawing.Point(23, 453)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(951, 153)
        Me.DataGridView1.TabIndex = 34
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Kode "
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nomor Serial"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'Column1
        '
        Me.Column1.HeaderText = "No Penjualan"
        Me.Column1.Name = "Column1"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Keterangan Rusak"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 200
        '
        'ComplainInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.DGVbrg)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.batal)
        Me.Controls.Add(Me.simpan)
        Me.Controls.Add(Me.kett)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.tisi)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "ComplainInput"
        Me.Text = "ComplainInput"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVbrg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Tgl2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Tgl1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CBlgn As System.Windows.Forms.ComboBox
    Friend WithEvents tisi As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents kett As System.Windows.Forms.TextBox
    Friend WithEvents simpan As System.Windows.Forms.Button
    Friend WithEvents batal As System.Windows.Forms.Button
    Friend WithEvents JTgaransi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents noJual As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DGVbrg As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents satuan As System.Windows.Forms.TextBox
    Friend WithEvents nbrg As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents INPUTbrg As System.Windows.Forms.Button
    Friend WithEvents qty As System.Windows.Forms.TextBox
    Friend WithEvents kbrg As System.Windows.Forms.TextBox
    Friend WithEvents ketRusak As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents k_brg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents n_brg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents qty1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dateFak As System.Windows.Forms.DateTimePicker
    Friend WithEvents nlgn As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents serial2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
