﻿Imports System.Drawing.Printing
Imports System.IO

Public Class ServiceInput

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpKet()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub ServiceInput_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        'If e.KeyCode = Keys.F1 Then
        '    Cari_Brg.Show()
        '    Cari_Brg.dari.Text = "SVC"
        '    Cari_Brg.no.Text = noRetur.Text
        'End If
    End Sub

    Private Sub ServiceInput_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub ServiceInput_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        b = a.table("%", "%")
        With CBsup
            .DisplayMember = "N_SUP"
            .ValueMember = "K_SUP"
            .DataSource = b
        End With

        Dim c As New clsCCTV.clsKunjungan
        Dim d As DataSet
        d = c.table(noRetur.Text)
        DGV3.DataSource = d.Tables("DETJADKUNJUNGAN2")
    End Sub

    Private Sub butInput_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butInput.Click
        If kode1.Text = "" Then
            butSIM.Focus()
            Exit Sub
        End If
        butSIM.Enabled = True
        'If hrg.Text = Harga.Text Or Harga.Text > hrg.Text Then

        kode.Enabled = False
        frmMenu.MenuStrip1.Enabled = False
        Dim a As Boolean = True
        Dim b As Integer
        For b = 0 To DGVsvc.RowCount - 1
            Console.WriteLine(kode1.Text & ":" & DGVsvc.Item("K_BRG", b).Value)
            If kode1.Text = DGVsvc.Item("K_BRG", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        With DGVsvc
            .Rows.Add(1)
            .Rows(DGVsvc.Rows.Count() - 1).Cells("K_BRG").Value = kode1.Text
            .Rows(DGVsvc.Rows.Count() - 1).Cells("N_BRG").Value = nama.Text
            .Rows(DGVsvc.Rows.Count() - 1).Cells("QTY").Value = qty1.Text
            .Rows(DGVsvc.Rows.Count() - 1).Cells("SAT").Value = satuan.Text
            .Rows(DGVsvc.Rows.Count() - 1).Cells("KET").Value = Ket1.Text
            butSIM.Visible = True
            kode.Focus()
        End With

        kode1.Clear()
        nama.Clear()
        qty1.Clear()
        satuan.Clear()
        Ket1.Clear()
        qty1.Focus()

    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If DGVsvc.RowCount.ToString = 0 Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsService
        Dim t As String
        t = a.no()

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("KET")

        Dim i As Integer
        For i = 0 To DGVsvc.Rows.Count - 1
            table1.Rows.Add(t, DGVsvc.Item("K_BRG", i).Value.ToString, DGVsvc.Item("N_BRG", i).Value.ToString, DGVsvc.Item("QTY", i).Value.ToString, DGVsvc.Item("SAT", i).Value.ToString, DGVsvc.Item("KET", i).Value.ToString)
        Next

        Dim b As Boolean
        b = a.input(t, date1.Text, CBsup.SelectedValue, ket2.Text, noRetur.Text, table1)

        If b = True Then
            MsgBox("Berhasil input service")
            Dim ask As DialogResult
            ask = MessageBox.Show("Print Faktur?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.No Then
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
                Exit Sub
            End If

            Dim otc As New clsCCTV.clsService
            tmpnofak = t
            tb = otc.table(tmpnofak)

            Dim jumlah As Integer = tb.Tables("DETSERVICE").Rows.Count - 1

            ReDim tmpKode(jumlah)
            ReDim tmpNama(jumlah)
            ReDim tmpQty(jumlah)
            ReDim tmpSat(jumlah)
            ReDim tmpKet(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah

                tmpKode(ipo) = tb.Tables("DETSERVICE").Rows(ipo)("K_BRG").ToString
                tmpNama(ipo) = tb.Tables("DETSERVICE").Rows(ipo)("N_BRG").ToString
                tmpQty(ipo) = tb.Tables("DETSERVICE").Rows(ipo)("QTY").ToString
                tmpSat(ipo) = tb.Tables("DETSERVICE").Rows(ipo)("SAT").ToString
                tmpKet(ipo) = tb.Tables("DETSERVICE").Rows(ipo)("KET").ToString
            Next

            arrke = 0
            printFont = New Font("Times New Roman", 10)

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Me.faktur
            PrintDoc.Print()
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal input")
        End If
    End Sub
    Private Sub butBAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBAT.Click
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub DGVsvc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVsvc.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVsvc.CurrentRow.Index.ToString

        DGVsvc.Rows.RemoveAt(baris)
    End Sub

    Public Sub faktur(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim count As Integer = 0
        Dim leftMargin As Single = ev.MarginBounds.Left
        Dim topMargin As Single = ev.MarginBounds.Top
        Dim line As String = Nothing

        'hitung line per halaman 
        'linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics)

        Dim ukuran As New Font("Times New Roman", 11, FontStyle.Regular)
        Dim ukJD As New Font("Times New Roman", 10, FontStyle.Bold)
        Dim ukJ As New Font("Times New Roman", 10, FontStyle.Regular)
        Dim ukMn As New Font("Times New Roman", 9.5, FontStyle.Regular)
        Dim ukT As New Font("Times New Roman", 9, FontStyle.Bold)

        ev.Graphics.DrawString("FAKTUR SERVICE", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 350, 15)
        ev.Graphics.DrawString(tmpnofak, New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 385, 35)

        ev.Graphics.DrawImage(My.Resources.logo_sm, 30, 20, 190, 100)

        'ev.Graphics.DrawString("SEJAHTERA SENTOSA", New Font(FontFamily.GenericSerif, 9, FontStyle.Bold), Brushes.Black, 60, 85)

        ev.Graphics.DrawString(tb.Tables("DATSERVICE").Rows(0)("TGL_FAK"), New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 405, 55)

        ev.Graphics.DrawString("Kepada : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 620, 40)
        'ev.Graphics.DrawString(tb.Tables("DATSERVICE").Rows(0)("N_SUP"), New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 620, 60)
        ev.Graphics.DrawString("Supplier", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 620, 60)

        ''Dim displayRectangleK As New Rectangle(New Point(620, 80), New Size(200, 200))
        ''ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleK)
        ''Dim formatK As New StringFormat(StringFormatFlags.NoClip)
        ''formatK.LineAlignment = StringAlignment.Near ' = top
        ''formatK.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ''ev.Graphics.DrawString(tb.Tables("DATSERVICE").Rows(0)("ALAMAT"), ukJ, Brushes.Black, RectangleF.op_Implicit(displayRectangleK), formatK)

        ev.Graphics.DrawString("Dengan rincian barang sebagai berikut : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 30, 130)

        ev.Graphics.DrawLine(Pens.Black, 25, 150, 820, 150)
        ev.Graphics.DrawLine(Pens.Black, 25, 150, 25, 350) 'vertikal 
        ev.Graphics.DrawLine(Pens.Black, 45, 150, 45, 350) 'ok
        ev.Graphics.DrawString("Kode Barang", ukJD, Brushes.Black, 60, 150)
        ev.Graphics.DrawLine(Pens.Black, 155, 150, 155, 350)
        ev.Graphics.DrawString("Nama Barang", ukJD, Brushes.Black, 200, 150)
        ev.Graphics.DrawLine(Pens.Black, 400, 150, 400, 350)
        ev.Graphics.DrawString("Qty", ukJD, Brushes.Black, 405, 150)
        ev.Graphics.DrawLine(Pens.Black, 440, 150, 440, 350)
        ev.Graphics.DrawString("Sat", ukJD, Brushes.Black, 445, 150)
        ev.Graphics.DrawLine(Pens.Black, 480, 150, 480, 350)
        ev.Graphics.DrawString("Keterangan", ukJD, Brushes.Black, 500, 150)
        ev.Graphics.DrawLine(Pens.Black, 25, 170, 820, 170) 'baris ke2 atas
        ev.Graphics.DrawLine(Pens.Black, 820, 150, 820, 350)

        While count < 12
            If arrke > UBound(tmpKode) Then
                line = Nothing
            Else
                line = "aa"

                ev.Graphics.DrawString(arrke + 1, ukMn, Brushes.Black, 30, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpKode(arrke), ukMn, Brushes.Black, 55, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpNama(arrke), ukMn, Brushes.Black, 160, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpQty(arrke), ukMn, Brushes.Black, 405, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpSat(arrke), ukMn, Brushes.Black, 445, 170 + ((count Mod 12) * 12))
                'ev.Graphics.DrawString(FormatNumber(tmpHrg(arrke), 2), ukMn, Brushes.Black, 525, 170 + ((count Mod 12) * 12), New StringFormat(LeftRightAlignment.Right))
                ev.Graphics.DrawString(tmpKet(arrke), ukMn, Brushes.Black, 490, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawLine(Pens.Black, 25, 350, 820, 350)

            End If

            If line Is Nothing Then
                Exit While
            End If
            count += 1
            arrke += 1
        End While

        'ev.Graphics.DrawString(FormatNumber(tb.Tables("DATSERVICE").Rows(0)("PPN"), 2), ukT, Brushes.Black, 815, 406, New StringFormat(LeftRightAlignment.Right))

        Dim displayRectangleL As New Rectangle(New Point(70, 355), New Size(400, 150))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleL)
        Dim formatL As New StringFormat(StringFormatFlags.NoClip)
        formatL.LineAlignment = StringAlignment.Near ' = top
        formatL.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ev.Graphics.DrawString("Ket : ", ukJ, Brushes.Black, 30, 355)
        ev.Graphics.DrawString(tb.Tables("DATSERVICE").Rows(0)("KET"), ukMn, Brushes.Black, RectangleF.op_Implicit(displayRectangleL), formatL)

        ev.Graphics.DrawString("Penerima", ukJ, Brushes.Black, 195, 420)
        ev.Graphics.DrawString("Pengirim", ukJ, Brushes.Black, 600, 420)
        ' ev.Graphics.DrawString("SEJAHTERA MANDIRI", ukJ, Brushes.Black, 460, 458)
        ev.Graphics.DrawString("(.....................................)", ukMn, Brushes.Black, 155, 475)
        ev.Graphics.DrawString("(.....................................)", ukMn, Brushes.Black, 565, 475)

        ev.Graphics.DrawString("***Perhatian !!", New Font("Courier", 7, FontStyle.Bold), Brushes.Black, 400, 450)
        ev.Graphics.DrawString("Telitilah barang-barang yang akan diterima.***", New Font("Courier", 7, FontStyle.Italic), Brushes.Black, 330, 470)

        If line = "aa" And arrke <= UBound(tmpKode) Then
            hal = hal + 1
            ev.HasMorePages = True
        Else
            ev.HasMorePages = False
            Exit Sub
        End If
    End Sub

    Private Sub DGV3_DoubleClick(sender As Object, e As EventArgs) Handles DGV3.DoubleClick
        Dim row As Integer
        row = DGV3.CurrentRow.Index.ToString

        kode1.Text = DGV3.Item("K_BRG3", row).Value.ToString
        nama.Text = DGV3.Item("N_BRG3", row).Value.ToString
        satuan.Text = DGV3.Item("SAT3", row).Value.ToString
        qty1.Text = 0
    End Sub

End Class