﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PerbaikanHasilEditHapus
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.nofak = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tsat = New System.Windows.Forms.TextBox()
        Me.GAGAL = New System.Windows.Forms.Button()
        Me.nbrg = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BERHASIL = New System.Windows.Forms.Button()
        Me.tqty = New System.Windows.Forms.TextBox()
        Me.kbrg = New System.Windows.Forms.TextBox()
        Me.DGV1 = New System.Windows.Forms.DataGridView()
        Me.DGV3 = New System.Windows.Forms.DataGridView()
        Me.HAPUS = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.DGV2 = New System.Windows.Forms.DataGridView()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ket = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.teknisi = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ktek = New System.Windows.Forms.Label()
        Me.K_BRG2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(351, 25)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(308, 22)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "EDIT HAPUS HASIL PERBAIKAN" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'date1
        '
        Me.date1.Enabled = False
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(320, 100)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(92, 20)
        Me.date1.TabIndex = 56
        '
        'nofak
        '
        Me.nofak.Enabled = False
        Me.nofak.Location = New System.Drawing.Point(320, 69)
        Me.nofak.Name = "nofak"
        Me.nofak.Size = New System.Drawing.Size(106, 20)
        Me.nofak.TabIndex = 54
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(304, 103)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(10, 13)
        Me.Label13.TabIndex = 52
        Me.Label13.Text = ":"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(304, 72)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(10, 13)
        Me.Label12.TabIndex = 51
        Me.Label12.Text = ":"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(193, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "No. Hasil"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(193, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Tanggal"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(228, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 13)
        Me.Label6.TabIndex = 61
        Me.Label6.Text = "Pilih Barang "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.tsat)
        Me.GroupBox1.Controls.Add(Me.GAGAL)
        Me.GroupBox1.Controls.Add(Me.nbrg)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.BERHASIL)
        Me.GroupBox1.Controls.Add(Me.tqty)
        Me.GroupBox1.Controls.Add(Me.kbrg)
        Me.GroupBox1.Location = New System.Drawing.Point(158, 330)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(683, 108)
        Me.GroupBox1.TabIndex = 62
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(534, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Sat"
        '
        'tsat
        '
        Me.tsat.Enabled = False
        Me.tsat.Location = New System.Drawing.Point(537, 36)
        Me.tsat.Name = "tsat"
        Me.tsat.Size = New System.Drawing.Size(58, 20)
        Me.tsat.TabIndex = 38
        '
        'GAGAL
        '
        Me.GAGAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GAGAL.Location = New System.Drawing.Point(349, 69)
        Me.GAGAL.Name = "GAGAL"
        Me.GAGAL.Size = New System.Drawing.Size(87, 32)
        Me.GAGAL.TabIndex = 37
        Me.GAGAL.Text = "GAGAL"
        Me.GAGAL.UseVisualStyleBackColor = True
        '
        'nbrg
        '
        Me.nbrg.Enabled = False
        Me.nbrg.Location = New System.Drawing.Point(226, 37)
        Me.nbrg.Name = "nbrg"
        Me.nbrg.Size = New System.Drawing.Size(223, 20)
        Me.nbrg.TabIndex = 35
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(223, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 13)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "Nama Barang"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(461, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(23, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Qty"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(57, 21)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(69, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Kode Barang"
        '
        'BERHASIL
        '
        Me.BERHASIL.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BERHASIL.Location = New System.Drawing.Point(243, 69)
        Me.BERHASIL.Name = "BERHASIL"
        Me.BERHASIL.Size = New System.Drawing.Size(87, 32)
        Me.BERHASIL.TabIndex = 23
        Me.BERHASIL.Text = "BERHASIL"
        Me.BERHASIL.UseVisualStyleBackColor = True
        '
        'tqty
        '
        Me.tqty.Location = New System.Drawing.Point(464, 36)
        Me.tqty.Name = "tqty"
        Me.tqty.Size = New System.Drawing.Size(58, 20)
        Me.tqty.TabIndex = 22
        '
        'kbrg
        '
        Me.kbrg.Enabled = False
        Me.kbrg.Location = New System.Drawing.Point(60, 37)
        Me.kbrg.Name = "kbrg"
        Me.kbrg.Size = New System.Drawing.Size(152, 20)
        Me.kbrg.TabIndex = 0
        '
        'DGV1
        '
        Me.DGV1.AllowUserToAddRows = False
        Me.DGV1.AllowUserToDeleteRows = False
        Me.DGV1.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG1, Me.NO_FAK1, Me.N_BRG1, Me.QTY1, Me.SAT1})
        Me.DGV1.Location = New System.Drawing.Point(65, 444)
        Me.DGV1.Name = "DGV1"
        Me.DGV1.ReadOnly = True
        Me.DGV1.RowHeadersVisible = False
        Me.DGV1.Size = New System.Drawing.Size(428, 134)
        Me.DGV1.TabIndex = 63
        '
        'DGV3
        '
        Me.DGV3.AllowUserToAddRows = False
        Me.DGV3.AllowUserToDeleteRows = False
        Me.DGV3.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.DGV3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG2, Me.NO_FAK, Me.N_BRG2, Me.QTY2, Me.SAT2})
        Me.DGV3.Location = New System.Drawing.Point(504, 444)
        Me.DGV3.Name = "DGV3"
        Me.DGV3.ReadOnly = True
        Me.DGV3.RowHeadersVisible = False
        Me.DGV3.Size = New System.Drawing.Size(431, 134)
        Me.DGV3.TabIndex = 64
        '
        'HAPUS
        '
        Me.HAPUS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HAPUS.Location = New System.Drawing.Point(848, 590)
        Me.HAPUS.Name = "HAPUS"
        Me.HAPUS.Size = New System.Drawing.Size(87, 32)
        Me.HAPUS.TabIndex = 65
        Me.HAPUS.Text = "HAPUS"
        Me.HAPUS.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(744, 590)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 32)
        Me.Button3.TabIndex = 66
        Me.Button3.Text = "SIMPAN"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'DGV2
        '
        Me.DGV2.AllowUserToAddRows = False
        Me.DGV2.AllowUserToDeleteRows = False
        Me.DGV2.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGV2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT})
        Me.DGV2.Location = New System.Drawing.Point(221, 160)
        Me.DGV2.Name = "DGV2"
        Me.DGV2.ReadOnly = True
        Me.DGV2.RowHeadersVisible = False
        Me.DGV2.Size = New System.Drawing.Size(540, 164)
        Me.DGV2.TabIndex = 67
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle3
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'ket
        '
        Me.ket.AcceptsReturn = True
        Me.ket.Location = New System.Drawing.Point(488, 590)
        Me.ket.Multiline = True
        Me.ket.Name = "ket"
        Me.ket.Size = New System.Drawing.Size(229, 63)
        Me.ket.TabIndex = 70
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(472, 593)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(10, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = ":"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(404, 593)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 13)
        Me.Label11.TabIndex = 68
        Me.Label11.Text = "Keterangan"
        '
        'teknisi
        '
        Me.teknisi.Enabled = False
        Me.teknisi.Location = New System.Drawing.Point(607, 69)
        Me.teknisi.Name = "teknisi"
        Me.teknisi.Size = New System.Drawing.Size(209, 20)
        Me.teknisi.TabIndex = 73
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(591, 72)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(10, 13)
        Me.Label15.TabIndex = 72
        Me.Label15.Text = ":"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(480, 72)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(72, 13)
        Me.Label16.TabIndex = 71
        Me.Label16.Text = "Nama Teknisi"
        '
        'ktek
        '
        Me.ktek.AutoSize = True
        Me.ktek.Location = New System.Drawing.Point(604, 92)
        Me.ktek.Name = "ktek"
        Me.ktek.Size = New System.Drawing.Size(10, 13)
        Me.ktek.TabIndex = 74
        Me.ktek.Text = ":"
        Me.ktek.Visible = False
        '
        'K_BRG2
        '
        Me.K_BRG2.DataPropertyName = "K_BRG"
        Me.K_BRG2.HeaderText = "Kode "
        Me.K_BRG2.Name = "K_BRG2"
        Me.K_BRG2.ReadOnly = True
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "NO_FAK"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'N_BRG2
        '
        Me.N_BRG2.DataPropertyName = "N_BRG"
        Me.N_BRG2.HeaderText = "Nama"
        Me.N_BRG2.Name = "N_BRG2"
        Me.N_BRG2.ReadOnly = True
        '
        'QTY2
        '
        Me.QTY2.DataPropertyName = "QTY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.QTY2.DefaultCellStyle = DataGridViewCellStyle2
        Me.QTY2.HeaderText = "Qty"
        Me.QTY2.Name = "QTY2"
        Me.QTY2.ReadOnly = True
        '
        'SAT2
        '
        Me.SAT2.DataPropertyName = "SAT"
        Me.SAT2.HeaderText = "Sat"
        Me.SAT2.Name = "SAT2"
        Me.SAT2.ReadOnly = True
        '
        'K_BRG1
        '
        Me.K_BRG1.DataPropertyName = "K_BRG"
        Me.K_BRG1.HeaderText = "Kode "
        Me.K_BRG1.Name = "K_BRG1"
        Me.K_BRG1.ReadOnly = True
        '
        'NO_FAK1
        '
        Me.NO_FAK1.DataPropertyName = "NO_FAK"
        Me.NO_FAK1.HeaderText = "NO_FAK"
        Me.NO_FAK1.Name = "NO_FAK1"
        Me.NO_FAK1.ReadOnly = True
        Me.NO_FAK1.Visible = False
        '
        'N_BRG1
        '
        Me.N_BRG1.DataPropertyName = "N_BRG"
        Me.N_BRG1.HeaderText = "Nama"
        Me.N_BRG1.Name = "N_BRG1"
        Me.N_BRG1.ReadOnly = True
        '
        'QTY1
        '
        Me.QTY1.DataPropertyName = "QTY"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.QTY1.DefaultCellStyle = DataGridViewCellStyle1
        Me.QTY1.HeaderText = "Qty"
        Me.QTY1.Name = "QTY1"
        Me.QTY1.ReadOnly = True
        '
        'SAT1
        '
        Me.SAT1.DataPropertyName = "SAT"
        Me.SAT1.HeaderText = "Sat"
        Me.SAT1.Name = "SAT1"
        Me.SAT1.ReadOnly = True
        '
        'PerbaikanHasilEditHapus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Beige
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.ktek)
        Me.Controls.Add(Me.teknisi)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.ket)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.DGV2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.HAPUS)
        Me.Controls.Add(Me.DGV3)
        Me.Controls.Add(Me.DGV1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.date1)
        Me.Controls.Add(Me.nofak)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "PerbaikanHasilEditHapus"
        Me.Text = "PerbaikanHasil"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents nofak As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GAGAL As System.Windows.Forms.Button
    Friend WithEvents nbrg As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents BERHASIL As System.Windows.Forms.Button
    Friend WithEvents tqty As System.Windows.Forms.TextBox
    Friend WithEvents kbrg As System.Windows.Forms.TextBox
    Friend WithEvents DGV1 As System.Windows.Forms.DataGridView
    Friend WithEvents DGV3 As System.Windows.Forms.DataGridView
    Friend WithEvents HAPUS As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DGV2 As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tsat As System.Windows.Forms.TextBox
    Friend WithEvents ket As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents teknisi As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ktek As System.Windows.Forms.Label
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
