﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Koreksi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.kode = New System.Windows.Forms.TextBox()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.stok = New System.Windows.Forms.TextBox()
        Me.stokbaru = New System.Windows.Forms.TextBox()
        Me.stokakhir = New System.Windows.Forms.TextBox()
        Me.butKoreksi = New System.Windows.Forms.Button()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.sat = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.ket = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.EventLog1 = New System.Diagnostics.EventLog()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(45, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kode Barang"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(45, 125)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nama Barang"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 162)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Stok"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(45, 194)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Stok Koreksi"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 225)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Stok Akhir"
        '
        'kode
        '
        Me.kode.Enabled = False
        Me.kode.Location = New System.Drawing.Point(130, 93)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(232, 20)
        Me.kode.TabIndex = 5
        '
        'nama
        '
        Me.nama.Enabled = False
        Me.nama.Location = New System.Drawing.Point(130, 125)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(232, 20)
        Me.nama.TabIndex = 6
        '
        'stok
        '
        Me.stok.Enabled = False
        Me.stok.Location = New System.Drawing.Point(130, 159)
        Me.stok.Name = "stok"
        Me.stok.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.stok.Size = New System.Drawing.Size(103, 20)
        Me.stok.TabIndex = 7
        Me.stok.Text = "0"
        '
        'stokbaru
        '
        Me.stokbaru.Location = New System.Drawing.Point(130, 191)
        Me.stokbaru.Name = "stokbaru"
        Me.stokbaru.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.stokbaru.Size = New System.Drawing.Size(103, 20)
        Me.stokbaru.TabIndex = 0
        Me.stokbaru.Text = "0"
        '
        'stokakhir
        '
        Me.stokakhir.Enabled = False
        Me.stokakhir.Location = New System.Drawing.Point(130, 222)
        Me.stokakhir.Name = "stokakhir"
        Me.stokakhir.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.stokakhir.Size = New System.Drawing.Size(103, 20)
        Me.stokakhir.TabIndex = 9
        '
        'butKoreksi
        '
        Me.butKoreksi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butKoreksi.Location = New System.Drawing.Point(130, 330)
        Me.butKoreksi.Name = "butKoreksi"
        Me.butKoreksi.Size = New System.Drawing.Size(75, 32)
        Me.butKoreksi.TabIndex = 2
        Me.butKoreksi.Text = "Koreksi"
        Me.butKoreksi.UseVisualStyleBackColor = True
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(225, 330)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(75, 32)
        Me.butBAT.TabIndex = 11
        Me.butBAT.Text = "Batal"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'sat
        '
        Me.sat.AutoSize = True
        Me.sat.Location = New System.Drawing.Point(239, 194)
        Me.sat.Name = "sat"
        Me.sat.Size = New System.Drawing.Size(29, 13)
        Me.sat.TabIndex = 12
        Me.sat.Text = "Stok"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(153, 56)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(103, 20)
        Me.date1.TabIndex = 13
        '
        'ket
        '
        Me.ket.Location = New System.Drawing.Point(130, 253)
        Me.ket.Multiline = True
        Me.ket.Name = "ket"
        Me.ket.Size = New System.Drawing.Size(232, 57)
        Me.ket.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(45, 256)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Keterangan"
        '
        'EventLog1
        '
        Me.EventLog1.SynchronizingObject = Me
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lucida Fax", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(136, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(136, 18)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "KOREKSI STOK"
        '
        'Koreksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(416, 405)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.ket)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.date1)
        Me.Controls.Add(Me.sat)
        Me.Controls.Add(Me.butBAT)
        Me.Controls.Add(Me.butKoreksi)
        Me.Controls.Add(Me.stokakhir)
        Me.Controls.Add(Me.stokbaru)
        Me.Controls.Add(Me.stok)
        Me.Controls.Add(Me.nama)
        Me.Controls.Add(Me.kode)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "Koreksi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Koreksi"
        Me.TopMost = True
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents kode As System.Windows.Forms.TextBox
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents stok As System.Windows.Forms.TextBox
    Friend WithEvents stokbaru As System.Windows.Forms.TextBox
    Friend WithEvents stokakhir As System.Windows.Forms.TextBox
    Friend WithEvents butKoreksi As System.Windows.Forms.Button
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents sat As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ket As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents EventLog1 As System.Diagnostics.EventLog
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
