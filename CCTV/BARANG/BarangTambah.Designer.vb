﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BarangTambah
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Satuan = New System.Windows.Forms.TextBox()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.kodeBrg = New System.Windows.Forms.TextBox()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CBjenis = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ket = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.harga = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.namaBrg = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBserial = New System.Windows.Forms.CheckBox()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel2.Controls.Add(Me.CBserial)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Satuan)
        Me.Panel2.Controls.Add(Me.butBAT)
        Me.Panel2.Controls.Add(Me.kodeBrg)
        Me.Panel2.Controls.Add(Me.butSIM)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.CBjenis)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.ket)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.harga)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.namaBrg)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Location = New System.Drawing.Point(261, 183)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(488, 363)
        Me.Panel2.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(66, 155)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Satuan"
        '
        'Satuan
        '
        Me.Satuan.Location = New System.Drawing.Point(201, 152)
        Me.Satuan.Name = "Satuan"
        Me.Satuan.Size = New System.Drawing.Size(97, 20)
        Me.Satuan.TabIndex = 3
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(313, 315)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(75, 32)
        Me.butBAT.TabIndex = 7
        Me.butBAT.Text = "BATAL"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'kodeBrg
        '
        Me.kodeBrg.Location = New System.Drawing.Point(201, 57)
        Me.kodeBrg.Name = "kodeBrg"
        Me.kodeBrg.Size = New System.Drawing.Size(187, 20)
        Me.kodeBrg.TabIndex = 0
        '
        'butSIM
        '
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(201, 315)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(75, 32)
        Me.butSIM.TabIndex = 6
        Me.butSIM.Text = "SIMPAN"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(66, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Kode Barang"
        '
        'CBjenis
        '
        Me.CBjenis.FormattingEnabled = True
        Me.CBjenis.Items.AddRange(New Object() {"B", "J"})
        Me.CBjenis.Location = New System.Drawing.Point(201, 120)
        Me.CBjenis.Name = "CBjenis"
        Me.CBjenis.Size = New System.Drawing.Size(55, 21)
        Me.CBjenis.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(66, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Nama Barang"
        '
        'ket
        '
        Me.ket.Location = New System.Drawing.Point(201, 216)
        Me.ket.Multiline = True
        Me.ket.Name = "ket"
        Me.ket.Size = New System.Drawing.Size(187, 92)
        Me.ket.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(66, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Barang (B) / Jasa (J)"
        '
        'harga
        '
        Me.harga.Location = New System.Drawing.Point(201, 182)
        Me.harga.Name = "harga"
        Me.harga.Size = New System.Drawing.Size(187, 20)
        Me.harga.TabIndex = 4
        Me.harga.Text = "0"
        Me.harga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(66, 185)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Harga Jual Terendah "
        '
        'namaBrg
        '
        Me.namaBrg.Location = New System.Drawing.Point(201, 89)
        Me.namaBrg.Name = "namaBrg"
        Me.namaBrg.Size = New System.Drawing.Size(187, 20)
        Me.namaBrg.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(66, 219)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Keterangan"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Location = New System.Drawing.Point(253, 185)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(493, 369)
        Me.Panel1.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Modern No. 20", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(423, 150)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(177, 21)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "TAMBAH BARANG"
        '
        'CBserial
        '
        Me.CBserial.AutoSize = True
        Me.CBserial.BackColor = System.Drawing.Color.Maroon
        Me.CBserial.ForeColor = System.Drawing.Color.Transparent
        Me.CBserial.Location = New System.Drawing.Point(300, 119)
        Me.CBserial.Name = "CBserial"
        Me.CBserial.Size = New System.Drawing.Size(64, 17)
        Me.CBserial.TabIndex = 21
        Me.CBserial.Text = "SERIAL"
        Me.CBserial.UseVisualStyleBackColor = False
        '
        'BarangTambah
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "BarangTambah"
        Me.Text = "BarangTambah"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents kodeBrg As System.Windows.Forms.TextBox
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CBjenis As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ket As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents harga As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents namaBrg As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Satuan As System.Windows.Forms.TextBox
    Friend WithEvents CBserial As System.Windows.Forms.CheckBox
End Class
