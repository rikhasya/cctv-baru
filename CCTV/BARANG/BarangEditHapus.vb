﻿Public Class BarangEditHapus

    Private Sub BarangEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub butUBAH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUBAH.Click
        If kodeBrg.Text = "" Or namaBrg.Text = "" Then
            MsgBox("Kode / Nama tidak boleh kosong")
            Exit Sub
        ElseIf Not IsNumeric(harga.Text) Then
            MsgBox("Cek Harga")
            Exit Sub
        End If

        Dim aktif As Integer = 0
        If CBaktif.Checked = True Then
            aktif = 1
        End If

        Dim serial As Integer = 0
        If CBserial.Checked = True Then
            serial = 1
        End If

        Dim a As New clsCCTV.clsBarang
        Dim b As Boolean
        b = a.update(kodeBrg.Text, namaBrg.Text, CBjenis.SelectedItem, harga.Text, ket.Text, Satuan.Text, aktif, serial)

        If b = True Then
            MsgBox("Berhasil Mengubah Data")
            Me.Close()
        Else
            MsgBox("Gagal Mengubah Data")
        End If
    End Sub

    Private Sub butHAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAP.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & namaBrg.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsBarang
        Dim b As Boolean
        b = a.delete(kodeBrg.Text)

        If b = True Then
            MsgBox("Berhasil Menghapus Data")
            Me.Close()
        Else
            MsgBox("Gagal Menghapus Data")
        End If
    End Sub

    Private Sub harga_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles harga.Leave
        If IsNumeric(harga.Text) Then
            harga.Text = FormatNumber(harga.Text, 2)
        End If
    End Sub

End Class