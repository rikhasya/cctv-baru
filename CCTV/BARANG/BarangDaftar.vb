﻿Public Class BarangDaftar

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim jenis As String = "%"
        If CBjenis.SelectedItem = "B" Then
            jenis = "B"
        ElseIf CBjenis.SelectedItem = "J" Then
            jenis = "J"
        Else
            jenis = "%"
        End If

        Dim aktif As String = 0
        If CBaktif.CheckState = CheckState.Checked Then
            aktif = 1
        ElseIf CBaktif.CheckState = CheckState.Indeterminate Then
            aktif = "%"
        End If

        Dim serial As String = 0
        If CBserial.CheckState = CheckState.Checked Then
            serial = 1
        ElseIf CBserial.CheckState = CheckState.Indeterminate Then
            serial = "%"
        End If

        Dim a As New clsCCTV.clsBarang
        Dim b As DataTable
        b = a.table("%" & kodeBrg.Text & "%", "%" & namaBrg.Text & "%", jenis, aktif, serial)

        DGVbarang.DataSource = b
    End Sub

    Private Sub DGVbarang_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVbarang.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("BARANGEDITHAPUS")
        If b = True Then
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As BarangEditHapus
            frm = New BarangEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Barang"
            Dim row As Integer
            row = DGVbarang.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.kodeBrg.Text = DGVbarang.Item("K_BRG", row).Value.ToString
            frm.namaBrg.Text = DGVbarang.Item("N_BRG", row).Value.ToString
            frm.Satuan.Text = DGVbarang.Item("SAT", row).Value.ToString
            frm.CBjenis.SelectedItem = DGVbarang.Item("BJ", row).Value.ToString
            frm.harga.Text = FormatNumber(DGVbarang.Item("H_JUAL", row).Value.ToString, 2)
            frm.ket.Text = DGVbarang.Item("KET", row).Value.ToString
            If DGVbarang.Item("AKTIF", row).Value = 1 Then
                frm.CBaktif.Checked = True
            End If

            If DGVbarang.Item("SERIAL", row).Value = 1 Then
                frm.CBserial.Checked = True
            End If

            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If

    End Sub

    Private Sub DGVbarang_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVbarang.KeyDown
        Dim row As Integer
        row = DGVbarang.CurrentRow.Index.ToString
        
        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("KOREKSI")
            If b = True Then
                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As Koreksi
                frm = New Koreksi
                frm.MdiParent = frmMenu
                frm.Text = "Koreksi"
                frm.MdiParent = frmMenu
                frm.kode.Text = DGVbarang.Item("K_BRG", row).Value.ToString
                frm.nama.Text = DGVbarang.Item("N_BRG", row).Value.ToString
                frm.sat.Text = DGVbarang.Item("SAT", row).Value.ToString
                frm.stok.Text = DGVbarang.Item("QTY", row).Value.ToString

                frm.Show()
                frm.Dock = DockStyle.Fill
                Me.Close()
            Else
                MsgBox("anda tdk di otorisasi")
            End If
        End If
    End Sub

    Private Sub BarangDaftar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub BarangDaftar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class