﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BarangDaftar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CBjenis = New System.Windows.Forms.ComboBox()
        Me.DGVbarang = New System.Windows.Forms.DataGridView()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.namaBrg = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CBaktif = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.kodeBrg = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CBserial = New System.Windows.Forms.CheckBox()
        Me.SERIAL = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.H_JUAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AKTIF = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.DGVbarang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CBjenis
        '
        Me.CBjenis.FormattingEnabled = True
        Me.CBjenis.Items.AddRange(New Object() {"", "B", "J"})
        Me.CBjenis.Location = New System.Drawing.Point(602, 13)
        Me.CBjenis.Name = "CBjenis"
        Me.CBjenis.Size = New System.Drawing.Size(121, 21)
        Me.CBjenis.TabIndex = 2
        '
        'DGVbarang
        '
        Me.DGVbarang.AllowUserToAddRows = False
        Me.DGVbarang.AllowUserToDeleteRows = False
        Me.DGVbarang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVbarang.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SERIAL, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.BJ, Me.H_JUAL, Me.KET, Me.AKTIF})
        Me.DGVbarang.Location = New System.Drawing.Point(26, 100)
        Me.DGVbarang.Name = "DGVbarang"
        Me.DGVbarang.ReadOnly = True
        Me.DGVbarang.RowHeadersVisible = False
        Me.DGVbarang.Size = New System.Drawing.Size(940, 526)
        Me.DGVbarang.TabIndex = 14
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(866, 12)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 3
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(58, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Kode Barang"
        '
        'namaBrg
        '
        Me.namaBrg.Location = New System.Drawing.Point(350, 14)
        Me.namaBrg.Name = "namaBrg"
        Me.namaBrg.Size = New System.Drawing.Size(163, 20)
        Me.namaBrg.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 629)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(214, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "* Doubleclick untuk edit/hapus data barang"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.CBserial)
        Me.Panel1.Controls.Add(Me.CBaktif)
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.CBjenis)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.namaBrg)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.kodeBrg)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-3, 46)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'CBaktif
        '
        Me.CBaktif.AutoSize = True
        Me.CBaktif.Location = New System.Drawing.Point(813, 15)
        Me.CBaktif.Name = "CBaktif"
        Me.CBaktif.Size = New System.Drawing.Size(47, 17)
        Me.CBaktif.TabIndex = 7
        Me.CBaktif.Text = "Aktif"
        Me.CBaktif.ThreeState = True
        Me.CBaktif.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(272, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Barang"
        '
        'kodeBrg
        '
        Me.kodeBrg.Location = New System.Drawing.Point(133, 13)
        Me.kodeBrg.Name = "kodeBrg"
        Me.kodeBrg.Size = New System.Drawing.Size(119, 20)
        Me.kodeBrg.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(528, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Barang/Jasa"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(416, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(135, 24)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Daftar Barang"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(260, 629)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "* F1 untuk koreksi qty "
        '
        'CBserial
        '
        Me.CBserial.AutoSize = True
        Me.CBserial.Location = New System.Drawing.Point(748, 16)
        Me.CBserial.Name = "CBserial"
        Me.CBserial.Size = New System.Drawing.Size(52, 17)
        Me.CBserial.TabIndex = 8
        Me.CBserial.Text = "Serial"
        Me.CBserial.ThreeState = True
        Me.CBserial.UseVisualStyleBackColor = True
        '
        'SERIAL
        '
        Me.SERIAL.DataPropertyName = "SERIAL"
        Me.SERIAL.FalseValue = "0"
        Me.SERIAL.HeaderText = "Serial"
        Me.SERIAL.Name = "SERIAL"
        Me.SERIAL.ReadOnly = True
        Me.SERIAL.TrueValue = "1"
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Satuan"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'BJ
        '
        Me.BJ.DataPropertyName = "BJ"
        Me.BJ.HeaderText = "Jenis"
        Me.BJ.Name = "BJ"
        Me.BJ.ReadOnly = True
        '
        'H_JUAL
        '
        Me.H_JUAL.DataPropertyName = "H_JUAL"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.H_JUAL.DefaultCellStyle = DataGridViewCellStyle1
        Me.H_JUAL.HeaderText = "Harga Jual Terendah"
        Me.H_JUAL.Name = "H_JUAL"
        Me.H_JUAL.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.KET.DefaultCellStyle = DataGridViewCellStyle2
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'AKTIF
        '
        Me.AKTIF.DataPropertyName = "AKTIF"
        Me.AKTIF.FalseValue = "0"
        Me.AKTIF.HeaderText = "Aktif"
        Me.AKTIF.Name = "AKTIF"
        Me.AKTIF.ReadOnly = True
        Me.AKTIF.TrueValue = "1"
        '
        'BarangDaftar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DGVbarang)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "BarangDaftar"
        Me.Text = "BarangDaftar"
        CType(Me.DGVbarang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CBjenis As System.Windows.Forms.ComboBox
    Friend WithEvents DGVbarang As System.Windows.Forms.DataGridView
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents namaBrg As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kodeBrg As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CBaktif As System.Windows.Forms.CheckBox
    Friend WithEvents CBserial As System.Windows.Forms.CheckBox
    Friend WithEvents SERIAL As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents H_JUAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AKTIF As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
