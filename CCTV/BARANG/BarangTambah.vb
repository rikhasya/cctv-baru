﻿Public Class BarangTambah

    Private Sub BarangTambah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If kodeBrg.Text = "" Or namaBrg.Text = "" Then
            MsgBox("Kode / Nama tidak boleh kosong")
            Exit Sub
        ElseIf Not IsNumeric(harga.Text) Then
            MsgBox("Cek Harga")
            Exit Sub
        End If

        If Not (CBjenis.SelectedItem = "B" Or CBjenis.SelectedItem = "J") Then
            MsgBox("Cek barang jasa")
            Exit Sub
        End If

        Dim serial As Integer = 0
        If CBserial.Checked = True Then
            serial = 1
        End If

        Dim a As New clsCCTV.clsBarang
        Dim b As Boolean
        b = a.input(kodeBrg.Text, namaBrg.Text, CBjenis.SelectedItem, CDbl(harga.Text), ket.Text, Satuan.Text, serial)

        If b = True Then
            MsgBox("Berhasil Menambahkan Barang Baru")
            kodeBrg.Clear()
            namaBrg.Clear()
            CBjenis.SelectedItem = ""
            harga.Text = 0
            ket.Clear()
            Satuan.Clear()
            kodeBrg.Focus()

        Else
            MsgBox("Gagal, Cek Data Lagi!")
        End If
    End Sub

    Private Sub butBAT_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBAT.Click
        Me.Close()
    End Sub

    Private Sub harga_Leave(sender As Object, e As System.EventArgs) Handles harga.Leave
        If IsNumeric(harga.Text) Then
            harga.Text = FormatNumber(harga.Text, 2)
        End If
    End Sub

End Class