﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mutasi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.kode = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DGVmutasi = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TGL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KODE_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NAMA_BELI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KODE2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NAMA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALDO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGLJAM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVmutasi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Felix Titling", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(60, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 28)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "MUTASI"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(212, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Kode Barang "
        '
        'kode
        '
        Me.kode.Location = New System.Drawing.Point(215, 37)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(140, 20)
        Me.kode.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(395, 21)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 36)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "CARI"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DGVmutasi
        '
        Me.DGVmutasi.AllowUserToAddRows = False
        Me.DGVmutasi.AllowUserToDeleteRows = False
        Me.DGVmutasi.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVmutasi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVmutasi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TGL, Me.NO_FAK, Me.KODE_BRG, Me.NAMA_BELI, Me.KODE2, Me.NAMA, Me.QTY, Me.SAT, Me.HARGA, Me.SALDO, Me.TGLJAM})
        Me.DGVmutasi.Location = New System.Drawing.Point(22, 74)
        Me.DGVmutasi.Name = "DGVmutasi"
        Me.DGVmutasi.ReadOnly = True
        Me.DGVmutasi.RowHeadersVisible = False
        Me.DGVmutasi.Size = New System.Drawing.Size(956, 555)
        Me.DGVmutasi.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(357, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(23, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "*F1"
        '
        'TGL
        '
        Me.TGL.DataPropertyName = "TGL_FAK"
        Me.TGL.HeaderText = "Tanggal"
        Me.TGL.Name = "TGL"
        Me.TGL.ReadOnly = True
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'KODE_BRG
        '
        Me.KODE_BRG.DataPropertyName = "K_BRG"
        Me.KODE_BRG.HeaderText = "Kode Barang"
        Me.KODE_BRG.Name = "KODE_BRG"
        Me.KODE_BRG.ReadOnly = True
        '
        'NAMA_BELI
        '
        Me.NAMA_BELI.DataPropertyName = "N_BRG"
        Me.NAMA_BELI.HeaderText = "Nama Barang"
        Me.NAMA_BELI.Name = "NAMA_BELI"
        Me.NAMA_BELI.ReadOnly = True
        '
        'KODE2
        '
        Me.KODE2.DataPropertyName = "KODE"
        Me.KODE2.HeaderText = "Kode"
        Me.KODE2.Name = "KODE2"
        Me.KODE2.ReadOnly = True
        Me.KODE2.Visible = False
        '
        'NAMA
        '
        Me.NAMA.DataPropertyName = "NAMA"
        Me.NAMA.HeaderText = "Nama Sup / Lgn"
        Me.NAMA.Name = "NAMA"
        Me.NAMA.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle1
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Satuan"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.HARGA.DefaultCellStyle = DataGridViewCellStyle2
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.ReadOnly = True
        '
        'SALDO
        '
        Me.SALDO.DataPropertyName = "SALDO"
        Me.SALDO.HeaderText = "Saldo"
        Me.SALDO.Name = "SALDO"
        Me.SALDO.ReadOnly = True
        '
        'TGLJAM
        '
        Me.TGLJAM.DataPropertyName = "TGLJAM"
        Me.TGLJAM.HeaderText = "Datestamp"
        Me.TGLJAM.Name = "TGLJAM"
        Me.TGLJAM.ReadOnly = True
        '
        'Mutasi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.BurlyWood
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DGVmutasi)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.kode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "Mutasi"
        Me.Text = "Mutasi"
        CType(Me.DGVmutasi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents kode As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DGVmutasi As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TGL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KODE_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NAMA_BELI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KODE2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NAMA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALDO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGLJAM As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
