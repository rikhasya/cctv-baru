﻿Public Class frmMenu

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ListBarangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBarangToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("BARANGDATA")
        If b = True Then

            Me.Text = "Daftar Barang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As BarangDaftar
            frm = New BarangDaftar

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If

    End Sub

    Private Sub TambahBarangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TambahBarangToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("BARANGTAMBAH")
        If b = True Then

            Me.Text = "Tambah Barang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As BarangTambah
            frm = New BarangTambah

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If

    End Sub

    Private Sub TambahCustomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TambahCustomerToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("CUSTOMERTAMBAH")
        If b = True Then

            Me.Text = "Tambah Customer"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As CustomerTambah
            frm = New CustomerTambah

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If

    End Sub

    Private Sub DaftarCustomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarCustomerToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("CUSTOMERDATA")
        If b = True Then

            Me.Text = "Daftar Customer"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As CustomerDaftar
            frm = New CustomerDaftar

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub TambahSalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TambahSalesToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SALESTAMBAH")
        If b = True Then

            Me.Text = "Tambah Sales"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SalesTambah
            frm = New SalesTambah

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DaftarSalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarSalesToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SALESDATA")
        If b = True Then

            Me.Text = "Daftar Sales"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SalesDaftar
            frm = New SalesDaftar

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub TabahSupplierToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabahSupplierToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SUPPLIERTAMBAH")
        If b = True Then

            Me.Text = "Tambah Supplier"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SupplierTambah
            frm = New SupplierTambah

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub PembelianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PembelianToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("BELI_INPUT")
        If b = True Then

            Me.Text = "Input PO Pembelian"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As POPembelian
            frm = New POPembelian

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub PenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("JUAL_INPUT")
        If b = True Then

            Me.Text = "Input Penjualan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Penjualan
            frm = New Penjualan

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanPembelianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanPembelianToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("BELI_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Pembelian"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Pembelian_Laporan
            frm = New Pembelian_Laporan

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub ReturPembelianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPembelianToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURBELI_INPUT")
        If b = True Then

            Me.Text = "Retur Pembelian"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Pembelian_Retur
            frm = New Pembelian_Retur

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanReturPembelianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanReturPembelianToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURBELI_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Retur Pembelian"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Pembelian_LaporanRetur
            frm = New Pembelian_LaporanRetur

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanPenjualanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("JUAL_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Penjualan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Penjualan_Laporan
            frm = New Penjualan_Laporan

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub ReturPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPenjualanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURJUAL_INPUT")
        If b = True Then

            Me.Text = "Retur Penjualan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Penjualan_Retur
            frm = New Penjualan_Retur

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanReturPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanReturPenjualanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURJUAL_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Retur Penjualan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Penjualan_LaporanRetur
            frm = New Penjualan_LaporanRetur

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub TambahUserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TambahUserToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("USERTAMBAH")
        If b = True Then

            Me.Text = "Tambah User"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As UserTambah
            frm = New UserTambah

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DaftarSupplierToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarSupplierToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SUPPLIERDATA")
        If b = True Then

            Me.Text = "Daftar Supplier"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SupplierDaftar
            frm = New SupplierDaftar

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub SOJualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOJualToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SO_INPUT")
        If b = True Then

            Me.Text = "SO Penjualan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SO
            frm = New SO

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanSOJualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanSOJualToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SO_LAPORAN")
        If b = True Then

            Me.Text = "Laporan SO Penjualan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SO_Laporan
            frm = New SO_Laporan

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanSuratJalanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanSuratJalanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SJ_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Surat Jalan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SuratJalanLap
            frm = New SuratJalanLap

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DataHutangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataHutangToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("HUTANGDATA")
        If b = True Then

            Me.Text = "Data Hutang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As HutangData
            frm = New HutangData

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub PembayaranHutangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PembayaranHutangToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("HUTANGBAYAR")
        If b = True Then

            Me.Text = "Pembayaran Hutang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As HutangPembayaran
            frm = New HutangPembayaran

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DataHutangToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataHutangToolStripMenuItem1.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PIUTANGDATA")
        If b = True Then

            Me.Text = "Data Piutang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As PiutangData
            frm = New PiutangData

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub PenerimaanPiutangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenerimaanPiutangToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PIUTANGTERIMA")
        If b = True Then

            Me.Text = "Penerimaan Piutang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As PiutangTerima
            frm = New PiutangTerima

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub TambahAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TambahAccountToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("ACCTAMBAH")
        If b = True Then

            Me.Text = "Tambah Account"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ACCInput
            frm = New ACCInput
            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DataAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataAccountToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("ACCDATA")
        If b = True Then

            Me.Text = "Data Account"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ACCdata
            frm = New ACCdata

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub InputServiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SERVICE_INPUT")
        If b = True Then

            Me.Text = "Input Service"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ServiceInput
            frm = New ServiceInput

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanServiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SERVICE_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Service"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ServiceLaporan
            frm = New ServiceLaporan

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DataKasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataKasToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("DATAKAS")
        If b = True Then

            Me.Text = "Data Kas"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As KasData
            frm = New KasData

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanHasilServiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SVCHASIL_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Hasil Service"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As ServiceLaporanHasil
            frm = New ServiceLaporanHasil

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub MutasiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MutasiToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("MUTASI")
        If b = True Then

            Me.Text = "Mutasi Barang"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Mutasi
            frm = New Mutasi

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanReturSuratJalanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanReturSuratJalanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURSJ_LAPORAN")
        If b = True Then

            Me.Text = "Laporan Retur Surat Jalan"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SuratJalanReturLap
            frm = New SuratJalanReturLap

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanKoreksiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanKoreksiToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("KOREKSILAPORAN")
        If b = True Then

            Me.Text = "Laporan Koreksi"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As KoreksiLaporan
            frm = New KoreksiLaporan

            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DataUserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataUserToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("USERDATA")
        If b = True Then

            Me.Text = "Data User"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As UserDaftar
            frm = New UserDaftar
            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub LaporanQtyNegativeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanQtyNegativeToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("QTYNEGLAP")
        If b = True Then

            Me.Text = "Laporan Qty Negative"
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As QtyNeg
            frm = New QtyNeg
            frm.MdiParent = Me
            frm.Show()
            frm.Dock = DockStyle.Fill

        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub InputBiayaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InputBiayaToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("KASTOBANK")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If

        Me.Text = "Input Biaya"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As KasToBank
        frm = New KasToBank
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub InputComplainToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InputComplainToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("COMPLAININPUT")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Input Complain"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As ComplainInput
        frm = New ComplainInput
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanComplainToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanComplainToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("COMPLAINLAP")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Laporan Complain"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As ComplainLaporan
        frm = New ComplainLaporan
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub JadwalKunjunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JadwalKunjunganToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("JADWALLAP")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Jadwal Kunjungan"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As JadwalLaporan
        frm = New JadwalLaporan
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanPerbaikanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanPerbaikanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PERBAIKANLAP")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Laporan Hasil Perbaikan"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As PerbaikanLaporan
        frm = New PerbaikanLaporan
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanServiceSupplierToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanServiceSupplierToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SERVICE_LAPORAN")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Laporan Service Supplier"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As ServiceLaporan
        frm = New ServiceLaporan
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanHasilServiceSupplierToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanHasilServiceSupplierToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SVCHASIL_LAPORAN")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Laporan Hasil Service Supplier"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As ServiceLaporanHasil
        frm = New ServiceLaporanHasil
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub TambahTeknisiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahTeknisiToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("TEHNISITAMBAH")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Tambah Teknisi"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As TeknisiTambah
        frm = New TeknisiTambah

        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub DaftarTeknisiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DaftarTeknisiToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("TEHNISIDATA")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Daftar Teknisi"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As TeknisiDaftar
        frm = New TeknisiDaftar

        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanHasilPerbaikanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanHasilPerbaikanToolStripMenuItem.Click
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PERBAIKANHASILLAP")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        Me.Text = "Laporan Hasil Service Perbaikan"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As PerbaikanHasilLaporan
        frm = New PerbaikanHasilLaporan

        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanHasilKunjunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanHasilKunjunganToolStripMenuItem.Click
        'Dim a As New clsCCTV.clsPengguna
        'Dim b As Boolean
        'b = a.cek("PERBAIKANHASILLAP")
        'If b = False Then
        '    MsgBox("anda tdk di otorisasi")
        '    Exit Sub
        'End If
        Me.Text = "Laporan Hasil Kunjungan"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As HasilVisitLaporan
        frm = New HasilVisitLaporan
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub SetoranAntarAccToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetoranAntarAccToolStripMenuItem.Click
        Me.Text = "Input Setoran Antar Account"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As SetoranKas
        frm = New SetoranKas
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

    Private Sub LaporanPOPembelianToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanPOPembelianToolStripMenuItem.Click
        Me.Text = "Laporan PO Pembelian"
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As POPembelian_Laporan
        frm = New POPembelian_Laporan
        frm.MdiParent = Me
        frm.Show()
        frm.Dock = DockStyle.Fill
    End Sub

End Class