﻿Public Class Otoritas

    Private Sub butUPDT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUPDT.Click
        Dim BARANGTAMBAH As String
        If CBBarangTambah.Checked = True Then
            BARANGTAMBAH = "Y"
        Else
            BARANGTAMBAH = "N"
        End If

        Dim BARANGDATA As String
        If CBBarangData.Checked = True Then
            BARANGDATA = "Y"
        Else
            BARANGDATA = "N"
        End If

        Dim BARANGEDITHAPUS As String
        If CBBarangEditHapus.Checked = True Then
            BARANGEDITHAPUS = "Y"
        Else
            BARANGEDITHAPUS = "N"
        End If

        Dim MUTASI As String
        If CBmutasi.Checked = True Then
            MUTASI = "Y"
        Else
            MUTASI = "N"
        End If

        Dim KOREKSI As String
        If CBkoreksi.Checked = True Then
            KOREKSI = "Y"
        Else
            KOREKSI = "N"
        End If

        Dim KOREKSILAPORAN As String
        If CBkoreksiLaporan.Checked = True Then
            KOREKSILAPORAN = "Y"
        Else
            KOREKSILAPORAN = "N"
        End If

        Dim PRINTULANGLAPORAN As String
        If CBprintulangLaporan.Checked = True Then
            PRINTULANGLAPORAN = "Y"
        Else
            PRINTULANGLAPORAN = "N"
        End If

        Dim SALESTAMBAH As String
        If CBSalesTambah.Checked = True Then
            SALESTAMBAH = "Y"
        Else
            SALESTAMBAH = "N"
        End If

        Dim SALESDATA As String
        If CBSalesData.Checked = True Then
            SALESDATA = "Y"
        Else
            SALESDATA = "N"
        End If

        Dim SALESEDITHAPUS As String
        If CBSalesEditHapus.Checked = True Then
            SALESEDITHAPUS = "Y"
        Else
            SALESEDITHAPUS = "N"
        End If

        Dim CUSTOMERTAMBAH As String
        If CBCustomerTambah.Checked = True Then
            CUSTOMERTAMBAH = "Y"
        Else
            CUSTOMERTAMBAH = "N"
        End If

        Dim CUSTOMERDATA As String
        If CBCustomerData.Checked = True Then
            CUSTOMERDATA = "Y"
        Else
            CUSTOMERDATA = "N"
        End If

        Dim CUSTOMEREDITHAPUS As String
        If CBCustomerEditHapus.Checked = True Then
            CUSTOMEREDITHAPUS = "Y"
        Else
            CUSTOMEREDITHAPUS = "N"
        End If

        Dim SUPPLIERTAMBAH As String
        If CBSupplierTambah.Checked = True Then
            SUPPLIERTAMBAH = "Y"
        Else
            SUPPLIERTAMBAH = "N"
        End If

        Dim SUPPLIERDATA As String
        If CBSupplierData.Checked = True Then
            SUPPLIERDATA = "Y"
        Else
            SUPPLIERDATA = "N"
        End If

        Dim SUPPLIEREDITHAPUS As String
        If CBSupplierEditHapus.Checked = True Then
            SUPPLIEREDITHAPUS = "Y"
        Else
            SUPPLIEREDITHAPUS = "N"
        End If

        Dim USERTAMBAH As String
        If CBUserTambah.Checked = True Then
            USERTAMBAH = "Y"
        Else
            USERTAMBAH = "N"
        End If

        Dim USERDATA As String
        If CBUserData.Checked = True Then
            USERDATA = "Y"
        Else
            USERDATA = "N"
        End If

        Dim USEREDITHAPUS As String
        If CBUserEditHapus.Checked = True Then
            USEREDITHAPUS = "Y"
        Else
            USEREDITHAPUS = "N"
        End If

        Dim BELI_INPUT As String
        If CBBeli_Input.Checked = True Then
            BELI_INPUT = "Y"
        Else
            BELI_INPUT = "N"
        End If

        Dim BELI_LAPORAN As String
        If CBBeli_Laporan.Checked = True Then
            BELI_LAPORAN = "Y"
        Else
            BELI_LAPORAN = "N"
        End If

        Dim BELI_EDITHAPUS As String
        If CBBeli_EditHapus.Checked = True Then
            BELI_EDITHAPUS = "Y"
        Else
            BELI_EDITHAPUS = "N"
        End If

        Dim RETURBELI_INPUT As String
        If CBReturBeli_Input.Checked = True Then
            RETURBELI_INPUT = "Y"
        Else
            RETURBELI_INPUT = "N"
        End If

        Dim RETURBELI_LAPORAN As String
        If CBReturBeli_Laporan.Checked = True Then
            RETURBELI_LAPORAN = "Y"
        Else
            RETURBELI_LAPORAN = "N"
        End If

        Dim RETURBELI_EDITHAPUS As String
        If CBReturBeli_EditHapus.Checked = True Then
            RETURBELI_EDITHAPUS = "Y"
        Else
            RETURBELI_EDITHAPUS = "N"
        End If

        Dim SO_INPUT As String
        If CBSO_Input.Checked = True Then
            SO_INPUT = "Y"
        Else
            SO_INPUT = "N"
        End If

        Dim SO_LAPORAN As String
        If CBSO_Laporan.Checked = True Then
            SO_LAPORAN = "Y"
        Else
            SO_LAPORAN = "N"
        End If

        Dim SO_EDITHAPUS As String
        If CBSO_EditHapus.Checked = True Then
            SO_EDITHAPUS = "Y"
        Else
            SO_EDITHAPUS = "N"
        End If

        Dim JUAL_INPUT As String
        If CBJual_Input.Checked = True Then
            JUAL_INPUT = "Y"
        Else
            JUAL_INPUT = "N"
        End If

        Dim JUAL_LAPORAN As String
        If CBJual_Laporan.Checked = True Then
            JUAL_LAPORAN = "Y"
        Else
            JUAL_LAPORAN = "N"
        End If

        Dim JUAL_EDITHAPUS As String
        If CBJual_EditHapus.Checked = True Then
            JUAL_EDITHAPUS = "Y"
        Else
            JUAL_EDITHAPUS = "N"
        End If

        Dim RETURJUAL_INPUT As String
        If CBReturJual_Input.Checked = True Then
            RETURJUAL_INPUT = "Y"
        Else
            RETURJUAL_INPUT = "N"
        End If

        Dim RETURJUAL_LAPORAN As String
        If CBReturJual_Laporan.Checked = True Then
            RETURJUAL_LAPORAN = "Y"
        Else
            RETURJUAL_LAPORAN = "N"
        End If

        Dim RETURJUAL_EDITHAPUS As String
        If CBReturJual_EditHapus.Checked = True Then
            RETURJUAL_EDITHAPUS = "Y"
        Else
            RETURJUAL_EDITHAPUS = "N"
        End If

        Dim SJ_INPUT As String
        If CBSJ_Input.Checked = True Then
            SJ_INPUT = "Y"
        Else
            SJ_INPUT = "N"
        End If

        Dim SJ_LAPORAN As String
        If CBSJ_Laporan.Checked = True Then
            SJ_LAPORAN = "Y"
        Else
            SJ_LAPORAN = "N"
        End If

        Dim SJ_EDITHAPUS As String
        If CBSJ_EditHapus.Checked = True Then
            SJ_EDITHAPUS = "Y"
        Else
            SJ_EDITHAPUS = "N"
        End If

        Dim RETURSJ_INPUT As String
        If CBReturSJ_Input.Checked = True Then
            RETURSJ_INPUT = "Y"
        Else
            RETURSJ_INPUT = "N"
        End If

        Dim RETURSJ_LAPORAN As String
        If CBReturSJ_Laporan.Checked = True Then
            RETURSJ_LAPORAN = "Y"
        Else
            RETURSJ_LAPORAN = "N"
        End If

        Dim RETURSJ_EDITHAPUS As String
        If CBReturSJ_EditHapus.Checked = True Then
            RETURSJ_EDITHAPUS = "Y"
        Else
            RETURSJ_EDITHAPUS = "N"
        End If

        Dim FAKTUR_INPUT As String
        If CBFaktur_Input.Checked = True Then
            FAKTUR_INPUT = "Y"
        Else
            FAKTUR_INPUT = "N"
        End If

        Dim SERVICE_INPUT As String
        If CBService_Input.Checked = True Then
            SERVICE_INPUT = "Y"
        Else
            SERVICE_INPUT = "N"
        End If

        Dim SERVICE_LAPORAN As String
        If CBService_Laporan.Checked = True Then
            SERVICE_LAPORAN = "Y"
        Else
            SERVICE_LAPORAN = "N"
        End If

        Dim SERVICE_EDITHAPUS As String
        If CBService_EditHapus.Checked = True Then
            SERVICE_EDITHAPUS = "Y"
        Else
            SERVICE_EDITHAPUS = "N"
        End If

        Dim SVCHASIL_INPUT As String
        If CBSVCHasil_Input.Checked = True Then
            SVCHASIL_INPUT = "Y"
        Else
            SVCHASIL_INPUT = "N"
        End If

        Dim SVCHASIL_LAPORAN As String
        If CBSVCHasil_Laporan.Checked = True Then
            SVCHASIL_LAPORAN = "Y"
        Else
            SVCHASIL_LAPORAN = "N"
        End If

        Dim SVCHASIL_EDITHAPUS As String
        If CBSVCHasil_EditHapus.Checked = True Then
            SVCHASIL_EDITHAPUS = "Y"
        Else
            SVCHASIL_EDITHAPUS = "N"
        End If

        Dim DATAKAS As String
        If CBDataKas.Checked = True Then
            DATAKAS = "Y"
        Else
            DATAKAS = "N"
        End If

        Dim INPUTDP As String
        If CBInputDP.Checked = True Then
            INPUTDP = "Y"
        Else
            INPUTDP = "N"
        End If

        Dim ACCTAMBAH As String
        If CBAccTambah.Checked = True Then
            ACCTAMBAH = "Y"
        Else
            ACCTAMBAH = "N"
        End If

        Dim ACCDATA As String
        If CBAccData.Checked = True Then
            ACCDATA = "Y"
        Else
            ACCDATA = "N"
        End If

        Dim ACCEDITHAPUS As String
        If CBAccEditHapus.Checked = True Then
            ACCEDITHAPUS = "Y"
        Else
            ACCEDITHAPUS = "N"
        End If

        Dim HUTANGDATA As String
        If CBHutangData.Checked = True Then
            HUTANGDATA = "Y"
        Else
            HUTANGDATA = "N"
        End If

        Dim HUTANGLIST As String
        If CBHutangList.Checked = True Then
            HUTANGLIST = "Y"
        Else
            HUTANGLIST = "N"
        End If

        Dim HUTANGBAYAR As String
        If CBHutangBayar.Checked = True Then
            HUTANGBAYAR = "Y"
        Else
            HUTANGBAYAR = "N"
        End If

        Dim PIUTANGDATA As String
        If CBPiutangData.Checked = True Then
            PIUTANGDATA = "Y"
        Else
            PIUTANGDATA = "N"
        End If

        Dim PIUTANGLIST As String
        If CBPiutangList.Checked = True Then
            PIUTANGLIST = "Y"
        Else
            PIUTANGLIST = "N"
        End If

        Dim PIUTANGTERIMA As String
        If CBPiutangTerima.Checked = True Then
            PIUTANGTERIMA = "Y"
        Else
            PIUTANGTERIMA = "N"
        End If

        Dim OTORITAS As String
        If CBOtoritas.Checked = True Then
            OTORITAS = "Y"
        Else
            OTORITAS = "N"
        End If

        Dim QTYNEGLAP As String
        If CBQTYNEGLAP.Checked = True Then
            QTYNEGLAP = "Y"
        Else
            QTYNEGLAP = "N"
        End If

        Dim KASTOBANK As String
        If CBKASTOBANK.Checked = True Then
            KASTOBANK = "Y"
        Else
            KASTOBANK = "N"
        End If

        Dim COMPLAININPUT As String
        If CBCOMPLAININPUT.Checked = True Then
            COMPLAININPUT = "Y"
        Else
            COMPLAININPUT = "N"
        End If

        Dim COMPLAINLAP As String
        If CBCOMPLAINLAP.Checked = True Then
            COMPLAINLAP = "Y"
        Else
            COMPLAINLAP = "N"
        End If

        Dim COMPEDITHAPUS As String
        If CBCOMPEDITHAPUS.Checked = True Then
            COMPEDITHAPUS = "Y"
        Else
            COMPEDITHAPUS = "N"
        End If

        Dim JADWALINPUT As String
        If CBJADWALINPUT.Checked = True Then
            JADWALINPUT = "Y"
        Else
            JADWALINPUT = "N"
        End If

        Dim JADWALLAP As String
        If CBJADWALLAP.Checked = True Then
            JADWALLAP = "Y"
        Else
            JADWALLAP = "N"
        End If

        Dim JADWALEDITHAPUS As String
        If CBJADWALEDITHAPUS.Checked = True Then
            JADWALEDITHAPUS = "Y"
        Else
            JADWALEDITHAPUS = "N"
        End If

        Dim JADWALHASIL As String
        If CBJADWALHASIL.Checked = True Then
            JADWALHASIL = "Y"
        Else
            JADWALHASIL = "N"
        End If

        Dim PERBAIKANINPUT As String
        If CBPERBAIKANINPUT.Checked = True Then
            PERBAIKANINPUT = "Y"
        Else
            PERBAIKANINPUT = "N"
        End If

        Dim PERBAIKANLAP As String
        If CBPERBAIKANLAP.Checked = True Then
            PERBAIKANLAP = "Y"
        Else
            PERBAIKANLAP = "N"
        End If

        Dim PERBAIKANEDITHAPUS As String
        If CBPERBAIKANEDITHAPUS.Checked = True Then
            PERBAIKANEDITHAPUS = "Y"
        Else
            PERBAIKANEDITHAPUS = "N"
        End If

        Dim PERBAIKANHASIL As String
        If CBPERBAIKANHASIL.Checked = True Then
            PERBAIKANHASIL = "Y"
        Else
            PERBAIKANHASIL = "N"
        End If

        Dim PERBAIKANHASILLAP As String
        If CBPERBAIKANHASILLAP.Checked = True Then
            PERBAIKANHASILLAP = "Y"
        Else
            PERBAIKANHASILLAP = "N"
        End If

        Dim PERBAIKANHASILEDITHAPUS As String
        If CBPERBAIKANHASILEDITHAPUS.Checked = True Then
            PERBAIKANHASILEDITHAPUS = "Y"
        Else
            PERBAIKANHASILEDITHAPUS = "N"
        End If

        Dim TEHNISITAMBAH As String
        If CBTEHNISITAMBAH.Checked = True Then
            TEHNISITAMBAH = "Y"
        Else
            TEHNISITAMBAH = "N"
        End If

        Dim TEHNISIDATA As String
        If CBTEHNISIDATA.Checked = True Then
            TEHNISIDATA = "Y"
        Else
            TEHNISIDATA = "N"
        End If

        Dim TEHNISIEDITHAPUS As String
        If CBTEHNISIEDITHAPUS.Checked = True Then
            TEHNISIEDITHAPUS = "Y"
        Else
            TEHNISIEDITHAPUS = "N"
        End If

        Dim DetailSerialInput As String
        If CBDetailSerialInput.Checked = True Then
            DetailSerialInput = "Y"
        Else
            DetailSerialInput = "N"
        End If

        Dim DetailSerialEdit As String
        If CBDetailSerialEdit.Checked = True Then
            DetailSerialEdit = "Y"
        Else
            DetailSerialEdit = "N"
        End If

        Dim CekDataKas As String
        If CBCekDataKas.Checked = True Then
            CekDataKas = "Y"
        Else
            CekDataKas = "N"
        End If

        Dim LapJual_Harga As String
        If CBLapJual_Harga.Checked = True Then
            LapJual_Harga = "Y"
        Else
            LapJual_Harga = "N"
        End If

        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.updateotoritas(user.Text, BARANGTAMBAH, BARANGDATA, BARANGEDITHAPUS, MUTASI, KOREKSI, KOREKSILAPORAN, PRINTULANGLAPORAN, SALESTAMBAH, SALESDATA, SALESEDITHAPUS, CUSTOMERTAMBAH, CUSTOMERDATA, CUSTOMEREDITHAPUS, SUPPLIERTAMBAH, SUPPLIERDATA, SUPPLIEREDITHAPUS, USERTAMBAH, USERDATA, USEREDITHAPUS, BELI_INPUT, BELI_LAPORAN, BELI_EDITHAPUS, RETURBELI_INPUT, RETURBELI_LAPORAN, RETURBELI_EDITHAPUS, SO_INPUT, SO_LAPORAN, SO_EDITHAPUS, JUAL_INPUT, JUAL_LAPORAN, JUAL_EDITHAPUS, RETURJUAL_INPUT, RETURJUAL_LAPORAN, RETURJUAL_EDITHAPUS, SJ_INPUT, SJ_LAPORAN, SJ_EDITHAPUS, RETURSJ_INPUT, RETURSJ_LAPORAN, RETURSJ_EDITHAPUS, FAKTUR_INPUT, SERVICE_INPUT, SERVICE_LAPORAN, SERVICE_EDITHAPUS, SVCHASIL_INPUT, SVCHASIL_LAPORAN, SVCHASIL_EDITHAPUS, DATAKAS, INPUTDP, ACCTAMBAH, ACCDATA, ACCEDITHAPUS, HUTANGDATA, HUTANGLIST, HUTANGBAYAR, PIUTANGDATA, PIUTANGLIST, PIUTANGTERIMA, OTORITAS, QTYNEGLAP, KASTOBANK, COMPLAININPUT, COMPLAINLAP, COMPEDITHAPUS, JADWALINPUT, JADWALLAP, JADWALEDITHAPUS, JADWALHASIL, PERBAIKANINPUT, PERBAIKANLAP, PERBAIKANEDITHAPUS, PERBAIKANHASIL, PERBAIKANHASILLAP, PERBAIKANHASILEDITHAPUS, TEHNISITAMBAH, TEHNISIDATA, TEHNISIEDITHAPUS, DetailSerialEdit, DetailSerialInput, CekDataKas, LapJual_Harga)

        If CBadmin.Checked = True Then
            Dim c As Boolean
            c = a.updateadmin(user.Text)
            If c = True Then
                Console.WriteLine("admin yes")
            Else
                Console.WriteLine("admin failed")
            End If
        End If
        If b = True Then
            MsgBox("Berhasil update otoritas user " & user.Text)
            Me.Close()
        Else
            MsgBox("Gagal update otoritas")
            Exit Sub
        End If

    End Sub

    Private Sub Otoritas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsPengguna
        Dim tb As DataTable
        tb = a.table(user.Text)

        If tb.Rows(0)("BARANGTAMBAH").ToString = "Y" Then
            CBBarangTambah.Checked = True
        Else
            CBBarangTambah.Checked = False
        End If

        If tb.Rows(0)("BARANGDATA").ToString = "Y" Then
            CBBarangData.Checked = True
        Else
            CBBarangData.Checked = False
        End If

        If tb.Rows(0)("BARANGEDITHAPUS").ToString = "Y" Then
            CBBarangEditHapus.Checked = True
        Else
            CBBarangEditHapus.Checked = False
        End If

        If tb.Rows(0)("MUTASI").ToString = "Y" Then
            CBmutasi.Checked = True
        Else
            CBmutasi.Checked = False
        End If

        If tb.Rows(0)("KOREKSI").ToString = "Y" Then
            CBkoreksi.Checked = True
        Else
            CBkoreksi.Checked = False
        End If

        If tb.Rows(0)("KOREKSILAPORAN").ToString = "Y" Then
            CBkoreksiLaporan.Checked = True
        Else
            CBkoreksiLaporan.Checked = False
        End If

        If tb.Rows(0)("PRINTULANGLAPORAN").ToString = "Y" Then
            CBprintulangLaporan.Checked = True
        Else
            CBprintulangLaporan.Checked = False
        End If

        If tb.Rows(0)("SALESTAMBAH").ToString = "Y" Then
            CBSalesTambah.Checked = True
        Else
            CBSalesTambah.Checked = False
        End If

        If tb.Rows(0)("SALESDATA").ToString = "Y" Then
            CBSalesData.Checked = True
        Else
            CBSalesData.Checked = False
        End If

        If tb.Rows(0)("SALESEDITHAPUS").ToString = "Y" Then
            CBSalesEditHapus.Checked = True
        Else
            CBSalesEditHapus.Checked = False
        End If

        If tb.Rows(0)("CUSTOMERTAMBAH").ToString = "Y" Then
            CBCustomerTambah.Checked = True
        Else
            CBCustomerTambah.Checked = False
        End If

        If tb.Rows(0)("CUSTOMERDATA").ToString = "Y" Then
            CBCustomerData.Checked = True
        Else
            CBCustomerData.Checked = False
        End If

        If tb.Rows(0)("CUSTOMEREDITHAPUS").ToString = "Y" Then
            CBCustomerEditHapus.Checked = True
        Else
            CBCustomerEditHapus.Checked = False
        End If

        If tb.Rows(0)("SUPPLIERTAMBAH").ToString = "Y" Then
            CBSupplierTambah.Checked = True
        Else
            CBSupplierTambah.Checked = False
        End If

        If tb.Rows(0)("SUPPLIERDATA").ToString = "Y" Then
            CBSupplierData.Checked = True
        Else
            CBSupplierData.Checked = False
        End If

        If tb.Rows(0)("SUPPLIEREDITHAPUS").ToString = "Y" Then
            CBSupplierEditHapus.Checked = True
        Else
            CBSupplierEditHapus.Checked = False
        End If

        If tb.Rows(0)("USERTAMBAH").ToString = "Y" Then
            CBUserTambah.Checked = True
        Else
            CBUserTambah.Checked = False
        End If

        If tb.Rows(0)("USERDATA").ToString = "Y" Then
            CBUserData.Checked = True
        Else
            CBUserData.Checked = False
        End If

        If tb.Rows(0)("USEREDITHAPUS").ToString = "Y" Then
            CBUserEditHapus.Checked = True
        Else
            CBUserEditHapus.Checked = False
        End If

        If tb.Rows(0)("BELI_INPUT").ToString = "Y" Then
            CBBeli_Input.Checked = True
        Else
            CBBeli_Input.Checked = False
        End If

        If tb.Rows(0)("BELI_LAPORAN").ToString = "Y" Then
            CBBeli_Laporan.Checked = True
        Else
            CBBeli_Laporan.Checked = False
        End If

        If tb.Rows(0)("BELI_EDITHAPUS").ToString = "Y" Then
            CBBeli_EditHapus.Checked = True
        Else
            CBBeli_EditHapus.Checked = False
        End If

        If tb.Rows(0)("RETURBELI_INPUT").ToString = "Y" Then
            CBReturBeli_Input.Checked = True
        Else
            CBReturBeli_Input.Checked = False
        End If

        If tb.Rows(0)("RETURBELI_LAPORAN").ToString = "Y" Then
            CBReturBeli_Laporan.Checked = True
        Else
            CBReturBeli_Laporan.Checked = False
        End If

        If tb.Rows(0)("RETURBELI_EDITHAPUS").ToString = "Y" Then
            CBReturBeli_EditHapus.Checked = True
        Else
            CBReturBeli_EditHapus.Checked = False
        End If

        If tb.Rows(0)("SO_INPUT").ToString = "Y" Then
            CBSO_Input.Checked = True
        Else
            CBSO_Input.Checked = False
        End If

        If tb.Rows(0)("SO_LAPORAN").ToString = "Y" Then
            CBSO_Laporan.Checked = True
        Else
            CBSO_Laporan.Checked = False
        End If

        If tb.Rows(0)("SO_EDITHAPUS").ToString = "Y" Then
            CBSO_EditHapus.Checked = True
        Else
            CBSO_EditHapus.Checked = False
        End If

        If tb.Rows(0)("JUAL_INPUT").ToString = "Y" Then
            CBJual_Input.Checked = True
        Else
            CBJual_Input.Checked = False
        End If

        If tb.Rows(0)("JUAL_LAPORAN").ToString = "Y" Then
            CBJual_Laporan.Checked = True
        Else
            CBJual_Laporan.Checked = False
        End If

        If tb.Rows(0)("JUAL_EDITHAPUS").ToString = "Y" Then
            CBJual_EditHapus.Checked = True
        Else
            CBJual_EditHapus.Checked = False
        End If

        If tb.Rows(0)("RETURJUAL_INPUT").ToString = "Y" Then
            CBReturJual_Input.Checked = True
        Else
            CBReturJual_Input.Checked = False
        End If

        If tb.Rows(0)("RETURJUAL_LAPORAN").ToString = "Y" Then
            CBReturJual_Laporan.Checked = True
        Else
            CBReturJual_Laporan.Checked = False
        End If

        If tb.Rows(0)("RETURJUAL_EDITHAPUS").ToString = "Y" Then
            CBReturJual_EditHapus.Checked = True
        Else
            CBReturJual_EditHapus.Checked = False
        End If

        If tb.Rows(0)("SJ_INPUT").ToString = "Y" Then
            CBSJ_Input.Checked = True
        Else
            CBSJ_Input.Checked = False
        End If

        If tb.Rows(0)("SJ_LAPORAN").ToString = "Y" Then
            CBSJ_Laporan.Checked = True
        Else
            CBSJ_Laporan.Checked = False
        End If

        If tb.Rows(0)("SJ_EDITHAPUS").ToString = "Y" Then
            CBSJ_EditHapus.Checked = True
        Else
            CBSJ_EditHapus.Checked = False
        End If

        If tb.Rows(0)("RETURSJ_INPUT").ToString = "Y" Then
            CBReturSJ_Input.Checked = True
        Else
            CBReturSJ_Input.Checked = False
        End If

        If tb.Rows(0)("RETURSJ_LAPORAN").ToString = "Y" Then
            CBReturSJ_Laporan.Checked = True
        Else
            CBReturSJ_Laporan.Checked = False
        End If

        If tb.Rows(0)("RETURSJ_EDITHAPUS").ToString = "Y" Then
            CBReturSJ_EditHapus.Checked = True
        Else
            CBReturSJ_EditHapus.Checked = False
        End If

        If tb.Rows(0)("FAKTUR_INPUT").ToString = "Y" Then
            CBFaktur_Input.Checked = True
        Else
            CBFaktur_Input.Checked = False
        End If

        If tb.Rows(0)("SERVICE_INPUT").ToString = "Y" Then
            CBService_Input.Checked = True
        Else
            CBService_Input.Checked = False
        End If

        If tb.Rows(0)("SERVICE_LAPORAN").ToString = "Y" Then
            CBService_Laporan.Checked = True
        Else
            CBService_Laporan.Checked = False
        End If

        If tb.Rows(0)("SERVICE_EDITHAPUS").ToString = "Y" Then
            CBService_EditHapus.Checked = True
        Else
            CBService_EditHapus.Checked = False
        End If

        If tb.Rows(0)("SVCHASIL_INPUT").ToString = "Y" Then
            CBSVCHasil_Input.Checked = True
        Else
            CBSVCHasil_Input.Checked = False
        End If

        If tb.Rows(0)("SVCHASIL_LAPORAN").ToString = "Y" Then
            CBSVCHasil_Laporan.Checked = True
        Else
            CBSVCHasil_Laporan.Checked = False
        End If

        If tb.Rows(0)("SVCHASIL_EDITHAPUS").ToString = "Y" Then
            CBSVCHasil_EditHapus.Checked = True
        Else
            CBSVCHasil_EditHapus.Checked = False
        End If

        If tb.Rows(0)("DATAKAS").ToString = "Y" Then
            CBDataKas.Checked = True
        Else
            CBDataKas.Checked = False
        End If

        If tb.Rows(0)("INPUTDP").ToString = "Y" Then
            CBInputDP.Checked = True
        Else
            CBInputDP.Checked = False
        End If

        If tb.Rows(0)("ACCTAMBAH").ToString = "Y" Then
            CBAccTambah.Checked = True
        Else
            CBAccTambah.Checked = False
        End If

        If tb.Rows(0)("ACCDATA").ToString = "Y" Then
            CBAccData.Checked = True
        Else
            CBAccData.Checked = False
        End If

        If tb.Rows(0)("ACCEDITHAPUS").ToString = "Y" Then
            CBAccEditHapus.Checked = True
        Else
            CBAccEditHapus.Checked = False
        End If

        If tb.Rows(0)("HUTANGDATA").ToString = "Y" Then
            CBHutangData.Checked = True
        Else
            CBHutangData.Checked = False
        End If

        If tb.Rows(0)("HUTANGLIST").ToString = "Y" Then
            CBHutangList.Checked = True
        Else
            CBHutangList.Checked = False
        End If

        If tb.Rows(0)("HUTANGBAYAR").ToString = "Y" Then
            CBHutangBayar.Checked = True
        Else
            CBHutangBayar.Checked = False
        End If

        If tb.Rows(0)("PIUTANGDATA").ToString = "Y" Then
            CBPiutangData.Checked = True
        Else
            CBPiutangData.Checked = False
        End If

        If tb.Rows(0)("PIUTANGLIST").ToString = "Y" Then
            CBPiutangList.Checked = True
        Else
            CBPiutangList.Checked = False
        End If

        If tb.Rows(0)("PIUTANGTERIMA").ToString = "Y" Then
            CBPiutangTerima.Checked = True
        Else
            CBPiutangTerima.Checked = False
        End If

        If tb.Rows(0)("OTORITAS").ToString = "Y" Then
            CBOtoritas.Checked = True
        Else
            CBOtoritas.Checked = False
        End If

        If tb.Rows(0)("QTYNEGLAP").ToString = "Y" Then
            CBqtyNegLap.Checked = True
        Else
            CBqtyNegLap.Checked = False
        End If

        If tb.Rows(0)("KASTOBANK").ToString = "Y" Then
            CBkastobank.Checked = True
        Else
            CBkastobank.Checked = False
        End If

        If tb.Rows(0)("COMPLAININPUT").ToString = "Y" Then
            CBComplainInput.Checked = True
        Else
            CBComplainInput.Checked = False
        End If

        If tb.Rows(0)("COMPLAINLAP").ToString = "Y" Then
            CBcomplainLap.Checked = True
        Else
            CBcomplainLap.Checked = False
        End If

        If tb.Rows(0)("COMPEDITHAPUS").ToString = "Y" Then
            CBcompEditHapus.Checked = True
        Else
            CBcompEditHapus.Checked = False
        End If

        If tb.Rows(0)("JADWALINPUT").ToString = "Y" Then
            CBjadwalInput.Checked = True
        Else
            CBjadwalInput.Checked = False
        End If

        If tb.Rows(0)("JADWALLAP").ToString = "Y" Then
            CBjadwalLap.Checked = True
        Else
            CBjadwalLap.Checked = False
        End If

        If tb.Rows(0)("JADWALEDITHAPUS").ToString = "Y" Then
            CBjadwalEditHapus.Checked = True
        Else
            CBjadwalEditHapus.Checked = False
        End If

        If tb.Rows(0)("JADWALHASIL").ToString = "Y" Then
            CBjadwalHasil.Checked = True
        Else
            CBjadwalHasil.Checked = False
        End If

        If tb.Rows(0)("PERBAIKANINPUT").ToString = "Y" Then
            CBperbaikanInput.Checked = True
        Else
            CBperbaikanInput.Checked = False
        End If

        If tb.Rows(0)("PERBAIKANLAP").ToString = "Y" Then
            CBperbaikanLap.Checked = True
        Else
            CBperbaikanLap.Checked = False
        End If

        If tb.Rows(0)("PERBAIKANEDITHAPUS").ToString = "Y" Then
            CBperbaikanEditHapus.Checked = True
        Else
            CBperbaikanEditHapus.Checked = False
        End If

        If tb.Rows(0)("PERBAIKANHASIL").ToString = "Y" Then
            CBperbaikanHasil.Checked = True
        Else
            CBperbaikanHasil.Checked = False
        End If

        If tb.Rows(0)("PERBAIKANHASILLAP").ToString = "Y" Then
            CBperbaikanHasilLap.Checked = True
        Else
            CBperbaikanHasilLap.Checked = False
        End If

        If tb.Rows(0)("PERBAIKANHASILEDITHAPUS").ToString = "Y" Then
            CBPerbaikanHasilEditHapus.Checked = True
        Else
            CBPerbaikanHasilEditHapus.Checked = False
        End If

        If tb.Rows(0)("TEHNISITAMBAH").ToString = "Y" Then
            CBTehnisiTambah.Checked = True
        Else
            CBTehnisiTambah.Checked = False
        End If

        If tb.Rows(0)("TEHNISIDATA").ToString = "Y" Then
            CBTehnisiData.Checked = True
        Else
            CBTehnisiData.Checked = False
        End If

        If tb.Rows(0)("TEHNISIEDITHAPUS").ToString = "Y" Then
            CBTehnisiEditHapus.Checked = True
        Else
            CBTehnisiEditHapus.Checked = False
        End If

        If tb.Rows(0)("DetailSerialInput").ToString = "Y" Then
            CBDetailSerialInput.Checked = True
        Else
            CBDetailSerialInput.Checked = False
        End If

        If tb.Rows(0)("DetailSerialEdit").ToString = "Y" Then
            CBDetailSerialEdit.Checked = True
        Else
            CBDetailSerialEdit.Checked = False
        End If

        If tb.Rows(0)("CekDataKas").ToString = "Y" Then
            CBCekDataKas.Checked = True
        Else
            CBCekDataKas.Checked = False
        End If

        If tb.Rows(0)("LapJual_Harga").ToString = "Y" Then
            CBLapJual_Harga.Checked = True
        Else
            CBLapJual_Harga.Checked = False
        End If

    End Sub

    Private Sub butBTL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBTL.Click
        Me.Close()
    End Sub

End Class