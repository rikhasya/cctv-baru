﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Otoritas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.user = New System.Windows.Forms.TextBox()
        Me.CBBarangData = New System.Windows.Forms.CheckBox()
        Me.CBBarangTambah = New System.Windows.Forms.CheckBox()
        Me.CBBarangEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBCustomerData = New System.Windows.Forms.CheckBox()
        Me.CBCustomerTambah = New System.Windows.Forms.CheckBox()
        Me.CBCustomerEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBSupplierData = New System.Windows.Forms.CheckBox()
        Me.CBSupplierTambah = New System.Windows.Forms.CheckBox()
        Me.CBSupplierEditHapus = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CBkoreksiLaporan = New System.Windows.Forms.CheckBox()
        Me.CBqtyNegLap = New System.Windows.Forms.CheckBox()
        Me.CBkoreksi = New System.Windows.Forms.CheckBox()
        Me.CBmutasi = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.CBSalesEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBSalesData = New System.Windows.Forms.CheckBox()
        Me.CBSalesTambah = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.CBReturBeli_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBReturBeli_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBReturBeli_Input = New System.Windows.Forms.CheckBox()
        Me.CBBeli_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBBeli_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBBeli_Input = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.CBDetailSerialEdit = New System.Windows.Forms.CheckBox()
        Me.CBDetailSerialInput = New System.Windows.Forms.CheckBox()
        Me.CBReturSJ_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBSJ_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBReturSJ_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBReturSJ_Input = New System.Windows.Forms.CheckBox()
        Me.CBFaktur_Input = New System.Windows.Forms.CheckBox()
        Me.CBSJ_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBSJ_Input = New System.Windows.Forms.CheckBox()
        Me.CBSO_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBSO_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBSO_Input = New System.Windows.Forms.CheckBox()
        Me.CBReturJual_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBReturJual_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBReturJual_Input = New System.Windows.Forms.CheckBox()
        Me.CBJual_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBJual_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBJual_Input = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.CBHutangBayar = New System.Windows.Forms.CheckBox()
        Me.CBHutangList = New System.Windows.Forms.CheckBox()
        Me.CBHutangData = New System.Windows.Forms.CheckBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.CBPiutangTerima = New System.Windows.Forms.CheckBox()
        Me.CBPiutangList = New System.Windows.Forms.CheckBox()
        Me.CBPiutangData = New System.Windows.Forms.CheckBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.CBAccEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBAccData = New System.Windows.Forms.CheckBox()
        Me.CBAccTambah = New System.Windows.Forms.CheckBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.CBCekDataKas = New System.Windows.Forms.CheckBox()
        Me.CBInputDP = New System.Windows.Forms.CheckBox()
        Me.CBkastobank = New System.Windows.Forms.CheckBox()
        Me.CBDataKas = New System.Windows.Forms.CheckBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.CBOtoritas = New System.Windows.Forms.CheckBox()
        Me.CBUserEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBUserData = New System.Windows.Forms.CheckBox()
        Me.CBUserTambah = New System.Windows.Forms.CheckBox()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.CBPerbaikanHasilEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBperbaikanHasilLap = New System.Windows.Forms.CheckBox()
        Me.CBperbaikanHasil = New System.Windows.Forms.CheckBox()
        Me.CBjadwalHasil = New System.Windows.Forms.CheckBox()
        Me.CBperbaikanEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBperbaikanLap = New System.Windows.Forms.CheckBox()
        Me.CBperbaikanInput = New System.Windows.Forms.CheckBox()
        Me.CBjadwalEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBjadwalLap = New System.Windows.Forms.CheckBox()
        Me.CBjadwalInput = New System.Windows.Forms.CheckBox()
        Me.CBcompEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBcomplainLap = New System.Windows.Forms.CheckBox()
        Me.CBComplainInput = New System.Windows.Forms.CheckBox()
        Me.CBSVCHasil_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBSVCHasil_Laporan = New System.Windows.Forms.CheckBox()
        Me.CBSVCHasil_Input = New System.Windows.Forms.CheckBox()
        Me.CBService_EditHapus = New System.Windows.Forms.CheckBox()
        Me.CBService_Input = New System.Windows.Forms.CheckBox()
        Me.CBService_Laporan = New System.Windows.Forms.CheckBox()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.CBprintulangLaporan = New System.Windows.Forms.CheckBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.CBadmin = New System.Windows.Forms.CheckBox()
        Me.butBTL = New System.Windows.Forms.Button()
        Me.butUPDT = New System.Windows.Forms.Button()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.CBTehnisiEditHapus = New System.Windows.Forms.CheckBox()
        Me.CBTehnisiData = New System.Windows.Forms.CheckBox()
        Me.CBTehnisiTambah = New System.Windows.Forms.CheckBox()
        Me.CBLapJual_Harga = New System.Windows.Forms.CheckBox()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(147, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(357, 55)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.SandyBrown
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(53, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "OTORITAS"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Chocolate
        Me.Panel2.Controls.Add(Me.user)
        Me.Panel2.Location = New System.Drawing.Point(167, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(189, 55)
        Me.Panel2.TabIndex = 0
        '
        'user
        '
        Me.user.Enabled = False
        Me.user.Location = New System.Drawing.Point(14, 22)
        Me.user.Name = "user"
        Me.user.Size = New System.Drawing.Size(145, 20)
        Me.user.TabIndex = 0
        '
        'CBBarangData
        '
        Me.CBBarangData.AutoSize = True
        Me.CBBarangData.Location = New System.Drawing.Point(15, 20)
        Me.CBBarangData.Name = "CBBarangData"
        Me.CBBarangData.Size = New System.Drawing.Size(92, 17)
        Me.CBBarangData.TabIndex = 1
        Me.CBBarangData.Text = "Daftar Barang"
        Me.CBBarangData.UseVisualStyleBackColor = True
        '
        'CBBarangTambah
        '
        Me.CBBarangTambah.AutoSize = True
        Me.CBBarangTambah.Location = New System.Drawing.Point(15, 43)
        Me.CBBarangTambah.Name = "CBBarangTambah"
        Me.CBBarangTambah.Size = New System.Drawing.Size(102, 17)
        Me.CBBarangTambah.TabIndex = 2
        Me.CBBarangTambah.Text = "Tambah Barang"
        Me.CBBarangTambah.UseVisualStyleBackColor = True
        '
        'CBBarangEditHapus
        '
        Me.CBBarangEditHapus.AutoSize = True
        Me.CBBarangEditHapus.Location = New System.Drawing.Point(15, 66)
        Me.CBBarangEditHapus.Name = "CBBarangEditHapus"
        Me.CBBarangEditHapus.Size = New System.Drawing.Size(115, 17)
        Me.CBBarangEditHapus.TabIndex = 3
        Me.CBBarangEditHapus.Text = "Edit Hapus Barang"
        Me.CBBarangEditHapus.UseVisualStyleBackColor = True
        '
        'CBCustomerData
        '
        Me.CBCustomerData.AutoSize = True
        Me.CBCustomerData.Location = New System.Drawing.Point(15, 21)
        Me.CBCustomerData.Name = "CBCustomerData"
        Me.CBCustomerData.Size = New System.Drawing.Size(102, 17)
        Me.CBCustomerData.TabIndex = 4
        Me.CBCustomerData.Text = "Daftar Customer"
        Me.CBCustomerData.UseVisualStyleBackColor = True
        '
        'CBCustomerTambah
        '
        Me.CBCustomerTambah.AutoSize = True
        Me.CBCustomerTambah.Location = New System.Drawing.Point(15, 44)
        Me.CBCustomerTambah.Name = "CBCustomerTambah"
        Me.CBCustomerTambah.Size = New System.Drawing.Size(112, 17)
        Me.CBCustomerTambah.TabIndex = 5
        Me.CBCustomerTambah.Text = "Tambah Customer"
        Me.CBCustomerTambah.UseVisualStyleBackColor = True
        '
        'CBCustomerEditHapus
        '
        Me.CBCustomerEditHapus.AutoSize = True
        Me.CBCustomerEditHapus.Location = New System.Drawing.Point(15, 67)
        Me.CBCustomerEditHapus.Name = "CBCustomerEditHapus"
        Me.CBCustomerEditHapus.Size = New System.Drawing.Size(125, 17)
        Me.CBCustomerEditHapus.TabIndex = 6
        Me.CBCustomerEditHapus.Text = "Edit Hapus Customer"
        Me.CBCustomerEditHapus.UseVisualStyleBackColor = True
        '
        'CBSupplierData
        '
        Me.CBSupplierData.AutoSize = True
        Me.CBSupplierData.Location = New System.Drawing.Point(15, 18)
        Me.CBSupplierData.Name = "CBSupplierData"
        Me.CBSupplierData.Size = New System.Drawing.Size(96, 17)
        Me.CBSupplierData.TabIndex = 7
        Me.CBSupplierData.Text = "Daftar Supplier"
        Me.CBSupplierData.UseVisualStyleBackColor = True
        '
        'CBSupplierTambah
        '
        Me.CBSupplierTambah.AutoSize = True
        Me.CBSupplierTambah.Location = New System.Drawing.Point(15, 41)
        Me.CBSupplierTambah.Name = "CBSupplierTambah"
        Me.CBSupplierTambah.Size = New System.Drawing.Size(106, 17)
        Me.CBSupplierTambah.TabIndex = 8
        Me.CBSupplierTambah.Text = "Tambah Supplier"
        Me.CBSupplierTambah.UseVisualStyleBackColor = True
        '
        'CBSupplierEditHapus
        '
        Me.CBSupplierEditHapus.AutoSize = True
        Me.CBSupplierEditHapus.Location = New System.Drawing.Point(15, 64)
        Me.CBSupplierEditHapus.Name = "CBSupplierEditHapus"
        Me.CBSupplierEditHapus.Size = New System.Drawing.Size(119, 17)
        Me.CBSupplierEditHapus.TabIndex = 9
        Me.CBSupplierEditHapus.Text = "Edit Hapus Supplier"
        Me.CBSupplierEditHapus.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.NavajoWhite
        Me.GroupBox1.Controls.Add(Me.CBkoreksiLaporan)
        Me.GroupBox1.Controls.Add(Me.CBqtyNegLap)
        Me.GroupBox1.Controls.Add(Me.CBkoreksi)
        Me.GroupBox1.Controls.Add(Me.CBmutasi)
        Me.GroupBox1.Controls.Add(Me.CBBarangEditHapus)
        Me.GroupBox1.Controls.Add(Me.CBBarangData)
        Me.GroupBox1.Controls.Add(Me.CBBarangTambah)
        Me.GroupBox1.Location = New System.Drawing.Point(834, 111)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(146, 177)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "BARANG"
        '
        'CBkoreksiLaporan
        '
        Me.CBkoreksiLaporan.AutoSize = True
        Me.CBkoreksiLaporan.Location = New System.Drawing.Point(15, 132)
        Me.CBkoreksiLaporan.Name = "CBkoreksiLaporan"
        Me.CBkoreksiLaporan.Size = New System.Drawing.Size(103, 17)
        Me.CBkoreksiLaporan.TabIndex = 6
        Me.CBkoreksiLaporan.Text = "Laporan Koreksi"
        Me.CBkoreksiLaporan.UseVisualStyleBackColor = True
        '
        'CBqtyNegLap
        '
        Me.CBqtyNegLap.AutoSize = True
        Me.CBqtyNegLap.Location = New System.Drawing.Point(15, 155)
        Me.CBqtyNegLap.Name = "CBqtyNegLap"
        Me.CBqtyNegLap.Size = New System.Drawing.Size(88, 17)
        Me.CBqtyNegLap.TabIndex = 13
        Me.CBqtyNegLap.Text = "Qty Negative"
        Me.CBqtyNegLap.UseVisualStyleBackColor = True
        '
        'CBkoreksi
        '
        Me.CBkoreksi.AutoSize = True
        Me.CBkoreksi.Location = New System.Drawing.Point(15, 110)
        Me.CBkoreksi.Name = "CBkoreksi"
        Me.CBkoreksi.Size = New System.Drawing.Size(61, 17)
        Me.CBkoreksi.TabIndex = 5
        Me.CBkoreksi.Text = "Koreksi"
        Me.CBkoreksi.UseVisualStyleBackColor = True
        '
        'CBmutasi
        '
        Me.CBmutasi.AutoSize = True
        Me.CBmutasi.Location = New System.Drawing.Point(15, 88)
        Me.CBmutasi.Name = "CBmutasi"
        Me.CBmutasi.Size = New System.Drawing.Size(57, 17)
        Me.CBmutasi.TabIndex = 4
        Me.CBmutasi.Text = "Mutasi"
        Me.CBmutasi.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.AntiqueWhite
        Me.GroupBox2.Controls.Add(Me.CBCustomerEditHapus)
        Me.GroupBox2.Controls.Add(Me.CBCustomerData)
        Me.GroupBox2.Controls.Add(Me.CBCustomerTambah)
        Me.GroupBox2.Location = New System.Drawing.Point(21, 230)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(146, 89)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "CUSTOMER"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.MistyRose
        Me.GroupBox3.Controls.Add(Me.CBSupplierEditHapus)
        Me.GroupBox3.Controls.Add(Me.CBSupplierData)
        Me.GroupBox3.Controls.Add(Me.CBSupplierTambah)
        Me.GroupBox3.Location = New System.Drawing.Point(21, 321)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(146, 87)
        Me.GroupBox3.TabIndex = 12
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "SUPPLIER"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.GroupBox4.Controls.Add(Me.CBSalesEditHapus)
        Me.GroupBox4.Controls.Add(Me.CBSalesData)
        Me.GroupBox4.Controls.Add(Me.CBSalesTambah)
        Me.GroupBox4.Location = New System.Drawing.Point(21, 410)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(146, 90)
        Me.GroupBox4.TabIndex = 13
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "SALES"
        '
        'CBSalesEditHapus
        '
        Me.CBSalesEditHapus.AutoSize = True
        Me.CBSalesEditHapus.Location = New System.Drawing.Point(16, 65)
        Me.CBSalesEditHapus.Name = "CBSalesEditHapus"
        Me.CBSalesEditHapus.Size = New System.Drawing.Size(107, 17)
        Me.CBSalesEditHapus.TabIndex = 12
        Me.CBSalesEditHapus.Text = "Edit Hapus Sales"
        Me.CBSalesEditHapus.UseVisualStyleBackColor = True
        '
        'CBSalesData
        '
        Me.CBSalesData.AutoSize = True
        Me.CBSalesData.Location = New System.Drawing.Point(16, 19)
        Me.CBSalesData.Name = "CBSalesData"
        Me.CBSalesData.Size = New System.Drawing.Size(84, 17)
        Me.CBSalesData.TabIndex = 10
        Me.CBSalesData.Text = "Daftar Sales"
        Me.CBSalesData.UseVisualStyleBackColor = True
        '
        'CBSalesTambah
        '
        Me.CBSalesTambah.AutoSize = True
        Me.CBSalesTambah.Location = New System.Drawing.Point(16, 42)
        Me.CBSalesTambah.Name = "CBSalesTambah"
        Me.CBSalesTambah.Size = New System.Drawing.Size(94, 17)
        Me.CBSalesTambah.TabIndex = 11
        Me.CBSalesTambah.Text = "Tambah Sales"
        Me.CBSalesTambah.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.LightGoldenrodYellow
        Me.GroupBox5.Controls.Add(Me.CBReturBeli_EditHapus)
        Me.GroupBox5.Controls.Add(Me.CBReturBeli_Laporan)
        Me.GroupBox5.Controls.Add(Me.CBReturBeli_Input)
        Me.GroupBox5.Controls.Add(Me.CBBeli_EditHapus)
        Me.GroupBox5.Controls.Add(Me.CBBeli_Laporan)
        Me.GroupBox5.Controls.Add(Me.CBBeli_Input)
        Me.GroupBox5.Location = New System.Drawing.Point(672, 111)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(150, 163)
        Me.GroupBox5.TabIndex = 14
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "PEMBELIAN"
        '
        'CBReturBeli_EditHapus
        '
        Me.CBReturBeli_EditHapus.AutoSize = True
        Me.CBReturBeli_EditHapus.Location = New System.Drawing.Point(15, 134)
        Me.CBReturBeli_EditHapus.Name = "CBReturBeli_EditHapus"
        Me.CBReturBeli_EditHapus.Size = New System.Drawing.Size(109, 17)
        Me.CBReturBeli_EditHapus.TabIndex = 20
        Me.CBReturBeli_EditHapus.Text = "Edit/Hapus Retur"
        Me.CBReturBeli_EditHapus.UseVisualStyleBackColor = True
        '
        'CBReturBeli_Laporan
        '
        Me.CBReturBeli_Laporan.AutoSize = True
        Me.CBReturBeli_Laporan.Location = New System.Drawing.Point(15, 111)
        Me.CBReturBeli_Laporan.Name = "CBReturBeli_Laporan"
        Me.CBReturBeli_Laporan.Size = New System.Drawing.Size(94, 17)
        Me.CBReturBeli_Laporan.TabIndex = 19
        Me.CBReturBeli_Laporan.Text = "Laporan Retur"
        Me.CBReturBeli_Laporan.UseVisualStyleBackColor = True
        '
        'CBReturBeli_Input
        '
        Me.CBReturBeli_Input.AutoSize = True
        Me.CBReturBeli_Input.Location = New System.Drawing.Point(15, 88)
        Me.CBReturBeli_Input.Name = "CBReturBeli_Input"
        Me.CBReturBeli_Input.Size = New System.Drawing.Size(131, 17)
        Me.CBReturBeli_Input.TabIndex = 18
        Me.CBReturBeli_Input.Text = "Input Retur Pembelian"
        Me.CBReturBeli_Input.UseVisualStyleBackColor = True
        '
        'CBBeli_EditHapus
        '
        Me.CBBeli_EditHapus.AutoSize = True
        Me.CBBeli_EditHapus.Location = New System.Drawing.Point(15, 65)
        Me.CBBeli_EditHapus.Name = "CBBeli_EditHapus"
        Me.CBBeli_EditHapus.Size = New System.Drawing.Size(132, 17)
        Me.CBBeli_EditHapus.TabIndex = 17
        Me.CBBeli_EditHapus.Text = "Edit/Hapus Pembelian"
        Me.CBBeli_EditHapus.UseVisualStyleBackColor = True
        '
        'CBBeli_Laporan
        '
        Me.CBBeli_Laporan.AutoSize = True
        Me.CBBeli_Laporan.Location = New System.Drawing.Point(15, 42)
        Me.CBBeli_Laporan.Name = "CBBeli_Laporan"
        Me.CBBeli_Laporan.Size = New System.Drawing.Size(117, 17)
        Me.CBBeli_Laporan.TabIndex = 16
        Me.CBBeli_Laporan.Text = "Laporan Pembelian"
        Me.CBBeli_Laporan.UseVisualStyleBackColor = True
        '
        'CBBeli_Input
        '
        Me.CBBeli_Input.AutoSize = True
        Me.CBBeli_Input.Location = New System.Drawing.Point(15, 19)
        Me.CBBeli_Input.Name = "CBBeli_Input"
        Me.CBBeli_Input.Size = New System.Drawing.Size(102, 17)
        Me.CBBeli_Input.TabIndex = 15
        Me.CBBeli_Input.Text = "Input Pembelian"
        Me.CBBeli_Input.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Cornsilk
        Me.GroupBox6.Controls.Add(Me.CBLapJual_Harga)
        Me.GroupBox6.Controls.Add(Me.CBDetailSerialEdit)
        Me.GroupBox6.Controls.Add(Me.CBDetailSerialInput)
        Me.GroupBox6.Controls.Add(Me.CBReturSJ_Laporan)
        Me.GroupBox6.Controls.Add(Me.CBSJ_Laporan)
        Me.GroupBox6.Controls.Add(Me.CBReturSJ_EditHapus)
        Me.GroupBox6.Controls.Add(Me.CBReturSJ_Input)
        Me.GroupBox6.Controls.Add(Me.CBFaktur_Input)
        Me.GroupBox6.Controls.Add(Me.CBSJ_EditHapus)
        Me.GroupBox6.Controls.Add(Me.CBSJ_Input)
        Me.GroupBox6.Controls.Add(Me.CBSO_EditHapus)
        Me.GroupBox6.Controls.Add(Me.CBSO_Laporan)
        Me.GroupBox6.Controls.Add(Me.CBSO_Input)
        Me.GroupBox6.Controls.Add(Me.CBReturJual_EditHapus)
        Me.GroupBox6.Controls.Add(Me.CBReturJual_Laporan)
        Me.GroupBox6.Controls.Add(Me.CBReturJual_Input)
        Me.GroupBox6.Controls.Add(Me.CBJual_EditHapus)
        Me.GroupBox6.Controls.Add(Me.CBJual_Laporan)
        Me.GroupBox6.Controls.Add(Me.CBJual_Input)
        Me.GroupBox6.Location = New System.Drawing.Point(343, 111)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(321, 245)
        Me.GroupBox6.TabIndex = 15
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "PENJUALAN"
        '
        'CBDetailSerialEdit
        '
        Me.CBDetailSerialEdit.AutoSize = True
        Me.CBDetailSerialEdit.Location = New System.Drawing.Point(174, 202)
        Me.CBDetailSerialEdit.Name = "CBDetailSerialEdit"
        Me.CBDetailSerialEdit.Size = New System.Drawing.Size(116, 17)
        Me.CBDetailSerialEdit.TabIndex = 33
        Me.CBDetailSerialEdit.Text = "Hapus Detail Serial"
        Me.CBDetailSerialEdit.UseVisualStyleBackColor = True
        '
        'CBDetailSerialInput
        '
        Me.CBDetailSerialInput.AutoSize = True
        Me.CBDetailSerialInput.Location = New System.Drawing.Point(174, 179)
        Me.CBDetailSerialInput.Name = "CBDetailSerialInput"
        Me.CBDetailSerialInput.Size = New System.Drawing.Size(109, 17)
        Me.CBDetailSerialInput.TabIndex = 32
        Me.CBDetailSerialInput.Text = "Input Detail Serial"
        Me.CBDetailSerialInput.UseVisualStyleBackColor = True
        '
        'CBReturSJ_Laporan
        '
        Me.CBReturSJ_Laporan.AutoSize = True
        Me.CBReturSJ_Laporan.Location = New System.Drawing.Point(174, 110)
        Me.CBReturSJ_Laporan.Name = "CBReturSJ_Laporan"
        Me.CBReturSJ_Laporan.Size = New System.Drawing.Size(109, 17)
        Me.CBReturSJ_Laporan.TabIndex = 31
        Me.CBReturSJ_Laporan.Text = "Laporan Retur SJ"
        Me.CBReturSJ_Laporan.UseVisualStyleBackColor = True
        '
        'CBSJ_Laporan
        '
        Me.CBSJ_Laporan.AutoSize = True
        Me.CBSJ_Laporan.Location = New System.Drawing.Point(174, 66)
        Me.CBSJ_Laporan.Name = "CBSJ_Laporan"
        Me.CBSJ_Laporan.Size = New System.Drawing.Size(121, 17)
        Me.CBSJ_Laporan.TabIndex = 30
        Me.CBSJ_Laporan.Text = "Laporan Surat Jalan"
        Me.CBSJ_Laporan.UseVisualStyleBackColor = True
        '
        'CBReturSJ_EditHapus
        '
        Me.CBReturSJ_EditHapus.AutoSize = True
        Me.CBReturSJ_EditHapus.Location = New System.Drawing.Point(174, 133)
        Me.CBReturSJ_EditHapus.Name = "CBReturSJ_EditHapus"
        Me.CBReturSJ_EditHapus.Size = New System.Drawing.Size(142, 17)
        Me.CBReturSJ_EditHapus.TabIndex = 29
        Me.CBReturSJ_EditHapus.Text = "Hapus Retur Surat Jalan"
        Me.CBReturSJ_EditHapus.UseVisualStyleBackColor = True
        '
        'CBReturSJ_Input
        '
        Me.CBReturSJ_Input.AutoSize = True
        Me.CBReturSJ_Input.Location = New System.Drawing.Point(174, 87)
        Me.CBReturSJ_Input.Name = "CBReturSJ_Input"
        Me.CBReturSJ_Input.Size = New System.Drawing.Size(135, 17)
        Me.CBReturSJ_Input.TabIndex = 28
        Me.CBReturSJ_Input.Text = "Input Retur Surat Jalan"
        Me.CBReturSJ_Input.UseVisualStyleBackColor = True
        '
        'CBFaktur_Input
        '
        Me.CBFaktur_Input.AutoSize = True
        Me.CBFaktur_Input.Location = New System.Drawing.Point(174, 156)
        Me.CBFaktur_Input.Name = "CBFaktur_Input"
        Me.CBFaktur_Input.Size = New System.Drawing.Size(119, 17)
        Me.CBFaktur_Input.TabIndex = 26
        Me.CBFaktur_Input.Text = "Input Faktur Project"
        Me.CBFaktur_Input.UseVisualStyleBackColor = True
        '
        'CBSJ_EditHapus
        '
        Me.CBSJ_EditHapus.AutoSize = True
        Me.CBSJ_EditHapus.Location = New System.Drawing.Point(174, 43)
        Me.CBSJ_EditHapus.Name = "CBSJ_EditHapus"
        Me.CBSJ_EditHapus.Size = New System.Drawing.Size(113, 17)
        Me.CBSJ_EditHapus.TabIndex = 25
        Me.CBSJ_EditHapus.Text = "Hapus Surat Jalan"
        Me.CBSJ_EditHapus.UseVisualStyleBackColor = True
        '
        'CBSJ_Input
        '
        Me.CBSJ_Input.AutoSize = True
        Me.CBSJ_Input.Location = New System.Drawing.Point(174, 20)
        Me.CBSJ_Input.Name = "CBSJ_Input"
        Me.CBSJ_Input.Size = New System.Drawing.Size(106, 17)
        Me.CBSJ_Input.TabIndex = 24
        Me.CBSJ_Input.Text = "Input Surat Jalan"
        Me.CBSJ_Input.UseVisualStyleBackColor = True
        '
        'CBSO_EditHapus
        '
        Me.CBSO_EditHapus.AutoSize = True
        Me.CBSO_EditHapus.Location = New System.Drawing.Point(14, 64)
        Me.CBSO_EditHapus.Name = "CBSO_EditHapus"
        Me.CBSO_EditHapus.Size = New System.Drawing.Size(151, 17)
        Me.CBSO_EditHapus.TabIndex = 23
        Me.CBSO_EditHapus.Text = "Edit/Hapus SO  Penjualan"
        Me.CBSO_EditHapus.UseVisualStyleBackColor = True
        '
        'CBSO_Laporan
        '
        Me.CBSO_Laporan.AutoSize = True
        Me.CBSO_Laporan.Location = New System.Drawing.Point(14, 41)
        Me.CBSO_Laporan.Name = "CBSO_Laporan"
        Me.CBSO_Laporan.Size = New System.Drawing.Size(133, 17)
        Me.CBSO_Laporan.TabIndex = 22
        Me.CBSO_Laporan.Text = "Laporan SO Penjualan"
        Me.CBSO_Laporan.UseVisualStyleBackColor = True
        '
        'CBSO_Input
        '
        Me.CBSO_Input.AutoSize = True
        Me.CBSO_Input.Location = New System.Drawing.Point(14, 18)
        Me.CBSO_Input.Name = "CBSO_Input"
        Me.CBSO_Input.Size = New System.Drawing.Size(118, 17)
        Me.CBSO_Input.TabIndex = 21
        Me.CBSO_Input.Text = "Input SO Penjualan"
        Me.CBSO_Input.UseVisualStyleBackColor = True
        '
        'CBReturJual_EditHapus
        '
        Me.CBReturJual_EditHapus.AutoSize = True
        Me.CBReturJual_EditHapus.Location = New System.Drawing.Point(14, 221)
        Me.CBReturJual_EditHapus.Name = "CBReturJual_EditHapus"
        Me.CBReturJual_EditHapus.Size = New System.Drawing.Size(109, 17)
        Me.CBReturJual_EditHapus.TabIndex = 20
        Me.CBReturJual_EditHapus.Text = "Edit/Hapus Retur"
        Me.CBReturJual_EditHapus.UseVisualStyleBackColor = True
        '
        'CBReturJual_Laporan
        '
        Me.CBReturJual_Laporan.AutoSize = True
        Me.CBReturJual_Laporan.Location = New System.Drawing.Point(14, 200)
        Me.CBReturJual_Laporan.Name = "CBReturJual_Laporan"
        Me.CBReturJual_Laporan.Size = New System.Drawing.Size(94, 17)
        Me.CBReturJual_Laporan.TabIndex = 19
        Me.CBReturJual_Laporan.Text = "Laporan Retur"
        Me.CBReturJual_Laporan.UseVisualStyleBackColor = True
        '
        'CBReturJual_Input
        '
        Me.CBReturJual_Input.AutoSize = True
        Me.CBReturJual_Input.Location = New System.Drawing.Point(14, 177)
        Me.CBReturJual_Input.Name = "CBReturJual_Input"
        Me.CBReturJual_Input.Size = New System.Drawing.Size(129, 17)
        Me.CBReturJual_Input.TabIndex = 18
        Me.CBReturJual_Input.Text = "Input Retur Penjualan"
        Me.CBReturJual_Input.UseVisualStyleBackColor = True
        '
        'CBJual_EditHapus
        '
        Me.CBJual_EditHapus.AutoSize = True
        Me.CBJual_EditHapus.Location = New System.Drawing.Point(14, 154)
        Me.CBJual_EditHapus.Name = "CBJual_EditHapus"
        Me.CBJual_EditHapus.Size = New System.Drawing.Size(130, 17)
        Me.CBJual_EditHapus.TabIndex = 17
        Me.CBJual_EditHapus.Text = "Edit/Hapus Penjualan"
        Me.CBJual_EditHapus.UseVisualStyleBackColor = True
        '
        'CBJual_Laporan
        '
        Me.CBJual_Laporan.AutoSize = True
        Me.CBJual_Laporan.Location = New System.Drawing.Point(14, 109)
        Me.CBJual_Laporan.Name = "CBJual_Laporan"
        Me.CBJual_Laporan.Size = New System.Drawing.Size(115, 17)
        Me.CBJual_Laporan.TabIndex = 16
        Me.CBJual_Laporan.Text = "Laporan Penjualan"
        Me.CBJual_Laporan.UseVisualStyleBackColor = True
        '
        'CBJual_Input
        '
        Me.CBJual_Input.AutoSize = True
        Me.CBJual_Input.Location = New System.Drawing.Point(14, 86)
        Me.CBJual_Input.Name = "CBJual_Input"
        Me.CBJual_Input.Size = New System.Drawing.Size(100, 17)
        Me.CBJual_Input.TabIndex = 15
        Me.CBJual_Input.Text = "Input Penjualan"
        Me.CBJual_Input.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.CBHutangBayar)
        Me.GroupBox7.Controls.Add(Me.CBHutangList)
        Me.GroupBox7.Controls.Add(Me.CBHutangData)
        Me.GroupBox7.Location = New System.Drawing.Point(6, 164)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(146, 89)
        Me.GroupBox7.TabIndex = 16
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Hutang"
        '
        'CBHutangBayar
        '
        Me.CBHutangBayar.AutoSize = True
        Me.CBHutangBayar.Location = New System.Drawing.Point(15, 67)
        Me.CBHutangBayar.Name = "CBHutangBayar"
        Me.CBHutangBayar.Size = New System.Drawing.Size(123, 17)
        Me.CBHutangBayar.TabIndex = 9
        Me.CBHutangBayar.Text = "Pembayaran Hutang"
        Me.CBHutangBayar.UseVisualStyleBackColor = True
        '
        'CBHutangList
        '
        Me.CBHutangList.AutoSize = True
        Me.CBHutangList.Location = New System.Drawing.Point(15, 21)
        Me.CBHutangList.Name = "CBHutangList"
        Me.CBHutangList.Size = New System.Drawing.Size(80, 17)
        Me.CBHutangList.TabIndex = 7
        Me.CBHutangList.Text = "List Hutang"
        Me.CBHutangList.UseVisualStyleBackColor = True
        '
        'CBHutangData
        '
        Me.CBHutangData.AutoSize = True
        Me.CBHutangData.Location = New System.Drawing.Point(15, 44)
        Me.CBHutangData.Name = "CBHutangData"
        Me.CBHutangData.Size = New System.Drawing.Size(87, 17)
        Me.CBHutangData.TabIndex = 8
        Me.CBHutangData.Text = "Data Hutang"
        Me.CBHutangData.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.CBPiutangTerima)
        Me.GroupBox8.Controls.Add(Me.CBPiutangList)
        Me.GroupBox8.Controls.Add(Me.CBPiutangData)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 264)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(146, 90)
        Me.GroupBox8.TabIndex = 17
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Piutang"
        '
        'CBPiutangTerima
        '
        Me.CBPiutangTerima.AutoSize = True
        Me.CBPiutangTerima.Location = New System.Drawing.Point(15, 67)
        Me.CBPiutangTerima.Name = "CBPiutangTerima"
        Me.CBPiutangTerima.Size = New System.Drawing.Size(124, 17)
        Me.CBPiutangTerima.TabIndex = 9
        Me.CBPiutangTerima.Text = "Pembayaran Piutang"
        Me.CBPiutangTerima.UseVisualStyleBackColor = True
        '
        'CBPiutangList
        '
        Me.CBPiutangList.AutoSize = True
        Me.CBPiutangList.Location = New System.Drawing.Point(15, 21)
        Me.CBPiutangList.Name = "CBPiutangList"
        Me.CBPiutangList.Size = New System.Drawing.Size(81, 17)
        Me.CBPiutangList.TabIndex = 7
        Me.CBPiutangList.Text = "List Piutang"
        Me.CBPiutangList.UseVisualStyleBackColor = True
        '
        'CBPiutangData
        '
        Me.CBPiutangData.AutoSize = True
        Me.CBPiutangData.Location = New System.Drawing.Point(15, 44)
        Me.CBPiutangData.Name = "CBPiutangData"
        Me.CBPiutangData.Size = New System.Drawing.Size(88, 17)
        Me.CBPiutangData.TabIndex = 8
        Me.CBPiutangData.Text = "Data Piutang"
        Me.CBPiutangData.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.CBAccEditHapus)
        Me.GroupBox9.Controls.Add(Me.CBAccData)
        Me.GroupBox9.Controls.Add(Me.CBAccTambah)
        Me.GroupBox9.Location = New System.Drawing.Point(6, 67)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(146, 86)
        Me.GroupBox9.TabIndex = 18
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Account"
        '
        'CBAccEditHapus
        '
        Me.CBAccEditHapus.AutoSize = True
        Me.CBAccEditHapus.Location = New System.Drawing.Point(15, 67)
        Me.CBAccEditHapus.Name = "CBAccEditHapus"
        Me.CBAccEditHapus.Size = New System.Drawing.Size(102, 17)
        Me.CBAccEditHapus.TabIndex = 9
        Me.CBAccEditHapus.Text = "Edit/Hapus Acc"
        Me.CBAccEditHapus.UseVisualStyleBackColor = True
        '
        'CBAccData
        '
        Me.CBAccData.AutoSize = True
        Me.CBAccData.Location = New System.Drawing.Point(15, 21)
        Me.CBAccData.Name = "CBAccData"
        Me.CBAccData.Size = New System.Drawing.Size(85, 17)
        Me.CBAccData.TabIndex = 7
        Me.CBAccData.Text = "List Account"
        Me.CBAccData.UseVisualStyleBackColor = True
        '
        'CBAccTambah
        '
        Me.CBAccTambah.AutoSize = True
        Me.CBAccTambah.Location = New System.Drawing.Point(15, 44)
        Me.CBAccTambah.Name = "CBAccTambah"
        Me.CBAccTambah.Size = New System.Drawing.Size(93, 17)
        Me.CBAccTambah.TabIndex = 8
        Me.CBAccTambah.Text = "Input Account"
        Me.CBAccTambah.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.BackColor = System.Drawing.Color.Moccasin
        Me.GroupBox10.Controls.Add(Me.CBCekDataKas)
        Me.GroupBox10.Controls.Add(Me.CBInputDP)
        Me.GroupBox10.Controls.Add(Me.CBkastobank)
        Me.GroupBox10.Controls.Add(Me.CBDataKas)
        Me.GroupBox10.Controls.Add(Me.GroupBox9)
        Me.GroupBox10.Controls.Add(Me.GroupBox7)
        Me.GroupBox10.Controls.Add(Me.GroupBox8)
        Me.GroupBox10.Location = New System.Drawing.Point(177, 113)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(159, 360)
        Me.GroupBox10.TabIndex = 19
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "AKUNTING"
        '
        'CBCekDataKas
        '
        Me.CBCekDataKas.AutoSize = True
        Me.CBCekDataKas.Location = New System.Drawing.Point(75, 44)
        Me.CBCekDataKas.Name = "CBCekDataKas"
        Me.CBCekDataKas.Size = New System.Drawing.Size(66, 17)
        Me.CBCekDataKas.TabIndex = 20
        Me.CBCekDataKas.Text = "Cek Kas"
        Me.CBCekDataKas.UseVisualStyleBackColor = True
        '
        'CBInputDP
        '
        Me.CBInputDP.AutoSize = True
        Me.CBInputDP.Location = New System.Drawing.Point(6, 43)
        Me.CBInputDP.Name = "CBInputDP"
        Me.CBInputDP.Size = New System.Drawing.Size(68, 17)
        Me.CBInputDP.TabIndex = 19
        Me.CBInputDP.Text = "Input DP"
        Me.CBInputDP.UseVisualStyleBackColor = True
        '
        'CBkastobank
        '
        Me.CBkastobank.AutoSize = True
        Me.CBkastobank.Location = New System.Drawing.Point(75, 20)
        Me.CBkastobank.Name = "CBkastobank"
        Me.CBkastobank.Size = New System.Drawing.Size(71, 17)
        Me.CBkastobank.TabIndex = 14
        Me.CBkastobank.Text = "Input Kas"
        Me.CBkastobank.UseVisualStyleBackColor = True
        '
        'CBDataKas
        '
        Me.CBDataKas.AutoSize = True
        Me.CBDataKas.Location = New System.Drawing.Point(6, 20)
        Me.CBDataKas.Name = "CBDataKas"
        Me.CBDataKas.Size = New System.Drawing.Size(70, 17)
        Me.CBDataKas.TabIndex = 5
        Me.CBDataKas.Text = "Data Kas"
        Me.CBDataKas.UseVisualStyleBackColor = True
        '
        'GroupBox11
        '
        Me.GroupBox11.BackColor = System.Drawing.Color.PapayaWhip
        Me.GroupBox11.Controls.Add(Me.CBOtoritas)
        Me.GroupBox11.Controls.Add(Me.CBUserEditHapus)
        Me.GroupBox11.Controls.Add(Me.CBUserData)
        Me.GroupBox11.Controls.Add(Me.CBUserTambah)
        Me.GroupBox11.Location = New System.Drawing.Point(21, 111)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(146, 117)
        Me.GroupBox11.TabIndex = 20
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "USER"
        '
        'CBOtoritas
        '
        Me.CBOtoritas.AutoSize = True
        Me.CBOtoritas.Location = New System.Drawing.Point(15, 90)
        Me.CBOtoritas.Name = "CBOtoritas"
        Me.CBOtoritas.Size = New System.Drawing.Size(62, 17)
        Me.CBOtoritas.TabIndex = 10
        Me.CBOtoritas.Text = "Otoritas"
        Me.CBOtoritas.UseVisualStyleBackColor = True
        '
        'CBUserEditHapus
        '
        Me.CBUserEditHapus.AutoSize = True
        Me.CBUserEditHapus.Location = New System.Drawing.Point(15, 67)
        Me.CBUserEditHapus.Name = "CBUserEditHapus"
        Me.CBUserEditHapus.Size = New System.Drawing.Size(105, 17)
        Me.CBUserEditHapus.TabIndex = 9
        Me.CBUserEditHapus.Text = "Edit/Hapus User"
        Me.CBUserEditHapus.UseVisualStyleBackColor = True
        '
        'CBUserData
        '
        Me.CBUserData.AutoSize = True
        Me.CBUserData.Location = New System.Drawing.Point(15, 21)
        Me.CBUserData.Name = "CBUserData"
        Me.CBUserData.Size = New System.Drawing.Size(67, 17)
        Me.CBUserData.TabIndex = 7
        Me.CBUserData.Text = "List User"
        Me.CBUserData.UseVisualStyleBackColor = True
        '
        'CBUserTambah
        '
        Me.CBUserTambah.AutoSize = True
        Me.CBUserTambah.Location = New System.Drawing.Point(15, 44)
        Me.CBUserTambah.Name = "CBUserTambah"
        Me.CBUserTambah.Size = New System.Drawing.Size(75, 17)
        Me.CBUserTambah.TabIndex = 8
        Me.CBUserTambah.Text = "Input User"
        Me.CBUserTambah.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.Color.LemonChiffon
        Me.GroupBox12.Controls.Add(Me.CBPerbaikanHasilEditHapus)
        Me.GroupBox12.Controls.Add(Me.CBperbaikanHasilLap)
        Me.GroupBox12.Controls.Add(Me.CBperbaikanHasil)
        Me.GroupBox12.Controls.Add(Me.CBjadwalHasil)
        Me.GroupBox12.Controls.Add(Me.CBperbaikanEditHapus)
        Me.GroupBox12.Controls.Add(Me.CBperbaikanLap)
        Me.GroupBox12.Controls.Add(Me.CBperbaikanInput)
        Me.GroupBox12.Controls.Add(Me.CBjadwalEditHapus)
        Me.GroupBox12.Controls.Add(Me.CBjadwalLap)
        Me.GroupBox12.Controls.Add(Me.CBjadwalInput)
        Me.GroupBox12.Controls.Add(Me.CBcompEditHapus)
        Me.GroupBox12.Controls.Add(Me.CBcomplainLap)
        Me.GroupBox12.Controls.Add(Me.CBComplainInput)
        Me.GroupBox12.Controls.Add(Me.CBSVCHasil_EditHapus)
        Me.GroupBox12.Controls.Add(Me.CBSVCHasil_Laporan)
        Me.GroupBox12.Controls.Add(Me.CBSVCHasil_Input)
        Me.GroupBox12.Controls.Add(Me.CBService_EditHapus)
        Me.GroupBox12.Controls.Add(Me.CBService_Input)
        Me.GroupBox12.Controls.Add(Me.CBService_Laporan)
        Me.GroupBox12.Location = New System.Drawing.Point(342, 362)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(638, 157)
        Me.GroupBox12.TabIndex = 21
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "SERVICE"
        '
        'CBPerbaikanHasilEditHapus
        '
        Me.CBPerbaikanHasilEditHapus.AutoSize = True
        Me.CBPerbaikanHasilEditHapus.Location = New System.Drawing.Point(317, 134)
        Me.CBPerbaikanHasilEditHapus.Name = "CBPerbaikanHasilEditHapus"
        Me.CBPerbaikanHasilEditHapus.Size = New System.Drawing.Size(155, 17)
        Me.CBPerbaikanHasilEditHapus.TabIndex = 27
        Me.CBPerbaikanHasilEditHapus.Text = "Hasil Perbaikan Edit Hapus"
        Me.CBPerbaikanHasilEditHapus.UseVisualStyleBackColor = True
        '
        'CBperbaikanHasilLap
        '
        Me.CBperbaikanHasilLap.AutoSize = True
        Me.CBperbaikanHasilLap.Location = New System.Drawing.Point(317, 111)
        Me.CBperbaikanHasilLap.Name = "CBperbaikanHasilLap"
        Me.CBperbaikanHasilLap.Size = New System.Drawing.Size(142, 17)
        Me.CBperbaikanHasilLap.TabIndex = 26
        Me.CBperbaikanHasilLap.Text = "Laporan Hasil Perbaikan"
        Me.CBperbaikanHasilLap.UseVisualStyleBackColor = True
        '
        'CBperbaikanHasil
        '
        Me.CBperbaikanHasil.AutoSize = True
        Me.CBperbaikanHasil.Location = New System.Drawing.Point(317, 88)
        Me.CBperbaikanHasil.Name = "CBperbaikanHasil"
        Me.CBperbaikanHasil.Size = New System.Drawing.Size(127, 17)
        Me.CBperbaikanHasil.TabIndex = 25
        Me.CBperbaikanHasil.Text = "Input Hasil Perbaikan"
        Me.CBperbaikanHasil.UseVisualStyleBackColor = True
        '
        'CBjadwalHasil
        '
        Me.CBjadwalHasil.AutoSize = True
        Me.CBjadwalHasil.Location = New System.Drawing.Point(159, 88)
        Me.CBjadwalHasil.Name = "CBjadwalHasil"
        Me.CBjadwalHasil.Size = New System.Drawing.Size(133, 17)
        Me.CBjadwalHasil.TabIndex = 24
        Me.CBjadwalHasil.Text = "Input Hasil Kunjungan "
        Me.CBjadwalHasil.UseVisualStyleBackColor = True
        '
        'CBperbaikanEditHapus
        '
        Me.CBperbaikanEditHapus.AutoSize = True
        Me.CBperbaikanEditHapus.Location = New System.Drawing.Point(317, 65)
        Me.CBperbaikanEditHapus.Name = "CBperbaikanEditHapus"
        Me.CBperbaikanEditHapus.Size = New System.Drawing.Size(129, 17)
        Me.CBperbaikanEditHapus.TabIndex = 23
        Me.CBperbaikanEditHapus.Text = "Perbaikan Edit Hapus"
        Me.CBperbaikanEditHapus.UseVisualStyleBackColor = True
        '
        'CBperbaikanLap
        '
        Me.CBperbaikanLap.AutoSize = True
        Me.CBperbaikanLap.Location = New System.Drawing.Point(317, 42)
        Me.CBperbaikanLap.Name = "CBperbaikanLap"
        Me.CBperbaikanLap.Size = New System.Drawing.Size(116, 17)
        Me.CBperbaikanLap.TabIndex = 22
        Me.CBperbaikanLap.Text = "Laporan Perbaikan"
        Me.CBperbaikanLap.UseVisualStyleBackColor = True
        '
        'CBperbaikanInput
        '
        Me.CBperbaikanInput.AutoSize = True
        Me.CBperbaikanInput.Location = New System.Drawing.Point(317, 19)
        Me.CBperbaikanInput.Name = "CBperbaikanInput"
        Me.CBperbaikanInput.Size = New System.Drawing.Size(101, 17)
        Me.CBperbaikanInput.TabIndex = 21
        Me.CBperbaikanInput.Text = "Input Perbaikan"
        Me.CBperbaikanInput.UseVisualStyleBackColor = True
        '
        'CBjadwalEditHapus
        '
        Me.CBjadwalEditHapus.AutoSize = True
        Me.CBjadwalEditHapus.Location = New System.Drawing.Point(159, 65)
        Me.CBjadwalEditHapus.Name = "CBjadwalEditHapus"
        Me.CBjadwalEditHapus.Size = New System.Drawing.Size(114, 17)
        Me.CBjadwalEditHapus.TabIndex = 20
        Me.CBjadwalEditHapus.Text = "Jadwal Edit Hapus"
        Me.CBjadwalEditHapus.UseVisualStyleBackColor = True
        '
        'CBjadwalLap
        '
        Me.CBjadwalLap.AutoSize = True
        Me.CBjadwalLap.Location = New System.Drawing.Point(159, 42)
        Me.CBjadwalLap.Name = "CBjadwalLap"
        Me.CBjadwalLap.Size = New System.Drawing.Size(101, 17)
        Me.CBjadwalLap.TabIndex = 19
        Me.CBjadwalLap.Text = "Laporan Jadwal"
        Me.CBjadwalLap.UseVisualStyleBackColor = True
        '
        'CBjadwalInput
        '
        Me.CBjadwalInput.AutoSize = True
        Me.CBjadwalInput.Location = New System.Drawing.Point(159, 19)
        Me.CBjadwalInput.Name = "CBjadwalInput"
        Me.CBjadwalInput.Size = New System.Drawing.Size(86, 17)
        Me.CBjadwalInput.TabIndex = 18
        Me.CBjadwalInput.Text = "Input Jadwal"
        Me.CBjadwalInput.UseVisualStyleBackColor = True
        '
        'CBcompEditHapus
        '
        Me.CBcompEditHapus.AutoSize = True
        Me.CBcompEditHapus.Location = New System.Drawing.Point(15, 65)
        Me.CBcompEditHapus.Name = "CBcompEditHapus"
        Me.CBcompEditHapus.Size = New System.Drawing.Size(124, 17)
        Me.CBcompEditHapus.TabIndex = 17
        Me.CBcompEditHapus.Text = "Complain Edit Hapus"
        Me.CBcompEditHapus.UseVisualStyleBackColor = True
        '
        'CBcomplainLap
        '
        Me.CBcomplainLap.AutoSize = True
        Me.CBcomplainLap.Location = New System.Drawing.Point(15, 42)
        Me.CBcomplainLap.Name = "CBcomplainLap"
        Me.CBcomplainLap.Size = New System.Drawing.Size(114, 17)
        Me.CBcomplainLap.TabIndex = 16
        Me.CBcomplainLap.Text = "Laporan Complain "
        Me.CBcomplainLap.UseVisualStyleBackColor = True
        '
        'CBComplainInput
        '
        Me.CBComplainInput.AutoSize = True
        Me.CBComplainInput.Location = New System.Drawing.Point(15, 19)
        Me.CBComplainInput.Name = "CBComplainInput"
        Me.CBComplainInput.Size = New System.Drawing.Size(96, 17)
        Me.CBComplainInput.TabIndex = 15
        Me.CBComplainInput.Text = "Input Complain"
        Me.CBComplainInput.UseVisualStyleBackColor = True
        '
        'CBSVCHasil_EditHapus
        '
        Me.CBSVCHasil_EditHapus.AutoSize = True
        Me.CBSVCHasil_EditHapus.Location = New System.Drawing.Point(498, 134)
        Me.CBSVCHasil_EditHapus.Name = "CBSVCHasil_EditHapus"
        Me.CBSVCHasil_EditHapus.Size = New System.Drawing.Size(106, 17)
        Me.CBSVCHasil_EditHapus.TabIndex = 12
        Me.CBSVCHasil_EditHapus.Text = "Edit/Hapus Hasil"
        Me.CBSVCHasil_EditHapus.UseVisualStyleBackColor = True
        '
        'CBSVCHasil_Laporan
        '
        Me.CBSVCHasil_Laporan.AutoSize = True
        Me.CBSVCHasil_Laporan.Location = New System.Drawing.Point(498, 111)
        Me.CBSVCHasil_Laporan.Name = "CBSVCHasil_Laporan"
        Me.CBSVCHasil_Laporan.Size = New System.Drawing.Size(130, 17)
        Me.CBSVCHasil_Laporan.TabIndex = 11
        Me.CBSVCHasil_Laporan.Text = "Laporan Hasil Service"
        Me.CBSVCHasil_Laporan.UseVisualStyleBackColor = True
        '
        'CBSVCHasil_Input
        '
        Me.CBSVCHasil_Input.AutoSize = True
        Me.CBSVCHasil_Input.Location = New System.Drawing.Point(498, 88)
        Me.CBSVCHasil_Input.Name = "CBSVCHasil_Input"
        Me.CBSVCHasil_Input.Size = New System.Drawing.Size(115, 17)
        Me.CBSVCHasil_Input.TabIndex = 10
        Me.CBSVCHasil_Input.Text = "Input Hasil Service"
        Me.CBSVCHasil_Input.UseVisualStyleBackColor = True
        '
        'CBService_EditHapus
        '
        Me.CBService_EditHapus.AutoSize = True
        Me.CBService_EditHapus.Location = New System.Drawing.Point(498, 65)
        Me.CBService_EditHapus.Name = "CBService_EditHapus"
        Me.CBService_EditHapus.Size = New System.Drawing.Size(119, 17)
        Me.CBService_EditHapus.TabIndex = 9
        Me.CBService_EditHapus.Text = "Edit/Hapus Service"
        Me.CBService_EditHapus.UseVisualStyleBackColor = True
        '
        'CBService_Input
        '
        Me.CBService_Input.AutoSize = True
        Me.CBService_Input.Location = New System.Drawing.Point(498, 19)
        Me.CBService_Input.Name = "CBService_Input"
        Me.CBService_Input.Size = New System.Drawing.Size(89, 17)
        Me.CBService_Input.TabIndex = 7
        Me.CBService_Input.Text = "Input Service"
        Me.CBService_Input.UseVisualStyleBackColor = True
        '
        'CBService_Laporan
        '
        Me.CBService_Laporan.AutoSize = True
        Me.CBService_Laporan.Location = New System.Drawing.Point(498, 42)
        Me.CBService_Laporan.Name = "CBService_Laporan"
        Me.CBService_Laporan.Size = New System.Drawing.Size(104, 17)
        Me.CBService_Laporan.TabIndex = 8
        Me.CBService_Laporan.Text = "Laporan Service"
        Me.CBService_Laporan.UseVisualStyleBackColor = True
        '
        'GroupBox13
        '
        Me.GroupBox13.BackColor = System.Drawing.Color.NavajoWhite
        Me.GroupBox13.Controls.Add(Me.CBprintulangLaporan)
        Me.GroupBox13.Location = New System.Drawing.Point(673, 280)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(149, 54)
        Me.GroupBox13.TabIndex = 22
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "LAPORAN"
        '
        'CBprintulangLaporan
        '
        Me.CBprintulangLaporan.AutoSize = True
        Me.CBprintulangLaporan.Location = New System.Drawing.Point(16, 22)
        Me.CBprintulangLaporan.Name = "CBprintulangLaporan"
        Me.CBprintulangLaporan.Size = New System.Drawing.Size(120, 17)
        Me.CBprintulangLaporan.TabIndex = 21
        Me.CBprintulangLaporan.Text = "Print Ulang Laporan"
        Me.CBprintulangLaporan.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightSeaGreen
        Me.Panel3.Location = New System.Drawing.Point(12, 24)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(155, 55)
        Me.Panel3.TabIndex = 23
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.LightSeaGreen
        Me.Panel4.Controls.Add(Me.CBadmin)
        Me.Panel4.Controls.Add(Me.butBTL)
        Me.Panel4.Controls.Add(Me.butUPDT)
        Me.Panel4.Location = New System.Drawing.Point(504, 24)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(504, 55)
        Me.Panel4.TabIndex = 24
        '
        'CBadmin
        '
        Me.CBadmin.AutoSize = True
        Me.CBadmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBadmin.ForeColor = System.Drawing.Color.White
        Me.CBadmin.Location = New System.Drawing.Point(13, 24)
        Me.CBadmin.Name = "CBadmin"
        Me.CBadmin.Size = New System.Drawing.Size(110, 19)
        Me.CBadmin.TabIndex = 20
        Me.CBadmin.Text = "Set As Admin"
        Me.CBadmin.UseVisualStyleBackColor = True
        '
        'butBTL
        '
        Me.butBTL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBTL.Location = New System.Drawing.Point(316, 16)
        Me.butBTL.Name = "butBTL"
        Me.butBTL.Size = New System.Drawing.Size(75, 28)
        Me.butBTL.TabIndex = 1
        Me.butBTL.Text = "BATAL"
        Me.butBTL.UseVisualStyleBackColor = True
        '
        'butUPDT
        '
        Me.butUPDT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUPDT.Location = New System.Drawing.Point(223, 16)
        Me.butUPDT.Name = "butUPDT"
        Me.butUPDT.Size = New System.Drawing.Size(75, 28)
        Me.butUPDT.TabIndex = 0
        Me.butUPDT.Text = "UPDATE"
        Me.butUPDT.UseVisualStyleBackColor = True
        '
        'GroupBox14
        '
        Me.GroupBox14.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.GroupBox14.Controls.Add(Me.CBTehnisiEditHapus)
        Me.GroupBox14.Controls.Add(Me.CBTehnisiData)
        Me.GroupBox14.Controls.Add(Me.CBTehnisiTambah)
        Me.GroupBox14.Location = New System.Drawing.Point(21, 503)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(146, 90)
        Me.GroupBox14.TabIndex = 14
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "TEHNISI"
        '
        'CBTehnisiEditHapus
        '
        Me.CBTehnisiEditHapus.AutoSize = True
        Me.CBTehnisiEditHapus.Location = New System.Drawing.Point(16, 65)
        Me.CBTehnisiEditHapus.Name = "CBTehnisiEditHapus"
        Me.CBTehnisiEditHapus.Size = New System.Drawing.Size(115, 17)
        Me.CBTehnisiEditHapus.TabIndex = 12
        Me.CBTehnisiEditHapus.Text = "Edit Hapus Tehnisi"
        Me.CBTehnisiEditHapus.UseVisualStyleBackColor = True
        '
        'CBTehnisiData
        '
        Me.CBTehnisiData.AutoSize = True
        Me.CBTehnisiData.Location = New System.Drawing.Point(16, 19)
        Me.CBTehnisiData.Name = "CBTehnisiData"
        Me.CBTehnisiData.Size = New System.Drawing.Size(92, 17)
        Me.CBTehnisiData.TabIndex = 10
        Me.CBTehnisiData.Text = "Daftar Tehnisi"
        Me.CBTehnisiData.UseVisualStyleBackColor = True
        '
        'CBTehnisiTambah
        '
        Me.CBTehnisiTambah.AutoSize = True
        Me.CBTehnisiTambah.Location = New System.Drawing.Point(16, 42)
        Me.CBTehnisiTambah.Name = "CBTehnisiTambah"
        Me.CBTehnisiTambah.Size = New System.Drawing.Size(102, 17)
        Me.CBTehnisiTambah.TabIndex = 11
        Me.CBTehnisiTambah.Text = "Tambah Tehnisi"
        Me.CBTehnisiTambah.UseVisualStyleBackColor = True
        '
        'CBLapJual_Harga
        '
        Me.CBLapJual_Harga.AutoSize = True
        Me.CBLapJual_Harga.Location = New System.Drawing.Point(14, 130)
        Me.CBLapJual_Harga.Name = "CBLapJual_Harga"
        Me.CBLapJual_Harga.Size = New System.Drawing.Size(156, 17)
        Me.CBLapJual_Harga.TabIndex = 34
        Me.CBLapJual_Harga.Text = "Laporan Penjualan + Harga"
        Me.CBLapJual_Harga.UseVisualStyleBackColor = True
        '
        'Otoritas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.GroupBox14)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.GroupBox13)
        Me.Controls.Add(Me.GroupBox12)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.GroupBox10)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Otoritas"
        Me.Text = "Otoritas"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents CBBarangData As System.Windows.Forms.CheckBox
    Friend WithEvents CBBarangTambah As System.Windows.Forms.CheckBox
    Friend WithEvents CBBarangEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBCustomerData As System.Windows.Forms.CheckBox
    Friend WithEvents CBCustomerTambah As System.Windows.Forms.CheckBox
    Friend WithEvents CBCustomerEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBSupplierData As System.Windows.Forms.CheckBox
    Friend WithEvents CBSupplierTambah As System.Windows.Forms.CheckBox
    Friend WithEvents CBSupplierEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CBkoreksiLaporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBkoreksi As System.Windows.Forms.CheckBox
    Friend WithEvents CBmutasi As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents CBSalesEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBSalesData As System.Windows.Forms.CheckBox
    Friend WithEvents CBSalesTambah As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents CBReturBeli_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturBeli_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturBeli_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBBeli_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBBeli_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBBeli_Input As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents CBReturSJ_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturSJ_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBFaktur_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBSJ_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBSJ_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBSO_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBSO_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBSO_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturJual_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturJual_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturJual_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBJual_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBJual_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBJual_Input As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents CBHutangBayar As System.Windows.Forms.CheckBox
    Friend WithEvents CBHutangList As System.Windows.Forms.CheckBox
    Friend WithEvents CBHutangData As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents CBPiutangTerima As System.Windows.Forms.CheckBox
    Friend WithEvents CBPiutangList As System.Windows.Forms.CheckBox
    Friend WithEvents CBPiutangData As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents CBAccEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBAccData As System.Windows.Forms.CheckBox
    Friend WithEvents CBAccTambah As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents CBDataKas As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents CBOtoritas As System.Windows.Forms.CheckBox
    Friend WithEvents CBUserEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBUserData As System.Windows.Forms.CheckBox
    Friend WithEvents CBUserTambah As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents CBSVCHasil_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBSVCHasil_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBSVCHasil_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBService_EditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBService_Input As System.Windows.Forms.CheckBox
    Friend WithEvents CBService_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents CBprintulangLaporan As System.Windows.Forms.CheckBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents CBInputDP As System.Windows.Forms.CheckBox
    Friend WithEvents CBReturSJ_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents CBSJ_Laporan As System.Windows.Forms.CheckBox
    Friend WithEvents butUPDT As System.Windows.Forms.Button
    Friend WithEvents butBTL As System.Windows.Forms.Button
    Friend WithEvents CBqtyNegLap As System.Windows.Forms.CheckBox
    Friend WithEvents CBkastobank As System.Windows.Forms.CheckBox
    Friend WithEvents CBComplainInput As System.Windows.Forms.CheckBox
    Friend WithEvents CBcompEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBcomplainLap As System.Windows.Forms.CheckBox
    Friend WithEvents CBjadwalEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBjadwalLap As System.Windows.Forms.CheckBox
    Friend WithEvents CBjadwalInput As System.Windows.Forms.CheckBox
    Friend WithEvents CBjadwalHasil As System.Windows.Forms.CheckBox
    Friend WithEvents CBperbaikanEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBperbaikanLap As System.Windows.Forms.CheckBox
    Friend WithEvents CBperbaikanInput As System.Windows.Forms.CheckBox
    Friend WithEvents CBPerbaikanHasilEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBperbaikanHasilLap As System.Windows.Forms.CheckBox
    Friend WithEvents CBperbaikanHasil As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents CBTehnisiEditHapus As System.Windows.Forms.CheckBox
    Friend WithEvents CBTehnisiData As System.Windows.Forms.CheckBox
    Friend WithEvents CBTehnisiTambah As System.Windows.Forms.CheckBox
    Friend WithEvents CBadmin As System.Windows.Forms.CheckBox
    Friend WithEvents user As System.Windows.Forms.TextBox
    Friend WithEvents CBDetailSerialEdit As System.Windows.Forms.CheckBox
    Friend WithEvents CBDetailSerialInput As System.Windows.Forms.CheckBox
    Friend WithEvents CBCekDataKas As System.Windows.Forms.CheckBox
    Friend WithEvents CBLapJual_Harga As System.Windows.Forms.CheckBox
End Class
