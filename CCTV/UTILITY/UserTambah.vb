﻿Public Class UserTambah

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan user baru ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        Dim admin As Boolean
        If CBadmin.Checked = True Then
            admin = True
        Else
            admin = False
        End If
        b = a.add(userid.Text.ToUpper, pass.Text, admin)
        If b = True Then
            MsgBox("Berhasil input user baru")
            userid.Clear()
            pass.Clear()
            userid.Focus()
        Else
            MsgBox("Gagal input user baru")
            Exit Sub
        End If
    End Sub

    Private Sub UserTambah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

End Class