﻿Public Class UserDaftar


    Private Sub DGVuser_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVuser.DoubleClick
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As Otoritas
        frm = New Otoritas
        frm.MdiParent = frmMenu
        frm.Text = "Otoritas User"
        Dim row As Integer
        row = DGVuser.CurrentRow.Index.ToString
        frm.MdiParent = frmMenu
        frm.user.Text = DGVuser.Item("USER", row).Value.ToString

        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()

    End Sub

    Private Sub UserDaftar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadd()
    End Sub

    Sub loadd()
        Dim a As New clsCCTV.clsPengguna
        Dim b As DataTable
        b = a.table()
        DGVuser.DataSource = b
    End Sub

    Private Sub DGVuser_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVuser.KeyDown
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus User ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGVuser.CurrentRow.Index

        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.hapususer(DGVuser.Item("USER", row).Value.ToString)

        If b = True Then
            MsgBox("Berhasil Hapus User")
            loadd()
        Else
            MsgBox("Gagal Hapus User")
            Exit Sub
        End If
    End Sub

End Class