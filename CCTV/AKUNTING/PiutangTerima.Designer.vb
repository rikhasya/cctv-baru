﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PiutangTerima
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.DGVpiutang = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.bayart = New System.Windows.Forms.TextBox()
        Me.jumlaht = New System.Windows.Forms.TextBox()
        Me.noFak = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lgn = New System.Windows.Forms.TextBox()
        Me.dateJT = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.giro = New System.Windows.Forms.TextBox()
        Me.CBacc = New System.Windows.Forms.ComboBox()
        Me.CBlgn = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.keterangan = New System.Windows.Forms.TextBox()
        Me.butINPUT = New System.Windows.Forms.Button()
        Me.total = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.retail = New System.Windows.Forms.Label()
        Me.klgn = New System.Windows.Forms.Label()
        Me.dp = New System.Windows.Forms.Label()
        CType(Me.DGVpiutang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(366, 15)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(26, 13)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "* F1"
        '
        'DGVpiutang
        '
        Me.DGVpiutang.AllowUserToAddRows = False
        Me.DGVpiutang.AllowUserToDeleteRows = False
        Me.DGVpiutang.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVpiutang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVpiutang.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.JUMLAH, Me.BAYAR})
        Me.DGVpiutang.Location = New System.Drawing.Point(31, 148)
        Me.DGVpiutang.Name = "DGVpiutang"
        Me.DGVpiutang.ReadOnly = True
        Me.DGVpiutang.RowHeadersVisible = False
        Me.DGVpiutang.Size = New System.Drawing.Size(553, 268)
        Me.DGVpiutang.TabIndex = 16
        '
        'NO_FAK
        '
        Me.NO_FAK.HeaderText = "No. Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'JUMLAH
        '
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.HeaderText = "Bayar"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightSeaGreen
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.bayart)
        Me.Panel3.Controls.Add(Me.jumlaht)
        Me.Panel3.Controls.Add(Me.noFak)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Location = New System.Drawing.Point(582, 165)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(408, 126)
        Me.Panel3.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(61, 124)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(0, 15)
        Me.Label11.TabIndex = 8
        '
        'bayart
        '
        Me.bayart.Location = New System.Drawing.Point(170, 85)
        Me.bayart.Name = "bayart"
        Me.bayart.Size = New System.Drawing.Size(192, 20)
        Me.bayart.TabIndex = 0
        Me.bayart.Text = "0"
        Me.bayart.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'jumlaht
        '
        Me.jumlaht.Enabled = False
        Me.jumlaht.Location = New System.Drawing.Point(170, 48)
        Me.jumlaht.Name = "jumlaht"
        Me.jumlaht.Size = New System.Drawing.Size(192, 20)
        Me.jumlaht.TabIndex = 6
        Me.jumlaht.Text = "0"
        Me.jumlaht.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'noFak
        '
        Me.noFak.Enabled = False
        Me.noFak.Location = New System.Drawing.Point(170, 12)
        Me.noFak.Name = "noFak"
        Me.noFak.Size = New System.Drawing.Size(192, 20)
        Me.noFak.TabIndex = 0
        Me.noFak.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(61, 86)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 15)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Bayar"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(61, 49)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 15)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Jumlah"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(61, 13)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 15)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "No. Faktur"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(70, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(45, 17)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Total"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Teal
        Me.Panel1.Controls.Add(Me.lgn)
        Me.Panel1.Controls.Add(Me.dateJT)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.giro)
        Me.Panel1.Controls.Add(Me.CBacc)
        Me.Panel1.Controls.Add(Me.CBlgn)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 73)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(978, 69)
        Me.Panel1.TabIndex = 0
        '
        'lgn
        '
        Me.lgn.Enabled = False
        Me.lgn.Location = New System.Drawing.Point(15, 35)
        Me.lgn.Name = "lgn"
        Me.lgn.Size = New System.Drawing.Size(179, 20)
        Me.lgn.TabIndex = 5
        Me.lgn.Visible = False
        '
        'dateJT
        '
        Me.dateJT.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateJT.Location = New System.Drawing.Point(607, 33)
        Me.dateJT.Name = "dateJT"
        Me.dateJT.Size = New System.Drawing.Size(92, 20)
        Me.dateJT.TabIndex = 4
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(491, 34)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(92, 20)
        Me.date1.TabIndex = 3
        '
        'giro
        '
        Me.giro.Location = New System.Drawing.Point(329, 34)
        Me.giro.Name = "giro"
        Me.giro.Size = New System.Drawing.Size(139, 20)
        Me.giro.TabIndex = 2
        '
        'CBacc
        '
        Me.CBacc.FormattingEnabled = True
        Me.CBacc.Location = New System.Drawing.Point(220, 34)
        Me.CBacc.Name = "CBacc"
        Me.CBacc.Size = New System.Drawing.Size(86, 21)
        Me.CBacc.TabIndex = 1
        '
        'CBlgn
        '
        Me.CBlgn.FormattingEnabled = True
        Me.CBlgn.Location = New System.Drawing.Point(19, 34)
        Me.CBlgn.Name = "CBlgn"
        Me.CBlgn.Size = New System.Drawing.Size(179, 21)
        Me.CBlgn.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(604, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Tanggal JT"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(488, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Tanggal Serah"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(18, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Langganan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(326, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "No. Giro"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(220, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Account"
        '
        'butSIM
        '
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(757, 422)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(106, 35)
        Me.butSIM.TabIndex = 4
        Me.butSIM.Text = "SIMPAN"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(30, 425)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 13)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Keterangan :"
        '
        'keterangan
        '
        Me.keterangan.Location = New System.Drawing.Point(104, 422)
        Me.keterangan.Multiline = True
        Me.keterangan.Name = "keterangan"
        Me.keterangan.Size = New System.Drawing.Size(480, 76)
        Me.keterangan.TabIndex = 3
        '
        'butINPUT
        '
        Me.butINPUT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINPUT.Location = New System.Drawing.Point(752, 297)
        Me.butINPUT.Name = "butINPUT"
        Me.butINPUT.Size = New System.Drawing.Size(192, 35)
        Me.butINPUT.TabIndex = 2
        Me.butINPUT.Text = "INPUT"
        Me.butINPUT.UseVisualStyleBackColor = True
        '
        'total
        '
        Me.total.Enabled = False
        Me.total.Location = New System.Drawing.Point(170, 11)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(192, 20)
        Me.total.TabIndex = 3
        Me.total.Text = "0"
        Me.total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightSeaGreen
        Me.Panel2.Controls.Add(Me.total)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Location = New System.Drawing.Point(582, 338)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(408, 40)
        Me.Panel2.TabIndex = 14
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(755, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(235, 22)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Penerimaan Piutang"
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(884, 422)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(106, 35)
        Me.butBAT.TabIndex = 5
        Me.butBAT.Text = "BATAL"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(30, 508)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(36, 13)
        Me.Label14.TabIndex = 20
        Me.Label14.Text = "Note :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(30, 522)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(442, 13)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "DP hanya boleh di bayar dengan account DP, dan langsung simpan setelah input fakt" & _
            "ur DP."
        '
        'retail
        '
        Me.retail.AutoSize = True
        Me.retail.Location = New System.Drawing.Point(590, 381)
        Me.retail.Name = "retail"
        Me.retail.Size = New System.Drawing.Size(10, 13)
        Me.retail.TabIndex = 22
        Me.retail.Text = "-"
        Me.retail.Visible = False
        '
        'klgn
        '
        Me.klgn.AutoSize = True
        Me.klgn.ForeColor = System.Drawing.Color.White
        Me.klgn.Location = New System.Drawing.Point(30, 57)
        Me.klgn.Name = "klgn"
        Me.klgn.Size = New System.Drawing.Size(47, 13)
        Me.klgn.TabIndex = 6
        Me.klgn.Text = "Account"
        Me.klgn.Visible = False
        '
        'dp
        '
        Me.dp.AutoSize = True
        Me.dp.Location = New System.Drawing.Point(590, 394)
        Me.dp.Name = "dp"
        Me.dp.Size = New System.Drawing.Size(10, 13)
        Me.dp.TabIndex = 23
        Me.dp.Text = "-"
        Me.dp.Visible = False
        '
        'PiutangTerima
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.dp)
        Me.Controls.Add(Me.klgn)
        Me.Controls.Add(Me.retail)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.DGVpiutang)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.butSIM)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.keterangan)
        Me.Controls.Add(Me.butINPUT)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butBAT)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "PiutangTerima"
        Me.Text = "PiutangTerima"
        CType(Me.DGVpiutang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents DGVpiutang As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents bayart As System.Windows.Forms.TextBox
    Friend WithEvents jumlaht As System.Windows.Forms.TextBox
    Friend WithEvents noFak As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dateJT As System.Windows.Forms.DateTimePicker
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents giro As System.Windows.Forms.TextBox
    Friend WithEvents CBacc As System.Windows.Forms.ComboBox
    Friend WithEvents CBlgn As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents keterangan As System.Windows.Forms.TextBox
    Friend WithEvents butINPUT As System.Windows.Forms.Button
    Friend WithEvents total As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents retail As System.Windows.Forms.Label
    Friend WithEvents lgn As System.Windows.Forms.TextBox
    Friend WithEvents klgn As System.Windows.Forms.Label
    Friend WithEvents dp As System.Windows.Forms.Label
End Class
