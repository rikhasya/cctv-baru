﻿Public Class PiutangTerima

    Private Sub PiutangTerima_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            If retail.Text = "y" Then
                MsgBox("Pembayaran Piutang Retail Tidak Bisa F1")
                Exit Sub
            End If
            Cari_faktur.Show()
            Cari_faktur.dari.Text = "PIUT"
            Cari_faktur.ksup.Text = CBlgn.SelectedValue
        End If
    End Sub

    Private Sub PiutangTerima_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub PiutangTerima_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With

        Dim o As New clsCCTV.clsACC
        Dim l As DataTable
        l = o.table("%", "%")
        With CBacc
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = l
        End With

        jumlaht.Text = FormatNumber(jumlaht.Text, 2)
        bayart.Text = FormatNumber(bayart.Text, 2)
        total.Text = FormatNumber(total.Text, 2)
    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If noFak.Text = "" Then
            butSIM.Focus()
            Exit Sub
        ElseIf CBacc.SelectedValue = "" Then
            MsgBox("Pilih Account")
            CBacc.Focus()
            Exit Sub
        ElseIf dp.Text = "1" And CBacc.Text <> "DP" Then
            MsgBox("Input DP jenis bayar harus DP")
            Exit Sub
        End If

        Dim a As Boolean = True
        Dim b As Integer

        For b = 0 To DGVpiutang.RowCount - 1
            Console.WriteLine(noFak.Text & ":" & DGVpiutang.Item("NO_FAK", b).Value)
            If noFak.Text = DGVpiutang.Item("NO_FAK", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("No. Faktur Sudah Di Input")
            Exit Sub
        End If
        With DGVpiutang
            .Rows.Add(1)
            .Rows(DGVpiutang.Rows.Count() - 1).Cells("NO_FAK").Value = noFak.Text
            .Rows(DGVpiutang.Rows.Count() - 1).Cells("JUMLAH").Value = jumlaht.Text
            .Rows(DGVpiutang.Rows.Count() - 1).Cells("BAYAR").Value = bayart.Text
        End With

        frmMenu.MenuStrip1.Enabled = False
        Panel1.Enabled = False
        bayart.Enabled = True
        noFak.Clear()
        jumlaht.Clear()
        bayart.Clear()
        dp.Text = 0
        hitung()
    End Sub

    Sub hitung()
        Dim tot As Integer = 0
        Dim i As Integer
        For i = 0 To DGVpiutang.RowCount - 1
            tot = tot + DGVpiutang.Item("BAYAR", i).Value
        Next

        total.Text = FormatNumber(tot, 2)
    End Sub

    Private Sub DGVpiutang_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVpiutang.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVpiutang.CurrentRow.Index.ToString

        DGVpiutang.Rows.RemoveAt(baris)
        hitung()
    End Sub

    Private Sub butBAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBAT.Click
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If CBacc.SelectedValue = "" Then
            MsgBox("Pilih Account")
            CBacc.Focus()
            Exit Sub
        End If

        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan penerimaan piutang ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If Windows.Forms.DialogResult.Yes Then

            Dim a As New clsCCTV.clsPiutHutang
            Dim t As String
            t = a.nokas("RV")

            Dim table1 As New DataTable("DETKAS")
            table1.Columns.Add("NO_PV")
            table1.Columns.Add("NO_FAK")
            table1.Columns.Add("BAYAR")

            Dim i As Integer
            For i = 0 To DGVpiutang.Rows.Count - 1
                table1.Rows.Add(t, DGVpiutang.Item("NO_FAK", i).Value.ToString, DGVpiutang.Item("BAYAR", i).Value)
            Next

            Dim namalgn As String
            Dim kodelgn As String
            If retail.Text = "y" Then
                namalgn = lgn.Text
                kodelgn = klgn.Text
            Else
                namalgn = CBlgn.Text
                kodelgn = CBlgn.SelectedValue
            End If

            Dim s As Boolean
            s = a.inputkas(t, CBacc.SelectedValue, giro.Text, kodelgn, namalgn, date1.Text, dateJT.Text, total.Text, keterangan.Text, clsCCTV.clsPengguna.CLUserID, table1)

            If s = True Then
                MsgBox("Berhasil Input Penerimaan Piutang")
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
            Else
                MsgBox("Gagal Input Penerimaan Piutang")
            End If
        End If
    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            CBacc.Focus()
        End If
    End Sub

    Private Sub CBacc_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBacc.KeyDown
        If e.KeyCode = Keys.Enter Then
            giro.Focus()
        End If
    End Sub

    
End Class