﻿Imports System
Imports System.Text.RegularExpressions
Public Class KasToBank

    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        e.Cancel = False
    End Sub

    Private Sub KasToBank_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub KasToBank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsACC
        Dim b As DataTable
        b = a.table("%", "%")
        With CBacc1
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = b
        End With

    End Sub

    Private Sub CBacc1_KeyDown(sender As Object, e As KeyEventArgs) Handles CBacc1.KeyDown
        If e.KeyCode = Keys.Enter Then
            giro.Focus()
        End If
    End Sub

    Private Sub CBacc1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles CBacc1.Validating
        If Microsoft.VisualBasic.IsDBNull(CBacc1.SelectedValue) Or IsNothing(CBacc1.SelectedValue) Then
            e.Cancel = True
            CBacc1.Select(0, CBacc1.Text.Length)
            ErrorProvider1.SetError(CBacc1, "koreksi account")
        End If
    End Sub

    Private Sub cbacc_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBacc1.Validated
        ErrorProvider1.SetError(CBacc1, "")
    End Sub

    Private Sub CBacc1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBacc1.SelectedIndexChanged
        If CBacc1.SelectedValue = "" Then
            MsgBox("Pilih Account!")
            CBacc1.Focus()
            Exit Sub
        End If
    End Sub

    Private Sub BATAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BATAL.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batalkan Input Biaya ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Me.Close()
    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsPiutHutang
        Dim b As String
        b = a.nokas("BYY")

        Dim table1 As New DataTable("DETKAS")
       
        Dim byr As Double = 0
        byr = CDbl(total.Text) * (-1)
        Dim c As Boolean
        Dim nm As String = Microsoft.VisualBasic.Left(nama.Text, 30)
        c = a.inputkas(b, CBacc1.SelectedValue, giro.Text, "", nama.Text, CDate(tanggal.Text), CDate(JT.Text), CDbl(total.Text), Keterangan.Text, clsCCTV.clsPengguna.CLUserID, table1)

        If c = True Then
            MsgBox("Berhasil Input Biaya")
            Me.Close()
        Else
            MsgBox("Berhasil Input Biaya")
        End If

    End Sub

    Sub hitung()
        Dim bb As Double = 0
        If IsNumeric(bayar.Text) And IsNumeric(pot.Text) And IsNumeric(bbank.Text) Then
            bb = CDbl(bayar.Text) - CDbl(pot.Text)
            total.Text = (bb + CDbl(bbank.Text))
            total.Text = FormatNumber(total.Text, 2)
        End If
    End Sub

    Private Sub bayar_Leave(sender As Object, e As EventArgs) Handles bayar.Leave
        If IsNumeric(bayar.Text) Then
            bayar.Text = FormatNumber(bayar.Text, 2)
            hitung()
        End If
    End Sub

    Private Sub pot_TextChanged(sender As Object, e As EventArgs) Handles pot.TextChanged
        If IsNumeric(pot.Text) Then
            pot.Text = FormatNumber(pot.Text, 2)
            hitung()
        End If
    End Sub

    Private Sub bbank_Leave(sender As Object, e As EventArgs) Handles bbank.Leave
        If IsNumeric(bbank.Text) Then
            bbank.Text = FormatNumber(bbank.Text, 2)
            hitung()
        End If
    End Sub

End Class