﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HutangPembayaran
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dateJT = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.giro = New System.Windows.Forms.TextBox()
        Me.CBacc = New System.Windows.Forms.ComboBox()
        Me.CBsup = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.total = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.bayart = New System.Windows.Forms.TextBox()
        Me.jumlaht = New System.Windows.Forms.TextBox()
        Me.noFak = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGVhutang = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butINPUT = New System.Windows.Forms.Button()
        Me.keterangan = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.DGVhutang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.Panel1.Controls.Add(Me.dateJT)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.giro)
        Me.Panel1.Controls.Add(Me.CBacc)
        Me.Panel1.Controls.Add(Me.CBsup)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 69)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(978, 69)
        Me.Panel1.TabIndex = 0
        '
        'dateJT
        '
        Me.dateJT.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateJT.Location = New System.Drawing.Point(607, 33)
        Me.dateJT.Name = "dateJT"
        Me.dateJT.Size = New System.Drawing.Size(92, 20)
        Me.dateJT.TabIndex = 4
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(491, 34)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(92, 20)
        Me.date1.TabIndex = 3
        '
        'giro
        '
        Me.giro.Location = New System.Drawing.Point(329, 34)
        Me.giro.Name = "giro"
        Me.giro.Size = New System.Drawing.Size(139, 20)
        Me.giro.TabIndex = 2
        '
        'CBacc
        '
        Me.CBacc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBacc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBacc.FormattingEnabled = True
        Me.CBacc.Location = New System.Drawing.Point(220, 34)
        Me.CBacc.Name = "CBacc"
        Me.CBacc.Size = New System.Drawing.Size(86, 21)
        Me.CBacc.TabIndex = 1
        '
        'CBsup
        '
        Me.CBsup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBsup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBsup.FormattingEnabled = True
        Me.CBsup.Location = New System.Drawing.Point(19, 34)
        Me.CBsup.Name = "CBsup"
        Me.CBsup.Size = New System.Drawing.Size(179, 21)
        Me.CBsup.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(604, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Tanggal JT"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(488, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Tanggal Serah"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(19, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Supplier"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(326, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "No. Giro"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(220, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Account"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Perpetua Titling MT", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(748, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(242, 22)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Pembayaran Hutang"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel2.Controls.Add(Me.total)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Location = New System.Drawing.Point(582, 334)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(408, 40)
        Me.Panel2.TabIndex = 4
        '
        'total
        '
        Me.total.Enabled = False
        Me.total.Location = New System.Drawing.Point(170, 11)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(192, 20)
        Me.total.TabIndex = 3
        Me.total.Text = "0"
        Me.total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(70, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(45, 17)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Total"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.bayart)
        Me.Panel3.Controls.Add(Me.jumlaht)
        Me.Panel3.Controls.Add(Me.noFak)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Location = New System.Drawing.Point(582, 161)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(408, 126)
        Me.Panel3.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(366, 15)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(26, 13)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "* F1"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(61, 124)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(0, 15)
        Me.Label11.TabIndex = 8
        '
        'bayart
        '
        Me.bayart.Location = New System.Drawing.Point(170, 85)
        Me.bayart.Name = "bayart"
        Me.bayart.Size = New System.Drawing.Size(192, 20)
        Me.bayart.TabIndex = 0
        Me.bayart.Text = "0"
        Me.bayart.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'jumlaht
        '
        Me.jumlaht.Enabled = False
        Me.jumlaht.Location = New System.Drawing.Point(170, 48)
        Me.jumlaht.Name = "jumlaht"
        Me.jumlaht.Size = New System.Drawing.Size(192, 20)
        Me.jumlaht.TabIndex = 6
        Me.jumlaht.Text = "0"
        Me.jumlaht.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'noFak
        '
        Me.noFak.Enabled = False
        Me.noFak.Location = New System.Drawing.Point(170, 12)
        Me.noFak.Name = "noFak"
        Me.noFak.Size = New System.Drawing.Size(192, 20)
        Me.noFak.TabIndex = 4
        Me.noFak.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(61, 86)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 15)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Bayar"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(61, 49)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 15)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Jumlah"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(61, 13)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 15)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "No. Faktur"
        '
        'DGVhutang
        '
        Me.DGVhutang.AllowUserToAddRows = False
        Me.DGVhutang.AllowUserToDeleteRows = False
        Me.DGVhutang.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVhutang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVhutang.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.JUMLAH, Me.BAYAR})
        Me.DGVhutang.Location = New System.Drawing.Point(31, 144)
        Me.DGVhutang.Name = "DGVhutang"
        Me.DGVhutang.ReadOnly = True
        Me.DGVhutang.RowHeadersVisible = False
        Me.DGVhutang.Size = New System.Drawing.Size(553, 268)
        Me.DGVhutang.TabIndex = 6
        '
        'NO_FAK
        '
        Me.NO_FAK.HeaderText = "No. Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'JUMLAH
        '
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.HeaderText = "Bayar"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        '
        'butINPUT
        '
        Me.butINPUT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINPUT.Location = New System.Drawing.Point(752, 293)
        Me.butINPUT.Name = "butINPUT"
        Me.butINPUT.Size = New System.Drawing.Size(192, 35)
        Me.butINPUT.TabIndex = 2
        Me.butINPUT.Text = "INPUT"
        Me.butINPUT.UseVisualStyleBackColor = True
        '
        'keterangan
        '
        Me.keterangan.Location = New System.Drawing.Point(104, 418)
        Me.keterangan.Multiline = True
        Me.keterangan.Name = "keterangan"
        Me.keterangan.Size = New System.Drawing.Size(480, 76)
        Me.keterangan.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(30, 421)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 13)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Keterangan :"
        '
        'butSIM
        '
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(757, 418)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(106, 35)
        Me.butSIM.TabIndex = 4
        Me.butSIM.Text = "SIMPAN"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(884, 418)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(106, 35)
        Me.butBAT.TabIndex = 11
        Me.butBAT.Text = "BATAL"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'HutangPembayaran
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.butBAT)
        Me.Controls.Add(Me.butSIM)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.keterangan)
        Me.Controls.Add(Me.butINPUT)
        Me.Controls.Add(Me.DGVhutang)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "HutangPembayaran"
        Me.Text = "HutangPembayaran"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DGVhutang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dateJT As System.Windows.Forms.DateTimePicker
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents giro As System.Windows.Forms.TextBox
    Friend WithEvents CBacc As System.Windows.Forms.ComboBox
    Friend WithEvents CBsup As System.Windows.Forms.ComboBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents DGVhutang As System.Windows.Forms.DataGridView
    Friend WithEvents total As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents bayart As System.Windows.Forms.TextBox
    Friend WithEvents jumlaht As System.Windows.Forms.TextBox
    Friend WithEvents noFak As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents butINPUT As System.Windows.Forms.Button
    Friend WithEvents keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
