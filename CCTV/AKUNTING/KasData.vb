﻿Public Class KasData

    Private Sub KasData_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub KasData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsACC
        Dim b As DataTable
        b = a.table("%", "%")
        With CBacc
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = b
        End With

        CBpilih.Text = "ALL"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim pilih As String = "%"
        If CBpilih.Text = "PEMBELIAN" Then
            pilih = "PV"
        ElseIf CBpilih.Text = "PENJUALAN" Then
            pilih = "RV"
        ElseIf CBpilih.Text = "DP" Then
            pilih = "DP"
        End If
        Dim a As New clsCCTV.clsPiutHutang
        Dim b As DataSet
        b = a.tableKas(Date1.Text, Date2.Text, "%" & Nama.Text & "%", CBacc.SelectedValue, pilih & "%")
        DGVkas.DataSource = b.Tables("DATKAS")

        Dim i As Integer
        Dim tot As Double = 0
        For i = 0 To DGVkas.RowCount - 1
            tot = tot + DGVkas.Item("BAYAR", i).Value
        Next

        totpembayaran.Text = FormatNumber(tot, 2)
    End Sub

    Private Sub DGVkas_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles DGVkas.CurrentCellDirtyStateChanged
     
        If DGVkas.IsCurrentCellDirty Then
            DGVkas.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub DGVkas_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DGVkas.CellValueChanged
        Try
            If DGVkas.Columns(e.ColumnIndex).Name = "CEK" Then
                If DGVkas.Item("CEK", DGVkas.CurrentRow.Index).Value.ToString = 1 Then
                    Dim c As New clsCCTV.clsPengguna
                    Dim d As Boolean
                    d = c.cek("CekDataKas")
                    If d = False Then
                        MsgBox(" anda tidak di otorisasi ")
                        DGVkas.Item("CEK", DGVkas.CurrentRow.Index).Value = 0
                        Exit Sub
                    End If
                    'cek kas
                    Dim a As New clsCCTV.clsPiutHutang
                    Dim b As Boolean
                    b = a.CEK(DGVkas.Item("NO_PV", DGVkas.CurrentRow.Index).Value.ToString, DGVkas.Item("CEK", DGVkas.CurrentRow.Index).Value.ToString)
                    DGVkas.Invalidate()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class