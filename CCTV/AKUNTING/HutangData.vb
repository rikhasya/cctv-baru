﻿Public Class HutangData

    Private Sub HutangData_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub HutangData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_SUP") = "ALL"
        row("K_SUP") = "0"
        b.Rows.Add(row)
        With CBsup
            .DisplayMember = "N_SUP"
            .ValueMember = "K_SUP"
            .DataSource = b
        End With
        CBsup.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim Lns As String
        If cbLunas.CheckState = CheckState.Checked Then
            Lns = "Y"
        ElseIf cbLunas.CheckState = CheckState.Indeterminate Then
            Lns = "%"
        ElseIf cbLunas.CheckState = CheckState.Unchecked Then
            Lns = "N"
        End If

        Dim sup As String
        If CBsup.Text = "ALL" Then
            sup = "%"
        Else
            sup = CBsup.SelectedValue
        End If

        Dim a As New clsCCTV.clsPiutHutang
        Dim b As DataTable
        b = a.tableHutang(date1.Text, date2.Text, sup, Lns)
        DGVhutang.DataSource = b
    End Sub

    Private Sub CBsup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBsup.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class