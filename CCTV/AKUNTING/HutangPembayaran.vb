﻿Public Class HutangPembayaran

    Private Sub HutangPembayaran_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_faktur.Show()
            Cari_faktur.dari.Text = "HUT"
            Cari_faktur.ksup.Text = CBsup.SelectedValue
        End If
    End Sub

    Private Sub HutangPembayaran_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub HutangPembayaran_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        b = a.table("%", "%")
        With CBsup
            .DisplayMember = "N_SUP"
            .ValueMember = "K_SUP"
            .DataSource = b
        End With

        Dim o As New clsCCTV.clsACC
        Dim l As DataTable
        l = o.table()
        With CBacc
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = l
        End With

        jumlaht.Text = FormatNumber(jumlaht.Text, 2)
        bayart.Text = FormatNumber(bayart.Text, 2)
        total.Text = FormatNumber(total.Text, 2)

    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If noFak.Text = "" Then
            butSIM.Focus()
            Exit Sub
        ElseIf CBacc.SelectedValue = "" Then
            MsgBox("Pilih Account")
            CBacc.Focus()
            Exit Sub
        End If

        frmMenu.MenuStrip1.Enabled = False
        Panel1.Enabled = False
        Dim a As Boolean = True
        Dim b As Integer

        For b = 0 To DGVhutang.RowCount - 1
            Console.WriteLine(noFak.Text & ":" & DGVhutang.Item("NO_FAK", b).Value)
            If noFak.Text = DGVhutang.Item("NO_FAK", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("No. Faktur Sudah Di Input")
            Exit Sub
        End If
        With DGVhutang
            .Rows.Add(1)
            .Rows(DGVhutang.Rows.Count() - 1).Cells("NO_FAK").Value = noFak.Text
            .Rows(DGVhutang.Rows.Count() - 1).Cells("JUMLAH").Value = jumlaht.Text
            .Rows(DGVhutang.Rows.Count() - 1).Cells("BAYAR").Value = bayart.Text
        End With
        hitung()
        noFak.Clear()
        jumlaht.Clear()
        bayart.Clear()
        noFak.Focus()

    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVhutang.RowCount - 1
            tot = tot + DGVhutang.Item("BAYAR", i).Value
        Next
        Try
            total.Text = FormatNumber(tot, 2)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DGVhutang_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVhutang.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVhutang.CurrentRow.Index.ToString

        DGVhutang.Rows.RemoveAt(baris)
        hitung()

    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click

        Dim i As Integer
        If CBacc.SelectedValue = "" Then
            MsgBox("Pilih Account")
            CBacc.Focus()
            Exit Sub
            'ElseIf i = 0 Then
            '    Exit Sub
        End If

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Lanjutkan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsPiutHutang
        Dim t As String
        t = a.nokas("PV")

        Dim table1 As New DataTable("DETKAS")
        table1.Columns.Add("NO_PV")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("BAYAR")

        For i = 0 To DGVhutang.Rows.Count - 1
            table1.Rows.Add(t, DGVhutang.Item("NO_FAK", i).Value.ToString, DGVhutang.Item("BAYAR", i).Value.ToString * -1)
        Next

        Dim tot As Double
        tot = CDbl(total.Text) * -1

        Dim b As Boolean
        b = a.inputkas(t, CBacc.SelectedValue, giro.Text, CBsup.SelectedValue, CBsup.Text, date1.Text, dateJT.Text, tot, keterangan.Text, clsCCTV.clsPengguna.CLUserID, table1)

        If b = True Then
            MsgBox("Berhasil Simpan Pembayaran Hutang")
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Simpan Pembayaran Hutang")
        End If
    End Sub

    Private Sub butBAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBAT.Click
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub CBsup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBsup.KeyDown
        If e.KeyCode = Keys.Enter Then
            CBacc.Focus()
        End If
    End Sub

    Private Sub CBacc_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBacc.KeyDown
        If e.KeyCode = Keys.Enter Then
            giro.Focus()
        End If
    End Sub

    Private Sub bayart_Leave(sender As Object, e As EventArgs) Handles bayart.Leave
        bayart.Text = FormatNumber(bayart.Text, 2)
    End Sub

    Private Sub bayart_TextChanged(sender As Object, e As EventArgs) Handles bayart.TextChanged

    End Sub
End Class