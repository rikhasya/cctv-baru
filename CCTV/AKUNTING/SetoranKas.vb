﻿Public Class SetoranKas

    Private Sub SetoranKas_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SetoranKas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsACC
        Dim b As DataTable
        b = a.table()
        With CBacc1
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = b
        End With

        Dim a2 As New clsCCTV.clsACC
        Dim b2 As DataTable
        b2 = a2.table()
        With CBacc2
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = b2
        End With
    End Sub

    Private Sub butINPUT_Click(sender As Object, e As EventArgs) Handles butINPUT.Click
        If CBacc1.Text = "" Or CBacc2.Text = "" Then
            MsgBox("Pilih Account")
            Exit Sub
        End If
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Setoran dari account " & CBacc1.Text & " ke account " & CBacc2.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Dim a As New clsCCTV.clsPiutHutang
        Dim t As String
        t = a.nokas("PB")
        Dim tt As String
        tt = a.nokas2("PB")
        Dim b As Boolean
        b = a.inputAntarACC(t, tt, CBacc1.SelectedValue, CBacc2.SelectedValue, giro.Text, "", nama.Text, CDate(tanggal.Text), CDate(JT.Text), CDbl(bayar.Text), Keterangan.Text, clsCCTV.clsPengguna.CLUserID)
        If b = True Then
            MsgBox("Berhasil Input Setoran Antar Account")
            Me.Close()
        Else
            MsgBox("Gagal Input Setoran Antar Account")
        End If
    End Sub

    Private Sub CBacc1_Leave(sender As Object, e As EventArgs) Handles CBacc1.Leave
        namaa()
    End Sub

    Sub namaa()
        If CBacc1.Text = "" Then
            CBacc1.Focus()
        ElseIf CBacc2.Text = "" Then
            CBacc2.Focus()
        End If
        nama.Text = "Setoran dari " & CBacc1.Text & " ke " & CBacc2.Text
    End Sub

    Private Sub CBacc2_Leave(sender As Object, e As EventArgs) Handles CBacc2.Leave
        namaa()
    End Sub

End Class