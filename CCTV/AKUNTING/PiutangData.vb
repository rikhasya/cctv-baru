﻿Public Class PiutangData

    Private Sub PiutangData_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub PiutangData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBlgn.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim Lns As String
        If cbLunas.CheckState = CheckState.Checked Then
            Lns = "Y"
        ElseIf cbLunas.CheckState = CheckState.Indeterminate Then
            Lns = "%"
        ElseIf cbLunas.CheckState = CheckState.Unchecked Then
            Lns = "N"
        End If

        Dim lgn As String = "%"
        If CBlgn.Text = "ALL" Then
            lgn = "%"
        Else
            lgn = CBlgn.SelectedValue
        End If
        Dim a As New clsCCTV.clsPiutHutang
        Dim b As DataTable
        b = a.tablePiutang(date1.Text, date2.Text, lgn, Lns)
        DGVpiutang.DataSource = b
    End Sub

    Private Sub DGVpiutang_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVpiutang.DoubleClick
        Dim row As Integer
        row = DGVpiutang.CurrentRow.Index.ToString

        Dim s As String = DGVpiutang.Item("NO_FAK", row).Value.ToString
        s = s.Substring(0, 2)
        If s = "JP" Then
            MsgBox("Penjualan Project Tidak Bisa Doubleclick")
            Exit Sub
        End If

        Dim frm As PiutangTerima
        frm = New PiutangTerima
        frm.MdiParent = frmMenu
        frm.noFak.Text = DGVpiutang.Item("NO_FAK", row).Value.ToString
        frm.bayart.Text = DGVpiutang.Item("SISA", row).Value.ToString
        frm.jumlaht.Text = DGVpiutang.Item("SISA", row).Value.ToString
        frm.lgn.Text = DGVpiutang.Item("N_LGN", row).Value.ToString
        frm.retail.Text = "y"
        frm.klgn.Text = DGVpiutang.Item("K_LGN", row).Value.ToString
        frm.CBlgn.Visible = False
        frm.lgn.Visible = True
        frm.bayart.Focus()
        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()
    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim lgn As String = "%"
        If CBlgn.Text = "ALL" Then
            lgn = "%"
        Else
            lgn = CBlgn.SelectedValue
        End If

        Dim a As New clsCCTV.clsPiutHutang
        Dim b As DataTable
        b = a.tablePiutangJT(CDate(date2.Text), lgn)
        DGVpiutang.DataSource = b
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class