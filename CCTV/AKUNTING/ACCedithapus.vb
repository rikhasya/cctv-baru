﻿Public Class ACCedithapus

    Private Sub butUBAH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUBAH.Click
        If k_acc.Text = "" Or n_acc.Text = "" Then
            MsgBox("Kode dan Nama harus diisi")
            Exit Sub
        End If

        Dim a As New clsCCTV.clsACC
        Dim b As Boolean
        b = a.update(k_acc.Text, n_acc.Text, clsCCTV.clsPengguna.CLUserID)

        If b = True Then
            MsgBox("Berhasil Mengubah Data Account")
            Me.Close()
        Else
            MsgBox("Gagal Mengubah Data")
            Exit Sub
        End If
    End Sub

    Private Sub ACCedithapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub ACCedithapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub buHAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buHAP.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Account?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsACC
        Dim b As Boolean
        b = a.hapus(k_acc.Text)

        If b = True Then
            MsgBox("Berhasil hapus account")
            Me.Close()
        Else
            MsgBox("Gagal hapus")
            Exit Sub
        End If

    End Sub
End Class