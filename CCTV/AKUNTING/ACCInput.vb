﻿Public Class ACCInput

    Private Sub ACCInput_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If k_acc.Text = "" Or n_acc.Text = "" Then
            MsgBox("Kode dan Nama harus diisi")
            Exit Sub
        End If

        Dim a As New clsCCTV.clsACC
        Dim b As Boolean
        b = a.input(k_acc.Text, n_acc.Text, clsCCTV.clsPengguna.CLUserID)

        If b = True Then
            MsgBox("Berhasil Input Data Account Baru")
            k_acc.Clear()
            n_acc.Clear()
        Else
            MsgBox("Gagal Input Data Account")
            Exit Sub
        End If
    End Sub

    Private Sub buBAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buBAT.Click
        Me.Close()
    End Sub

    Private Sub ACCInput_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class