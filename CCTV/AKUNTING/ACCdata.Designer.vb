﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ACCdata
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGVacc = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.kode = New System.Windows.Forms.TextBox()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.K_ACC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_ACC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVacc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVacc
        '
        Me.DGVacc.AllowUserToAddRows = False
        Me.DGVacc.AllowUserToDeleteRows = False
        Me.DGVacc.BackgroundColor = System.Drawing.Color.Peru
        Me.DGVacc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVacc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_ACC, Me.N_ACC})
        Me.DGVacc.Location = New System.Drawing.Point(198, 101)
        Me.DGVacc.Name = "DGVacc"
        Me.DGVacc.ReadOnly = True
        Me.DGVacc.RowHeadersVisible = False
        Me.DGVacc.Size = New System.Drawing.Size(283, 407)
        Me.DGVacc.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(525, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(142, 22)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "DATA ACCOUNT"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(526, 141)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Kode Account"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(526, 192)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Nama Account"
        '
        'kode
        '
        Me.kode.Location = New System.Drawing.Point(529, 157)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(72, 20)
        Me.kode.TabIndex = 0
        '
        'nama
        '
        Me.nama.Location = New System.Drawing.Point(529, 208)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(138, 20)
        Me.nama.TabIndex = 1
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.Location = New System.Drawing.Point(529, 248)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(72, 30)
        Me.butCARI.TabIndex = 2
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'K_ACC
        '
        Me.K_ACC.DataPropertyName = "K_ACC"
        Me.K_ACC.HeaderText = "Kode "
        Me.K_ACC.Name = "K_ACC"
        Me.K_ACC.ReadOnly = True
        '
        'N_ACC
        '
        Me.N_ACC.DataPropertyName = "N_ACC"
        Me.N_ACC.HeaderText = "Nama Account"
        Me.N_ACC.Name = "N_ACC"
        Me.N_ACC.ReadOnly = True
        '
        'ACCdata
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.butCARI)
        Me.Controls.Add(Me.nama)
        Me.Controls.Add(Me.kode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DGVacc)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "ACCdata"
        Me.Text = "ACCdata"
        CType(Me.DGVacc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVacc As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kode As System.Windows.Forms.TextBox
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents K_ACC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_ACC As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
