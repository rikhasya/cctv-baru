﻿Public Class ACCdata

    Private Sub ACCdata_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub


    Private Sub ACCdata_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loaddata()
    End Sub

    Sub loaddata()
        Dim a As New clsCCTV.clsACC
        Dim b As DataTable
        b = a.table("%" & kode.Text & "%", "%" & nama.Text & "%")
        DGVacc.DataSource = b
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        loaddata()
    End Sub

    Private Sub DGVacc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVacc.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("ACCEDITHAPUS")
        If b = True Then
            Dim row As Integer
            row = DGVacc.CurrentRow.Index.ToString
            ACCedithapus.Show()
            ACCedithapus.k_acc.Text = DGVacc.Item("K_ACC", row).Value.ToString
            ACCedithapus.n_acc.Text = DGVacc.Item("N_ACC", row).Value.ToString
        Else
            MsgBox("anda tdk di otorisasi")
        End If
        
    End Sub
End Class