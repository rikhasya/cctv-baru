﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KasData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVkas = New System.Windows.Forms.DataGridView()
        Me.Nama = New System.Windows.Forms.TextBox()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Date2 = New System.Windows.Forms.DateTimePicker()
        Me.Date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CBacc = New System.Windows.Forms.ComboBox()
        Me.totpembayaran = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CBpilih = New System.Windows.Forms.ComboBox()
        Me.CEK = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.NO_PV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jenisbayar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERIDCEK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGLCEK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVkas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVkas
        '
        Me.DGVkas.AllowUserToAddRows = False
        Me.DGVkas.AllowUserToDeleteRows = False
        Me.DGVkas.BackgroundColor = System.Drawing.Color.LightSeaGreen
        Me.DGVkas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVkas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CEK, Me.NO_PV, Me.Column1, Me.Column6, Me.jenisbayar, Me.Column5, Me.K_SUP, Me.Column7, Me.BAYAR, Me.Column12, Me.Column2, Me.VALID, Me.DATESTAMP, Me.USERIDCEK, Me.TGLCEK})
        Me.DGVkas.Location = New System.Drawing.Point(12, 77)
        Me.DGVkas.Name = "DGVkas"
        Me.DGVkas.RowHeadersVisible = False
        Me.DGVkas.Size = New System.Drawing.Size(978, 526)
        Me.DGVkas.TabIndex = 7
        '
        'Nama
        '
        Me.Nama.Location = New System.Drawing.Point(490, 40)
        Me.Nama.Name = "Nama"
        Me.Nama.Size = New System.Drawing.Size(151, 20)
        Me.Nama.TabIndex = 3
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.Location = New System.Drawing.Point(807, 40)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(62, 26)
        Me.butCARI.TabIndex = 4
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Script MT Bold", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(71, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 24)
        Me.Label5.TabIndex = 56
        Me.Label5.Text = "Data Kas"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(487, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Nama :"
        '
        'Date2
        '
        Me.Date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Date2.Location = New System.Drawing.Point(391, 40)
        Me.Date2.Name = "Date2"
        Me.Date2.Size = New System.Drawing.Size(83, 20)
        Me.Date2.TabIndex = 2
        '
        'Date1
        '
        Me.Date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Date1.Location = New System.Drawing.Point(286, 40)
        Me.Date1.Name = "Date1"
        Me.Date1.Size = New System.Drawing.Size(83, 20)
        Me.Date1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(369, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(23, 13)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "s/d"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(285, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "Tanggal :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(176, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "Jenis Bayar :"
        '
        'CBacc
        '
        Me.CBacc.FormattingEnabled = True
        Me.CBacc.Location = New System.Drawing.Point(179, 40)
        Me.CBacc.Name = "CBacc"
        Me.CBacc.Size = New System.Drawing.Size(89, 21)
        Me.CBacc.TabIndex = 0
        '
        'totpembayaran
        '
        Me.totpembayaran.Enabled = False
        Me.totpembayaran.Location = New System.Drawing.Point(782, 609)
        Me.totpembayaran.Name = "totpembayaran"
        Me.totpembayaran.Size = New System.Drawing.Size(208, 20)
        Me.totpembayaran.TabIndex = 60
        Me.totpembayaran.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(683, 612)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 13)
        Me.Label4.TabIndex = 61
        Me.Label4.Text = "Total Pembayaran"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(654, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 62
        Me.Label7.Text = "Beli/Jual :"
        '
        'CBpilih
        '
        Me.CBpilih.FormattingEnabled = True
        Me.CBpilih.Items.AddRange(New Object() {"ALL", "DP", "PEMBELIAN", "PENJUALAN"})
        Me.CBpilih.Location = New System.Drawing.Point(657, 40)
        Me.CBpilih.Name = "CBpilih"
        Me.CBpilih.Size = New System.Drawing.Size(121, 21)
        Me.CBpilih.TabIndex = 63
        '
        'CEK
        '
        Me.CEK.DataPropertyName = "CEK"
        Me.CEK.FalseValue = "0"
        Me.CEK.HeaderText = "Cek"
        Me.CEK.Name = "CEK"
        Me.CEK.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CEK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CEK.TrueValue = "1"
        '
        'NO_PV
        '
        Me.NO_PV.DataPropertyName = "NO_PV"
        Me.NO_PV.HeaderText = "No Pv"
        Me.NO_PV.Name = "NO_PV"
        Me.NO_PV.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "TGL_SERAH"
        Me.Column1.HeaderText = "Tanggal"
        Me.Column1.Name = "Column1"
        Me.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "TGL_JT"
        Me.Column6.HeaderText = "Tanggal JT"
        Me.Column6.Name = "Column6"
        Me.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'jenisbayar
        '
        Me.jenisbayar.DataPropertyName = "JNSBYR"
        Me.jenisbayar.HeaderText = "Jenis Bayar"
        Me.jenisbayar.Name = "jenisbayar"
        Me.jenisbayar.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.jenisbayar.Visible = False
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "N_SUP_N_LGN"
        Me.Column5.HeaderText = "Nama"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'K_SUP
        '
        Me.K_SUP.DataPropertyName = "K_LGN_K_SUP"
        Me.K_SUP.HeaderText = "Kode Sup"
        Me.K_SUP.Name = "K_SUP"
        Me.K_SUP.ReadOnly = True
        Me.K_SUP.Visible = False
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "NO_GIRO"
        Me.Column7.HeaderText = "No Giro"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.DataPropertyName = "BAYAR"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.BAYAR.DefaultCellStyle = DataGridViewCellStyle1
        Me.BAYAR.HeaderText = "Pembayaran"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "KET"
        Me.Column12.HeaderText = "Ket"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "USERID"
        Me.Column2.HeaderText = "UserID"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "DATESTAMP"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'USERIDCEK
        '
        Me.USERIDCEK.DataPropertyName = "USERIDCEK"
        Me.USERIDCEK.HeaderText = "UserdID Cek"
        Me.USERIDCEK.Name = "USERIDCEK"
        '
        'TGLCEK
        '
        Me.TGLCEK.DataPropertyName = "TGLJAMCEK"
        Me.TGLCEK.HeaderText = "Tanggal Cek"
        Me.TGLCEK.Name = "TGLCEK"
        '
        'KasData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 680)
        Me.Controls.Add(Me.CBpilih)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.totpembayaran)
        Me.Controls.Add(Me.CBacc)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Nama)
        Me.Controls.Add(Me.butCARI)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Date2)
        Me.Controls.Add(Me.Date1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DGVkas)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "KasData"
        Me.Text = "KasData"
        CType(Me.DGVkas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVkas As System.Windows.Forms.DataGridView
    Friend WithEvents Nama As System.Windows.Forms.TextBox
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CBacc As System.Windows.Forms.ComboBox
    Friend WithEvents totpembayaran As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CBpilih As System.Windows.Forms.ComboBox
    Friend WithEvents CEK As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NO_PV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jenisbayar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERIDCEK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGLCEK As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
