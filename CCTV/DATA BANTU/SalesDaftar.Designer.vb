﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SalesDaftar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGVsales = New System.Windows.Forms.DataGridView()
        Me.K_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.namaSls = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.kodeSls = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.DGVsales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGVsales
        '
        Me.DGVsales.AllowUserToAddRows = False
        Me.DGVsales.AllowUserToDeleteRows = False
        Me.DGVsales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsales.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_SLS, Me.N_SLS, Me.ALAMAT, Me.TELP, Me.TELP2})
        Me.DGVsales.Location = New System.Drawing.Point(28, 100)
        Me.DGVsales.Name = "DGVsales"
        Me.DGVsales.ReadOnly = True
        Me.DGVsales.RowHeadersVisible = False
        Me.DGVsales.Size = New System.Drawing.Size(940, 528)
        Me.DGVsales.TabIndex = 18
        '
        'K_SLS
        '
        Me.K_SLS.DataPropertyName = "K_SLS"
        Me.K_SLS.HeaderText = "Kode Sales"
        Me.K_SLS.Name = "K_SLS"
        Me.K_SLS.ReadOnly = True
        '
        'N_SLS
        '
        Me.N_SLS.DataPropertyName = "N_SLS"
        Me.N_SLS.HeaderText = "Nama Sales"
        Me.N_SLS.Name = "N_SLS"
        Me.N_SLS.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'TELP2
        '
        Me.TELP2.DataPropertyName = "TELP2"
        Me.TELP2.HeaderText = "Telp (2)"
        Me.TELP2.Name = "TELP2"
        Me.TELP2.ReadOnly = True
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(698, 12)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 2
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.namaSls)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.kodeSls)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-3, 46)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(195, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Kode Sales"
        '
        'namaSls
        '
        Me.namaSls.Location = New System.Drawing.Point(510, 14)
        Me.namaSls.Name = "namaSls"
        Me.namaSls.Size = New System.Drawing.Size(163, 20)
        Me.namaSls.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(421, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Sales"
        '
        'kodeSls
        '
        Me.kodeSls.Location = New System.Drawing.Point(279, 14)
        Me.kodeSls.Name = "kodeSls"
        Me.kodeSls.Size = New System.Drawing.Size(119, 20)
        Me.kodeSls.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 631)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(205, 13)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "* Doubleclick untuk edit/hapus data sales"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(423, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(120, 24)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Daftar Sales"
        '
        'SalesDaftar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.DGVsales)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "SalesDaftar"
        Me.Text = "SalesDaftar"
        CType(Me.DGVsales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVsales As System.Windows.Forms.DataGridView
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents namaSls As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kodeSls As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents K_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
