﻿Public Class CustomerEditHapus

    Private Sub CustomerEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub limit_Leave(sender As Object, e As EventArgs) Handles limit.Leave
        If Not IsNumeric(limit.Text) Then
            limit.Text = 0
        End If
        limit.Text = FormatNumber(limit.Text, 2)
    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If namaCus.Text = "" Then
            MsgBox("Nama harus di isi")
            namaCus.Focus()
            Exit Sub
        End If

        Dim piut As Integer
        If piutang.Checked = True Then
            piut = 1
        Else
            piut = 0
        End If

        Dim a As New clsCCTV.clsLangganan
        Dim b As Boolean
        b = a.update(namaCus.Text, alamat.Text, telp.Text, telp2.Text, fax.Text, kodeCus.Text, piut, limit.Text)

        If b = True Then
            MsgBox("Berhasil Mengubah Data")
            Me.Close()
        Else
            MsgBox("Gagal Mengubah Data")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & namaCus.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsLangganan
        Dim b As Boolean
        b = a.delete(kodeCus.Text)

        If b = True Then
            MsgBox("Berhasil Hapus Data Customer")
            Me.Close()
        Else
            MsgBox("Gagal Hapus Data")
        End If
    End Sub
End Class