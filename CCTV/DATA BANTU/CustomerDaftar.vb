﻿Public Class CustomerDaftar

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        If kodeLgn.Text.Length = 5 Then
            b = a.table(kodeLgn.Text, "%" & namaLgn.Text & "%")
        Else
            b = a.table("%" & kodeLgn.Text, "%" & namaLgn.Text & "%")
        End If
        DGVcustomer.DataSource = b
    End Sub

    Private Sub DGVcustomer_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVcustomer.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("CUSTOMEREDITHAPUS")
        If b = True Then
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
        Dim frm As CustomerEditHapus
        frm = New CustomerEditHapus
        frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Customer"
        Dim row As Integer
        row = DGVcustomer.CurrentRow.Index.ToString
        frm.MdiParent = frmMenu
        frm.kodeCus.Text = DGVcustomer.Item("K_LGN", row).Value.ToString
        frm.namaCus.Text = DGVcustomer.Item("N_LGN", row).Value.ToString
        frm.alamat.Text = DGVcustomer.Item("ALAMAT", row).Value.ToString
        frm.telp.Text = DGVcustomer.Item("TELP", row).Value.ToString
            frm.telp2.Text = DGVcustomer.Item("TELP2", row).Value.ToString
            frm.fax.Text = DGVcustomer.Item("FAX", row).Value.ToString
            frm.limit.Text = FormatNumber(DGVcustomer.Item("LIMIT", row).Value.ToString, 2)
            frm.Show()
        frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub CustomerDaftar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub CustomerDaftar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class