﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SupplierDaftar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGVsupplier = New System.Windows.Forms.DataGridView()
        Me.K_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FAX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.namaSup = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.kodeSup = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.DGVsupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGVsupplier
        '
        Me.DGVsupplier.AllowUserToAddRows = False
        Me.DGVsupplier.AllowUserToDeleteRows = False
        Me.DGVsupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsupplier.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_SUP, Me.N_SUP, Me.ALAMAT, Me.TELP, Me.TELP2, Me.FAX})
        Me.DGVsupplier.Location = New System.Drawing.Point(29, 114)
        Me.DGVsupplier.Name = "DGVsupplier"
        Me.DGVsupplier.ReadOnly = True
        Me.DGVsupplier.RowHeadersVisible = False
        Me.DGVsupplier.Size = New System.Drawing.Size(940, 528)
        Me.DGVsupplier.TabIndex = 22
        '
        'K_SUP
        '
        Me.K_SUP.DataPropertyName = "K_SUP"
        Me.K_SUP.HeaderText = "Kode Supplier"
        Me.K_SUP.Name = "K_SUP"
        Me.K_SUP.ReadOnly = True
        '
        'N_SUP
        '
        Me.N_SUP.DataPropertyName = "N_SUP"
        Me.N_SUP.HeaderText = "Nama Supplier"
        Me.N_SUP.Name = "N_SUP"
        Me.N_SUP.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'TELP2
        '
        Me.TELP2.DataPropertyName = "TELP2"
        Me.TELP2.HeaderText = "Telp (2)"
        Me.TELP2.Name = "TELP2"
        Me.TELP2.ReadOnly = True
        '
        'FAX
        '
        Me.FAX.DataPropertyName = "FAX"
        Me.FAX.HeaderText = "Fax"
        Me.FAX.Name = "FAX"
        Me.FAX.ReadOnly = True
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(704, 11)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 2
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 645)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(224, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "* Doubleclick untuk edit/hapus data customer"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.namaSup)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.kodeSup)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-2, 60)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(207, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Kode Supplier"
        '
        'namaSup
        '
        Me.namaSup.Location = New System.Drawing.Point(522, 14)
        Me.namaSup.Name = "namaSup"
        Me.namaSup.Size = New System.Drawing.Size(163, 20)
        Me.namaSup.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(433, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Supplier"
        '
        'kodeSup
        '
        Me.kodeSup.Location = New System.Drawing.Point(291, 14)
        Me.kodeSup.Name = "kodeSup"
        Me.kodeSup.Size = New System.Drawing.Size(119, 20)
        Me.kodeSup.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(424, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(147, 24)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Daftar Supplier"
        '
        'SupplierDaftar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.DGVsupplier)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "SupplierDaftar"
        Me.Text = "SupplierDaftar"
        CType(Me.DGVsupplier, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVsupplier As System.Windows.Forms.DataGridView
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents namaSup As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kodeSup As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents K_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FAX As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
