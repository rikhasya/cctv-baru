﻿Public Class TeknisiTambah

    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        e.Cancel = False
    End Sub

    Private Sub TeknisiTambah_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub TeknisiTambah_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub nTek_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nTek.Validated
        ErrorProvider1.SetError(nTek, "")
    End Sub

    Private Sub nTek_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles nTek.Validating
        If nTek.Text = "" Then
            e.Cancel = True
            nTek.Select(0, nTek.Text.Length)
            ErrorProvider1.SetError(nTek, "harus di isi")
        End If
    End Sub

    Private Sub butSIM_Click(sender As Object, e As EventArgs) Handles butSIM.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Teknisi Baru ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsTehnisi
        Dim b As Boolean
        b = a.input(nTek.Text, alamat.Text, telp.Text, telp2.Text)
        If b = True Then
            MsgBox("Berhasil Input Teknisi Baru")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Input Teknisi Baru")
            Exit Sub
        End If
    End Sub

    Private Sub butBAT_Click(sender As Object, e As EventArgs) Handles butBAT.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Input Teknisi Baru ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Me.Close()
        Exit Sub
    End Sub
End Class