﻿Public Class SalesEditHapus

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If nSales.Text = "" Then
            MsgBox("Nama harus di isi")
            nSales.Focus()
            Exit Sub
        End If

        Dim a As New clsCCTV.clsSales
        Dim b As Boolean
        b = a.update(nSales.Text, alamat.Text, telp.Text, telp2.Text, kSales.Text)

        If b = True Then
            MsgBox("Berhasil Mengubah Data")
            Me.Close()
        Else
            MsgBox("Gagal Mengubah Data")
        End If
    End Sub

    Private Sub SalesEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub butHAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAP.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & nSales.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsSales
        Dim b As Boolean
        b = a.delete(kSales.Text)

        If b = True Then
            MsgBox("Berhasil Menghapus Data Supplier")
            Me.Close()
        Else
            MsgBox("Gagal Menghapus Data")
        End If
    End Sub
End Class