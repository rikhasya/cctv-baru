﻿Public Class CustomerTambah

    Private Sub CustomerTambah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub CustomerTambah_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        limit.Text = FormatNumber(30000000, 2)
    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If NamaLgn.Text = "" Then
            MsgBox("Nama harus di isi")
            NamaLgn.Focus()
            Exit Sub
        End If

        Dim a As New clsCCTV.clsLangganan
        Dim b As Boolean
        b = a.input(NamaLgn.Text, alamat.Text, telp.Text, telp2.Text, fax.Text, limit.Text)

        If b = True Then
            MsgBox("Berhasil")
            Me.Close()
        Else
            MsgBox("Gagal")
        End If
    End Sub

    Private Sub limit_Leave(sender As Object, e As EventArgs) Handles limit.Leave
        If Not IsNumeric(limit.Text) Then
            limit.Text = 0
        End If
        limit.Text = FormatNumber(limit.Text, 2)
    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Simpan Customer Baru ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Me.Close()
    End Sub
End Class