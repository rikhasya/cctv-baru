﻿Public Class TeknisiEditHapus

    Private Sub butSIM_Click(sender As Object, e As EventArgs) Handles butSIM.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Simpan Perubahan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsTehnisi
        Dim b As Boolean
        b = a.update(nTek.Text, alamat.Text, telp.Text, telp2.Text, kTek.Text)

        If b = True Then
            MsgBox("Berhasil Simpan Perubahan")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Simpan Perubahan")
            Exit Sub
        End If
    End Sub

    Private Sub butHAP_Click(sender As Object, e As EventArgs) Handles butHAP.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Teknisi ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsTehnisi
        Dim b As Boolean
        b = a.delete(kTek.Text)

        If b = True Then
            MsgBox("Berhasil Hapus Teknisi")
            Me.Close()
            Exit Sub
        Else
            MsgBox("Gagal Hapus Teknisi")
            Exit Sub
        End If

    End Sub
End Class