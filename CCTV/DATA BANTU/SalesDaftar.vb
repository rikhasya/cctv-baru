﻿Public Class SalesDaftar

    Private Sub SalesDaftar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsSales
        Dim b As DataTable
        If kodeSls.Text.Length = 5 Then
            b = a.table(kodeSls.Text, "%" & namaSls.Text & "%")
        Else
            b = a.table("%" & kodeSls.Text, "%" & namaSls.Text & "%")
        End If
        
        DGVsales.DataSource = b
    End Sub

    Private Sub DGVsales_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVsales.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SALESEDITHAPUS")
        If b = True Then
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
        Dim frm As SalesEditHapus
        frm = New SalesEditHapus
        frm.MdiParent = frmMenu
        frm.Text = "Edit Hapus Sales"
        Dim row As Integer
        row = DGVsales.CurrentRow.Index.ToString
        frm.MdiParent = frmMenu
        frm.kSales.Text = DGVsales.Item("K_SLS", row).Value.ToString
        frm.nSales.Text = DGVsales.Item("N_SLS", row).Value.ToString
        frm.alamat.Text = DGVsales.Item("ALAMAT", row).Value.ToString
        frm.telp.Text = DGVsales.Item("TELP", row).Value.ToString
        frm.telp2.Text = DGVsales.Item("TELP2", row).Value.ToString
            frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub
End Class