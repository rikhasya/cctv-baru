﻿Public Class SupplierDaftar

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        If kodeSup.Text.Length = 5 Then
            b = a.table(kodeSup.Text, "%" & namaSup.Text & "%")
        Else
            b = a.table("%" & kodeSup.Text, "%" & namaSup.Text & "%")
        End If

        DGVsupplier.DataSource = b
    End Sub

    Private Sub DGVsupplier_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVsupplier.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SUPPLIEREDITHAPUS")
        If b = True Then
         
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As SupplierEditHapus
        frm = New SupplierEditHapus
        frm.MdiParent = frmMenu
        frm.Text = "Edit Hapus Supplier"
        Dim row As Integer
        row = DGVsupplier.CurrentRow.Index.ToString
        frm.MdiParent = frmMenu
        frm.kodeSup.Text = DGVsupplier.Item("K_SUP", row).Value.ToString
        frm.NamaSup.Text = DGVsupplier.Item("N_SUP", row).Value.ToString
        frm.alamat.Text = DGVsupplier.Item("ALAMAT", row).Value.ToString
        frm.telp.Text = DGVsupplier.Item("TELP", row).Value.ToString
        frm.telp2.Text = DGVsupplier.Item("TELP2", row).Value.ToString
        frm.fax.Text = DGVsupplier.Item("FAX", row).Value.ToString
            frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub SupplierDaftar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SupplierDaftar_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class