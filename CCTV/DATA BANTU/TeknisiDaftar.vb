﻿Public Class TeknisiDaftar

    Private Sub TeknisiDaftar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub butCARI_Click(sender As Object, e As EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsTehnisi
        Dim b As DataTable
        If kTek.Text.Length = 5 Then
            b = a.table(kTek.Text, "%" & nTek.Text & "%")
        Else
            b = a.table("%" & kTek.Text, "%" & nTek.Text & "%")
        End If
        DGVsales.DataSource = b
        b.Dispose()
    End Sub

    Private Sub DGVsales_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVsales.CellContentClick

    End Sub

    Private Sub DGVsales_DoubleClick(sender As Object, e As EventArgs) Handles DGVsales.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("TEHNISIEDITHAPUS")
        If b = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If
        If Me.MdiChildren.Length > 0 Then
            Dim childForm As Form = CType(ActiveMdiChild, Form)
            childForm.Close()
        End If
        Dim frm As TeknisiEditHapus
        frm = New TeknisiEditHapus
        frm.MdiParent = frmMenu
        frm.Text = "Edit / Hapus Teknisi"
        Dim row As Integer
        row = DGVsales.CurrentRow.Index.ToString
        frm.kTek.Text = DGVsales.Item("K_TEH", row).Value.ToString
        frm.nTek.Text = DGVsales.Item("N_TEH", row).Value.ToString
        frm.alamat.Text = DGVsales.Item("ALAMAT", row).Value.ToString
        frm.telp.Text = DGVsales.Item("TELP", row).Value.ToString
        frm.telp2.Text = DGVsales.Item("TELP2", row).Value.ToString
        frm.Show()
        frm.Dock = DockStyle.Fill
        Me.Close()

    End Sub
End Class