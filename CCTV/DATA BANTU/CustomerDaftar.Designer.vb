﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CustomerDaftar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.namaLgn = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.kodeLgn = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DGVcustomer = New System.Windows.Forms.DataGridView()
        Me.K_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FAX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUST_LAMA = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.LIMIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVcustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(714, 12)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 2
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(217, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Kode Customer"
        '
        'namaLgn
        '
        Me.namaLgn.Location = New System.Drawing.Point(532, 14)
        Me.namaLgn.Name = "namaLgn"
        Me.namaLgn.Size = New System.Drawing.Size(163, 20)
        Me.namaLgn.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 634)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(224, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "* Doubleclick untuk edit/hapus data customer"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.namaLgn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.kodeLgn)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-3, 46)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(443, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Customer"
        '
        'kodeLgn
        '
        Me.kodeLgn.Location = New System.Drawing.Point(301, 14)
        Me.kodeLgn.Name = "kodeLgn"
        Me.kodeLgn.Size = New System.Drawing.Size(119, 20)
        Me.kodeLgn.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(416, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(158, 24)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Daftar Customer"
        '
        'DGVcustomer
        '
        Me.DGVcustomer.AllowUserToAddRows = False
        Me.DGVcustomer.AllowUserToDeleteRows = False
        Me.DGVcustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVcustomer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_LGN, Me.N_LGN, Me.ALAMAT, Me.TELP, Me.TELP2, Me.FAX, Me.CUST_LAMA, Me.LIMIT, Me.USERID, Me.DATESTAMP})
        Me.DGVcustomer.Location = New System.Drawing.Point(31, 100)
        Me.DGVcustomer.Name = "DGVcustomer"
        Me.DGVcustomer.ReadOnly = True
        Me.DGVcustomer.RowHeadersVisible = False
        Me.DGVcustomer.Size = New System.Drawing.Size(940, 531)
        Me.DGVcustomer.TabIndex = 19
        '
        'K_LGN
        '
        Me.K_LGN.DataPropertyName = "K_LGN"
        Me.K_LGN.HeaderText = "Kode Customer"
        Me.K_LGN.Name = "K_LGN"
        Me.K_LGN.ReadOnly = True
        '
        'N_LGN
        '
        Me.N_LGN.DataPropertyName = "N_LGN"
        Me.N_LGN.HeaderText = "Nama Customer"
        Me.N_LGN.Name = "N_LGN"
        Me.N_LGN.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'TELP2
        '
        Me.TELP2.DataPropertyName = "TELP2"
        Me.TELP2.HeaderText = "Telp (2)"
        Me.TELP2.Name = "TELP2"
        Me.TELP2.ReadOnly = True
        '
        'FAX
        '
        Me.FAX.DataPropertyName = "FAX"
        Me.FAX.HeaderText = "Fax"
        Me.FAX.Name = "FAX"
        Me.FAX.ReadOnly = True
        '
        'CUST_LAMA
        '
        Me.CUST_LAMA.DataPropertyName = "CUST_LAMA"
        Me.CUST_LAMA.FalseValue = "0"
        Me.CUST_LAMA.HeaderText = "Izin Piutang"
        Me.CUST_LAMA.Name = "CUST_LAMA"
        Me.CUST_LAMA.ReadOnly = True
        Me.CUST_LAMA.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CUST_LAMA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CUST_LAMA.TrueValue = "1"
        '
        'LIMIT
        '
        Me.LIMIT.DataPropertyName = "LIMIT"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.LIMIT.DefaultCellStyle = DataGridViewCellStyle1
        Me.LIMIT.HeaderText = "Limit"
        Me.LIMIT.Name = "LIMIT"
        Me.LIMIT.ReadOnly = True
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "DATESATMP"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'CustomerDaftar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.DGVcustomer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "CustomerDaftar"
        Me.Text = "CustomerDaftar"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVcustomer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents namaLgn As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kodeLgn As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DGVcustomer As System.Windows.Forms.DataGridView
    Friend WithEvents K_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FAX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUST_LAMA As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents LIMIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
