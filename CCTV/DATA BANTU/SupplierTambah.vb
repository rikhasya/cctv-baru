﻿Public Class SupplierTambah

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        If NamaSup.Text = "" Then
            MsgBox("Nama harus di isi")
            NamaSup.Focus()
            Exit Sub
        End If
        Dim a As New clsCCTV.clsSupplier
        Dim b As Boolean
        b = a.input(NamaSup.Text, alamat.Text, telp.Text, telp2.Text, fax.Text)

        If b = True Then
            MsgBox("Berhasil Menambahkan Supplier")
            NamaSup.Clear()
            alamat.Clear()
            telp.Clear()
            telp2.Clear()
            fax.Clear()
        Else
            MsgBox("Gagal Menambahkan Supplier")
        End If
    End Sub

    Private Sub SupplierTambah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SupplierTambah_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

End Class