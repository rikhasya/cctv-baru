﻿Imports System.Drawing.Printing
Imports System.IO

Public Class PembelianEditHapus

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub PembelianEditHapus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.dari.Text = "EBELI"
            Cari_Brg.DGVcari.Focus()
        End If
    End Sub

    Private Sub PembelianEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub PembelianEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loaddata()
        hitung()

        Disc.Text = FormatNumber(Disc.Text, 2)
    End Sub

    Sub loaddata()
        Dim a As New clsCCTV.clsBeli
        Dim b As DataSet
        b = a.table(NoFak.Text)
        DGVbeli.DataSource = b.Tables("DETBELI")
        kode.Focus()
    End Sub

    Private Sub kode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles kode.KeyDown
        If e.KeyCode = Keys.Enter Then
            If kode.Text = "" Then
                butINPUT.Focus()
            Else
                Dim a As New clsCCTV.clsBarang
                Dim b As DataTable
                b = a.table(kode.Text)
                If b.Rows.Count = 1 Then
                    Nama.Text = b.Rows(0)("N_BRG").ToString
                    Sat.Text = b.Rows(0)("SAT").ToString
                    Qty.Focus()
                Else
                    Exit Sub
                End If
            End If
        End If
    End Sub


    Private Sub DGVbeli_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVbeli.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVbeli.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsBeli
        Dim b As Boolean
        b = a.hapus1brg(NoFak.Text, DGVbeli.Item("K_BRG", baris).Value.ToString)

        If b = True Then
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Hapus Barang")
        End If

    End Sub

    Private Sub butBATAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBATAL.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & NoFak.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsBeli
        Dim b As Boolean
        b = a.hapus(NoFak.Text)
        If b = True Then
            MsgBox("Berhasil Hapus Faktur No. " & NoFak.Text)
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Mengahus Faktur No. " & NoFak.Text)
        End If
    End Sub

    Private Sub butSELESAI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSELESAI.Click
        If Total2.Text = 0 Then
            Exit Sub
        End If
        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan perubahan ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsBeli
        Dim b As Boolean
        b = a.setvalid(NoFak.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text)

        If b = True Then
            MsgBox("Berhasil Menyimpan Perubahan")
            PRINT()
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Menyimpan Perubahan")
        End If
    End Sub

    Private Sub Harga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Harga.Leave, Qty.Leave, Pot1.Leave, Pot2.Leave
        If IsNumeric(Qty.Text) And IsNumeric(Harga.Text) And IsNumeric(Pot1.Text) And IsNumeric(Pot2.Text) Then

            Jumlah.Text = CDbl(Harga.Text) * CDbl(Qty.Text)
            Total.Text = CDbl(Jumlah.Text) * (100 - CDbl(Pot1.Text)) / 100 * (100 - CDbl(Pot2.Text)) / 100
            Harga.Text = FormatNumber(Harga.Text, 2)
            Jumlah.Text = FormatNumber(Jumlah.Text, 2)
            Total.Text = FormatNumber(Total.Text, 2)
            Total2.Text = FormatNumber(Total.Text, 2)
        End If
    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If kode.Text = "" Then
            butSELESAI.Focus()
            Exit Sub
        End If
        Dim a As New clsCCTV.clsBeli

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("HARGA")
        table1.Columns.Add("JUMLAH")
        table1.Columns.Add("POT1")
        table1.Columns.Add("POT2")
        table1.Columns.Add("TOTAL")

        table1.Rows.Add(NoFak.Text, kode.Text, Nama.Text, Qty.Text, Sat.Text, CDbl(Harga.Text), CDbl(Jumlah.Text), CDbl(Pot1.Text), CDbl(Pot2.Text), CDbl(Total.Text))

        Dim b As Boolean
        b = a.input1brg(NoFak.Text, table1)

        If b = True Then
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Input !")
        End If

        kode.Clear()
        Nama.Clear()
        Qty.Clear()
        Harga.Clear()
        Sat.Clear()
        Jumlah.Clear()
        Pot1.Clear()
        Pot2.Clear()
        Total.Clear()
        kode.Focus()

    End Sub

    Private Sub Total2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Total2.TextChanged
        hitung()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVbeli.RowCount - 1
            tot = tot + DGVbeli.Item("TOTAL_BRG", i).Value
        Next

        Try
            Total2.Text = FormatNumber(tot, 2)
            Disc.Text = FormatNumber(Disc.Text, 2)
            Grandtot.Text = CDbl(Total2.Text) - CDbl(Disc.Text)
            If cbPPN.Checked = True Then
                PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
            Else
                PPN.Text = "0.00"
            End If
            Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Disc_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Disc.Leave
        hitung()
    End Sub

    Private Sub cbPPN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPN.CheckedChanged
        hitung()
    End Sub

    Sub PRINT()

        Dim ask As DialogResult

        ask = MessageBox.Show("Print Faktur Pembelian ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsBeli
            Pembelian.tb = otc.table(NoFak.Text)

            With Pembelian
                .tmpnofak = .tb.Tables("DATBELI").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Pembelian.tb.Tables("DETBELI").Rows.Count - 1

            ReDim Pembelian.tmpKode(jumlah)
            ReDim Pembelian.tmpNama(jumlah)
            ReDim Pembelian.tmpQty(jumlah)
            ReDim Pembelian.tmpSat(jumlah)
            ReDim Pembelian.tmpHrg(jumlah)
            ReDim Pembelian.tmpJmlh(jumlah)
            ReDim Pembelian.tmpPot1(jumlah)
            ReDim Pembelian.tmpPot2(jumlah)
            ReDim Pembelian.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Pembelian
                    .tmpKode(ipo) = .tb.Tables("DETBELI").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETBELI").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETBELI").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETBELI").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETBELI").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETBELI").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETBELI").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETBELI").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETBELI").Rows(ipo)("TOTAL").ToString

                End With
            Next
            With Pembelian
                arrke = 0
                printFont = New Font("Times New Roman", 10)
            End With

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Pembelian.faktur
            PrintDoc.Print()
        End If
    End Sub

#Region "errorprov"

    Private Sub kode_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kode.Validated
        ErrorProvider1.SetError(kode, "")
    End Sub

    Private Sub kode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles kode.Validating
        If kode.Text = "" Then
            butSELESAI.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
            Else
                ErrorProvider1.SetError(kode, "koreksi kode barang")
            End If
        End If
    End Sub

    Private Sub Disc_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc.Validated
        ErrorProvider1.SetError(Disc, "")
    End Sub

    Private Sub Disc_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Disc.Validating
        If Not IsNumeric(Disc.Text) Or Disc.Text = "" Then
            e.Cancel = True
            Disc.Select(0, Disc.Text.Length)
            ErrorProvider1.SetError(Disc, "harap di isi")
        End If

    End Sub

    Private Sub Qty_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Qty.Validated
        ErrorProvider1.SetError(Qty, "")
    End Sub

    Private Sub Qty_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Qty.Validating
        If Not IsNumeric(Qty.Text) Or Qty.Text = "" Then
            e.Cancel = True
            Qty.Select(0, Qty.Text.Length)
            ErrorProvider1.SetError(Qty, "harap di isi, harus angka")
        End If
    End Sub

    Private Sub Harga_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Harga.Validated
        ErrorProvider1.SetError(Harga, "")
    End Sub

    Private Sub Harga_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Harga.Validating
        If Not IsNumeric(Harga.Text) Or Harga.Text = "" Then
            e.Cancel = True
            Harga.Select(0, Harga.Text.Length)
            ErrorProvider1.SetError(Harga, "harap di isi")
        End If
    End Sub

    Private Sub Pot1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot1.Validated
        ErrorProvider1.SetError(Pot1, "")
    End Sub

    Private Sub Pot1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot1.Validating
        If Not IsNumeric(Pot1.Text) Or Pot1.Text = "" Then
            e.Cancel = True
            Pot1.Select(0, Pot1.Text.Length)
            ErrorProvider1.SetError(Pot1, "harap di isi")
        End If
    End Sub

    Private Sub Pot2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot2.Validated
        ErrorProvider1.SetError(Pot2, "")
    End Sub

    Private Sub Pot2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot2.Validating
        If Not IsNumeric(Pot2.Text) Or Pot2.Text = "" Then
            e.Cancel = True
            Pot2.Select(0, Pot2.Text.Length)
            ErrorProvider1.SetError(Pot2, "harap di isi")
        End If
    End Sub

#End Region

    Private Sub kode_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles kode.Leave
        If kode.Text = "" Then
            butINPUT.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                kode.Text = b.Rows(0)("K_BRG").ToString
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
                Qty.Focus()
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub kode_TextChanged(sender As Object, e As EventArgs) Handles kode.TextChanged

    End Sub
End Class