﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pembelian_Laporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVheader = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TANGGAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_JT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRANDTOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GGTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_PO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_RETUR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GIRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CBsup = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVheader
        '
        Me.DGVheader.AllowUserToAddRows = False
        Me.DGVheader.AllowUserToDeleteRows = False
        Me.DGVheader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVheader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.TANGGAL, Me.TGL_JT, Me.N_SUP, Me.NO_SUP, Me.TOTAL1, Me.DISC, Me.GRANDTOT, Me.PPN, Me.GGTOTAL, Me.KET, Me.K_SUP, Me.USERID, Me.VALID, Me.BAYAR, Me.LUNAS, Me.NO_PO, Me.NO_RETUR, Me.GIRO})
        Me.DGVheader.Location = New System.Drawing.Point(12, 104)
        Me.DGVheader.Name = "DGVheader"
        Me.DGVheader.ReadOnly = True
        Me.DGVheader.RowHeadersVisible = False
        Me.DGVheader.Size = New System.Drawing.Size(978, 263)
        Me.DGVheader.TabIndex = 23
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'TANGGAL
        '
        Me.TANGGAL.DataPropertyName = "TGL_FAK"
        Me.TANGGAL.HeaderText = "Tanggal"
        Me.TANGGAL.Name = "TANGGAL"
        Me.TANGGAL.ReadOnly = True
        '
        'TGL_JT
        '
        Me.TGL_JT.DataPropertyName = "TGL_JT"
        Me.TGL_JT.HeaderText = "Tanggal JT"
        Me.TGL_JT.Name = "TGL_JT"
        Me.TGL_JT.ReadOnly = True
        '
        'N_SUP
        '
        Me.N_SUP.DataPropertyName = "N_SUP"
        Me.N_SUP.HeaderText = "Supplier"
        Me.N_SUP.Name = "N_SUP"
        Me.N_SUP.ReadOnly = True
        '
        'NO_SUP
        '
        Me.NO_SUP.DataPropertyName = "NO_SUP"
        Me.NO_SUP.HeaderText = "No Supplier"
        Me.NO_SUP.Name = "NO_SUP"
        Me.NO_SUP.ReadOnly = True
        '
        'TOTAL1
        '
        Me.TOTAL1.DataPropertyName = "TOTAL"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.TOTAL1.DefaultCellStyle = DataGridViewCellStyle1
        Me.TOTAL1.HeaderText = "Total"
        Me.TOTAL1.Name = "TOTAL1"
        Me.TOTAL1.ReadOnly = True
        '
        'DISC
        '
        Me.DISC.DataPropertyName = "DISC"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.DISC.DefaultCellStyle = DataGridViewCellStyle2
        Me.DISC.HeaderText = "Diskon"
        Me.DISC.Name = "DISC"
        Me.DISC.ReadOnly = True
        '
        'GRANDTOT
        '
        Me.GRANDTOT.DataPropertyName = "GTOTAL"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.GRANDTOT.DefaultCellStyle = DataGridViewCellStyle3
        Me.GRANDTOT.HeaderText = "Grand Total"
        Me.GRANDTOT.Name = "GRANDTOT"
        Me.GRANDTOT.ReadOnly = True
        '
        'PPN
        '
        Me.PPN.DataPropertyName = "PPN"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.PPN.DefaultCellStyle = DataGridViewCellStyle4
        Me.PPN.HeaderText = "PPN"
        Me.PPN.Name = "PPN"
        Me.PPN.ReadOnly = True
        '
        'GGTOTAL
        '
        Me.GGTOTAL.DataPropertyName = "GGTOTAL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.GGTOTAL.DefaultCellStyle = DataGridViewCellStyle5
        Me.GGTOTAL.HeaderText = "Grand Total"
        Me.GGTOTAL.Name = "GGTOTAL"
        Me.GGTOTAL.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'K_SUP
        '
        Me.K_SUP.DataPropertyName = "K_SUP"
        Me.K_SUP.HeaderText = "Kode Supplier"
        Me.K_SUP.Name = "K_SUP"
        Me.K_SUP.ReadOnly = True
        Me.K_SUP.Visible = False
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.DataPropertyName = "BAYAR"
        Me.BAYAR.HeaderText = "Bayar"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        Me.BAYAR.Visible = False
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        Me.LUNAS.Visible = False
        '
        'NO_PO
        '
        Me.NO_PO.DataPropertyName = "NO_PO"
        Me.NO_PO.HeaderText = "Nomer PO"
        Me.NO_PO.Name = "NO_PO"
        Me.NO_PO.ReadOnly = True
        Me.NO_PO.Visible = False
        '
        'NO_RETUR
        '
        Me.NO_RETUR.DataPropertyName = "NO_RETUR"
        Me.NO_RETUR.HeaderText = "No Retur"
        Me.NO_RETUR.Name = "NO_RETUR"
        Me.NO_RETUR.ReadOnly = True
        Me.NO_RETUR.Visible = False
        '
        'GIRO
        '
        Me.GIRO.DataPropertyName = "GIRO"
        Me.GIRO.HeaderText = "Giro"
        Me.GIRO.Name = "GIRO"
        Me.GIRO.ReadOnly = True
        Me.GIRO.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Belladone", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(382, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(175, 26)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Laporan Pembelian"
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(713, 10)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 3
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(445, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Supplier"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "* Doubleclick untuk edit/hapus laporan"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.CBsup)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.date2)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-2, 35)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'CBsup
        '
        Me.CBsup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBsup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBsup.FormattingEnabled = True
        Me.CBsup.Location = New System.Drawing.Point(527, 10)
        Me.CBsup.Name = "CBsup"
        Me.CBsup.Size = New System.Drawing.Size(180, 21)
        Me.CBsup.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(323, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "s/d"
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(347, 11)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(88, 20)
        Me.date2.TabIndex = 1
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(233, 11)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(181, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Tanggal"
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToAddRows = False
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.HARGA, Me.JUMLAH, Me.POT1, Me.POT2, Me.TOTAL})
        Me.DGVdetail.Location = New System.Drawing.Point(12, 388)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.ReadOnly = True
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(978, 250)
        Me.DGVdetail.TabIndex = 24
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle6
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.HARGA.DefaultCellStyle = DataGridViewCellStyle7
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.ReadOnly = True
        '
        'JUMLAH
        '
        Me.JUMLAH.DataPropertyName = "JUMLAH"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.JUMLAH.DefaultCellStyle = DataGridViewCellStyle8
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        '
        'POT1
        '
        Me.POT1.DataPropertyName = "POT1"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.POT1.DefaultCellStyle = DataGridViewCellStyle9
        Me.POT1.HeaderText = "Pot 1"
        Me.POT1.Name = "POT1"
        Me.POT1.ReadOnly = True
        '
        'POT2
        '
        Me.POT2.DataPropertyName = "POT2"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.POT2.DefaultCellStyle = DataGridViewCellStyle10
        Me.POT2.HeaderText = "Pot 2"
        Me.POT2.Name = "POT2"
        Me.POT2.ReadOnly = True
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle11
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 371)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Detail Laporan :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(210, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(146, 13)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "* F1 untuk print ulang laporan"
        '
        'Pembelian_Laporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.DGVheader)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "Pembelian_Laporan"
        Me.Text = "Pembelian_Laporan"
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVheader As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CBsup As System.Windows.Forms.ComboBox
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TANGGAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_JT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRANDTOT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GGTOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_PO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_RETUR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GIRO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
