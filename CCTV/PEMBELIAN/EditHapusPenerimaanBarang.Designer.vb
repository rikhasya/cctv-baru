﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditHapusPenerimaanBarang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Keterangan = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.butINPUT = New System.Windows.Forms.Button()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.klgn = New System.Windows.Forms.Label()
        Me.nlgn = New System.Windows.Forms.TextBox()
        Me.noSO = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Tanggal = New System.Windows.Forms.DateTimePicker()
        Me.noSJ = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DGV2 = New System.Windows.Forms.DataGridView()
        Me.NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SERIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butINP = New System.Windows.Forms.Button()
        Me.noserial = New System.Windows.Forms.TextBox()
        Me.kbrg2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTSJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY_SJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Keterangan
        '
        Me.Keterangan.Location = New System.Drawing.Point(665, 372)
        Me.Keterangan.Multiline = True
        Me.Keterangan.Name = "Keterangan"
        Me.Keterangan.Size = New System.Drawing.Size(271, 66)
        Me.Keterangan.TabIndex = 96
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(662, 356)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 97
        Me.Label15.Text = "Keterangan"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(818, 444)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 33)
        Me.Button2.TabIndex = 95
        Me.Button2.Text = "BATAL"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'butINPUT
        '
        Me.butINPUT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINPUT.Location = New System.Drawing.Point(726, 444)
        Me.butINPUT.Name = "butINPUT"
        Me.butINPUT.Size = New System.Drawing.Size(75, 33)
        Me.butINPUT.TabIndex = 94
        Me.butINPUT.Text = "INPUT SJ"
        Me.butINPUT.UseVisualStyleBackColor = True
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToAddRows = False
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.TOTSJ, Me.SAT, Me.QTY_SJ, Me.LUNAS, Me.HARGA, Me.JUMLAH, Me.POT1, Me.POT2, Me.TOTAL})
        Me.DGVdetail.Location = New System.Drawing.Point(37, 84)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(897, 228)
        Me.DGVdetail.TabIndex = 93
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.Label1.Location = New System.Drawing.Point(37, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(206, 23)
        Me.Label1.TabIndex = 92
        Me.Label1.Text = "Input Penerimaan Barang"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.klgn)
        Me.Panel1.Controls.Add(Me.nlgn)
        Me.Panel1.Controls.Add(Me.noSO)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Tanggal)
        Me.Panel1.Controls.Add(Me.noSJ)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(37, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(897, 42)
        Me.Panel1.TabIndex = 91
        '
        'klgn
        '
        Me.klgn.AutoSize = True
        Me.klgn.Location = New System.Drawing.Point(861, 24)
        Me.klgn.Name = "klgn"
        Me.klgn.Size = New System.Drawing.Size(31, 13)
        Me.klgn.TabIndex = 15
        Me.klgn.Text = "kode"
        Me.klgn.Visible = False
        '
        'nlgn
        '
        Me.nlgn.Enabled = False
        Me.nlgn.Location = New System.Drawing.Point(611, 11)
        Me.nlgn.Name = "nlgn"
        Me.nlgn.Size = New System.Drawing.Size(198, 20)
        Me.nlgn.TabIndex = 14
        '
        'noSO
        '
        Me.noSO.Enabled = False
        Me.noSO.Location = New System.Drawing.Point(253, 11)
        Me.noSO.Name = "noSO"
        Me.noSO.Size = New System.Drawing.Size(116, 20)
        Me.noSO.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(205, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "No. PO"
        '
        'Tanggal
        '
        Me.Tanggal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Tanggal.Location = New System.Drawing.Point(441, 11)
        Me.Tanggal.Name = "Tanggal"
        Me.Tanggal.Size = New System.Drawing.Size(92, 20)
        Me.Tanggal.TabIndex = 7
        '
        'noSJ
        '
        Me.noSJ.Enabled = False
        Me.noSJ.Location = New System.Drawing.Point(72, 11)
        Me.noSJ.Name = "noSJ"
        Me.noSJ.Size = New System.Drawing.Size(116, 20)
        Me.noSJ.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(560, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Supplier"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(389, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tanggal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. Terima"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.DGV2)
        Me.GroupBox1.Controls.Add(Me.butINP)
        Me.GroupBox1.Controls.Add(Me.noserial)
        Me.GroupBox1.Controls.Add(Me.kbrg2)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Location = New System.Drawing.Point(37, 314)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(557, 272)
        Me.GroupBox1.TabIndex = 98
        Me.GroupBox1.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 251)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(174, 13)
        Me.Label5.TabIndex = 93
        Me.Label5.Text = "Klik 2x di baris barang untuk Hapus"
        '
        'DGV2
        '
        Me.DGV2.AllowUserToAddRows = False
        Me.DGV2.AllowUserToDeleteRows = False
        Me.DGV2.BackgroundColor = System.Drawing.Color.FloralWhite
        Me.DGV2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO, Me.K_BRG1, Me.NO_SERIAL, Me.NO_FAK})
        Me.DGV2.Location = New System.Drawing.Point(15, 55)
        Me.DGV2.Name = "DGV2"
        Me.DGV2.ReadOnly = True
        Me.DGV2.RowHeadersVisible = False
        Me.DGV2.Size = New System.Drawing.Size(452, 191)
        Me.DGV2.TabIndex = 5
        '
        'NO
        '
        Me.NO.DataPropertyName = "NO"
        Me.NO.HeaderText = "No"
        Me.NO.Name = "NO"
        Me.NO.ReadOnly = True
        '
        'K_BRG1
        '
        Me.K_BRG1.DataPropertyName = "K_BRG"
        Me.K_BRG1.HeaderText = "Kode Barang"
        Me.K_BRG1.Name = "K_BRG1"
        Me.K_BRG1.ReadOnly = True
        '
        'NO_SERIAL
        '
        Me.NO_SERIAL.DataPropertyName = "SERIAL"
        Me.NO_SERIAL.HeaderText = "Nomor Serial"
        Me.NO_SERIAL.Name = "NO_SERIAL"
        Me.NO_SERIAL.ReadOnly = True
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "NO_FAK"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'butINP
        '
        Me.butINP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINP.Location = New System.Drawing.Point(473, 26)
        Me.butINP.Name = "butINP"
        Me.butINP.Size = New System.Drawing.Size(75, 23)
        Me.butINP.TabIndex = 4
        Me.butINP.Text = "INPUT"
        Me.butINP.UseVisualStyleBackColor = True
        '
        'noserial
        '
        Me.noserial.Location = New System.Drawing.Point(185, 29)
        Me.noserial.Name = "noserial"
        Me.noserial.Size = New System.Drawing.Size(282, 20)
        Me.noserial.TabIndex = 3
        '
        'kbrg2
        '
        Me.kbrg2.Enabled = False
        Me.kbrg2.Location = New System.Drawing.Point(15, 29)
        Me.kbrg2.Name = "kbrg2"
        Me.kbrg2.Size = New System.Drawing.Size(164, 20)
        Me.kbrg2.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(182, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "No Serial"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 13)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Kode Barang"
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle36.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle36
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        '
        'TOTSJ
        '
        Me.TOTSJ.DataPropertyName = "TOTSJ"
        Me.TOTSJ.HeaderText = "Total SJ"
        Me.TOTSJ.Name = "TOTSJ"
        Me.TOTSJ.Visible = False
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'QTY_SJ
        '
        Me.QTY_SJ.DataPropertyName = "QTY_SJ"
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle37.Format = "N2"
        Me.QTY_SJ.DefaultCellStyle = DataGridViewCellStyle37
        Me.QTY_SJ.HeaderText = "Qty Terima"
        Me.QTY_SJ.Name = "QTY_SJ"
        Me.QTY_SJ.ReadOnly = True
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.FalseValue = "N"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        Me.LUNAS.TrueValue = "Y"
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle38.Format = "N2"
        Me.HARGA.DefaultCellStyle = DataGridViewCellStyle38
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.Visible = False
        '
        'JUMLAH
        '
        Me.JUMLAH.DataPropertyName = "JUMLAH"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle39.Format = "N2"
        Me.JUMLAH.DefaultCellStyle = DataGridViewCellStyle39
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.Visible = False
        '
        'POT1
        '
        Me.POT1.DataPropertyName = "POT1"
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle40.Format = "N2"
        Me.POT1.DefaultCellStyle = DataGridViewCellStyle40
        Me.POT1.HeaderText = "Pot 1"
        Me.POT1.Name = "POT1"
        Me.POT1.Visible = False
        '
        'POT2
        '
        Me.POT2.DataPropertyName = "POT2"
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle41.Format = "N2"
        Me.POT2.DefaultCellStyle = DataGridViewCellStyle41
        Me.POT2.HeaderText = "Pot 2"
        Me.POT2.Name = "POT2"
        Me.POT2.Visible = False
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle42.Format = "N2"
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle42
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.Visible = False
        '
        'PenerimaanBarang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(976, 590)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Keterangan)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.butINPUT)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "PenerimaanBarang"
        Me.Text = "PenerimaanBarang"
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents butINPUT As System.Windows.Forms.Button
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents klgn As System.Windows.Forms.Label
    Friend WithEvents nlgn As System.Windows.Forms.TextBox
    Friend WithEvents noSO As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents noSJ As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTSJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY_SJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DGV2 As System.Windows.Forms.DataGridView
    Friend WithEvents NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SERIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents butINP As System.Windows.Forms.Button
    Friend WithEvents noserial As System.Windows.Forms.TextBox
    Friend WithEvents kbrg2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
