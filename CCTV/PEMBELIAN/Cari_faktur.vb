﻿Public Class Cari_faktur

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        If dari.Text = "RBELI" Then
            DGVfaktur.Enabled = True
            DGVfaktur.Focus()
            Dim a As New clsCCTV.clsBeli
            Dim n As DataSet
            n = a.table(date1.Text, date2.Text, ksup.Text)

            Dim i As Integer
            For i = 0 To n.Tables("DATBELI").Rows.Count - 1
                Console.WriteLine("dat" & n.Tables("DATBELI").Rows(i)(0).ToString)
            Next

            Dim parentColumn As DataColumn = n.Tables("DATBELI").Columns("NO_FAK")
            Dim masterbindingSource As New BindingSource
            masterbindingSource.DataSource = n
            masterbindingSource.DataMember = "DATBELI"
            DGVfaktur.DataSource = masterbindingSource
            DGVfaktur.Focus()

        ElseIf dari.Text = "RJUAL" Then
            DGVfaktur2.Visible = True
            DGVfaktur2.Focus()
            Dim a As New clsCCTV.clsJual
            Dim n As DataSet
            n = a.table(date1.Text, date2.Text, ksup.Text, "%")

            Dim parentColumn As DataColumn = n.Tables("DATJUAL").Columns("NO_FAK")
            Dim masterbindingSource As New BindingSource
            masterbindingSource.DataSource = n
            masterbindingSource.DataMember = "DATJUAL"
            DGVfaktur2.DataSource = masterbindingSource
            DGVfaktur2.Focus()

        ElseIf dari.Text = "HSVC" Then
            DGVfaktur1.Visible = True
            DGVfaktur1.Focus()
            Dim a As New clsCCTV.clsService
            Dim n As DataSet
            n = a.table(date1.Text, date2.Text, ksup.Text)

            Dim parentColumn As DataColumn = n.Tables("DATSERVICE").Columns("NO_FAK")
            Dim masterbindingSource As New BindingSource
            masterbindingSource.DataSource = n
            masterbindingSource.DataMember = "DATSERVICE"
            DGVfaktur1.DataSource = masterbindingSource
            DGVfaktur1.Focus()

        ElseIf dari.Text = "HUT" Then
            DGVfaktur3.Visible = True
            DGVfaktur3.Focus()
            input.Visible = True
            Dim a As New clsCCTV.clsPiutHutang
            Dim n As DataTable
            n = a.tableHutang(date1.Text, date2.Text, ksup.Text, "N")
            DGVfaktur3.DataSource = n
            DGVfaktur3.Focus()

        ElseIf dari.Text = "PIUT" Then
            DGVfaktur4.Visible = True
            DGVfaktur4.Focus()
            input.Visible = True
            Dim a As New clsCCTV.clsPiutHutang
            Dim n As DataTable
            n = a.tablePiutang(date1.Text, date2.Text, ksup.Text, "N")
            DGVfaktur4.DataSource = n
            DGVfaktur4.Focus()

        ElseIf dari.Text = "CMPN" Then
            DGVfaktur2.Visible = True
            DGVfaktur2.Focus()
            Dim a As New clsCCTV.clsJual
            Dim n As DataSet
            n = a.table(date1.Text, date2.Text, ksup.Text, "%")

            Dim parentColumn As DataColumn = n.Tables("DATJUAL").Columns("NO_FAK")
            Dim masterbindingSource As New BindingSource
            masterbindingSource.DataSource = n
            masterbindingSource.DataMember = "DATJUAL"
            DGVfaktur2.DataSource = masterbindingSource
            DGVfaktur2.Focus()

        End If

    End Sub

    Private Sub DGVfaktur_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVfaktur.DoubleClick
        Dim row As Integer
        row = DGVfaktur.CurrentRow.Index.ToString

        If dari.Text = "RBELI" Then
            Dim frm As Pembelian_Retur = CType(frmMenu.MdiChildren(0), Form)
            frm.noBeli.Text = DGVfaktur.Item("NO_FAK", row).Value.ToString
            frm.kode.Focus()
            Me.Close()

        End If
    End Sub

    Private Sub DGVfaktur2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVfaktur2.DoubleClick
        Dim row As Integer
        row = DGVfaktur2.CurrentRow.Index.ToString

        If dari.Text = "RJUAL" Then
            Dim frm As Penjualan_Retur = CType(frmMenu.MdiChildren(0), Form)
            frm.noJual.Text = DGVfaktur2.Item("NO_FAK1", row).Value.ToString
            frm.ksls.Text = DGVfaktur2.Item("K_SLS", row).Value.ToString
            frm.nsls.Text = DGVfaktur2.Item("N_SLS", row).Value.ToString
            frm.kode.Focus()
            Me.Close()

        ElseIf dari.Text = "CMPN" Then
            Dim frm As ComplainInput = CType(frmMenu.MdiChildren(0), Form)
            frm.noJual.Text = DGVfaktur2.Item("NO_FAK1", row).Value.ToString
            Dim s As System.DateTime = DGVfaktur2.Item("tgl_fak2", row).Value.ToString
            frm.JTgaransi.Text = s.AddYears(1)
            Console.WriteLine(DGVfaktur2.Item("tgl_fak2", row).Value.ToString)
            Me.Close()

        End If
    End Sub


    Private Sub DGVfaktur3_DoubleClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVfaktur3.DoubleClick
        Dim row As Integer
        row = DGVfaktur3.CurrentRow.Index.ToString

        If dari.Text = "HUT" Then
            Dim frm As HutangPembayaran = CType(frmMenu.MdiChildren(0), Form)
            frm.noFak.Text = DGVfaktur3.Item("NO_FAK2", row).Value.ToString
            frm.jumlaht.Text = DGVfaktur3.Item("GGTOTAL", row).Value.ToString
            frm.bayart.Text = DGVfaktur3.Item("SISA", row).Value.ToString
            frm.bayart.Focus()
            Me.Close()

        End If
    End Sub

    Private Sub DGVfaktur4_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVfaktur4.DoubleClick
        Dim row As Integer
        row = DGVfaktur4.CurrentRow.Index.ToString

        Dim frm As PiutangTerima = CType(frmMenu.MdiChildren(0), Form)

        Dim a As New clsCCTV.clsPiutHutang
        Dim b As Double = 0
        Dim depan As String = "%"
        depan = Microsoft.VisualBasic.Left(DGVfaktur4.Item("NO_FAK3", row).Value.ToString, 2)

        frm.noFak.Text = DGVfaktur4.Item("NO_FAK3", row).Value.ToString
        frm.jumlaht.Text = DGVfaktur4.Item("GGTOTAL3", row).Value.ToString
        frm.bayart.Text = DGVfaktur4.Item("SISA3", row).Value.ToString
        frm.bayart.Focus()
        frm.bayart.Enabled = True

        If depan = "JP" Then
            b = a.jumDP(DGVfaktur4.Item("NO_FAK3", row).Value.ToString)
            If b <> 0 Then
                MsgBox("Account harus DP, karena sudah ada pembayaran DP sebesar " & FormatNumber(b, 2))
                frm.bayart.Text = b
                frm.bayart.Enabled = False
                frm.dp.Text = "1"
                frm.CBacc.Focus()
            End If
        End If
        Me.Close()

    End Sub

    Private Sub DGVfaktur1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVfaktur1.DoubleClick
        Dim row As Integer
        row = DGVfaktur1.CurrentRow.Index.ToString

        If dari.Text = "HSVC" Then
            Dim frm As ServiceInputHasil = CType(frmMenu.MdiChildren(0), Form)
            frm.noService.Text = DGVfaktur1.Item("NO_FAK4", row).Value.ToString
            frm.qty1.Focus()
            Me.Close()
        End If
    End Sub

    Private Sub input_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles input.Click
        If dari.Text = "HUT" Then
            Dim frm As HutangPembayaran = CType(frmMenu.MdiChildren(0), 
        Form)  'size 800, 572
            Dim baris As Integer
            baris = DGVfaktur3.CurrentRow.Index.ToString
            Dim i As Integer
            Dim i2 As Integer
            For i = 0 To DGVfaktur3.RowCount - 1
                If DGVfaktur3.Item("check1", i).Value = 1 Then
                    frm.DGVhutang.Rows.Add(1)
                    frm.DGVhutang.Rows(i2).Cells("NO_FAK").Value() = DGVfaktur3.Item("NO_FAK2", i).Value.ToString
                    frm.DGVhutang.Rows(i2).Cells("JUMLAH").Value = FormatNumber(DGVfaktur3.Item("SISA", i).Value.ToString, 2)
                    frm.DGVhutang.Rows(i2).Cells("BAYAR").Value = FormatNumber(DGVfaktur3.Item("SISA", i).Value.ToString, 2)
                    i2 = i2 + 1
                End If
            Next
            frm.hitung()
            Me.Close()
        ElseIf dari.Text = "PIUT" Then
            Dim frm As PiutangTerima = CType(frmMenu.MdiChildren(0), 
        Form)  'size 800, 572
            Dim baris As Integer
            baris = DGVfaktur4.CurrentRow.Index.ToString
            Dim i As Integer
            Dim i2 As Integer
            For i = 0 To DGVfaktur4.RowCount - 1
                If DGVfaktur4.Item("check", i).Value = 1 Then
                    frm.DGVpiutang.Rows.Add(1)
                    frm.DGVpiutang.Rows(i2).Cells("NO_FAK").Value() = DGVfaktur4.Item("NO_FAK3", i).Value.ToString
                    frm.DGVpiutang.Rows(i2).Cells("JUMLAH").Value = FormatNumber(DGVfaktur4.Item("SISA3", i).Value.ToString, 2)
                    frm.DGVpiutang.Rows(i2).Cells("BAYAR").Value = FormatNumber(DGVfaktur4.Item("SISA3", i).Value.ToString, 2)
                    i2 = i2 + 1
                End If
            Next
            frm.hitung()
            Me.Close()
        End If
    End Sub

    Private Sub Cari_faktur_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Cari_faktur_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class