﻿Imports System.Drawing.Printing
Imports System.IO

Public Class Pembelian_Retur

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        e.Cancel = False
    End Sub

#Region "errorprov"

    Private Sub CBsupplier_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            noBeli.Focus()
        End If
    End Sub

    Private Sub Disc_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
        ErrorProvider1.SetError(Disc, "")
    End Sub

    Private Sub Disc_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not IsNumeric(Disc.Text) Or Disc.Text = "" Then
            e.Cancel = True
            Disc.Select(0, Disc.Text.Length)
            ErrorProvider1.SetError(Disc, "harap di isi")
        End If

    End Sub

    Private Sub Qty_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Qty.Validated
        ErrorProvider1.SetError(Qty, "")
    End Sub

    Private Sub Qty_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Qty.Validating
        If Not IsNumeric(Qty.Text) Or Qty.Text = "" Then
            e.Cancel = True
            Qty.Select(0, Qty.Text.Length)
            ErrorProvider1.SetError(Qty, "harap di isi, harus angka")
        End If
    End Sub

    Private Sub Harga_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Harga.Validated
        ErrorProvider1.SetError(Harga, "")
    End Sub

    Private Sub Harga_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Harga.Validating
        If Not IsNumeric(Harga.Text) Or Harga.Text = "" Then
            e.Cancel = True
            Harga.Select(0, Harga.Text.Length)
            ErrorProvider1.SetError(Harga, "harap di isi")
        End If
    End Sub

    Private Sub Pot1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot1.Validated
        ErrorProvider1.SetError(Pot1, "")
    End Sub

    Private Sub Pot1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot1.Validating
        If Not IsNumeric(Pot1.Text) Or Pot1.Text = "" Then
            e.Cancel = True
            Pot1.Select(0, Pot1.Text.Length)
            ErrorProvider1.SetError(Pot1, "harap di isi")
        End If
    End Sub

    Private Sub Pot2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot2.Validated
        ErrorProvider1.SetError(Pot2, "")
    End Sub

    Private Sub Pot2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot2.Validating
        If Not IsNumeric(Pot2.Text) Or Pot2.Text = "" Then
            e.Cancel = True
            Pot2.Select(0, Pot2.Text.Length)
            ErrorProvider1.SetError(Pot2, "harap di isi")
        End If
    End Sub

#End Region

    Private Sub kode_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles kode.Leave
        If kode.Text = "" Then
            butINPUT.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                kode.Text = b.Rows(0)("K_BRG").ToString
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
                Qty.Focus()
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub Pembelian_Retur_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.dari.Text = "RBELI"
            Cari_Brg.no.Text = noBeli.Text
            Cari_Brg.DGVretur.Focus()

        ElseIf e.KeyCode = Keys.F2 Then
            Cari_faktur.Show()
            Cari_faktur.dari.Text = "RBELI"
            'Cari_faktur.supplier.Text = CBsupplier.SelectedItem
            Cari_faktur.ksup.Text = kodesup.Text

        ElseIf e.KeyCode = Keys.F3 Then
            DaftarKontak.Show()
            DaftarKontak.dari = "RBELI"
            DaftarKontak.kode.Focus()
        End If
    End Sub

    Private Sub Pembelian_Retur_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Pembelian_Retur_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
    End Sub

    Private Sub butBATAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBATAL.Click
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub DGVretur_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVretur.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVretur.CurrentRow.Index.ToString

        kode.Text = DGVretur.Item("K_BRG", baris).Value.ToString
        Nama.Text = DGVretur.Item("N_BRG", baris).Value.ToString
        Qty.Text = DGVretur.Item("QTY_BRG", baris).Value.ToString
        Harga.Text = DGVretur.Item("HARGA_BRG", baris).Value.ToString
        Sat.Text = DGVretur.Item("SAT_BRG", baris).Value.ToString
        Jumlah.Text = DGVretur.Item("JUMLAH_BRG", baris).Value.ToString
        Pot1.Text = DGVretur.Item("POT1_BRG", baris).Value.ToString
        Pot2.Text = DGVretur.Item("POT2_BRG", baris).Value.ToString
        Total.Text = DGVretur.Item("TOTAL_BRG", baris).Value.ToString

        DGVretur.Rows.RemoveAt(baris)
        hitung()
    End Sub

    Private Sub Disc_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Disc.Leave
        hitung()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVretur.RowCount - 1
            tot = tot + DGVretur.Item("TOTAL_BRG", i).Value
        Next
        Try
            Total2.Text = FormatNumber(tot, 2)
            Disc.Text = FormatNumber(Disc.Text, 2)
            Dim a As Double = 0
            a = CDbl(Total2.Text) - CDbl(Disc.Text)
            Grandtot.Text = FormatNumber(a, 2)
            If cbPPN.Checked = True Then
                PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
            Else
                PPN.Text = "0.00"
            End If
            Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub butSELESAI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSELESAI.Click
        If Total2.Text = 0 Then
            Exit Sub
        End If

        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan data retur pembelian ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim b As New clsCCTV.clsReBeli
        Dim t As String
        t = b.no()

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("HARGA")
        table1.Columns.Add("JUMLAH")
        table1.Columns.Add("POT1")
        table1.Columns.Add("POT2")
        table1.Columns.Add("TOTAL")

        Dim i As Integer
        For i = 0 To DGVretur.Rows.Count - 1
            table1.Rows.Add(t, DGVretur.Item("K_BRG", i).Value.ToString, DGVretur.Item("N_BRG", i).Value.ToString, DGVretur.Item("QTY_BRG", i).Value.ToString, DGVretur.Item("SAT_BRG", i).Value.ToString, CDbl(DGVretur.Item("HARGA_BRG", i).Value.ToString), CDbl(DGVretur.Item("JUMLAH_BRG", i).Value.ToString), CDbl(DGVretur.Item("POT1_BRG", i).Value.ToString), CDbl(DGVretur.Item("POT2_BRG", i).Value.ToString), CDbl(DGVretur.Item("TOTAL_BRG", i).Value.ToString))
        Next

        Dim a As New clsCCTV.clsReBeli
        Dim p As Boolean
        p = a.input(t, Tanggal.Text, kodesup.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text, Tanggal2.Text, noBeli.Text, table1)

        If p = True Then
            MsgBox("Berhasil Simpan Data Retur Pembelian")
            ask = MessageBox.Show("Print Faktur?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.No Then
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
                Exit Sub
            End If

            Dim otc As New clsCCTV.clsReBeli
            tmpnofak = t
            tb = otc.table(tmpnofak)

            Dim jumlah As Integer = tb.Tables("DETREBELI").Rows.Count - 1

            ReDim tmpKode(jumlah)
            ReDim tmpNama(jumlah)
            ReDim tmpQty(jumlah)
            ReDim tmpSat(jumlah)
            ReDim tmpHrg(jumlah)
            ReDim tmpJmlh(jumlah)
            ReDim tmpPot1(jumlah)
            ReDim tmpPot2(jumlah)
            ReDim tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah

                tmpKode(ipo) = tb.Tables("DETREBELI").Rows(ipo)("K_BRG").ToString
                tmpNama(ipo) = tb.Tables("DETREBELI").Rows(ipo)("N_BRG").ToString
                tmpQty(ipo) = tb.Tables("DETREBELI").Rows(ipo)("QTY").ToString
                tmpSat(ipo) = tb.Tables("DETREBELI").Rows(ipo)("SAT").ToString
                tmpHrg(ipo) = tb.Tables("DETREBELI").Rows(ipo)("HARGA").ToString
                tmpJmlh(ipo) = tb.Tables("DETREBELI").Rows(ipo)("JUMLAH").ToString
                tmpPot1(ipo) = tb.Tables("DETREBELI").Rows(ipo)("POT1").ToString
                tmpPot2(ipo) = tb.Tables("DETREBELI").Rows(ipo)("POT2").ToString
                tmpTot(ipo) = tb.Tables("DETREBELI").Rows(ipo)("TOTAL").ToString
            Next

            arrke = 0
            printFont = New Font("Times New Roman", 10)

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Me.faktur
            PrintDoc.Print()

            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Menyimpan Data Retur Pembelian")
        End If

    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If namasup.Text = "" Then
            MsgBox("Pilih Supplier")
            Exit Sub
        ElseIf Qty.Text > qty2.Text Then
            MsgBox("Qty retur tidak bisa lebih besar")
            Exit Sub
        ElseIf kode.Text = "" Then
            butSELESAI.Focus()
            Exit Sub
        End If

        Dim a As Boolean = True
        Dim b As Integer

        Panel1.Enabled = False
        frmMenu.MenuStrip1.Enabled = False

        For b = 0 To DGVretur.RowCount - 1
            Console.WriteLine(kode.Text & ":" & DGVretur.Item("K_BRG", b).Value)
            If kode.Text = DGVretur.Item("K_BRG", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        With DGVretur
            .Rows.Add(1)
            .Rows(DGVretur.Rows.Count() - 1).Cells("K_BRG").Value = kode.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("N_BRG").Value = Nama.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("QTY_BRG").Value = Qty.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("SAT_BRG").Value = Sat.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("HARGA_BRG").Value = Harga.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("JUMLAH_BRG").Value = Jumlah.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("pot1_BRG").Value = Pot1.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("pot2_BRG").Value = Pot2.Text
            .Rows(DGVretur.Rows.Count() - 1).Cells("TOTAL_BRG").Value = Total.Text
            butSELESAI.Visible = True
            kode.Focus()
        End With

        hitung()

        kode.Clear()
        Nama.Clear()
        Qty.Clear()
        qty2.Text = 0
        Sat.Clear()
        Harga.Clear()
        Jumlah.Clear()
        Pot1.Clear()
        Pot2.Clear()
        Total.Clear()
    End Sub

    Private Sub Harga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Harga.Leave, Qty.Leave, Pot1.Leave, Pot2.Leave

        If IsNumeric(Qty.Text) And IsNumeric(Harga.Text) And IsNumeric(Pot1.Text) And IsNumeric(Pot2.Text) Then

            Jumlah.Text = CDbl(Harga.Text) * CDbl(Qty.Text)
            Total.Text = CDbl(Jumlah.Text) * (100 - CDbl(Pot1.Text)) / 100 * (100 - CDbl(Pot2.Text)) / 100
            Harga.Text = FormatNumber(Harga.Text, 2)
            Jumlah.Text = FormatNumber(Jumlah.Text, 2)
            Total.Text = FormatNumber(Total.Text, 2)
            Total2.Text = FormatNumber(Total.Text, 2)
        End If

    End Sub

    Private Sub cbPPN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPN.CheckedChanged
        hitung()
    End Sub

    Public Sub faktur(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim count As Integer = 0
        Dim leftMargin As Single = ev.MarginBounds.Left
        Dim topMargin As Single = ev.MarginBounds.Top
        Dim line As String = Nothing

        'hitung line per halaman 
        'linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics)

        Dim ukuran As New Font("Times New Roman", 11, FontStyle.Regular)
        Dim ukJD As New Font("Times New Roman", 10, FontStyle.Bold)
        Dim ukJ As New Font("Times New Roman", 10, FontStyle.Regular)
        Dim ukMn As New Font("Times New Roman", 9, FontStyle.Regular)
        Dim ukT As New Font("Times New Roman", 9, FontStyle.Bold)

        ev.Graphics.DrawString("FAKTUR RETUR PEMBELIAN", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 320, 15)
        ev.Graphics.DrawString(tmpnofak, New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 385, 35)

        ev.Graphics.DrawImage(My.Resources.logo_sm, 30, 20, 190, 100)

        'ev.Graphics.DrawString("SEJAHTERA SENTOSA", New Font(FontFamily.GenericSerif, 9, FontStyle.Bold), Brushes.Black, 60, 85)

        ev.Graphics.DrawString(tb.Tables("DATREBELI").Rows(0)("TGL_FAK"), New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 405, 55)

        ev.Graphics.DrawString("Pengirim : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 620, 40)
        ev.Graphics.DrawString(tb.Tables("DATREBELI").Rows(0)("N_SUP"), New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 620, 60)

        Dim displayRectangleK As New Rectangle(New Point(620, 80), New Size(200, 200))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleK)
        Dim formatK As New StringFormat(StringFormatFlags.NoClip)
        formatK.LineAlignment = StringAlignment.Near ' = top
        formatK.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        'ev.Graphics.DrawString(tb.Tables("DATERBELI").Rows(0)("ALAMAT"), ukJ, Brushes.Black, RectangleF.op_Implicit(displayRectangleK), formatK)

        ev.Graphics.DrawString("Dengan rincian barang sebagai berikut : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 30, 130)

        ev.Graphics.DrawLine(Pens.Black, 25, 150, 820, 150)
        ev.Graphics.DrawLine(Pens.Black, 25, 150, 25, 350) 'vertikal 
        ev.Graphics.DrawLine(Pens.Black, 45, 150, 45, 350) 'ok
        ev.Graphics.DrawString("Kode Barang", ukJD, Brushes.Black, 60, 150)
        ev.Graphics.DrawLine(Pens.Black, 155, 150, 155, 350)
        ev.Graphics.DrawString("Nama Barang", ukJD, Brushes.Black, 200, 150)
        ev.Graphics.DrawLine(Pens.Black, 355, 150, 355, 350)
        ev.Graphics.DrawString("Qty", ukJD, Brushes.Black, 365, 150)
        ev.Graphics.DrawLine(Pens.Black, 402, 150, 402, 350)
        ev.Graphics.DrawString("Sat", ukJD, Brushes.Black, 412, 150)
        ev.Graphics.DrawLine(Pens.Black, 445, 150, 445, 350)
        ev.Graphics.DrawString("Harga", ukJD, Brushes.Black, 463, 150)
        ev.Graphics.DrawLine(Pens.Black, 530, 150, 530, 350)
        ev.Graphics.DrawString("Jumlah", ukJD, Brushes.Black, 553, 150)
        ev.Graphics.DrawLine(Pens.Black, 620, 150, 620, 350)
        ev.Graphics.DrawString("Pot1", ukJD, Brushes.Black, 627, 150)
        ev.Graphics.DrawLine(Pens.Black, 665, 150, 665, 350)
        ev.Graphics.DrawString("Pot2", ukJD, Brushes.Black, 670, 150)
        ev.Graphics.DrawLine(Pens.Black, 708, 150, 708, 350)
        ev.Graphics.DrawString("Total", ukJD, Brushes.Black, 740, 150)
        ev.Graphics.DrawLine(Pens.Black, 25, 170, 820, 170) 'baris ke2 atas
        ev.Graphics.DrawLine(Pens.Black, 820, 150, 820, 350) 'ujung
        ev.Graphics.DrawString("Total", ukJD, Brushes.Black, 620, 353)
        ev.Graphics.DrawString("Disc", ukJD, Brushes.Black, 620, 370)
        ev.Graphics.DrawString("Grand Total", ukJD, Brushes.Black, 620, 387)
        ev.Graphics.DrawString("PPN", ukJD, Brushes.Black, 620, 406)
        ev.Graphics.DrawString("Grand Total", ukJD, Brushes.Black, 620, 422)
        'ev.Graphics.DrawLine(Pens.Black, 720, 350, 720, 438) ' vertikal total
        'ev.Graphics.DrawLine(Pens.Black, 820, 350, 820, 438) ' vertikal total

        While count < 12
            If arrke > UBound(tmpKode) Then
                line = Nothing
            Else
                line = "aa"

                ev.Graphics.DrawString(arrke + 1, ukMn, Brushes.Black, 30, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpKode(arrke), ukMn, Brushes.Black, 55, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpNama(arrke), ukMn, Brushes.Black, 160, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpQty(arrke), ukMn, Brushes.Black, 365, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpSat(arrke), ukMn, Brushes.Black, 410, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(FormatNumber(tmpHrg(arrke), 2), ukMn, Brushes.Black, 525, 170 + ((count Mod 12) * 12), New StringFormat(LeftRightAlignment.Right))
                ev.Graphics.DrawString(FormatNumber(tmpJmlh(arrke), 2), ukMn, Brushes.Black, 615, 170 + ((count Mod 12) * 12), New StringFormat(LeftRightAlignment.Right))
                ev.Graphics.DrawString(FormatNumber(tmpPot1(arrke), 2), ukMn, Brushes.Black, 635, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(FormatNumber(FormatNumber(tmpPot2(arrke), 2), 2), ukMn, Brushes.Black, 680, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(FormatNumber(tmpTot(arrke), 2), ukMn, Brushes.Black, 815, 170 + ((count Mod 12) * 12), New StringFormat(LeftRightAlignment.Right))
                ev.Graphics.DrawLine(Pens.Black, 25, 350, 820, 350)

            End If

            If line Is Nothing Then
                Exit While
            End If
            count += 1
            arrke += 1
        End While
        ev.Graphics.DrawString(FormatNumber(tb.Tables("DATREBELI").Rows(0)("TOTAL"), 2), ukT, Brushes.Black, 815, 353, New StringFormat(LeftRightAlignment.Right))
        ev.Graphics.DrawString(FormatNumber(tb.Tables("DATREBELI").Rows(0)("DISC"), 2), ukT, Brushes.Black, 815, 370, New StringFormat(LeftRightAlignment.Right))
        ev.Graphics.DrawString(FormatNumber(tb.Tables("DATREBELI").Rows(0)("GTOTAL"), 2), ukT, Brushes.Black, 815, 387, New StringFormat(LeftRightAlignment.Right))
        ev.Graphics.DrawString(FormatNumber(tb.Tables("DATREBELI").Rows(0)("PPN"), 2), ukT, Brushes.Black, 815, 406, New StringFormat(LeftRightAlignment.Right))
        ev.Graphics.DrawString(FormatNumber(tb.Tables("DATREBELI").Rows(0)("GGTOTAL"), 2), ukT, Brushes.Black, 815, 422, New StringFormat(LeftRightAlignment.Right))

        Dim displayRectangleL As New Rectangle(New Point(70, 355), New Size(300, 200))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleL)
        Dim formatL As New StringFormat(StringFormatFlags.NoClip)
        formatL.LineAlignment = StringAlignment.Near ' = top
        formatL.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ev.Graphics.DrawString("Ket : ", ukJ, Brushes.Black, 30, 355)
        ev.Graphics.DrawString(tb.Tables("DATREBELI").Rows(0)("KET"), ukMn, Brushes.Black, RectangleF.op_Implicit(displayRectangleL), formatL)

        ev.Graphics.DrawString("Penerima", ukJ, Brushes.Black, 100, 430)
        ev.Graphics.DrawString("Pengirim", ukJ, Brushes.Black, 500, 430)
        ' ev.Graphics.DrawString("SEJAHTERA MANDIRI", ukJ, Brushes.Black, 460, 458)
        ev.Graphics.DrawString("(.....................................)", ukMn, Brushes.Black, 55, 485)
        ev.Graphics.DrawString("(.....................................)", ukMn, Brushes.Black, 465, 485)

        ev.Graphics.DrawString("***Perhatian !!", New Font("Courier", 7, FontStyle.Bold), Brushes.Black, 300, 480)
        ev.Graphics.DrawString("Telitilah barang-barang yang akan diterima.***", New Font("Courier", 7, FontStyle.Italic), Brushes.Black, 230, 480)

        If line = "aa" And arrke <= UBound(tmpKode) Then
            hal = hal + 1
            ev.HasMorePages = True
        Else
            ev.HasMorePages = False
        End If
    End Sub

End Class