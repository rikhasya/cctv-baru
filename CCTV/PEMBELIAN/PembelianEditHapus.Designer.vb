﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PembelianEditHapus
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TanggalJT = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.NoSup = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Tanggal = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.supplier = New System.Windows.Forms.TextBox()
        Me.NoFak = New System.Windows.Forms.TextBox()
        Me.butSELESAI = New System.Windows.Forms.Button()
        Me.butBATAL = New System.Windows.Forms.Button()
        Me.Keterangan = New System.Windows.Forms.TextBox()
        Me.DGVbeli = New System.Windows.Forms.DataGridView()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT1_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT2_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Sat = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.butINPUT = New System.Windows.Forms.Button()
        Me.Total = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Pot2 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Pot1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Jumlah = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Harga = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Qty = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Nama = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.kode = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Grandtot2 = New System.Windows.Forms.TextBox()
        Me.cbPPN = New System.Windows.Forms.CheckBox()
        Me.PPN = New System.Windows.Forms.TextBox()
        Me.Grandtot = New System.Windows.Forms.TextBox()
        Me.Disc = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Total2 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.DGVbeli, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TanggalJT
        '
        Me.TanggalJT.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.TanggalJT.Location = New System.Drawing.Point(749, 8)
        Me.TanggalJT.Name = "TanggalJT"
        Me.TanggalJT.Size = New System.Drawing.Size(86, 20)
        Me.TanggalJT.TabIndex = 7
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(684, 12)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(61, 13)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "Tanggal JT"
        '
        'NoSup
        '
        Me.NoSup.Location = New System.Drawing.Point(572, 8)
        Me.NoSup.Name = "NoSup"
        Me.NoSup.Size = New System.Drawing.Size(100, 20)
        Me.NoSup.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(504, 12)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "No Supplier"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(291, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Supplier"
        '
        'Tanggal
        '
        Me.Tanggal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Tanggal.Location = New System.Drawing.Point(192, 9)
        Me.Tanggal.Name = "Tanggal"
        Me.Tanggal.Size = New System.Drawing.Size(86, 20)
        Me.Tanggal.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(140, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tanggal"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Lavender
        Me.Panel1.Controls.Add(Me.supplier)
        Me.Panel1.Controls.Add(Me.NoFak)
        Me.Panel1.Controls.Add(Me.TanggalJT)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.NoSup)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Tanggal)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(133, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(853, 37)
        Me.Panel1.TabIndex = 67
        '
        'supplier
        '
        Me.supplier.Location = New System.Drawing.Point(342, 9)
        Me.supplier.Name = "supplier"
        Me.supplier.Size = New System.Drawing.Size(156, 20)
        Me.supplier.TabIndex = 9
        '
        'NoFak
        '
        Me.NoFak.Enabled = False
        Me.NoFak.Location = New System.Drawing.Point(12, 9)
        Me.NoFak.Name = "NoFak"
        Me.NoFak.Size = New System.Drawing.Size(122, 20)
        Me.NoFak.TabIndex = 8
        '
        'butSELESAI
        '
        Me.butSELESAI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSELESAI.Location = New System.Drawing.Point(801, 601)
        Me.butSELESAI.Name = "butSELESAI"
        Me.butSELESAI.Size = New System.Drawing.Size(75, 34)
        Me.butSELESAI.TabIndex = 4
        Me.butSELESAI.Text = "SELESAI"
        Me.butSELESAI.UseVisualStyleBackColor = True
        '
        'butBATAL
        '
        Me.butBATAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBATAL.Location = New System.Drawing.Point(886, 601)
        Me.butBATAL.Name = "butBATAL"
        Me.butBATAL.Size = New System.Drawing.Size(75, 34)
        Me.butBATAL.TabIndex = 80
        Me.butBATAL.Text = "HAPUS"
        Me.butBATAL.UseVisualStyleBackColor = True
        '
        'Keterangan
        '
        Me.Keterangan.Location = New System.Drawing.Point(77, 464)
        Me.Keterangan.Multiline = True
        Me.Keterangan.Name = "Keterangan"
        Me.Keterangan.Size = New System.Drawing.Size(329, 103)
        Me.Keterangan.TabIndex = 2
        '
        'DGVbeli
        '
        Me.DGVbeli.AllowUserToAddRows = False
        Me.DGVbeli.AllowUserToDeleteRows = False
        Me.DGVbeli.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVbeli.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_BRG, Me.NO_FAK, Me.N_BRG, Me.QTY_BRG, Me.SAT_BRG, Me.HARGA_BRG, Me.JUMLAH_BRG, Me.POT1_BRG, Me.POT2_BRG, Me.TOTAL_BRG})
        Me.DGVbeli.Location = New System.Drawing.Point(12, 156)
        Me.DGVbeli.Name = "DGVbeli"
        Me.DGVbeli.ReadOnly = True
        Me.DGVbeli.RowHeadersVisible = False
        Me.DGVbeli.Size = New System.Drawing.Size(976, 299)
        Me.DGVbeli.TabIndex = 1
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Fak"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY_BRG
        '
        Me.QTY_BRG.DataPropertyName = "QTY"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.QTY_BRG.DefaultCellStyle = DataGridViewCellStyle1
        Me.QTY_BRG.HeaderText = "Qty"
        Me.QTY_BRG.Name = "QTY_BRG"
        Me.QTY_BRG.ReadOnly = True
        '
        'SAT_BRG
        '
        Me.SAT_BRG.DataPropertyName = "SAT"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.SAT_BRG.DefaultCellStyle = DataGridViewCellStyle2
        Me.SAT_BRG.HeaderText = "Sat"
        Me.SAT_BRG.Name = "SAT_BRG"
        Me.SAT_BRG.ReadOnly = True
        '
        'HARGA_BRG
        '
        Me.HARGA_BRG.DataPropertyName = "HARGA"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.HARGA_BRG.DefaultCellStyle = DataGridViewCellStyle3
        Me.HARGA_BRG.HeaderText = "Harga"
        Me.HARGA_BRG.Name = "HARGA_BRG"
        Me.HARGA_BRG.ReadOnly = True
        '
        'JUMLAH_BRG
        '
        Me.JUMLAH_BRG.DataPropertyName = "JUMLAH"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.JUMLAH_BRG.DefaultCellStyle = DataGridViewCellStyle4
        Me.JUMLAH_BRG.HeaderText = "Jumlah"
        Me.JUMLAH_BRG.Name = "JUMLAH_BRG"
        Me.JUMLAH_BRG.ReadOnly = True
        '
        'POT1_BRG
        '
        Me.POT1_BRG.DataPropertyName = "POT1"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.POT1_BRG.DefaultCellStyle = DataGridViewCellStyle5
        Me.POT1_BRG.HeaderText = "Pot 1"
        Me.POT1_BRG.Name = "POT1_BRG"
        Me.POT1_BRG.ReadOnly = True
        '
        'POT2_BRG
        '
        Me.POT2_BRG.DataPropertyName = "POT2"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        Me.POT2_BRG.DefaultCellStyle = DataGridViewCellStyle6
        Me.POT2_BRG.HeaderText = "Pot 2"
        Me.POT2_BRG.Name = "POT2_BRG"
        Me.POT2_BRG.ReadOnly = True
        '
        'TOTAL_BRG
        '
        Me.TOTAL_BRG.DataPropertyName = "TOTAL"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.TOTAL_BRG.DefaultCellStyle = DataGridViewCellStyle7
        Me.TOTAL_BRG.HeaderText = "Total"
        Me.TOTAL_BRG.Name = "TOTAL_BRG"
        Me.TOTAL_BRG.ReadOnly = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 464)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 74
        Me.Label15.Text = "Keterangan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Bernard MT Condensed", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 24)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "E-Pembelian"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Sat)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.butINPUT)
        Me.GroupBox1.Controls.Add(Me.Total)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Pot2)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Pot1)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Jumlah)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Harga)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Qty)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Nama)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.kode)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(976, 88)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "BARANG"
        '
        'Sat
        '
        Me.Sat.Enabled = False
        Me.Sat.Location = New System.Drawing.Point(714, 19)
        Me.Sat.Name = "Sat"
        Me.Sat.Size = New System.Drawing.Size(61, 20)
        Me.Sat.TabIndex = 21
        Me.Sat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(671, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Satuan"
        '
        'butINPUT
        '
        Me.butINPUT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINPUT.Location = New System.Drawing.Point(876, 48)
        Me.butINPUT.Name = "butINPUT"
        Me.butINPUT.Size = New System.Drawing.Size(75, 23)
        Me.butINPUT.TabIndex = 5
        Me.butINPUT.Text = "INPUT"
        Me.butINPUT.UseVisualStyleBackColor = True
        '
        'Total
        '
        Me.Total.Enabled = False
        Me.Total.Location = New System.Drawing.Point(714, 51)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(151, 20)
        Me.Total.TabIndex = 20
        Me.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(677, 54)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(31, 13)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Total"
        '
        'Pot2
        '
        Me.Pot2.Location = New System.Drawing.Point(615, 51)
        Me.Pot2.Name = "Pot2"
        Me.Pot2.Size = New System.Drawing.Size(46, 20)
        Me.Pot2.TabIndex = 4
        Me.Pot2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(577, 54)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(32, 13)
        Me.Label13.TabIndex = 17
        Me.Label13.Text = "Pot 2"
        '
        'Pot1
        '
        Me.Pot1.Location = New System.Drawing.Point(514, 51)
        Me.Pot1.Name = "Pot1"
        Me.Pot1.Size = New System.Drawing.Size(46, 20)
        Me.Pot1.TabIndex = 3
        Me.Pot1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(476, 54)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 13)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Pot 1"
        '
        'Jumlah
        '
        Me.Jumlah.Enabled = False
        Me.Jumlah.Location = New System.Drawing.Point(295, 51)
        Me.Jumlah.Name = "Jumlah"
        Me.Jumlah.Size = New System.Drawing.Size(164, 20)
        Me.Jumlah.TabIndex = 0
        Me.Jumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(249, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Jumlah"
        '
        'Harga
        '
        Me.Harga.Location = New System.Drawing.Point(61, 50)
        Me.Harga.Name = "Harga"
        Me.Harga.Size = New System.Drawing.Size(150, 20)
        Me.Harga.TabIndex = 2
        Me.Harga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(19, 54)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(36, 13)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Harga"
        '
        'Qty
        '
        Me.Qty.Location = New System.Drawing.Point(615, 19)
        Me.Qty.Name = "Qty"
        Me.Qty.Size = New System.Drawing.Size(46, 20)
        Me.Qty.TabIndex = 1
        Me.Qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(585, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 13)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Qty"
        '
        'Nama
        '
        Me.Nama.Enabled = False
        Me.Nama.Location = New System.Drawing.Point(295, 19)
        Me.Nama.Name = "Nama"
        Me.Nama.Size = New System.Drawing.Size(265, 20)
        Me.Nama.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(251, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Nama "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(214, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "* F1"
        '
        'kode
        '
        Me.kode.Location = New System.Drawing.Point(61, 19)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(151, 20)
        Me.kode.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(20, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Kode "
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(693, 580)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(63, 13)
        Me.Label22.TabIndex = 113
        Me.Label22.Text = "Grand Total"
        '
        'Grandtot2
        '
        Me.Grandtot2.Enabled = False
        Me.Grandtot2.Location = New System.Drawing.Point(778, 575)
        Me.Grandtot2.Name = "Grandtot2"
        Me.Grandtot2.Size = New System.Drawing.Size(208, 20)
        Me.Grandtot2.TabIndex = 112
        Me.Grandtot2.Text = "0"
        Me.Grandtot2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cbPPN
        '
        Me.cbPPN.AutoSize = True
        Me.cbPPN.Location = New System.Drawing.Point(696, 550)
        Me.cbPPN.Name = "cbPPN"
        Me.cbPPN.Size = New System.Drawing.Size(48, 17)
        Me.cbPPN.TabIndex = 110
        Me.cbPPN.Text = "PPN"
        Me.cbPPN.UseVisualStyleBackColor = True
        '
        'PPN
        '
        Me.PPN.Enabled = False
        Me.PPN.Location = New System.Drawing.Point(778, 547)
        Me.PPN.Name = "PPN"
        Me.PPN.Size = New System.Drawing.Size(208, 20)
        Me.PPN.TabIndex = 111
        Me.PPN.Text = "0"
        Me.PPN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Grandtot
        '
        Me.Grandtot.Enabled = False
        Me.Grandtot.Location = New System.Drawing.Point(778, 520)
        Me.Grandtot.Name = "Grandtot"
        Me.Grandtot.Size = New System.Drawing.Size(208, 20)
        Me.Grandtot.TabIndex = 109
        Me.Grandtot.Text = "0"
        Me.Grandtot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Disc
        '
        Me.Disc.Location = New System.Drawing.Point(778, 492)
        Me.Disc.Name = "Disc"
        Me.Disc.Size = New System.Drawing.Size(208, 20)
        Me.Disc.TabIndex = 3
        Me.Disc.Text = "0"
        Me.Disc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(693, 467)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(31, 13)
        Me.Label16.TabIndex = 105
        Me.Label16.Text = "Total"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(693, 523)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(63, 13)
        Me.Label18.TabIndex = 108
        Me.Label18.Text = "Grand Total"
        '
        'Total2
        '
        Me.Total2.Enabled = False
        Me.Total2.Location = New System.Drawing.Point(778, 464)
        Me.Total2.Name = "Total2"
        Me.Total2.Size = New System.Drawing.Size(208, 20)
        Me.Total2.TabIndex = 106
        Me.Total2.Text = "0"
        Me.Total2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(693, 497)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(28, 13)
        Me.Label17.TabIndex = 107
        Me.Label17.Text = "Disc"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'PembelianEditHapus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Grandtot2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cbPPN)
        Me.Controls.Add(Me.butSELESAI)
        Me.Controls.Add(Me.PPN)
        Me.Controls.Add(Me.Grandtot)
        Me.Controls.Add(Me.butBATAL)
        Me.Controls.Add(Me.Disc)
        Me.Controls.Add(Me.Keterangan)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.DGVbeli)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Total2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label17)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "PembelianEditHapus"
        Me.Text = "PembelianEditHapus"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVbeli, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TanggalJT As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents NoSup As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NoFak As System.Windows.Forms.TextBox
    Friend WithEvents butSELESAI As System.Windows.Forms.Button
    Friend WithEvents butBATAL As System.Windows.Forms.Button
    Friend WithEvents Keterangan As System.Windows.Forms.TextBox
    Friend WithEvents DGVbeli As System.Windows.Forms.DataGridView
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Sat As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents butINPUT As System.Windows.Forms.Button
    Friend WithEvents Total As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Pot2 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Pot1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Jumlah As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Harga As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Qty As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Nama As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents kode As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT1_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT2_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Grandtot2 As System.Windows.Forms.TextBox
    Friend WithEvents cbPPN As System.Windows.Forms.CheckBox
    Friend WithEvents PPN As System.Windows.Forms.TextBox
    Friend WithEvents Grandtot As System.Windows.Forms.TextBox
    Friend WithEvents Disc As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Total2 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents supplier As System.Windows.Forms.TextBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
End Class
