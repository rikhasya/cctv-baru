﻿Imports System.Drawing.Printing
Imports System.IO

Public Class Pembelian_LaporanRetur

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsReBeli
        Dim tb As DataSet

        Dim tmpsup As String = "%"
        If CBsup.SelectedValue <> "0" Then
            tmpsup = CBsup.SelectedValue
        End If

        tb = a.table(date1.Text, date2.Text, tmpsup)

        Dim i As Integer
        For i = 0 To tb.Tables("DATREBELI").Rows.Count - 1
            Console.WriteLine("dat" & tb.Tables("DATREBELI").Rows(i)(0).ToString)
        Next

        For i = 0 To tb.Tables("DETREBELI").Rows.Count - 1
            Console.WriteLine("det" & tb.Tables("DETREBELI").Rows(i)(0).ToString)
        Next

        Dim parentColumn As DataColumn = tb.Tables("DATREBELI").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETREBELI").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETREBELI", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATREBELI"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETREBELI"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVheader.Focus()
    End Sub

    Private Sub Pembelian_LaporanRetur_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Pembelian_LaporanRetur_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSupplier
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_SUP") = "ALL"
        row("K_SUP") = "0"
        b.Rows.Add(row)
        With CBsup
            .DisplayMember = "N_SUP"
            .ValueMember = "K_SUP"
            .DataSource = b
        End With
        CBsup.SelectedValue = "0"
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURBELI_EDITHAPUS")
        If b = True Then
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            If DGVheader.Item("BAYAR", row).Value <> 0 Then
                MsgBox("Sudah Pernah Di Bayar")
                Exit Sub
            End If

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Pembelian_ReturEditHapus
            frm = New Pembelian_ReturEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Retur Pembelian"
            frm.MdiParent = frmMenu
            frm.nofak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.Tanggal.Text = FormatDateTime(DGVheader.Item("TANGGAL", row).Value.ToString, DateFormat.ShortDate)
            frm.supplier.Text = DGVheader.Item("N_SUP", row).Value.ToString
            frm.Tanggal2.Text = FormatDateTime(DGVheader.Item("TGL_JT", row).Value.ToString, DateFormat.ShortDate)
            frm.Total2.Text = DGVheader.Item("TOTAL1", row).Value.ToString
            frm.Disc.Text = DGVheader.Item("DISC", row).Value.ToString
            frm.Grandtot.Text = DGVheader.Item("GRANDTOT", row).Value.ToString
            frm.Keterangan.Text = DGVheader.Item("KET", row).Value.ToString

            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("PRINTULANGLAPORAN")
            If b = True Then
                PRINT()
            Else
                MsgBox("anda tdk di otorisasi")
            End If
        End If
    End Sub

    Sub PRINT()
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Ulang Faktur Retur Pembelian?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsReBeli
            Pembelian_Retur.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With Pembelian_Retur
                .tmpnofak = .tb.Tables("DATREBELI").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Pembelian_Retur.tb.Tables("DETREBELI").Rows.Count - 1

            ReDim Pembelian_Retur.tmpKode(jumlah)
            ReDim Pembelian_Retur.tmpNama(jumlah)
            ReDim Pembelian_Retur.tmpQty(jumlah)
            ReDim Pembelian_Retur.tmpSat(jumlah)
            ReDim Pembelian_Retur.tmpHrg(jumlah)
            ReDim Pembelian_Retur.tmpJmlh(jumlah)
            ReDim Pembelian_Retur.tmpPot1(jumlah)
            ReDim Pembelian_Retur.tmpPot2(jumlah)
            ReDim Pembelian_Retur.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Pembelian_Retur
                    .tmpKode(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("TOTAL").ToString

                End With
            Next
            Pembelian_Retur.arrke = 0

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Pembelian_Retur.faktur
            PrintDoc.Print()
        End If
    End Sub

    Private Sub CBsup_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBsup.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class