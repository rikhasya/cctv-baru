﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cari_faktur
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dari = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.ksup = New System.Windows.Forms.Label()
        Me.DGVfaktur = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TANGGAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRANDTOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPN1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GGTOTAL1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_JT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_PO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_RETUR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GIRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVfaktur2 = New System.Windows.Forms.DataGridView()
        Me.NO_FAK1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tgl_fak2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GTOTAL2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PEMBAYARAN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EKSPEDISI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.B_EXPEDISI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USER_PRINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP_PRINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVfaktur3 = New System.Windows.Forms.DataGridView()
        Me.DGVfaktur4 = New System.Windows.Forms.DataGridView()
        Me.DGVfaktur1 = New System.Windows.Forms.DataGridView()
        Me.NO_FAK4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_RETUR4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_FAK4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SUP4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.input = New System.Windows.Forms.Button()
        Me.check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.NO_FAK3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_RETUR3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_FAK3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GGTOTAL3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SISA3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.check1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_RETUR2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GGTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SISA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVfaktur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVfaktur2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVfaktur3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVfaktur4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVfaktur1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Controls.Add(Me.dari)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(55, 69)
        Me.Panel1.TabIndex = 1
        '
        'dari
        '
        Me.dari.AutoSize = True
        Me.dari.ForeColor = System.Drawing.Color.Snow
        Me.dari.Location = New System.Drawing.Point(6, 30)
        Me.dari.Name = "dari"
        Me.dari.Size = New System.Drawing.Size(10, 13)
        Me.dari.TabIndex = 9
        Me.dari.Text = "-"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(90, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Tanggal "
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(145, 48)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 0
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(271, 48)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(88, 20)
        Me.date2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(236, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "s / d"
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.Location = New System.Drawing.Point(380, 31)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 34)
        Me.butCARI.TabIndex = 2
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'ksup
        '
        Me.ksup.AutoSize = True
        Me.ksup.Enabled = False
        Me.ksup.Font = New System.Drawing.Font("Microsoft Sans Serif", 3.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ksup.Location = New System.Drawing.Point(142, 33)
        Me.ksup.Name = "ksup"
        Me.ksup.Size = New System.Drawing.Size(0, 5)
        Me.ksup.TabIndex = 8
        '
        'DGVfaktur
        '
        Me.DGVfaktur.AllowUserToAddRows = False
        Me.DGVfaktur.AllowUserToDeleteRows = False
        Me.DGVfaktur.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DGVfaktur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVfaktur.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.TANGGAL, Me.N_SUP, Me.NO_SUP, Me.TOTAL1, Me.DISC, Me.GRANDTOT, Me.PPN1, Me.GGTOTAL1, Me.KET, Me.K_SUP, Me.USERID, Me.VALID, Me.TGL_JT, Me.BAYAR, Me.LUNAS, Me.NO_PO, Me.NO_RETUR, Me.GIRO})
        Me.DGVfaktur.Enabled = False
        Me.DGVfaktur.Location = New System.Drawing.Point(12, 87)
        Me.DGVfaktur.Name = "DGVfaktur"
        Me.DGVfaktur.ReadOnly = True
        Me.DGVfaktur.RowHeadersVisible = False
        Me.DGVfaktur.Size = New System.Drawing.Size(875, 361)
        Me.DGVfaktur.TabIndex = 24
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'TANGGAL
        '
        Me.TANGGAL.DataPropertyName = "TGL_FAK"
        Me.TANGGAL.HeaderText = "Tanggal"
        Me.TANGGAL.Name = "TANGGAL"
        Me.TANGGAL.ReadOnly = True
        '
        'N_SUP
        '
        Me.N_SUP.DataPropertyName = "N_SUP"
        Me.N_SUP.HeaderText = "Supplier"
        Me.N_SUP.Name = "N_SUP"
        Me.N_SUP.ReadOnly = True
        '
        'NO_SUP
        '
        Me.NO_SUP.DataPropertyName = "NO_SUP"
        Me.NO_SUP.HeaderText = "No Supplier"
        Me.NO_SUP.Name = "NO_SUP"
        Me.NO_SUP.ReadOnly = True
        '
        'TOTAL1
        '
        Me.TOTAL1.DataPropertyName = "TOTAL"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.TOTAL1.DefaultCellStyle = DataGridViewCellStyle1
        Me.TOTAL1.HeaderText = "Total"
        Me.TOTAL1.Name = "TOTAL1"
        Me.TOTAL1.ReadOnly = True
        '
        'DISC
        '
        Me.DISC.DataPropertyName = "DISC"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.DISC.DefaultCellStyle = DataGridViewCellStyle2
        Me.DISC.HeaderText = "Diskon"
        Me.DISC.Name = "DISC"
        Me.DISC.ReadOnly = True
        '
        'GRANDTOT
        '
        Me.GRANDTOT.DataPropertyName = "GTOTAL"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.GRANDTOT.DefaultCellStyle = DataGridViewCellStyle3
        Me.GRANDTOT.HeaderText = "Grand Total"
        Me.GRANDTOT.Name = "GRANDTOT"
        Me.GRANDTOT.ReadOnly = True
        '
        'PPN1
        '
        Me.PPN1.DataPropertyName = "PPN"
        Me.PPN1.HeaderText = "PPN"
        Me.PPN1.Name = "PPN1"
        Me.PPN1.ReadOnly = True
        '
        'GGTOTAL1
        '
        Me.GGTOTAL1.DataPropertyName = "GGTOTAL"
        Me.GGTOTAL1.HeaderText = "Grand Total"
        Me.GGTOTAL1.Name = "GGTOTAL1"
        Me.GGTOTAL1.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'K_SUP
        '
        Me.K_SUP.DataPropertyName = "K_SUP"
        Me.K_SUP.HeaderText = "Kode Supplier"
        Me.K_SUP.Name = "K_SUP"
        Me.K_SUP.ReadOnly = True
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'TGL_JT
        '
        Me.TGL_JT.DataPropertyName = "TGL_JT"
        Me.TGL_JT.HeaderText = "Tanggal JT"
        Me.TGL_JT.Name = "TGL_JT"
        Me.TGL_JT.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.DataPropertyName = "BAYAR"
        Me.BAYAR.HeaderText = "Bayar"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        Me.LUNAS.Visible = False
        '
        'NO_PO
        '
        Me.NO_PO.DataPropertyName = "NO_PO"
        Me.NO_PO.HeaderText = "Nomer PO"
        Me.NO_PO.Name = "NO_PO"
        Me.NO_PO.ReadOnly = True
        Me.NO_PO.Visible = False
        '
        'NO_RETUR
        '
        Me.NO_RETUR.DataPropertyName = "NO_RETUR"
        Me.NO_RETUR.HeaderText = "No Retur"
        Me.NO_RETUR.Name = "NO_RETUR"
        Me.NO_RETUR.ReadOnly = True
        '
        'GIRO
        '
        Me.GIRO.DataPropertyName = "GIRO"
        Me.GIRO.HeaderText = "Giro"
        Me.GIRO.Name = "GIRO"
        Me.GIRO.ReadOnly = True
        Me.GIRO.Visible = False
        '
        'DGVfaktur2
        '
        Me.DGVfaktur2.AllowUserToAddRows = False
        Me.DGVfaktur2.AllowUserToDeleteRows = False
        Me.DGVfaktur2.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DGVfaktur2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVfaktur2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK1, Me.tgl_fak2, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn3, Me.ALAMAT, Me.TELP, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.PPN, Me.GTOTAL2, Me.N_SLS, Me.K_SLS, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.PEMBAYARAN, Me.EKSPEDISI, Me.B_EXPEDISI, Me.KET2, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn10, Me.DATESTAMP1, Me.LUNAS1, Me.NO_SO, Me.USER_PRINT, Me.DATESTAMP_PRINT, Me.DATESTAMP2})
        Me.DGVfaktur2.Location = New System.Drawing.Point(12, 87)
        Me.DGVfaktur2.Name = "DGVfaktur2"
        Me.DGVfaktur2.ReadOnly = True
        Me.DGVfaktur2.RowHeadersVisible = False
        Me.DGVfaktur2.Size = New System.Drawing.Size(875, 361)
        Me.DGVfaktur2.TabIndex = 25
        Me.DGVfaktur2.Visible = False
        '
        'NO_FAK1
        '
        Me.NO_FAK1.DataPropertyName = "NO_FAK"
        Me.NO_FAK1.HeaderText = "No Faktur"
        Me.NO_FAK1.Name = "NO_FAK1"
        Me.NO_FAK1.ReadOnly = True
        '
        'tgl_fak2
        '
        Me.tgl_fak2.DataPropertyName = "TGL_FAK"
        Me.tgl_fak2.HeaderText = "Tanggal"
        Me.tgl_fak2.Name = "tgl_fak2"
        Me.tgl_fak2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "TGL_JT"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Tanggal JT"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "N_LGN"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Langganan"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "TOTAL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "DISC"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn6.HeaderText = "Diskon"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "GTOTAL"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn7.HeaderText = "Grand Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'PPN
        '
        Me.PPN.DataPropertyName = "PPN"
        Me.PPN.HeaderText = "PPN"
        Me.PPN.Name = "PPN"
        Me.PPN.ReadOnly = True
        '
        'GTOTAL2
        '
        Me.GTOTAL2.DataPropertyName = "GGTOTAL"
        Me.GTOTAL2.HeaderText = "Grand Total"
        Me.GTOTAL2.Name = "GTOTAL2"
        Me.GTOTAL2.ReadOnly = True
        '
        'N_SLS
        '
        Me.N_SLS.DataPropertyName = "N_SLS"
        Me.N_SLS.HeaderText = "Sales"
        Me.N_SLS.Name = "N_SLS"
        Me.N_SLS.ReadOnly = True
        '
        'K_SLS
        '
        Me.K_SLS.DataPropertyName = "K_SLS"
        Me.K_SLS.HeaderText = "Kode Sales"
        Me.K_SLS.Name = "K_SLS"
        Me.K_SLS.ReadOnly = True
        Me.K_SLS.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "KET"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Keterangan"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "K_LGN"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Kode Langganan"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'PEMBAYARAN
        '
        Me.PEMBAYARAN.DataPropertyName = "PEMBAYARAN"
        Me.PEMBAYARAN.HeaderText = "Pembayaran"
        Me.PEMBAYARAN.Name = "PEMBAYARAN"
        Me.PEMBAYARAN.ReadOnly = True
        Me.PEMBAYARAN.Visible = False
        '
        'EKSPEDISI
        '
        Me.EKSPEDISI.DataPropertyName = "EKSPEDISI"
        Me.EKSPEDISI.HeaderText = "Pengiriman"
        Me.EKSPEDISI.Name = "EKSPEDISI"
        Me.EKSPEDISI.ReadOnly = True
        Me.EKSPEDISI.Visible = False
        '
        'B_EXPEDISI
        '
        Me.B_EXPEDISI.DataPropertyName = "B_EXPEDISI"
        Me.B_EXPEDISI.HeaderText = "Biaya Pengiriman"
        Me.B_EXPEDISI.Name = "B_EXPEDISI"
        Me.B_EXPEDISI.ReadOnly = True
        Me.B_EXPEDISI.Visible = False
        '
        'KET2
        '
        Me.KET2.DataPropertyName = "KET2"
        Me.KET2.HeaderText = "Keterangan2"
        Me.KET2.Name = "KET2"
        Me.KET2.ReadOnly = True
        Me.KET2.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "VALID"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Valid"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "USERID"
        Me.DataGridViewTextBoxColumn10.HeaderText = "UserID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DATESTAMP1
        '
        Me.DATESTAMP1.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP1.HeaderText = "Datestamp"
        Me.DATESTAMP1.Name = "DATESTAMP1"
        Me.DATESTAMP1.ReadOnly = True
        '
        'LUNAS1
        '
        Me.LUNAS1.DataPropertyName = "LUNAS"
        Me.LUNAS1.HeaderText = "Lunas"
        Me.LUNAS1.Name = "LUNAS1"
        Me.LUNAS1.ReadOnly = True
        Me.LUNAS1.Visible = False
        '
        'NO_SO
        '
        Me.NO_SO.DataPropertyName = "NO_SO"
        Me.NO_SO.HeaderText = "No SO"
        Me.NO_SO.Name = "NO_SO"
        Me.NO_SO.ReadOnly = True
        Me.NO_SO.Visible = False
        '
        'USER_PRINT
        '
        Me.USER_PRINT.DataPropertyName = "USER_PRINT"
        Me.USER_PRINT.HeaderText = "User Print"
        Me.USER_PRINT.Name = "USER_PRINT"
        Me.USER_PRINT.ReadOnly = True
        Me.USER_PRINT.Visible = False
        '
        'DATESTAMP_PRINT
        '
        Me.DATESTAMP_PRINT.DataPropertyName = "DATESTAMP_PRINT"
        Me.DATESTAMP_PRINT.HeaderText = "Datestamp Print"
        Me.DATESTAMP_PRINT.Name = "DATESTAMP_PRINT"
        Me.DATESTAMP_PRINT.ReadOnly = True
        Me.DATESTAMP_PRINT.Visible = False
        '
        'DATESTAMP2
        '
        Me.DATESTAMP2.DataPropertyName = "DATESTAMP1"
        Me.DATESTAMP2.HeaderText = "DATESTAMP1"
        Me.DATESTAMP2.Name = "DATESTAMP2"
        Me.DATESTAMP2.ReadOnly = True
        '
        'DGVfaktur3
        '
        Me.DGVfaktur3.AllowUserToAddRows = False
        Me.DGVfaktur3.AllowUserToDeleteRows = False
        Me.DGVfaktur3.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVfaktur3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVfaktur3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.check1, Me.NO_FAK2, Me.NO_RETUR2, Me.TGL_FAK, Me.N_SUP2, Me.GGTOTAL, Me.BAYAR2, Me.SISA, Me.LUNAS2})
        Me.DGVfaktur3.Location = New System.Drawing.Point(12, 87)
        Me.DGVfaktur3.Name = "DGVfaktur3"
        Me.DGVfaktur3.RowHeadersVisible = False
        Me.DGVfaktur3.Size = New System.Drawing.Size(875, 361)
        Me.DGVfaktur3.TabIndex = 26
        Me.DGVfaktur3.Visible = False
        '
        'DGVfaktur4
        '
        Me.DGVfaktur4.AllowUserToAddRows = False
        Me.DGVfaktur4.AllowUserToDeleteRows = False
        Me.DGVfaktur4.BackgroundColor = System.Drawing.Color.Snow
        Me.DGVfaktur4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVfaktur4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.check, Me.NO_FAK3, Me.NO_RETUR3, Me.TGL_FAK3, Me.N_LGN3, Me.GGTOTAL3, Me.BAYAR3, Me.SISA3, Me.LUNAS3})
        Me.DGVfaktur4.Location = New System.Drawing.Point(12, 87)
        Me.DGVfaktur4.Name = "DGVfaktur4"
        Me.DGVfaktur4.RowHeadersVisible = False
        Me.DGVfaktur4.Size = New System.Drawing.Size(875, 361)
        Me.DGVfaktur4.TabIndex = 27
        Me.DGVfaktur4.Visible = False
        '
        'DGVfaktur1
        '
        Me.DGVfaktur1.AllowUserToAddRows = False
        Me.DGVfaktur1.AllowUserToDeleteRows = False
        Me.DGVfaktur1.BackgroundColor = System.Drawing.Color.Wheat
        Me.DGVfaktur1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVfaktur1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK4, Me.NO_RETUR4, Me.TGL_FAK4, Me.N_SUP4, Me.ALAMAT4, Me.TELP4, Me.KET4, Me.K_SUP4, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DATESTAMP})
        Me.DGVfaktur1.Location = New System.Drawing.Point(12, 87)
        Me.DGVfaktur1.Name = "DGVfaktur1"
        Me.DGVfaktur1.ReadOnly = True
        Me.DGVfaktur1.RowHeadersVisible = False
        Me.DGVfaktur1.Size = New System.Drawing.Size(875, 361)
        Me.DGVfaktur1.TabIndex = 3
        Me.DGVfaktur1.Visible = False
        '
        'NO_FAK4
        '
        Me.NO_FAK4.DataPropertyName = "NO_FAK"
        Me.NO_FAK4.HeaderText = "No Faktur"
        Me.NO_FAK4.Name = "NO_FAK4"
        Me.NO_FAK4.ReadOnly = True
        '
        'NO_RETUR4
        '
        Me.NO_RETUR4.DataPropertyName = "NO_RETUR"
        Me.NO_RETUR4.HeaderText = "No Retur"
        Me.NO_RETUR4.Name = "NO_RETUR4"
        Me.NO_RETUR4.ReadOnly = True
        '
        'TGL_FAK4
        '
        Me.TGL_FAK4.DataPropertyName = "TGL_FAK"
        Me.TGL_FAK4.HeaderText = "Tanggal"
        Me.TGL_FAK4.Name = "TGL_FAK4"
        Me.TGL_FAK4.ReadOnly = True
        '
        'N_SUP4
        '
        Me.N_SUP4.DataPropertyName = "N_SUP"
        Me.N_SUP4.HeaderText = "Supplier"
        Me.N_SUP4.Name = "N_SUP4"
        Me.N_SUP4.ReadOnly = True
        '
        'ALAMAT4
        '
        Me.ALAMAT4.DataPropertyName = "ALAMAT"
        Me.ALAMAT4.HeaderText = "Alamat"
        Me.ALAMAT4.Name = "ALAMAT4"
        Me.ALAMAT4.ReadOnly = True
        '
        'TELP4
        '
        Me.TELP4.DataPropertyName = "TELP"
        Me.TELP4.HeaderText = "Telp"
        Me.TELP4.Name = "TELP4"
        Me.TELP4.ReadOnly = True
        Me.TELP4.Visible = False
        '
        'KET4
        '
        Me.KET4.DataPropertyName = "KET"
        Me.KET4.HeaderText = "Keterangan"
        Me.KET4.Name = "KET4"
        Me.KET4.ReadOnly = True
        '
        'K_SUP4
        '
        Me.K_SUP4.DataPropertyName = "K_SUP"
        Me.K_SUP4.HeaderText = "Kode Supplier"
        Me.K_SUP4.Name = "K_SUP4"
        Me.K_SUP4.ReadOnly = True
        Me.K_SUP4.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "USERID"
        Me.DataGridViewTextBoxColumn19.HeaderText = "UserID"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "VALID"
        Me.DataGridViewTextBoxColumn20.HeaderText = "Valid"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "DATESTAMP"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'input
        '
        Me.input.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.input.Location = New System.Drawing.Point(812, 454)
        Me.input.Name = "input"
        Me.input.Size = New System.Drawing.Size(75, 33)
        Me.input.TabIndex = 4
        Me.input.Text = "INPUT"
        Me.input.UseVisualStyleBackColor = True
        Me.input.Visible = False
        '
        'check
        '
        Me.check.FalseValue = "0"
        Me.check.HeaderText = "Pilih"
        Me.check.Name = "check"
        Me.check.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.check.TrueValue = "1"
        '
        'NO_FAK3
        '
        Me.NO_FAK3.DataPropertyName = "NO_FAK"
        Me.NO_FAK3.HeaderText = "No. Faktur"
        Me.NO_FAK3.Name = "NO_FAK3"
        Me.NO_FAK3.ReadOnly = True
        '
        'NO_RETUR3
        '
        Me.NO_RETUR3.DataPropertyName = "NO_RETUR"
        Me.NO_RETUR3.HeaderText = "No. Retur"
        Me.NO_RETUR3.Name = "NO_RETUR3"
        Me.NO_RETUR3.ReadOnly = True
        '
        'TGL_FAK3
        '
        Me.TGL_FAK3.DataPropertyName = "TGL_FAK"
        Me.TGL_FAK3.HeaderText = "Tanggal "
        Me.TGL_FAK3.Name = "TGL_FAK3"
        Me.TGL_FAK3.ReadOnly = True
        '
        'N_LGN3
        '
        Me.N_LGN3.DataPropertyName = "N_LGN"
        Me.N_LGN3.HeaderText = "Nama Langganan"
        Me.N_LGN3.Name = "N_LGN3"
        Me.N_LGN3.ReadOnly = True
        '
        'GGTOTAL3
        '
        Me.GGTOTAL3.DataPropertyName = "GGTOTAL"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.GGTOTAL3.DefaultCellStyle = DataGridViewCellStyle10
        Me.GGTOTAL3.HeaderText = "Grand Total"
        Me.GGTOTAL3.Name = "GGTOTAL3"
        Me.GGTOTAL3.ReadOnly = True
        '
        'BAYAR3
        '
        Me.BAYAR3.DataPropertyName = "BAYAR"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        Me.BAYAR3.DefaultCellStyle = DataGridViewCellStyle11
        Me.BAYAR3.HeaderText = "Bayar"
        Me.BAYAR3.Name = "BAYAR3"
        Me.BAYAR3.ReadOnly = True
        '
        'SISA3
        '
        Me.SISA3.DataPropertyName = "SISA"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        Me.SISA3.DefaultCellStyle = DataGridViewCellStyle12
        Me.SISA3.HeaderText = "Sisa"
        Me.SISA3.Name = "SISA3"
        Me.SISA3.ReadOnly = True
        '
        'LUNAS3
        '
        Me.LUNAS3.DataPropertyName = "LUNAS"
        Me.LUNAS3.HeaderText = "Lunas"
        Me.LUNAS3.Name = "LUNAS3"
        Me.LUNAS3.ReadOnly = True
        '
        'check1
        '
        Me.check1.FalseValue = "0"
        Me.check1.HeaderText = "Pilih"
        Me.check1.Name = "check1"
        Me.check1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.check1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.check1.TrueValue = "1"
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No. Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        '
        'NO_RETUR2
        '
        Me.NO_RETUR2.DataPropertyName = "NO_RETUR"
        Me.NO_RETUR2.HeaderText = "No. Retur"
        Me.NO_RETUR2.Name = "NO_RETUR2"
        Me.NO_RETUR2.ReadOnly = True
        '
        'TGL_FAK
        '
        Me.TGL_FAK.DataPropertyName = "TGL_FAK"
        Me.TGL_FAK.HeaderText = "Tanggal "
        Me.TGL_FAK.Name = "TGL_FAK"
        Me.TGL_FAK.ReadOnly = True
        '
        'N_SUP2
        '
        Me.N_SUP2.DataPropertyName = "N_SUP"
        Me.N_SUP2.HeaderText = "Nama Supplier"
        Me.N_SUP2.Name = "N_SUP2"
        Me.N_SUP2.ReadOnly = True
        '
        'GGTOTAL
        '
        Me.GGTOTAL.DataPropertyName = "GGTOTAL"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.GGTOTAL.DefaultCellStyle = DataGridViewCellStyle7
        Me.GGTOTAL.HeaderText = "Grand Total"
        Me.GGTOTAL.Name = "GGTOTAL"
        Me.GGTOTAL.ReadOnly = True
        '
        'BAYAR2
        '
        Me.BAYAR2.DataPropertyName = "BAYAR"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.BAYAR2.DefaultCellStyle = DataGridViewCellStyle8
        Me.BAYAR2.HeaderText = "Bayar"
        Me.BAYAR2.Name = "BAYAR2"
        Me.BAYAR2.ReadOnly = True
        '
        'SISA
        '
        Me.SISA.DataPropertyName = "SISA"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.SISA.DefaultCellStyle = DataGridViewCellStyle9
        Me.SISA.HeaderText = "Sisa"
        Me.SISA.Name = "SISA"
        Me.SISA.ReadOnly = True
        '
        'LUNAS2
        '
        Me.LUNAS2.DataPropertyName = "LUNAS"
        Me.LUNAS2.HeaderText = "Lunas"
        Me.LUNAS2.Name = "LUNAS2"
        Me.LUNAS2.ReadOnly = True
        '
        'Cari_faktur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(899, 499)
        Me.Controls.Add(Me.input)
        Me.Controls.Add(Me.DGVfaktur1)
        Me.Controls.Add(Me.DGVfaktur4)
        Me.Controls.Add(Me.DGVfaktur3)
        Me.Controls.Add(Me.DGVfaktur2)
        Me.Controls.Add(Me.DGVfaktur)
        Me.Controls.Add(Me.ksup)
        Me.Controls.Add(Me.butCARI)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.date2)
        Me.Controls.Add(Me.date1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "Cari_faktur"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cari_faktur"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVfaktur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVfaktur2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVfaktur3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVfaktur4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVfaktur1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents ksup As System.Windows.Forms.Label
    Friend WithEvents dari As System.Windows.Forms.Label
    Friend WithEvents DGVfaktur As System.Windows.Forms.DataGridView
    Friend WithEvents DGVfaktur2 As System.Windows.Forms.DataGridView
    Friend WithEvents DGVfaktur3 As System.Windows.Forms.DataGridView
    Friend WithEvents DGVfaktur4 As System.Windows.Forms.DataGridView
    Friend WithEvents DGVfaktur1 As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_RETUR4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_FAK4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SUP4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents input As System.Windows.Forms.Button
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TANGGAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRANDTOT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPN1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GGTOTAL1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_JT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_PO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_RETUR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GIRO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tgl_fak2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GTOTAL2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PEMBAYARAN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EKSPEDISI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents B_EXPEDISI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USER_PRINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP_PRINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents check1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_RETUR2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GGTOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SISA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NO_FAK3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_RETUR3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_FAK3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GGTOTAL3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SISA3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
