﻿Imports System.Drawing.Printing
Imports System.IO

Public Class Pembelian_ReturEditHapus

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub Pembelian_ReturEditHapus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.dari.Text = "ERBELI"
            Cari_Brg.no.Text = noBeli.Text
            Cari_Brg.DGVretur.Focus()
        End If
    End Sub

    Private Sub kode_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles kode.Leave
        If kode.Text = "" Then
            butINPUT.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                kode.Text = b.Rows(0)("K_BRG").ToString
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
                Qty.Focus()
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub Pembelian_ReturEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Pembelian_ReturEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        loaddata()
        hitung()

        Total2.Text = FormatNumber(Total2.Text, 2)
        Disc.Text = FormatNumber(Disc.Text, 2)
        Grandtot.Text = FormatNumber(Grandtot.Text, 2)
    End Sub

    Sub loaddata()
        Dim a As New clsCCTV.clsReBeli
        Dim b As DataSet
        b = a.table(nofak.Text)
        DGVretur.DataSource = b.Tables("DETREBELI")
        kode.Focus()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVretur.RowCount - 1
            tot = tot + DGVretur.Item("TOTAL_BRG", i).Value
        Next
        Try
            Total2.Text = FormatNumber(tot, 2)
            Disc.Text = FormatNumber(Disc.Text, 2)
            Grandtot.Text = FormatNumber(CDbl(Total2.Text) - CDbl(Disc.Text), 2)
            If cbPPN.Checked = True Then
                PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
            Else
                PPN.Text = "0.00"
            End If
            Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Harga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Harga.Leave, Qty.Leave, Pot1.Leave, Pot2.Leave
        If IsNumeric(Qty.Text) And IsNumeric(Harga.Text) And IsNumeric(Pot1.Text) And IsNumeric(Pot2.Text) Then

            Jumlah.Text = CDbl(Harga.Text) * CDbl(Qty.Text)
            Total.Text = CDbl(Jumlah.Text) * (100 - CDbl(Pot1.Text)) / 100 * (100 - CDbl(Pot2.Text)) / 100
            Harga.Text = FormatNumber(Harga.Text, 2)
            Jumlah.Text = FormatNumber(Jumlah.Text, 2)
            Total.Text = FormatNumber(Total.Text, 2)
            Total2.Text = FormatNumber(Total.Text, 2)
        End If
    End Sub

    Private Sub DGVretur_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVretur.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVretur.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsReBeli
        Dim b As Boolean
        b = a.hapus1brg(nofak.Text, DGVretur.Item("K_BRG", baris).Value.ToString)

        If b = True Then
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Hapus Barang")
        End If

    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If kode.Text = "" Then
            butSELESAI.Focus()
            Exit Sub
        ElseIf Qty.Text > qty2.Text Then
            MsgBox("Qty retur tidak bisa lebih besar")
            Exit Sub
        End If
        Dim a As New clsCCTV.clsReBeli

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("HARGA")
        table1.Columns.Add("JUMLAH")
        table1.Columns.Add("POT1")
        table1.Columns.Add("POT2")
        table1.Columns.Add("TOTAL")

        table1.Rows.Add(nofak.Text, kode.Text, Nama.Text, Qty.Text, CDbl(Sat.Text), CDbl(Harga.Text), CDbl(Jumlah.Text), CDbl(Pot1.Text), CDbl(Pot2.Text), CDbl(Total.Text))

        Dim b As Boolean
        b = a.input1brg(nofak.Text, table1)

        If b = True Then
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Input !")
        End If

        kode.Clear()
        Nama.Clear()
        Qty.Clear()
        qty2.Text = 0
        Harga.Clear()
        Sat.Clear()
        Jumlah.Clear()
        Pot1.Clear()
        Pot2.Clear()
        Total.Clear()
        kode.Focus()

    End Sub

    Private Sub Disc_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Disc.Leave
        hitung()
    End Sub

    Private Sub butSELESAI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSELESAI.Click
        If Total2.Text = 0 Then
            Exit Sub
        End If
        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan perubahan ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsReBeli
        Dim b As Boolean
        b = a.setvalid(nofak.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text)

        If b = True Then
            MsgBox("Berhasil Menyimpan Perubahan")
            PRINT()
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Menyimpan Perubahan")
        End If
    End Sub

    Sub PRINT()
    
        Dim ask As DialogResult

        ask = MessageBox.Show("Print Faktur Retur Pembelian ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsReBeli
            Pembelian_Retur.tb = otc.table(nofak.Text)

            With Pembelian_Retur
                .tmpnofak = .tb.Tables("DATREBELI").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Pembelian_Retur.tb.Tables("DETREBELI").Rows.Count - 1

            ReDim Pembelian_Retur.tmpKode(jumlah)
            ReDim Pembelian_Retur.tmpNama(jumlah)
            ReDim Pembelian_Retur.tmpQty(jumlah)
            ReDim Pembelian_Retur.tmpSat(jumlah)
            ReDim Pembelian_Retur.tmpHrg(jumlah)
            ReDim Pembelian_Retur.tmpJmlh(jumlah)
            ReDim Pembelian_Retur.tmpPot1(jumlah)
            ReDim Pembelian_Retur.tmpPot2(jumlah)
            ReDim Pembelian_Retur.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Pembelian_Retur
                    .tmpKode(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETREBELI").Rows(ipo)("TOTAL").ToString

                End With
            Next
            With Pembelian_Retur
                arrke = 0
                printFont = New Font("Times New Roman", 10)
            End With

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Pembelian_Retur.faktur
            PrintDoc.Print()
        End If
    End Sub

    Private Sub butHAPUS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAPUS.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsReBeli
        Dim b As Boolean
        b = a.hapus(nofak.Text)
        If b = True Then
            MsgBox("Berhasil Hapus Faktur No. " & nofak.Text)
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Mengahus Faktur No. " & nofak.Text)
        End If
    End Sub
End Class