﻿Public Class Cari_Brg
    Public klgn As String
    Public DET As String = "N"
    Public nocm As String

    Sub cari2()

        Dim serial As String = 0
        If CBserial.CheckState = CheckState.Checked Then
            serial = 1
        ElseIf CBserial.CheckState = CheckState.Indeterminate Then
            serial = "%"
        End If

        If dari.Text = "BELI" Or dari.Text = "EBELI" Or dari.Text = "SO" Or dari.Text = "ESO" Or dari.Text = "MTSI" Or dari.Text = "EJUAL" Or dari.Text = "PBKN" Or dari.Text = "EPBK" Then
            DGVcari.Visible = True
            DGVcari.Focus()
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            Dim jenis As String = "%"
            If CBjenis.SelectedItem = "B" Then
                jenis = "B"
            ElseIf CBjenis.SelectedItem = "J" Then
                jenis = "J"
            End If
            b = a.table("%" & kode.Text & "%", "%" & nama.Text & "%", jenis, 1, serial)
            DGVcari.DataSource = b
            DGVcari.Focus()

        ElseIf dari.Text = "RBELI" Or dari.Text = "ERBELI" Then

            CBjenis.Enabled = False
            kode.Enabled = False
            nama.Enabled = False
            DGVretur.Visible = True
            DGVretur.Focus()
            Dim a As New clsCCTV.clsBeli
            Dim b As DataSet

            b = a.table(no.Text)
            DGVretur.DataSource = b.Tables("DETBELI")
            DGVretur.Focus()

        ElseIf Not (no.Text = "") And dari.Text = "SOJL" Then

            CBjenis.Enabled = False
            kode.Enabled = False
            nama.Enabled = False
            DGVretur.Visible = True
            DGVretur.Focus()
            Dim a As New clsCCTV.clsSOJual
            Dim b As DataSet

            b = a.table(no.Text)
            DGVretur.DataSource = b.Tables("DETSOJUAL")
            DGVretur.Focus()

        ElseIf no.Text = "" And dari.Text = "SOJL" Then
            DGVcari.Visible = True
            DGVcari.Focus()
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            Dim jenis As String = "%"

            If CBjenis.SelectedItem = "B" Then
                jenis = "B"
            ElseIf CBjenis.SelectedItem = "J" Then
                jenis = "J"
            End If

            b = a.table("%" & kode.Text & "%", "%" & nama.Text & "%", jenis, 1, serial)
            DGVcari.DataSource = b
            DGVcari.Focus()

        ElseIf dari.Text = "RJUAL" Then

            CBjenis.Enabled = False
            kode.Enabled = False
            nama.Enabled = False
            DGVretur.Visible = True
            DGVretur.Focus()
            Dim a As New clsCCTV.clsJual
            Dim b As DataSet

            b = a.table(no.Text)
            DGVretur.DataSource = b.Tables("DETJUAL")
            DGVretur.Focus()

        ElseIf dari.Text = "SVC" Or dari.Text = "ESVC" Then

            CBjenis.Enabled = False
            kode.Enabled = False
            nama.Enabled = False
            DGVretur.Visible = True
            DGVretur.Focus()
            Dim a As New clsCCTV.clsReJual
            Dim b As DataSet

            b = a.table(no.Text)
            DGVretur.DataSource = b.Tables("DETREJUAL")
            DGVretur.Focus()

        ElseIf dari.Text = "HSVC" Or dari.Text = "EHSV" Then

            CBjenis.Enabled = False
            kode.Enabled = False
            nama.Enabled = False
            DGVsvc.Visible = True
            DGVsvc.Focus()
            Dim a As New clsCCTV.clsService
            Dim b As DataSet

            b = a.table(no.Text)
            DGVsvc.DataSource = b.Tables("DETSERVICE")
            DGVsvc.Focus()

        ElseIf dari.Text = "JDWL" Or dari.Text = "EJDW" Then

            Dim jenis As String = "%"
            If CBjenis.SelectedItem = "B" Then
                jenis = "B"
            ElseIf CBjenis.SelectedItem = "J" Then
                jenis = "J"
            End If

            If DET = "Y" Then
                Dim a As New clsCCTV.clsComplain
                Dim b As DataSet
                DGVjdw.Visible = True
                DGVjdw.Focus()
                b = a.table(nocm)
                DGVjdw.DataSource = b.Tables("DETCOMPLAIN")
                DGVjdw.Focus()
            Else
                Dim a As New clsCCTV.clsBarang
                Dim b As DataTable
                DGVcari.Visible = True
                DGVcari.Focus()
                b = a.table("%" & kode.Text & "%", "%" & nama.Text & "%", jenis, 1, serial)
                DGVcari.DataSource = b
                DGVcari.Focus()
            End If

        ElseIf dari.Text = "CMPL" Or dari.Text = "ECMP" Then
            CBjenis.Enabled = False
            kode.Enabled = False
            nama.Enabled = False
            CBserial.Enabled = True
            If no.Text <> "JR" Then
                kode.Enabled = True
                nama.Enabled = True
                CBjenis.Enabled = True

                DGVcari.Visible = True
                DGVcari.Focus()
                Dim a As New clsCCTV.clsBarang
                Dim b As DataTable
                Dim jenis As String = "%"
                If CBjenis.SelectedItem = "B" Then
                    jenis = "B"
                ElseIf CBjenis.SelectedItem = "J" Then
                    jenis = "J"
                End If
                If CBserial.CheckState = CheckState.Checked Then
                    serial = 1
                ElseIf CBserial.CheckState = CheckState.Indeterminate Then
                    serial = "%"
                End If
                b = a.table("%" & kode.Text & "%", "%" & nama.Text & "%", jenis, 1, serial)
                DGVcari.DataSource = b
                DGVcari.Focus()
            Else
                DGVretur.Visible = True
                DGVretur.Focus()
                Dim a As New clsCCTV.clsJual
                Dim b As DataSet
                b = a.table(no.Text)
                DGVretur.DataSource = b.Tables("DETJUAL")
                DGVretur.Focus()
            End If
        End If
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        cari2()
    End Sub

    Private Sub DGVcari_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVcari.DoubleClick
        Dim baris As Integer
        baris = DGVcari.CurrentRow.Index.ToString

        If dari.Text = "BELI" Then
            Dim frm As Pembelian = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.Nama.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            ' frm.Harga.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            frm.Sat.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.kode.Focus()

        ElseIf dari.Text = "EBELI" Then
            Dim frm As PembelianEditHapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.Nama.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            ' frm.Harga.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            frm.Sat.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()

        ElseIf dari.Text = "EJUAL" Then
            Dim frm As PenjualanEditHapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.Nama.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.tableJual(DGVcari.Item("K_BRG", baris).Value.ToString, klgn)
            If b.Rows.Count = 1 Then
                frm.hrg.Text = b.Rows(0)("HARGA").ToString
            End If
            frm.Sat.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()

        ElseIf dari.Text = "SO" Then
            Dim frm As SO = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.Nama.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.Harga.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            frm.hrg.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            frm.Sat.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()

        ElseIf dari.Text = "ESO" Then
            Dim frm As SOEditHapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.Nama.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.Harga.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            frm.hrg.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            frm.Sat.Text = DGVcari.Item("SAT1", baris).Value.ToString
            'frm.qty2.Text = DGVcari.Item("QTY1", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()

        ElseIf dari.Text = "SOJL" Then
            Dim frm As Penjualan = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.Nama.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.Harga.Text = DGVcari.Item("H_JUAL", baris).Value.ToString
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.tableJual(DGVcari.Item("K_BRG", baris).Value.ToString, klgn)
            If b.Rows.Count = 1 Then
                frm.hrg.Text = b.Rows(0)("HARGA").ToString
            End If
            frm.Sat.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()

        ElseIf dari.Text = "MTSI" Then
            Dim frm As Mutasi = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVcari.Item("K_BRG", baris).Value.ToString

        ElseIf dari.Text = "JDWL" Then
            Dim frm As JadwalInput = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.nbrg.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.satuan.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.qty.Text = 0
            frm.qty.Focus()

        ElseIf dari.Text = "EJDW" Then
            Dim frm As JadwalEdithapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.nbrg.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.satuan.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.qty.Text = 0
            frm.qty.Focus()

        ElseIf dari.Text = "PBKN" Then
            Dim frm As Perbaikan = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg1.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.nbrg1.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.tsat1.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.tqty1.Text = 0
            frm.tqty1.Focus()

        ElseIf dari.Text = "EPBK" Then
            Dim frm As PerbaikanEditHapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg1.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.nbrg1.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.tsat1.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.tqty1.Text = 0
            frm.tqty1.Focus()

        ElseIf dari.Text = "CMPL" Then
            Dim frm As ComplainInput = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.nbrg.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.satuan.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.qty.Text = 0
            frm.qty.Focus()

        ElseIf dari.Text = "ECMP" Then
            Dim frm As ComplainEdithapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVcari.Item("K_BRG", baris).Value.ToString
            frm.nbrg.Text = DGVcari.Item("N_BRG", baris).Value.ToString
            frm.satuan.Text = DGVcari.Item("SAT1", baris).Value.ToString
            frm.qty.Text = 0
            frm.qty.Focus()
        End If
        Me.Close()

    End Sub

    Private Sub Cari_Brg_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub DGVretur_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVretur.DoubleClick
        Dim baris As Integer
        baris = DGVretur.CurrentRow.Index.ToString

        If dari.Text = "RJUAL" Then
            Dim frm As Penjualan_Retur = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVretur.Item("K_BRG1", baris).Value.ToString
            frm.Nama.Text = DGVretur.Item("N_BRG1", baris).Value.ToString
            frm.Harga.Text = DGVretur.Item("HARGA", baris).Value.ToString
            frm.hrg.Text = DGVretur.Item("HARGA", baris).Value.ToString
            frm.Sat.Text = DGVretur.Item("SAT", baris).Value.ToString
            frm.qty2.Text = DGVretur.Item("QTY", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()
            Me.Close()

        ElseIf dari.Text = "RBELI" Then
            Dim frm As Pembelian_Retur = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVretur.Item("K_BRG1", baris).Value.ToString
            frm.Nama.Text = DGVretur.Item("N_BRG1", baris).Value.ToString
            frm.Harga.Text = DGVretur.Item("HARGA", baris).Value.ToString
            frm.Sat.Text = DGVretur.Item("SAT", baris).Value.ToString
            frm.qty2.Text = DGVretur.Item("QTY", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()
            Me.Close()

        ElseIf dari.Text = "ERBELI" Then
            Dim frm As Pembelian_ReturEditHapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kode.Text = DGVretur.Item("K_BRG1", baris).Value.ToString
            frm.Nama.Text = DGVretur.Item("N_BRG1", baris).Value.ToString
            frm.Harga.Text = DGVretur.Item("HARGA", baris).Value.ToString
            frm.Sat.Text = DGVretur.Item("SAT", baris).Value.ToString
            frm.qty2.Text = DGVretur.Item("QTY", baris).Value.ToString
            frm.Qty.Text = 0
            frm.Jumlah.Text = 0
            frm.Pot1.Text = 0
            frm.Pot2.Text = 0
            frm.Total.Text = 0
            frm.Qty.Focus()
            Me.Close()

        ElseIf dari.Text = "SVC" Then
            Dim frm As ServiceInput = CType(frmMenu.MdiChildren(0), Form)
            frm.kode1.Text = DGVretur.Item("K_BRG1", baris).Value.ToString
            frm.Nama.Text = DGVretur.Item("N_BRG1", baris).Value.ToString
            frm.satuan.Text = DGVretur.Item("SAT", baris).Value.ToString
            frm.qty1.Focus()
            Me.Close()

        ElseIf dari.Text = "ESVC" Then
            Dim frm As ServiceEditHapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kode1.Text = DGVretur.Item("K_BRG1", baris).Value.ToString
            frm.nama.Text = DGVretur.Item("N_BRG1", baris).Value.ToString
            frm.satuan.Text = DGVretur.Item("SAT", baris).Value.ToString
            frm.qty1.Focus()
            Me.Close()

        ElseIf dari.Text = "CMPL" Then
            Dim frm As ComplainInput = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVretur.Item("K_BRG1", baris).Value.ToString
            frm.nbrg.Text = DGVretur.Item("N_BRG1", baris).Value.ToString
            frm.satuan.Text = DGVretur.Item("SAT", baris).Value.ToString
            frm.qty.Text = 0
            frm.qty.Focus()
            Me.Close()
        End If
    End Sub
    Private Sub Cari_Brg_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cari2()
    End Sub

    Private Sub DGVsvc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVsvc.DoubleClick
        Dim baris As Integer
        baris = DGVsvc.CurrentRow.Index.ToString
        If dari.Text = "EHSV" Then
            Dim frm As ServiceEditHapusHasil = CType(frmMenu.MdiChildren(0), Form)
            frm.kode1.Text = DGVsvc.Item("K_BRG2", baris).Value.ToString
            frm.nama.Text = DGVsvc.Item("N_BRG2", baris).Value.ToString
            frm.satuan.Text = DGVsvc.Item("SAT2", baris).Value.ToString
            frm.biaya.Text = DGVsvc.Item("BIAYA", baris).Value.ToString
            frm.qty1.Focus()
            Me.Close()

        ElseIf dari.Text = "HSVC" Then
            Dim frm As ServiceInputHasil = CType(frmMenu.MdiChildren(0), Form)
            frm.kode1.Text = DGVsvc.Item("K_BRG2", baris).Value.ToString
            frm.nama.Text = DGVsvc.Item("N_BRG2", baris).Value.ToString
            frm.satuan.Text = DGVsvc.Item("SAT2", baris).Value.ToString
            frm.qty1.Focus()
            Me.Close()
        End If

    End Sub

    Private Sub DGVjdw_DoubleClick(sender As Object, e As EventArgs) Handles DGVjdw.DoubleClick
        Dim baris As Integer
        baris = DGVjdw.CurrentRow.Index.ToString
        If dari.Text = "JDWL" Then
            Dim frm As JadwalInput = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVjdw.Item("KBRG", baris).Value.ToString
            frm.nbrg.Text = DGVjdw.Item("NBRG", baris).Value.ToString
            frm.satuan.Text = DGVjdw.Item("SAT3", baris).Value.ToString
            frm.qty.Text = DGVjdw.Item("QTY3", baris).Value.ToString
            frm.qty.Focus()
            Me.Close()

        ElseIf dari.Text = "EJDW" Then
            Dim frm As JadwalEdithapus = CType(frmMenu.MdiChildren(0), Form)
            frm.kbrg.Text = DGVjdw.Item("KBRG", baris).Value.ToString
            frm.nbrg.Text = DGVjdw.Item("NBRG", baris).Value.ToString
            frm.satuan.Text = DGVjdw.Item("SAT3", baris).Value.ToString
            frm.qty.Text = DGVjdw.Item("QTY3", baris).Value.ToString
            frm.qty.Focus()
            Me.Close()
        End If
    End Sub
End Class