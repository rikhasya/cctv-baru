﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cari_Brg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVcari = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dari = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.no = New System.Windows.Forms.Label()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBjenis = New System.Windows.Forms.ComboBox()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.kode = New System.Windows.Forms.TextBox()
        Me.DGVretur = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVsvc = New System.Windows.Forms.DataGridView()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BIAYA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVjdw = New System.Windows.Forms.DataGridView()
        Me.NOFAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KBRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NBRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CBserial = New System.Windows.Forms.CheckBox()
        Me.SERIAL = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.H_JUAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVcari, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGVretur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVjdw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVcari
        '
        Me.DGVcari.AllowUserToAddRows = False
        Me.DGVcari.AllowUserToDeleteRows = False
        Me.DGVcari.BackgroundColor = System.Drawing.Color.Peru
        Me.DGVcari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVcari.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SERIAL, Me.K_BRG, Me.N_BRG, Me.QTY1, Me.SAT1, Me.BJ, Me.H_JUAL, Me.KET, Me.Column1})
        Me.DGVcari.Location = New System.Drawing.Point(12, 73)
        Me.DGVcari.Name = "DGVcari"
        Me.DGVcari.ReadOnly = True
        Me.DGVcari.RowHeadersVisible = False
        Me.DGVcari.Size = New System.Drawing.Size(750, 416)
        Me.DGVcari.TabIndex = 0
        Me.DGVcari.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Controls.Add(Me.dari)
        Me.Panel1.Location = New System.Drawing.Point(12, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(51, 48)
        Me.Panel1.TabIndex = 1
        '
        'dari
        '
        Me.dari.AutoSize = True
        Me.dari.ForeColor = System.Drawing.Color.White
        Me.dari.Location = New System.Drawing.Point(3, 17)
        Me.dari.Name = "dari"
        Me.dari.Size = New System.Drawing.Size(10, 13)
        Me.dari.TabIndex = 2
        Me.dari.Text = "-"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CBserial)
        Me.GroupBox1.Controls.Add(Me.no)
        Me.GroupBox1.Controls.Add(Me.butCARI)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.CBjenis)
        Me.GroupBox1.Controls.Add(Me.nama)
        Me.GroupBox1.Controls.Add(Me.kode)
        Me.GroupBox1.Location = New System.Drawing.Point(69, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(693, 58)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "CARI BERDASARKAN"
        '
        'no
        '
        Me.no.AutoSize = True
        Me.no.Enabled = False
        Me.no.Font = New System.Drawing.Font("Microsoft Sans Serif", 3.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.no.Location = New System.Drawing.Point(15, 24)
        Me.no.Name = "no"
        Me.no.Size = New System.Drawing.Size(0, 5)
        Me.no.TabIndex = 9
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.Location = New System.Drawing.Point(610, 17)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(54, 33)
        Me.butCARI.TabIndex = 3
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(473, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "B/J"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(324, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nama Barang"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(161, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Kode Barang"
        '
        'CBjenis
        '
        Me.CBjenis.FormattingEnabled = True
        Me.CBjenis.Items.AddRange(New Object() {"", "B", "J"})
        Me.CBjenis.Location = New System.Drawing.Point(463, 29)
        Me.CBjenis.Name = "CBjenis"
        Me.CBjenis.Size = New System.Drawing.Size(49, 21)
        Me.CBjenis.TabIndex = 2
        '
        'nama
        '
        Me.nama.Location = New System.Drawing.Point(272, 30)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(174, 20)
        Me.nama.TabIndex = 1
        '
        'kode
        '
        Me.kode.Location = New System.Drawing.Point(137, 30)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(119, 20)
        Me.kode.TabIndex = 0
        '
        'DGVretur
        '
        Me.DGVretur.AllowUserToAddRows = False
        Me.DGVretur.AllowUserToDeleteRows = False
        Me.DGVretur.BackgroundColor = System.Drawing.Color.DarkKhaki
        Me.DGVretur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVretur.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.K_BRG1, Me.N_BRG1, Me.QTY, Me.SAT, Me.HARGA, Me.JUMLAH, Me.POT1, Me.POT2, Me.TOTAL})
        Me.DGVretur.Location = New System.Drawing.Point(12, 73)
        Me.DGVretur.Name = "DGVretur"
        Me.DGVretur.ReadOnly = True
        Me.DGVretur.RowHeadersVisible = False
        Me.DGVretur.Size = New System.Drawing.Size(750, 416)
        Me.DGVretur.TabIndex = 2
        Me.DGVretur.Visible = False
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'K_BRG1
        '
        Me.K_BRG1.DataPropertyName = "K_BRG"
        Me.K_BRG1.HeaderText = "Kode Barang"
        Me.K_BRG1.Name = "K_BRG1"
        Me.K_BRG1.ReadOnly = True
        '
        'N_BRG1
        '
        Me.N_BRG1.DataPropertyName = "N_BRG"
        Me.N_BRG1.HeaderText = "Nama Barang"
        Me.N_BRG1.Name = "N_BRG1"
        Me.N_BRG1.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Satuan"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.ReadOnly = True
        '
        'JUMLAH
        '
        Me.JUMLAH.DataPropertyName = "JUMLAH"
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        '
        'POT1
        '
        Me.POT1.DataPropertyName = "POT1"
        Me.POT1.HeaderText = "Pot 1"
        Me.POT1.Name = "POT1"
        Me.POT1.ReadOnly = True
        '
        'POT2
        '
        Me.POT2.DataPropertyName = "POT2"
        Me.POT2.HeaderText = "Pot 2"
        Me.POT2.Name = "POT2"
        Me.POT2.ReadOnly = True
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'DGVsvc
        '
        Me.DGVsvc.AllowUserToAddRows = False
        Me.DGVsvc.AllowUserToDeleteRows = False
        Me.DGVsvc.BackgroundColor = System.Drawing.Color.Khaki
        Me.DGVsvc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsvc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG2, Me.N_BRG2, Me.QTY2, Me.SAT2, Me.BIAYA})
        Me.DGVsvc.Location = New System.Drawing.Point(12, 75)
        Me.DGVsvc.Name = "DGVsvc"
        Me.DGVsvc.ReadOnly = True
        Me.DGVsvc.RowHeadersVisible = False
        Me.DGVsvc.Size = New System.Drawing.Size(750, 416)
        Me.DGVsvc.TabIndex = 3
        Me.DGVsvc.Visible = False
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        '
        'K_BRG2
        '
        Me.K_BRG2.DataPropertyName = "K_BRG"
        Me.K_BRG2.HeaderText = "Kode Barang"
        Me.K_BRG2.Name = "K_BRG2"
        Me.K_BRG2.ReadOnly = True
        '
        'N_BRG2
        '
        Me.N_BRG2.DataPropertyName = "N_BRG"
        Me.N_BRG2.HeaderText = "Nama Barang"
        Me.N_BRG2.Name = "N_BRG2"
        Me.N_BRG2.ReadOnly = True
        '
        'QTY2
        '
        Me.QTY2.DataPropertyName = "QTY"
        Me.QTY2.HeaderText = "Qty"
        Me.QTY2.Name = "QTY2"
        Me.QTY2.ReadOnly = True
        '
        'SAT2
        '
        Me.SAT2.DataPropertyName = "SAT"
        Me.SAT2.HeaderText = "Satuan"
        Me.SAT2.Name = "SAT2"
        Me.SAT2.ReadOnly = True
        '
        'BIAYA
        '
        Me.BIAYA.DataPropertyName = "KET"
        Me.BIAYA.HeaderText = "Biaya"
        Me.BIAYA.Name = "BIAYA"
        Me.BIAYA.ReadOnly = True
        '
        'DGVjdw
        '
        Me.DGVjdw.AllowUserToAddRows = False
        Me.DGVjdw.AllowUserToDeleteRows = False
        Me.DGVjdw.BackgroundColor = System.Drawing.Color.BlanchedAlmond
        Me.DGVjdw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVjdw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NOFAK, Me.KBRG, Me.NBRG, Me.QTY3, Me.SAT3})
        Me.DGVjdw.Location = New System.Drawing.Point(12, 75)
        Me.DGVjdw.Name = "DGVjdw"
        Me.DGVjdw.ReadOnly = True
        Me.DGVjdw.RowHeadersVisible = False
        Me.DGVjdw.Size = New System.Drawing.Size(750, 416)
        Me.DGVjdw.TabIndex = 4
        Me.DGVjdw.Visible = False
        '
        'NOFAK
        '
        Me.NOFAK.DataPropertyName = "NO_FAK"
        Me.NOFAK.HeaderText = "No Faktur"
        Me.NOFAK.Name = "NOFAK"
        Me.NOFAK.ReadOnly = True
        '
        'KBRG
        '
        Me.KBRG.DataPropertyName = "K_BRG"
        Me.KBRG.HeaderText = "Kode Barang"
        Me.KBRG.Name = "KBRG"
        Me.KBRG.ReadOnly = True
        '
        'NBRG
        '
        Me.NBRG.DataPropertyName = "N_BRG"
        Me.NBRG.HeaderText = "Nama Barang"
        Me.NBRG.Name = "NBRG"
        Me.NBRG.ReadOnly = True
        '
        'QTY3
        '
        Me.QTY3.DataPropertyName = "QTY"
        Me.QTY3.HeaderText = "Qty"
        Me.QTY3.Name = "QTY3"
        Me.QTY3.ReadOnly = True
        '
        'SAT3
        '
        Me.SAT3.DataPropertyName = "SAT"
        Me.SAT3.HeaderText = "Satuan"
        Me.SAT3.Name = "SAT3"
        Me.SAT3.ReadOnly = True
        '
        'CBserial
        '
        Me.CBserial.AutoSize = True
        Me.CBserial.Location = New System.Drawing.Point(534, 29)
        Me.CBserial.Name = "CBserial"
        Me.CBserial.Size = New System.Drawing.Size(52, 17)
        Me.CBserial.TabIndex = 10
        Me.CBserial.Text = "Serial"
        Me.CBserial.ThreeState = True
        Me.CBserial.UseVisualStyleBackColor = True
        '
        'SERIAL
        '
        Me.SERIAL.DataPropertyName = "SERIAL"
        Me.SERIAL.FalseValue = "0"
        Me.SERIAL.HeaderText = "Serial"
        Me.SERIAL.Name = "SERIAL"
        Me.SERIAL.ReadOnly = True
        Me.SERIAL.TrueValue = "1"
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY1
        '
        Me.QTY1.DataPropertyName = "QTY"
        Me.QTY1.HeaderText = "Qty"
        Me.QTY1.Name = "QTY1"
        Me.QTY1.ReadOnly = True
        '
        'SAT1
        '
        Me.SAT1.DataPropertyName = "SAT"
        Me.SAT1.HeaderText = "Satuan"
        Me.SAT1.Name = "SAT1"
        Me.SAT1.ReadOnly = True
        '
        'BJ
        '
        Me.BJ.DataPropertyName = "BJ"
        Me.BJ.HeaderText = "Jenis"
        Me.BJ.Name = "BJ"
        Me.BJ.ReadOnly = True
        '
        'H_JUAL
        '
        Me.H_JUAL.DataPropertyName = "H_JUAL"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.H_JUAL.DefaultCellStyle = DataGridViewCellStyle1
        Me.H_JUAL.HeaderText = "Harga Jual "
        Me.H_JUAL.Name = "H_JUAL"
        Me.H_JUAL.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "AKTIF"
        Me.Column1.HeaderText = "Aktif"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Cari_Brg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(774, 503)
        Me.Controls.Add(Me.DGVjdw)
        Me.Controls.Add(Me.DGVsvc)
        Me.Controls.Add(Me.DGVretur)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.DGVcari)
        Me.KeyPreview = True
        Me.Name = "Cari_Brg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cari Barang"
        Me.TopMost = True
        CType(Me.DGVcari, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGVretur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVsvc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVjdw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DGVcari As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dari As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CBjenis As System.Windows.Forms.ComboBox
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents kode As System.Windows.Forms.TextBox
    Friend WithEvents no As System.Windows.Forms.Label
    Friend WithEvents DGVretur As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DGVsvc As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BIAYA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DGVjdw As System.Windows.Forms.DataGridView
    Friend WithEvents NOFAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KBRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NBRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CBserial As System.Windows.Forms.CheckBox
    Friend WithEvents SERIAL As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents H_JUAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
