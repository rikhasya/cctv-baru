﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SuratJalanEditHapus
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Keterangan = New System.Windows.Forms.TextBox()
        Me.butSIM = New System.Windows.Forms.Button()
        Me.butHAP = New System.Windows.Forms.Button()
        Me.POT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.POT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBsls = New System.Windows.Forms.ComboBox()
        Me.CBlgn = New System.Windows.Forms.ComboBox()
        Me.telp = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.alamat = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.noSO = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Tanggal = New System.Windows.Forms.DateTimePicker()
        Me.noSJ = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DGV2 = New System.Windows.Forms.DataGridView()
        Me.NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SERIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(677, 387)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 97
        Me.Label15.Text = "Keterangan"
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle7
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.HARGA.DefaultCellStyle = DataGridViewCellStyle8
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.ReadOnly = True
        Me.HARGA.Visible = False
        '
        'JUMLAH
        '
        Me.JUMLAH.DataPropertyName = "JUMLAH"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.JUMLAH.DefaultCellStyle = DataGridViewCellStyle9
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        Me.JUMLAH.Visible = False
        '
        'Keterangan
        '
        Me.Keterangan.Location = New System.Drawing.Point(680, 403)
        Me.Keterangan.Multiline = True
        Me.Keterangan.Name = "Keterangan"
        Me.Keterangan.Size = New System.Drawing.Size(264, 75)
        Me.Keterangan.TabIndex = 96
        '
        'butSIM
        '
        Me.butSIM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSIM.Location = New System.Drawing.Point(734, 484)
        Me.butSIM.Name = "butSIM"
        Me.butSIM.Size = New System.Drawing.Size(75, 33)
        Me.butSIM.TabIndex = 94
        Me.butSIM.Text = "TUTUP"
        Me.butSIM.UseVisualStyleBackColor = True
        '
        'butHAP
        '
        Me.butHAP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butHAP.Location = New System.Drawing.Point(826, 484)
        Me.butHAP.Name = "butHAP"
        Me.butHAP.Size = New System.Drawing.Size(75, 33)
        Me.butHAP.TabIndex = 95
        Me.butHAP.Text = "HAPUS"
        Me.butHAP.UseVisualStyleBackColor = True
        '
        'POT1
        '
        Me.POT1.DataPropertyName = "POT1"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.POT1.DefaultCellStyle = DataGridViewCellStyle10
        Me.POT1.HeaderText = "Pot 1"
        Me.POT1.Name = "POT1"
        Me.POT1.ReadOnly = True
        Me.POT1.Visible = False
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToAddRows = False
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.HARGA, Me.JUMLAH, Me.POT1, Me.POT2, Me.TOTAL})
        Me.DGVdetail.Location = New System.Drawing.Point(47, 149)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.ReadOnly = True
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(897, 222)
        Me.DGVdetail.TabIndex = 93
        '
        'POT2
        '
        Me.POT2.DataPropertyName = "POT2"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        Me.POT2.DefaultCellStyle = DataGridViewCellStyle11
        Me.POT2.HeaderText = "Pot 2"
        Me.POT2.Name = "POT2"
        Me.POT2.ReadOnly = True
        Me.POT2.Visible = False
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle12
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.Label1.Location = New System.Drawing.Point(47, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(150, 23)
        Me.Label1.TabIndex = 92
        Me.Label1.Text = "Hapus Surat Jalan "
        '
        'CBsls
        '
        Me.CBsls.Enabled = False
        Me.CBsls.FormattingEnabled = True
        Me.CBsls.Location = New System.Drawing.Point(288, 66)
        Me.CBsls.Name = "CBsls"
        Me.CBsls.Size = New System.Drawing.Size(198, 21)
        Me.CBsls.TabIndex = 15
        '
        'CBlgn
        '
        Me.CBlgn.Enabled = False
        Me.CBlgn.FormattingEnabled = True
        Me.CBlgn.Location = New System.Drawing.Point(288, 11)
        Me.CBlgn.Name = "CBlgn"
        Me.CBlgn.Size = New System.Drawing.Size(198, 21)
        Me.CBlgn.TabIndex = 14
        '
        'telp
        '
        Me.telp.Enabled = False
        Me.telp.Location = New System.Drawing.Point(288, 40)
        Me.telp.Name = "telp"
        Me.telp.Size = New System.Drawing.Size(198, 20)
        Me.telp.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(208, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Telp"
        '
        'alamat
        '
        Me.alamat.Enabled = False
        Me.alamat.Location = New System.Drawing.Point(573, 11)
        Me.alamat.Multiline = True
        Me.alamat.Name = "alamat"
        Me.alamat.Size = New System.Drawing.Size(305, 78)
        Me.alamat.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(515, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Alamat"
        '
        'noSO
        '
        Me.noSO.Enabled = False
        Me.noSO.Location = New System.Drawing.Point(69, 40)
        Me.noSO.Name = "noSO"
        Me.noSO.Size = New System.Drawing.Size(116, 20)
        Me.noSO.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "No. SO"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.CBsls)
        Me.Panel1.Controls.Add(Me.CBlgn)
        Me.Panel1.Controls.Add(Me.telp)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.alamat)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.noSO)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Tanggal)
        Me.Panel1.Controls.Add(Me.noSJ)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(47, 43)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(897, 102)
        Me.Panel1.TabIndex = 91
        '
        'Tanggal
        '
        Me.Tanggal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Tanggal.Location = New System.Drawing.Point(69, 68)
        Me.Tanggal.Name = "Tanggal"
        Me.Tanggal.Size = New System.Drawing.Size(92, 20)
        Me.Tanggal.TabIndex = 7
        '
        'noSJ
        '
        Me.noSJ.Enabled = False
        Me.noSJ.Location = New System.Drawing.Point(69, 11)
        Me.noSJ.Name = "noSJ"
        Me.noSJ.Size = New System.Drawing.Size(116, 20)
        Me.noSJ.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(208, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Sales"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(208, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Langganan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tanggal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. SJ "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DGV2)
        Me.GroupBox1.Location = New System.Drawing.Point(47, 374)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(509, 252)
        Me.GroupBox1.TabIndex = 98
        Me.GroupBox1.TabStop = False
        '
        'DGV2
        '
        Me.DGV2.AllowUserToAddRows = False
        Me.DGV2.AllowUserToDeleteRows = False
        Me.DGV2.BackgroundColor = System.Drawing.Color.FloralWhite
        Me.DGV2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO, Me.K_BRG1, Me.NO_SERIAL, Me.NO_FAK})
        Me.DGV2.Location = New System.Drawing.Point(15, 19)
        Me.DGV2.Name = "DGV2"
        Me.DGV2.ReadOnly = True
        Me.DGV2.RowHeadersVisible = False
        Me.DGV2.Size = New System.Drawing.Size(480, 225)
        Me.DGV2.TabIndex = 5
        '
        'NO
        '
        Me.NO.DataPropertyName = "NO"
        Me.NO.HeaderText = "No"
        Me.NO.Name = "NO"
        Me.NO.ReadOnly = True
        Me.NO.Width = 50
        '
        'K_BRG1
        '
        Me.K_BRG1.DataPropertyName = "K_BRG"
        Me.K_BRG1.HeaderText = "Kode Barang"
        Me.K_BRG1.Name = "K_BRG1"
        Me.K_BRG1.ReadOnly = True
        '
        'NO_SERIAL
        '
        Me.NO_SERIAL.DataPropertyName = "SERIAL"
        Me.NO_SERIAL.HeaderText = "Nomor Serial"
        Me.NO_SERIAL.Name = "NO_SERIAL"
        Me.NO_SERIAL.ReadOnly = True
        Me.NO_SERIAL.Width = 150
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "NO_FAK"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Visible = False
        '
        'SuratJalanEditHapus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Keterangan)
        Me.Controls.Add(Me.butSIM)
        Me.Controls.Add(Me.butHAP)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "SuratJalanEditHapus"
        Me.Text = "SuratJalanEditHapus"
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DGV2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Keterangan As System.Windows.Forms.TextBox
    Friend WithEvents butSIM As System.Windows.Forms.Button
    Friend WithEvents butHAP As System.Windows.Forms.Button
    Friend WithEvents POT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents POT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CBsls As System.Windows.Forms.ComboBox
    Friend WithEvents CBlgn As System.Windows.Forms.ComboBox
    Friend WithEvents telp As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents alamat As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents noSO As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents noSJ As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DGV2 As System.Windows.Forms.DataGridView
    Friend WithEvents NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SERIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
