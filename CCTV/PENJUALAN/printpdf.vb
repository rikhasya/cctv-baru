﻿Imports System.Drawing.Printing
Imports System.IO
Imports System
Imports iTextSharp.text
Imports iTextSharp.text.pdf

Public Class printpdf

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public untuk As String

    Private Sub printpdf_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        print()
    End Sub

    Sub print()

        Dim otc As New clsCCTV.clsJual
        tb = otc.table(tmpnofak)

        'Dim a As New clsCCTV.clsJual
        'Dim b As DataTable
        'b = a.table(cabang.Text, k_lgn.Text)

        Dim count As Integer = 0
        Dim line As String = Nothing

        Dim doc As Document = New Document(PageSize.A4)

        Dim filename As String
        If untuk = "FAKTUR PENJUALAN" Then
            filename = My.Computer.FileSystem.SpecialDirectories.MyDocuments.ToString & "\faktur_pdf\" & tmpnofak & ".pdf"
        Else
            filename = My.Computer.FileSystem.SpecialDirectories.MyDocuments.ToString & "\faktur_pdf\SJ-" & tmpnofak & ".pdf"
        End If

        Dim writer As PdfWriter = PdfWriter.GetInstance(doc, New FileStream(filename, FileMode.Create))
        doc.Open()
        doc.AddAuthor("clsUser.CLUserID" & ":" & clsCCTV.clsPengguna.CLUserID)

        Dim cb As PdfContentByte = writer.DirectContent

        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 14)

        'Dim i As Integer
        Dim hitbaris As Integer = 1
        Dim halhalke As Integer = 1

        'cb.Rectangle(20, 500, 565, 700) ' kolom besar 20=kiri 75=bawah 565=kanan 615=atas

        cb.MoveTo(20, 765) ' garis kode
        cb.LineTo(585, 765)
        cb.Stroke()

        cb.MoveTo(20, 750) ' garis pengusaha
        cb.LineTo(585, 750)
        cb.Stroke()

        cb.MoveTo(20, 620) ' garis antara npwp dan pembeli
        cb.LineTo(585, 620)
        cb.Stroke()

        cb.MoveTo(20, 605) ' garis pembeli bawah
        cb.LineTo(585, 605)
        cb.Stroke()


        Dim ct As New ColumnText(cb)

        cb.BeginText()


        cb.SetFontAndSize(bf, 14)

        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SEJAHTERA MANDIRI", 30, 810, 0)
        cb.SetFontAndSize(bf, 13)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, untuk, 300, 820, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, tmpnofak, 300, 805, 0)

        cb.SetFontAndSize(bf, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, tb.Tables("DATJUAL").Rows(0)("TGL_FAK"), 300, 790, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Kepada :", 400, 820, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tb.Tables("DATJUAL").Rows(0)("N_LGN"), 400, 805, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tb.Tables("DATJUAL").Rows(0)("ALAMAT"), 400, 795, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Dengan rincian barang sebagai berikut :", 30, 770, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Kode", 40, 755, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nama Barang", 100, 755, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Qty", 265, 755, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Sat", 290, 755, 0)
        If untuk = "FAKTUR PENJUALAN" Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Harga", 325, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Jumlah", 380, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Pot 1", 430, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Pot 2", 460, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Total", 520, 755, 0)

            'total bawah
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Total", 420, 590, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Disc", 420, 578, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Grand Total", 420, 566, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "PPN", 420, 554, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Grand Total", 420, 542, 0)

            'isinya
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DATJUAL").Rows(0)("TOTAL").ToString, 0), 570, 590, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DATJUAL").Rows(0)("DISC").ToString, 0), 570, 578, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DATJUAL").Rows(0)("GTOTAL").ToString, 0), 570, 566, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DATJUAL").Rows(0)("PPN").ToString, 0), 570, 554, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DATJUAL").Rows(0)("TOTAL").ToString, 0), 570, 542, 0)
        End If

        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Keterangan :", 30, 590, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tb.Tables("DATJUAL").Rows(0)("KET").ToString, 100, 590, 0)

        cb.SetFontAndSize(bf, 9)
        For i = 0 To tb.Tables("DETJUAL").Rows.Count - 1

            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, i + 1, 30, 740 - (i * 12), 0)

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tb.Tables("DETJUAL").Rows(i)("SAT").ToString, 290, 740 - (i * 12), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tb.Tables("DETJUAL").Rows(i)("N_BRG").ToString, 105, 740 - (i * 12), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, tb.Tables("DETJUAL").Rows(i)("K_BRG").ToString, 45, 740 - (i * 12), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DETJUAL").Rows(i)("QTY"), 0), 270, 740 - (i * 12), 0)
            If untuk = "FAKTUR PENJUALAN" Then
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DETJUAL").Rows(i)("HARGA"), 0), 350, 740 - (i * 12), 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DETJUAL").Rows(i)("JUMLAH"), 0), 410, 740 - (i * 12), 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DETJUAL").Rows(i)("POT1"), 2), 450, 740 - (i * 12), 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DETJUAL").Rows(i)("POT2"), 2), 480, 740 - (i * 12), 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, FormatNumber(tb.Tables("DETJUAL").Rows(i)("TOTAL").ToString, 0), 570, 740 - (i * 12), 0)
            End If
        Next

        cb.SetFontAndSize(bf, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Penerima", 60, 540, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Pengirim", 330, 540, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "(............................)", 40, 500, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "(............................)", 310, 500, 0)
        cb.SetFontAndSize(bf, 9)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "***Perhatian !!", 175, 510, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Telitilah barang-barang yang akan diterima.***", 125, 500, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "id : " & tb.Tables("DATJUAL").Rows(0)("USERID"), 450, 500, 0)
        '

        cb.EndText() ' end na

        doc.Close()

        Process.Start(filename)
    End Sub
End Class