﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Penjualan_Laporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CBsales = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CBlgn = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DGVheader = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TANGGAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP_PRINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRANDTOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GGTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PEMBAYARAN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EKSPEDISI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.B_EKSPEDISI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USER_PRINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_JT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GIRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.DGVserial = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVserial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(383, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(185, 24)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Laporan Penjualan"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(221, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "s/d"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "* Doubleclick untuk edit/hapus laporan"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(131, 12)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 0
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(245, 12)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(88, 20)
        Me.date2.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.CBsales)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.CBlgn)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.date2)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-1, 38)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'CBsales
        '
        Me.CBsales.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBsales.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBsales.FormattingEnabled = True
        Me.CBsales.Location = New System.Drawing.Point(707, 13)
        Me.CBsales.Name = "CBsales"
        Me.CBsales.Size = New System.Drawing.Size(115, 21)
        Me.CBsales.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(633, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Nama Sales"
        '
        'CBlgn
        '
        Me.CBlgn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBlgn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBlgn.FormattingEnabled = True
        Me.CBlgn.Location = New System.Drawing.Point(437, 12)
        Me.CBlgn.Name = "CBlgn"
        Me.CBlgn.Size = New System.Drawing.Size(180, 21)
        Me.CBlgn.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(79, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Tanggal"
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(828, 12)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 3
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(343, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Langganan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 384)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Detail Laporan :"
        '
        'DGVheader
        '
        Me.DGVheader.AllowUserToAddRows = False
        Me.DGVheader.AllowUserToDeleteRows = False
        Me.DGVheader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVheader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.NO_SO, Me.TANGGAL, Me.N_LGN, Me.KET, Me.KET2, Me.DATESTAMP_PRINT, Me.TOTAL1, Me.DISC, Me.GRANDTOT, Me.PPN, Me.GGTOTAL, Me.PEMBAYARAN, Me.EKSPEDISI, Me.B_EKSPEDISI, Me.ALAMAT, Me.TELP, Me.K_LGN, Me.USER_PRINT, Me.USERID, Me.TGL_JT, Me.N_SLS, Me.DATESTAMP, Me.VALID, Me.BAYAR, Me.LUNAS, Me.GIRO})
        Me.DGVheader.Location = New System.Drawing.Point(12, 107)
        Me.DGVheader.Name = "DGVheader"
        Me.DGVheader.ReadOnly = True
        Me.DGVheader.RowHeadersVisible = False
        Me.DGVheader.Size = New System.Drawing.Size(978, 274)
        Me.DGVheader.TabIndex = 32
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'NO_SO
        '
        Me.NO_SO.DataPropertyName = "NO_SO"
        Me.NO_SO.HeaderText = "No SO"
        Me.NO_SO.Name = "NO_SO"
        Me.NO_SO.ReadOnly = True
        '
        'TANGGAL
        '
        Me.TANGGAL.DataPropertyName = "TGL_FAK"
        Me.TANGGAL.HeaderText = "Tanggal"
        Me.TANGGAL.Name = "TANGGAL"
        Me.TANGGAL.ReadOnly = True
        '
        'N_LGN
        '
        Me.N_LGN.DataPropertyName = "N_LGN"
        Me.N_LGN.HeaderText = "Pelanggan"
        Me.N_LGN.Name = "N_LGN"
        Me.N_LGN.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan Print"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'KET2
        '
        Me.KET2.DataPropertyName = "KET2"
        Me.KET2.HeaderText = "Keterangan 2"
        Me.KET2.Name = "KET2"
        Me.KET2.ReadOnly = True
        '
        'DATESTAMP_PRINT
        '
        Me.DATESTAMP_PRINT.DataPropertyName = "DATESTAMP_PRINT"
        Me.DATESTAMP_PRINT.HeaderText = "Tgl Print"
        Me.DATESTAMP_PRINT.Name = "DATESTAMP_PRINT"
        Me.DATESTAMP_PRINT.ReadOnly = True
        '
        'TOTAL1
        '
        Me.TOTAL1.DataPropertyName = "TOTAL"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.TOTAL1.DefaultCellStyle = DataGridViewCellStyle1
        Me.TOTAL1.HeaderText = "Total"
        Me.TOTAL1.Name = "TOTAL1"
        Me.TOTAL1.ReadOnly = True
        '
        'DISC
        '
        Me.DISC.DataPropertyName = "DISC"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.DISC.DefaultCellStyle = DataGridViewCellStyle2
        Me.DISC.HeaderText = "Diskon"
        Me.DISC.Name = "DISC"
        Me.DISC.ReadOnly = True
        '
        'GRANDTOT
        '
        Me.GRANDTOT.DataPropertyName = "GTOTAL"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.GRANDTOT.DefaultCellStyle = DataGridViewCellStyle3
        Me.GRANDTOT.HeaderText = "Grand Total"
        Me.GRANDTOT.Name = "GRANDTOT"
        Me.GRANDTOT.ReadOnly = True
        '
        'PPN
        '
        Me.PPN.DataPropertyName = "PPN"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.PPN.DefaultCellStyle = DataGridViewCellStyle4
        Me.PPN.HeaderText = "PPN"
        Me.PPN.Name = "PPN"
        Me.PPN.ReadOnly = True
        '
        'GGTOTAL
        '
        Me.GGTOTAL.DataPropertyName = "GGTOTAL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.GGTOTAL.DefaultCellStyle = DataGridViewCellStyle5
        Me.GGTOTAL.HeaderText = "Grand Total"
        Me.GGTOTAL.Name = "GGTOTAL"
        Me.GGTOTAL.ReadOnly = True
        '
        'PEMBAYARAN
        '
        Me.PEMBAYARAN.DataPropertyName = "PEMBAYARAN"
        Me.PEMBAYARAN.HeaderText = "Pembayaran"
        Me.PEMBAYARAN.Name = "PEMBAYARAN"
        Me.PEMBAYARAN.ReadOnly = True
        '
        'EKSPEDISI
        '
        Me.EKSPEDISI.DataPropertyName = "EKSPEDISI"
        Me.EKSPEDISI.HeaderText = "Expedisi"
        Me.EKSPEDISI.Name = "EKSPEDISI"
        Me.EKSPEDISI.ReadOnly = True
        '
        'B_EKSPEDISI
        '
        Me.B_EKSPEDISI.DataPropertyName = "B_EXPEDISI"
        Me.B_EKSPEDISI.HeaderText = "Biaya Expedisi"
        Me.B_EKSPEDISI.Name = "B_EKSPEDISI"
        Me.B_EKSPEDISI.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ALAMAT.DefaultCellStyle = DataGridViewCellStyle6
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'K_LGN
        '
        Me.K_LGN.DataPropertyName = "K_LGN"
        Me.K_LGN.HeaderText = "Kode Supplier"
        Me.K_LGN.Name = "K_LGN"
        Me.K_LGN.ReadOnly = True
        Me.K_LGN.Visible = False
        '
        'USER_PRINT
        '
        Me.USER_PRINT.DataPropertyName = "USER_PRINT"
        Me.USER_PRINT.HeaderText = "UserID Print"
        Me.USER_PRINT.Name = "USER_PRINT"
        Me.USER_PRINT.ReadOnly = True
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'TGL_JT
        '
        Me.TGL_JT.DataPropertyName = "TGL_JT"
        Me.TGL_JT.HeaderText = "Tanggal JT"
        Me.TGL_JT.Name = "TGL_JT"
        Me.TGL_JT.ReadOnly = True
        '
        'N_SLS
        '
        Me.N_SLS.DataPropertyName = "N_SLS"
        Me.N_SLS.HeaderText = "Sales"
        Me.N_SLS.Name = "N_SLS"
        Me.N_SLS.ReadOnly = True
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "Datestamp"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.DataPropertyName = "BAYAR"
        Me.BAYAR.HeaderText = "Bayar"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        Me.BAYAR.Visible = False
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        Me.LUNAS.Visible = False
        '
        'GIRO
        '
        Me.GIRO.DataPropertyName = "GIRO"
        Me.GIRO.HeaderText = "Giro"
        Me.GIRO.Name = "GIRO"
        Me.GIRO.ReadOnly = True
        Me.GIRO.Visible = False
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToAddRows = False
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.HARGA, Me.JUMLAH, Me.POT1, Me.POT2, Me.TOTAL})
        Me.DGVdetail.Location = New System.Drawing.Point(12, 400)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.ReadOnly = True
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(614, 250)
        Me.DGVdetail.TabIndex = 33
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle7
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.HARGA.DefaultCellStyle = DataGridViewCellStyle8
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.ReadOnly = True
        '
        'JUMLAH
        '
        Me.JUMLAH.DataPropertyName = "JUMLAH"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.JUMLAH.DefaultCellStyle = DataGridViewCellStyle9
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        '
        'POT1
        '
        Me.POT1.DataPropertyName = "POT1"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.POT1.DefaultCellStyle = DataGridViewCellStyle10
        Me.POT1.HeaderText = "Pot 1"
        Me.POT1.Name = "POT1"
        Me.POT1.ReadOnly = True
        '
        'POT2
        '
        Me.POT2.DataPropertyName = "POT2"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        Me.POT2.DefaultCellStyle = DataGridViewCellStyle11
        Me.POT2.HeaderText = "Pot 2"
        Me.POT2.Name = "POT2"
        Me.POT2.ReadOnly = True
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle12
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(229, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(109, 13)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "* F1 untuk print faktur"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(364, 88)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(182, 13)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "* F2 untuk Input Detail Serial Number"
        '
        'DGVserial
        '
        Me.DGVserial.AllowUserToAddRows = False
        Me.DGVserial.AllowUserToDeleteRows = False
        Me.DGVserial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVserial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column1, Me.DataGridViewTextBoxColumn3})
        Me.DGVserial.Location = New System.Drawing.Point(632, 400)
        Me.DGVserial.Name = "DGVserial"
        Me.DGVserial.ReadOnly = True
        Me.DGVserial.RowHeadersVisible = False
        Me.DGVserial.Size = New System.Drawing.Size(358, 250)
        Me.DGVserial.TabIndex = 43
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(568, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(178, 13)
        Me.Label10.TabIndex = 44
        Me.Label10.Text = "* F3 untuk print Detail Serial Number"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "NO_FAK"
        Me.DataGridViewTextBoxColumn1.HeaderText = "No Faktur"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "K_BRG"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Kode Barang"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "N_BRG"
        Me.Column1.HeaderText = "Nama Barang"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "SERIAL"
        Me.DataGridViewTextBoxColumn3.HeaderText = "No Serial"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'Penjualan_Laporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.DGVserial)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.DGVheader)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "Penjualan_Laporan"
        Me.Text = "Penjualan_Laporan"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVserial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents DGVheader As System.Windows.Forms.DataGridView
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CBlgn As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CBsales As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TANGGAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP_PRINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRANDTOT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GGTOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PEMBAYARAN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EKSPEDISI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents B_EKSPEDISI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USER_PRINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_JT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GIRO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DGVserial As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
