﻿Public Class SOEditHapus

    Public stts As String

    Private Sub SOEditHapus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.dari.Text = "ESO"
        End If
    End Sub

    Private Sub kode_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles kode.Leave
        If kode.Text = "" Then
            butINPUT.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                kode.Text = b.Rows(0)("K_BRG").ToString
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
                Harga.Text = b.Rows(0)("H_JUAL").ToString
                Qty.Focus()
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub SOEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SOEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loaddata()

        Total2.Text = FormatNumber(Total2.Text, 2)
        Disc.Text = FormatNumber(Disc.Text, 2)
        Grandtot.Text = FormatNumber(Grandtot.Text, 2)
    End Sub

    Sub loaddata()
        Dim a As New clsCCTV.clsSOJual
        Dim b As DataSet
        b = a.table(NoFak.Text)
        DGVso.DataSource = b.Tables("DETSOJUAL")
        kode.Focus()
    End Sub

    Private Sub Harga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Harga.Leave, Qty.Leave, Pot1.Leave, Pot2.Leave

        If IsNumeric(Qty.Text) And IsNumeric(Harga.Text) And IsNumeric(Pot1.Text) And IsNumeric(Pot2.Text) Then

            Jumlah.Text = CDbl(Harga.Text) * CDbl(Qty.Text)
            Total.Text = CDbl(Jumlah.Text) * (100 - CDbl(Pot1.Text)) / 100 * (100 - CDbl(Pot2.Text)) / 100
            Harga.Text = FormatNumber(Harga.Text, 2)
            Jumlah.Text = FormatNumber(Jumlah.Text, 2)
            Total.Text = FormatNumber(Total.Text, 2)
            Total2.Text = FormatNumber(Total.Text, 2)
        End If

    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If kode.Text = "" Then
            butSIMPAN.Focus()
            Exit Sub
        ElseIf kode.Text = kode2.Text Then
            If Qty.Text < qty2.Text Then
                MsgBox("Qty Tidak Boleh Lebih Kecil")
                Exit Sub
            End If
        End If
        Dim a As New clsCCTV.clsSOJual

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("HARGA")
        table1.Columns.Add("JUMLAH")
        table1.Columns.Add("POT1")
        table1.Columns.Add("POT2")
        table1.Columns.Add("TOTAL")

        table1.Rows.Add(NoFak.Text, kode.Text, Nama.Text, Qty.Text, Sat.Text, CDbl(Harga.Text), CDbl(Jumlah.Text), CDbl(Pot1.Text), CDbl(Pot2.Text), CDbl(Total.Text))

        Dim b As Boolean
        b = a.input1brg(NoFak.Text, table1)

        If b = True Then
            kode.Text = ""
            qty2.Text = ""
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Input !")
        End If

        kode.Clear()
        Nama.Clear()
        Qty.Clear()
        Harga.Clear()
        Sat.Clear()
        Jumlah.Clear()
        Pot1.Clear()
        Pot2.Clear()
        Total.Clear()
        kode.Focus()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVso.RowCount - 1
            tot = tot + DGVso.Item("TOTAL_BRG", i).Value
        Next
        Total2.Text = FormatNumber(tot, 2)
        Disc.Text = FormatNumber(Disc.Text, 2)
        Grandtot.Text = FormatNumber(CDbl(Total2.Text) - CDbl(Disc.Text), 2)
        If cbPPN.Checked = True Then
            PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
        Else
            PPN.Text = "0.00"
        End If
        Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
    End Sub

    Private Sub butHAPUS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAPUS.Click
        If stts <> "SO" Then
            MsgBox("Status Faktur Bukan SO")
            Exit Sub
        End If

        Dim ask As DialogResult
        ask = MessageBox.Show("Hapus Faktur ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsSOJual
        Dim b As Boolean
        b = a.hapus(NoFak.Text)

        If b = True Then
            MsgBox("Berhasil Hapus Faktur SO")
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Hapus Faktur SO")
        End If
    End Sub

    Private Sub DGVso_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVso.DoubleClick
        Dim baris As Integer
        baris = DGVso.CurrentRow.Index.ToString

        If DGVso.Item("QTY_SJ", baris).Value.ToString <> 0 Then
            MsgBox("Tidak Bisa Di Hapus, Sudah Pernah Kirim Barang!")
            Exit Sub
        End If

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsSOJual
        Dim b As Boolean
        b = a.hapus1brg(NoFak.Text, DGVso.Item("K_BRG", baris).Value.ToString)

        If b = True Then
            kode.Text = DGVso.Item("K_BRG", baris).Value.ToString
            kode2.Text = DGVso.Item("K_BRG", baris).Value.ToString
            Nama.Text = DGVso.Item("N_BRG", baris).Value.ToString
            Qty.Text = DGVso.Item("QTY_BRG", baris).Value.ToString
            qty2.Text = DGVso.Item("QTY_BRG", baris).Value.ToString
            Sat.Text = DGVso.Item("SAT_BRG", baris).Value.ToString
            Harga.Text = DGVso.Item("HARGA_BRG", baris).Value.ToString
            Jumlah.Text = DGVso.Item("JUMLAH_BRG", baris).Value.ToString
            Pot1.Text = DGVso.Item("POT1_BRG", baris).Value.ToString
            Pot2.Text = DGVso.Item("POT2_BRG", baris).Value.ToString
            Total.Text = DGVso.Item("TOTAL_BRG", baris).Value.ToString
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Hapus Barang")
        End If
    End Sub

    Private Sub butSIMPAN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIMPAN.Click
        If Total2.Text = 0 Then
            Exit Sub
        End If
       

        Dim a As New clsCCTV.clsSOJual
        Dim b As Boolean
        b = a.setvalid(NoFak.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text, CBbayar.Text, CBexpedisi.Text, CBbiayaexp.Text, KET2.text)

        If b = True Then
            MsgBox("Berhasil Menyimpan Perubahan")
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Menyimpan Perubahan")
        End If
    End Sub

    Private Sub Disc_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc.Leave
        hitung()
    End Sub

    Private Sub cbPPN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPN.CheckedChanged
        hitung()
    End Sub
End Class