﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SuratJalanEditHapus

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub SuratJalanEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With

        loadd()
        load2()
    End Sub

    Sub loadd()
        Dim a As New clsCCTV.clsSJ
        Dim b As DataSet
        b = a.table(noSJ.Text)
        DGVdetail.DataSource = b.Tables("DETSJ")
    End Sub

    Sub load2()
        Dim a As New clsCCTV.clsSJ
        Dim b As DataSet
        b = a.table(noSJ.Text)
        DGV2.DataSource = b.Tables("DETSJ2")
    End Sub

    Private Sub butHAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAP.Click
        Dim x As New clsCCTV.clsSOJual
        Dim z As DataSet
        z = x.table(noSO.Text)
        If z.Tables("DATSOJUAL").Rows(0)("STATUS") = "FK" Then
            MsgBox("Sudah pernah input faktur")
            Exit Sub
        End If

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsSJ
        Dim b As Boolean
        b = a.hapus(noSJ.Text)

        If b = True Then
            MsgBox("Berhasil hapus SJ " & noSJ.Text)
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal hapus")
            Exit Sub
        End If
    End Sub

    Private Sub butSIM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSIM.Click
        Me.Close()
    End Sub

    Sub PRINT()

        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Surat Jalan?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsSJ
            TerimaBarang.tb = otc.table(noSJ.Text)

            With TerimaBarang
                .tmpnofak = .tb.Tables("DATSJ").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = TerimaBarang.tb.Tables("DETSJ").Rows.Count - 1

            ReDim TerimaBarang.tmpKode(jumlah)
            ReDim TerimaBarang.tmpNama(jumlah)
            ReDim TerimaBarang.tmpQty(jumlah)
            ReDim TerimaBarang.tmpSat(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With TerimaBarang
                    .tmpKode(ipo) = .tb.Tables("DETSJ").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETSJ").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETSJ").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETSJ").Rows(ipo)("SAT").ToString
                End With
            Next
            With TerimaBarang
                arrke = 0
                printFont = New Font("Times New Roman", 10)
            End With

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf TerimaBarang.SJ
            PrintDoc.Print()
        End If
    End Sub

    Private Sub DGV2_DoubleClick(sender As Object, e As EventArgs) Handles DGV2.DoubleClick
        Try
            Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus Serial ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

            Dim row As Integer
            row = DGV2.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsSJ
        Dim b As Boolean
            b = a.HapusSerial(noSJ.Text, DGV2.Item("K_BRG1", row).Value.ToString, DGV2.Item("NO_SERIAL", row).Value.ToString)
            If b = True Then
                load2()
            Else
                MsgBox("Gagal Hapus Serial")
            End If

        Catch ex As Exception

        End Try
    End Sub

   
End Class