﻿Public Class SO_Laporan

    Private Sub SO_Laporan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SO_Laporan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBlgn.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        DGVheader.AutoGenerateColumns = False 'before databinding

        Dim a As New clsCCTV.clsSOJual
        Dim tb As DataSet

        Dim tmpsup As String = "%"
        If CBlgn.SelectedValue <> "0" Then
            tmpsup = CBlgn.SelectedValue
        End If

        Dim status As String
        If CBstts.SelectedItem = "ALL" Then
            status = "%"
        Else
            status = CBstts.SelectedItem
        End If

        tb = a.table(date1.Text, date2.Text, tmpsup, status)

        Dim i As Integer
        For i = 0 To tb.Tables("DATSOJUAL").Rows.Count - 1
            Console.WriteLine("dat" & tb.Tables("DATSOJUAL").Rows(i)(0).ToString)
            Console.WriteLine("datestamp" & tb.Tables("DATSOJUAL").Rows(i)("DATESTAMP").ToString)
        Next

        For i = 0 To tb.Tables("DETSOJUAL").Rows.Count - 1
            Console.WriteLine("det" & tb.Tables("DETSOJUAL").Rows(i)(0).ToString)
        Next

        Dim parentColumn As DataColumn = tb.Tables("DATSOJUAL").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETSOJUAL").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETSOJUAL", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATSOJUAL"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETSOJUAL"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVheader.Focus()
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick

        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SO_EDITHAPUS")
        If b = True Then

            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString

            If DGVheader.Item("STATUS", row).Value.ToString = "FK" Then
                MsgBox("Sudah Pernah Kirim Faktur, Laporan Tidak Bisa Di Edit.")
                Exit Sub
            Else
                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As SOEditHapus
                frm = New SOEditHapus
                frm.MdiParent = frmMenu
                frm.Text = "Edit Hapus "
                frm.MdiParent = frmMenu
                frm.NoFak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
                frm.Tanggal.Text = FormatDateTime(DGVheader.Item("TANGGAL", row).Value.ToString, DateFormat.ShortDate)
                frm.langganan.Text = DGVheader.Item("N_LGN", row).Value.ToString
                frm.sales.Text = DGVheader.Item("N_SLS", row).Value.ToString
                frm.TanggalJT.Text = FormatDateTime(DGVheader.Item("TGL_JT", row).Value.ToString, DateFormat.ShortDate)
                frm.Total2.Text = DGVheader.Item("TOTAL1", row).Value.ToString
                frm.Disc.Text = DGVheader.Item("DISC", row).Value.ToString
                frm.Grandtot.Text = DGVheader.Item("GRANDTOT", row).Value.ToString
                frm.Keterangan.Text = DGVheader.Item("KET", row).Value.ToString
                frm.CBbayar.Text = DGVheader.Item("PEMBAYARAN", row).Value.ToString
                frm.CBbiayaexp.Text = DGVheader.Item("B_EKSPEDISI", row).Value.ToString
                frm.CBexpedisi.Text = DGVheader.Item("EKSPEDISI", row).Value.ToString
                frm.ket2.Text = DGVheader.Item("KET2", row).Value.ToString
                frm.stts = DGVheader.Item("STATUS", row).Value.ToString

                frm.Show()
                frm.Dock = DockStyle.Fill
                Me.Close()
            End If
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        Dim row As Integer
        row = DGVheader.CurrentRow.Index.ToString

        If e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("SJ_INPUT")
            If b = True Then
                If DGVheader.Item("STATUS", row).Value.ToString = "FK" Then
                    MsgBox("Sudah pernah input Faktur")
                    Exit Sub
                End If
                TerimaBarang.noSO.Text = DGVheader.Item("NO_FAK", row).Value.ToString
                TerimaBarang.nlgn.Text = DGVheader.Item("N_LGN", row).Value.ToString
                TerimaBarang.klgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
                TerimaBarang.alamat.Text = DGVheader.Item("ALAMAT", row).Value.ToString
                TerimaBarang.telp.Text = DGVheader.Item("TELP", row).Value.ToString
                TerimaBarang.Show()
              
            Else
                MsgBox("anda tdk di otorisasi")
            End If

        ElseIf e.KeyCode = Keys.F2 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("FAKTUR_INPUT")
            If b = True Then
                If DGVheader.Item("STATUS", row).Value.ToString = "SO" Then
                    MsgBox("Belum pernah input Surat Jalan")
                    Exit Sub
                ElseIf DGVheader.Item("STATUS", row).Value.ToString = "FK" Then
                    MsgBox("Sudah pernah input Faktur")
                    Exit Sub
                End If

                frmMenu.Text = "Input Faktur SO"
                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As SO_Faktur
                frm = New SO_Faktur
                frm.MdiParent = frmMenu

                frm.MdiParent = frmMenu
                frm.TanggalJT.Text = DGVheader.Item("TGL_JT", row).Value.ToString
                frm.noSO.Text = DGVheader.Item("NO_FAK", row).Value.ToString
                frm.langganan.Text = DGVheader.Item("N_LGN", row).Value.ToString
                frm.kLgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
                frm.sales.Text = DGVheader.Item("N_SLS", row).Value.ToString
                frm.kSls.Text = DGVheader.Item("K_SLS", row).Value.ToString
                If DGVheader.Item("PPN", row).Value.ToString <> 0 Then
                    frm.cbPPN.Checked = True
                Else
                    frm.cbPPN.Checked = False
                End If
                frm.CBbayar.Text = DGVheader.Item("PEMBAYARAN", row).Value.ToString
                frm.CBbiayaexp.Text = DGVheader.Item("B_EKSPEDISI", row).Value.ToString
                frm.CBexpedisi.Text = DGVheader.Item("EKSPEDISI", row).Value.ToString
                frm.ket2.Text = DGVheader.Item("KET2", row).Value.ToString
                frm.Total2.Text = DGVheader.Item("TOTAL1", row).Value.ToString
                frm.Disc.Text = DGVheader.Item("DISC", row).Value.ToString
                frm.Grandtot.Text = DGVheader.Item("GRANDTOT", row).Value.ToString
                Me.Close()
                frm.Show()
                frm.Dock = DockStyle.Fill
            Else
                MsgBox("anda tdk di otorisasi")
            End If
        ElseIf e.KeyCode = Keys.F3 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("INPUTDP")
            If b = True Then
                DP.NoSO.Text = DGVheader.Item("NO_FAK", row).Value.ToString
                DP.kLgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
                DP.langganan.Text = DGVheader.Item("N_LGN", row).Value.ToString
                DP.total.Text = FormatNumber(DGVheader.Item("GGTOTAL", row).Value, 2)
                DP.Show()
            Else
                MsgBox("anda tdk di otorisasi")
            End If
            End If

    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            CBstts.Focus()
        End If
    End Sub

    Private Sub CBstts_Keydown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBstts.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

    Private Sub DGVheader_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVheader.CellContentClick

    End Sub
End Class