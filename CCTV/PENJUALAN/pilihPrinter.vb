﻿Imports System.Drawing.Printing

Public Class pilihPrinter

    Private Sub pilihPrinter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim pkInstalledPrinters As String
        Dim defaultprinter As String
        Dim set1 As PrinterSettings = New PrinterSettings

        ' Find all printers installed
        For Each pkInstalledPrinters In _
            PrinterSettings.InstalledPrinters
            '  cboInstalledPrinters.Items.Add(pkInstalledPrinters)
            Console.WriteLine(pkInstalledPrinters)
            ComboBox1.Items.Add(pkInstalledPrinters)
            set1.PrinterName = pkInstalledPrinters

            If set1.IsDefaultPrinter Then
                defaultprinter = pkInstalledPrinters
            End If

        Next pkInstalledPrinters

        ComboBox1.Text = defaultprinter

        ' Set the combo to the first printer in the list
        'cboInstalledPrinters.SelectedIndex = 0
    End Sub

    Private Sub butPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrint.Click
        Dim PrintDoc As New PrintDocument
        PrintDoc.PrinterSettings.PrinterName = ComboBox1.Text
        AddHandler PrintDoc.PrintPage, AddressOf TerimaBarang.alkirim
        PrintDoc.Print()
        Exit Sub
    End Sub
End Class