﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SuratJalanDetail

    Private Sub SuratJalan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Dim tbb As New DataTable

    Private Sub SuratJalan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load()
    End Sub

    Sub load()
        s = noFak.Text
        s = s.Substring(0, 2)

        tbb.Columns.Add("NO")
        tbb.Columns.Add("K_BRG")
        tbb.Columns.Add("SERIAL")

        If s = "SJ" Then
            Dim a As New clsCCTV.clsSJ
            Dim b As DataSet
            b = a.table(noFak.Text)
            DGVdetail.DataSource = b.Tables("DETSJ")
            ' DGV2.DataSource = b.Tables("DETSJ2")
        ElseIf s = "JR" Then
            Dim a As New clsCCTV.clsJual
            Dim b As DataSet
            b = a.table(noFak.Text)
            DGVdetail.DataSource = b.Tables("DETJUAL")
            DGV2.DataSource = b.Tables("DETJUAL2")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Batal Input Surat Jalan ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Me.Close()
    End Sub

    Private Sub noSO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String = ""
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1
    Public s As String

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        'yg QTY = 0 tidak ikut masuk ke database
        'kalau ngisi qty dengan huruf ada protect

        Dim t1 As Integer = 0
        For i = 0 To DGV2.RowCount - 1
            t1 = t1 + 1
        Next

        Dim t2 As Integer = 0
        For i = 0 To DGVdetail.RowCount - 1
            t2 = t2 + DGVdetail.Item("QTYSERIAL", i).Value
        Next

        If t1 <> t2 Then
            MsgBox("Cek Banyak Serial Barang yg di Input")
            Exit Sub
        End If

        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan Detail Serial Number ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("SERIAL")
        table1.Columns.Add("NO_URUT")

        For i = 0 To DGV2.RowCount - 1
            table1.Rows.Add(noFak.Text, DGV2.Item("K_BRG1", i).Value.ToString, DGV2.Item("NO_SERIAL", i).Value.ToString, DGV2.Item("NO", i).Value.ToString)
        Next

        Dim a As New clsCCTV.clsSJ
        Dim b As Boolean
        b = a.inputSerial(table1)
        tmpnofak = noFak.Text
        printSJ1()
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub DGVdetail_DoubleClick(sender As Object, e As EventArgs) Handles DGVdetail.DoubleClick
        Dim row As Integer
        row = DGVdetail.CurrentRow.Index.ToString
        If DGVdetail.Item("QTYSERIAL", row).Value = 0 Then
            MsgBox("Bukan Barang Serial")
            Exit Sub
        End If
        kbrg2.Text = DGVdetail.Item("K_BRG", row).Value.ToString
        noserial.Focus()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles butINP.Click
        Dim i As Integer
        If noserial.Text = "" Then
            Exit Sub
        End If

        Dim jum As Integer = 0
        For i = 0 To DGV2.RowCount - 1
            If DGV2.Item("K_BRG1", i).Value.ToString = kbrg2.Text Then
                jum = jum + 1
            End If
        Next
        For i = 0 To DGVdetail.RowCount - 1
            If DGVdetail.Item("K_BRG", i).Value.ToString = kbrg2.Text Then
                If jum = DGVdetail.Item("QTYSERIAL", i).Value Then
                    MsgBox("Qty Sudah Cukup")
                    kbrg2.Clear()
                    noserial.Clear()
                    Exit Sub
                End If
            End If
        Next
        jum = jum + 1
        tbb.Rows.Add(jum, kbrg2.Text, noserial.Text)
        DGV2.DataSource = tbb
        noserial.Clear()
        noserial.Focus()
        frmMenu.MenuStrip1.Enabled = False
    End Sub

    Sub printSJ1()

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Print Detail Serial Number ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        s = s.Substring(0, 2)

        If s = "SJ" Then
            Dim a As New clsCCTV.clsSJ
            tb = a.table(tmpnofak)
        ElseIf s = "JR" Then
            Dim a As New clsCCTV.clsJual
            tb = a.table(tmpnofak)
        End If

        If s = "SJ" Then
            If tb.Tables("DATSJ").Rows.Count = 1 Then
                Console.WriteLine("BARIS ADA")
            Else
                Console.Write("BARIS TIDAK ADA")
                Exit Sub
            End If
        Else
            If tb.Tables("DATJUAL").Rows.Count = 1 Then
                Console.WriteLine("BARIS ADA")
            Else
                Console.Write("BARIS TIDAK ADA")
                Exit Sub
            End If
        End If

        Dim PrintDoc As New PrintDocument
        AddHandler PrintDoc.PrintPage, AddressOf Me.printSJ
        PrintDoc.Print()
    End Sub

    Public Sub printSJ(ByVal sender As Object, ByVal ev As PrintPageEventArgs)

        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim count As Integer = 0
        Dim leftMargin As Single = ev.MarginBounds.Left
        Dim topMargin As Single = ev.MarginBounds.Top
        Dim line As String = Nothing

        ev.Graphics.DrawString("DETAIL NOMOR SERIAL", New Font(FontFamily.GenericSerif, 14, FontStyle.Regular), Brushes.Black, 310, 10)
        ev.Graphics.DrawLine(Pens.Black, 300, 30, 540, 30)
        Dim bb As New Font("Time New Romans", 8, FontStyle.Regular)
        Dim aa As New Font("Time New Romans", 9, FontStyle.Regular)
        ev.Graphics.DrawString("No Faktur : ", aa, Brushes.Black, 40, 40)
        ev.Graphics.DrawString(tmpnofak, aa, Brushes.Black, 110, 40)
        If s = "SJ" Then
            ev.Graphics.DrawString(" Tgl : " & tb.Tables("DATSJ").Rows(0)("TGL_FAK"), aa, Brushes.Black, 190, 40)
        Else
            ev.Graphics.DrawString(" Tgl : " & tb.Tables("DATJUAL").Rows(0)("TGL_FAK"), aa, Brushes.Black, 190, 40)
        End If

        ev.Graphics.DrawString("Nama Langganan : ", aa, Brushes.Black, 400, 40)
        If s = "SJ" Then
            ev.Graphics.DrawString(tb.Tables("DATSJ").Rows(0)("N_LGN").ToString, aa, Brushes.Black, 520, 40)
        Else
            ev.Graphics.DrawString(tb.Tables("DATJUAL").Rows(0)("N_LGN").ToString, aa, Brushes.Black, 520, 40)
        End If

        ev.Graphics.DrawLine(Pens.Black, 30, 60, 390, 60) 'tidur
        ev.Graphics.DrawLine(Pens.Black, 30, 80, 390, 80) 'tidur
        ev.Graphics.DrawLine(Pens.Black, 30, 60, 30, 470) 'berdiri 
        ev.Graphics.DrawString("No", aa, Brushes.Black, 30, 63)
        ev.Graphics.DrawLine(Pens.Black, 50, 60, 50, 470) 'berdiri
        ev.Graphics.DrawString("Kode Barang", aa, Brushes.Black, 55, 63)
        ev.Graphics.DrawLine(Pens.Black, 210, 60, 210, 470) 'berdiri
        ev.Graphics.DrawString("No Serial", aa, Brushes.Black, 220, 63)
        ev.Graphics.DrawLine(Pens.Black, 390, 60, 390, 470) 'berdiri
        ev.Graphics.DrawLine(Pens.Black, 30, 470, 390, 470) 'tidur

        ev.Graphics.DrawLine(Pens.Black, 420, 60, 770, 60) 'tidur
        ev.Graphics.DrawLine(Pens.Black, 420, 80, 770, 80) 'tidur
        ev.Graphics.DrawLine(Pens.Black, 420, 60, 420, 470) 'berdiri
        ev.Graphics.DrawString("No", aa, Brushes.Black, 420, 63)
        ev.Graphics.DrawLine(Pens.Black, 445, 60, 445, 470) 'berdiri
        ev.Graphics.DrawString("Kode Barang", aa, Brushes.Black, 450, 63)
        ev.Graphics.DrawLine(Pens.Black, 600, 60, 600, 470) 'berdiri
        ev.Graphics.DrawString("No Serial", aa, Brushes.Black, 610, 63)
        ev.Graphics.DrawLine(Pens.Black, 770, 60, 770, 470) 'berdiri
        ev.Graphics.DrawLine(Pens.Black, 420, 470, 770, 470) 'tidur

        Dim cc As New Font("Time New Romans", 8, FontStyle.Regular)
        Dim i As Integer

        If s = "SJ" Then
            For i = 0 To tb.Tables("DETSJ2").Rows.Count - 1
                'line = "ab"
                'For i = 0 To 50
                If i >= 25 Then
                    ev.Graphics.DrawString(i + 1, cc, Brushes.Black, 440, 90 + ((i Mod 25) * 15), New StringFormat(LeftRightAlignment.Right))
                    ev.Graphics.DrawString(tb.Tables("DETSJ2").Rows(i)("K_BRG").ToString, cc, Brushes.Black, 450, 90 + ((i Mod 25) * 15))
                    ev.Graphics.DrawString(tb.Tables("DETSJ2").Rows(i)("SERIAL").ToString, cc, Brushes.Black, 610, 90 + ((i Mod 25) * 15))
                    ' ev.Graphics.DrawString(i + 1, cc, Brushes.Black, 440, 90 + ((i Mod 25) * 15), New StringFormat(LeftRightAlignment.Right))
                    ' ev.Graphics.DrawString("DS-2CE1582P(N)-IR1/IR3", cc, Brushes.Black, 450, 90 + ((i Mod 25) * 15))
                    ' ev.Graphics.DrawString("12256-9568-8365656", cc, Brushes.Black, 610, 90 + ((i Mod 25) * 15))

                Else
                    ev.Graphics.DrawString(i + 1, cc, Brushes.Black, 50, 90 + ((i Mod 25) * 15), New StringFormat(LeftRightAlignment.Right))
                    ev.Graphics.DrawString(tb.Tables("DETSJ2").Rows(i)("K_BRG").ToString, cc, Brushes.Black, 55, 90 + ((i Mod 25) * 15))
                    ev.Graphics.DrawString(tb.Tables("DETSJ2").Rows(i)("SERIAL").ToString, cc, Brushes.Black, 220, 90 + ((i Mod 25) * 15))
                    'ev.Graphics.DrawString(i + 1, cc, Brushes.Black, 50, 90 + ((i Mod 25) * 15), New StringFormat(LeftRightAlignment.Right))
                    'ev.Graphics.DrawString("DS-2CE1582P(N)-IR1/IR3", cc, Brushes.Black, 55, 90 + ((i Mod 25) * 15))
                    'ev.Graphics.DrawString("12256-9568-836563456", cc, Brushes.Black, 220, 90 + ((i Mod 25) * 15))
                End If

            Next
        Else
            For i = 0 To tb.Tables("DETJUAL2").Rows.Count - 1
                'line = "ab"
                If i >= 20 Then
                    ev.Graphics.DrawString(i + 1, cc, Brushes.Black, 440, 90 + ((i Mod 25) * 15), New StringFormat(LeftRightAlignment.Right))
                    ev.Graphics.DrawString(tb.Tables("DETJUAL2").Rows(i)("K_BRG").ToString, cc, Brushes.Black, 450, 90 + ((i Mod 25) * 15))
                    ev.Graphics.DrawString(tb.Tables("DETJUAL2").Rows(i)("SERIAL").ToString, cc, Brushes.Black, 610, 90 + ((i Mod 25) * 15))
                Else
                    ev.Graphics.DrawString(i + 1, cc, Brushes.Black, 50, 90 + ((i Mod 25) * 15), New StringFormat(LeftRightAlignment.Right))
                    ev.Graphics.DrawString(tb.Tables("DETJUAL2").Rows(i)("K_BRG").ToString, cc, Brushes.Black, 55, 90 + ((i Mod 25) * 15))
                    ev.Graphics.DrawString(tb.Tables("DETJUAL2").Rows(i)("SERIAL").ToString, cc, Brushes.Black, 220, 90 + ((i Mod 25) * 15))

                End If
            Next
        End If

    End Sub

    Private Sub noserial_Leave(sender As Object, e As EventArgs) Handles noserial.Leave
        butINP.PerformClick()
    End Sub

    Private Sub DGV2_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV2.CellDoubleClick

        Dim a As New clsCCTV.clsPengguna
        Dim b1 As Boolean
        b1 = a.cek("DetailSerialEdit")
        If b1 = False Then
            MsgBox("anda tdk di otorisasi")
            Exit Sub
        End If

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim row As Integer
        row = DGV2.CurrentRow.Index.ToString

        Dim baris As Integer
        baris = DGV2.CurrentRow.Index.ToString
        tbb.Rows(baris).Delete()
        For i = 0 To tbb.Rows.Count - 1
            tbb.Rows(i).Item("NO") = i + 1
        Next

        'Dim b As New clsCCTV.clsSJ
        'Dim p As Boolean
        'p = b.HapusSerial(noFak.Text, DGV2.Item("K_BRG1", row).Value, DGV2.Item("NO_SERIAL", row).Value)

        'If p = True Then
        'Else
        '    MsgBox("Gagal Hapus Detail Serial Number")

        'End If
        'load()

    End Sub

End Class