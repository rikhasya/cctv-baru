﻿Imports System.Drawing.Printing
Imports System.IO

Public Class PenjualanEditHapus

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1
    Public klgn As String

    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        e.Cancel = False
    End Sub

    Private Sub kode_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles kode.Leave
        If kode.Text = "" Then
            butINPUT.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                kode.Text = b.Rows(0)("K_BRG").ToString
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
                Harga.Text = b.Rows(0)("H_JUAL").ToString
                Qty.Focus()
            Else
                Exit Sub
            End If
        End If
    End Sub

#Region "errorprov"

    Private Sub Disc_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc.Leave
        hitung()
    End Sub

    Private Sub Disc_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc.Validated
        ErrorProvider1.SetError(Disc, "")
    End Sub

    Private Sub Disc_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Disc.Validating
        If Not IsNumeric(Disc.Text) Or Disc.Text = "" Then
            e.Cancel = True
            Disc.Select(0, Disc.Text.Length)
            ErrorProvider1.SetError(Disc, "harap di isi")
        End If

    End Sub

    Private Sub Qty_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Qty.Validated
        ErrorProvider1.SetError(Qty, "")
    End Sub

    Private Sub Qty_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Qty.Validating
        If Not IsNumeric(Qty.Text) Or Qty.Text = "" Then
            e.Cancel = True
            Qty.Select(0, Qty.Text.Length)
            ErrorProvider1.SetError(Qty, "harap di isi, harus angka")
        End If
    End Sub

    Private Sub Harga_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Harga.Validated
        ErrorProvider1.SetError(Harga, "")
    End Sub

    Private Sub Harga_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Harga.Validating
        If Not IsNumeric(Harga.Text) Or Harga.Text = "" Then
            e.Cancel = True
            Harga.Select(0, Harga.Text.Length)
            ErrorProvider1.SetError(Harga, "harap di isi")
        End If
    End Sub

    Private Sub Pot1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot1.Validated
        ErrorProvider1.SetError(Pot1, "")
    End Sub

    Private Sub Pot1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot1.Validating
        If Not IsNumeric(Pot1.Text) Or Pot1.Text = "" Then
            e.Cancel = True
            Pot1.Select(0, Pot1.Text.Length)
            ErrorProvider1.SetError(Pot1, "harap di isi")
        End If
    End Sub

    Private Sub Pot2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot2.Validated
        ErrorProvider1.SetError(Pot2, "")
    End Sub

    Private Sub Pot2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot2.Validating
        If Not IsNumeric(Pot2.Text) Or Pot2.Text = "" Then
            e.Cancel = True
            Pot2.Select(0, Pot2.Text.Length)
            ErrorProvider1.SetError(Pot2, "harap di isi")
        End If
    End Sub

#End Region

    Private Sub PenjualanEditHapus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.no.Text = noSO.Text
            Cari_Brg.dari.Text = "EJUAL"
            Cari_Brg.DGVretur.Focus()
        End If
    End Sub

    Private Sub PenjualanEditHapus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub PenjualanEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loaddata()
        hitung()
        Disc.Text = FormatNumber(Disc.Text, 2)
        If Not (noSO.Text = "") Then
            butINPUT.Enabled = False
        End If
    End Sub

    Sub loaddata()
        Dim a As New clsCCTV.clsJual
        Dim b As New DataSet
        b = a.table(noFak.Text)
        DGVso.DataSource = b.Tables("DETJUAL")
        kode.Focus()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVso.RowCount - 1
            tot = tot + DGVso.Item("TOTAL_BRG", i).Value
        Next

        Total2.Text = FormatNumber(tot, 2)
        Disc.Text = FormatNumber(Disc.Text, 2)
        Grandtot.Text = FormatNumber(CDbl(Total2.Text) - CDbl(Disc.Text), 2)
        If cbPPN.Checked = True Then
            PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
        Else
            PPN.Text = "0.00"
        End If
        Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
    End Sub

    Private Sub Harga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Harga.Leave, Qty.Leave, Pot1.Leave, Pot2.Leave

        If IsNumeric(Qty.Text) And IsNumeric(Harga.Text) And IsNumeric(Pot1.Text) And IsNumeric(Pot2.Text) Then

            Jumlah.Text = CDbl(Harga.Text) * CDbl(Qty.Text)
            Total.Text = CDbl(Jumlah.Text) * (100 - CDbl(Pot1.Text)) / 100 * (100 - CDbl(Pot2.Text)) / 100
            Harga.Text = FormatNumber(Harga.Text, 2)
            Jumlah.Text = FormatNumber(Jumlah.Text, 2)
            Total.Text = FormatNumber(Total.Text, 2)
            Total2.Text = FormatNumber(Total.Text, 2)
        End If

    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If kode.Text = "" Then
            butSELESAI.Focus()
            Exit Sub
        End If

        Panel1.Enabled = False
        frmMenu.MenuStrip1.Enabled = False

        Dim a As New clsCCTV.clsJual

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("HARGA")
        table1.Columns.Add("JUMLAH")
        table1.Columns.Add("POT1")
        table1.Columns.Add("POT2")
        table1.Columns.Add("TOTAL")

        table1.Rows.Add(noFak.Text, kode.Text, Nama.Text, Qty.Text, Sat.Text, CDbl(Harga.Text), CDbl(Jumlah.Text), CDbl(Pot1.Text), CDbl(Pot2.Text), CDbl(Total.Text))

        Dim b As Boolean
        b = a.input1brg(noFak.Text, table1)

        If b = True Then
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Input !")
        End If

        kode.Clear()
        Nama.Clear()
        Qty.Clear()
        Harga.Clear()
        Sat.Clear()
        Jumlah.Clear()
        Pot1.Clear()
        Pot2.Clear()
        Total.Clear()
        kode.Focus()
        Qty.Enabled = True
    End Sub


    Private Sub DGVso_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVso.DoubleClick
        If Not (noSO.Text = "") Then
            Exit Sub
        End If

        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVso.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsJual
        Dim b As Boolean
        b = a.hapus1brg(noFak.Text, DGVso.Item("K_BRG", baris).Value.ToString)

        If b = True Then
            
            kode.Text = DGVso.Item("K_BRG", baris).Value.ToString
            Nama.Text = DGVso.Item("N_BRG", baris).Value.ToString
            Qty.Text = DGVso.Item("QTY_BRG", baris).Value.ToString
            Harga.Text = DGVso.Item("HARGA_BRG", baris).Value.ToString
            Sat.Text = DGVso.Item("SAT_BRG", baris).Value.ToString
            Jumlah.Text = DGVso.Item("JUMLAH_BRG", baris).Value.ToString
            Pot1.Text = DGVso.Item("POT1_BRG", baris).Value.ToString
            Pot2.Text = DGVso.Item("POT2_BRG", baris).Value.ToString
            Total.Text = DGVso.Item("TOTAL_BRG", baris).Value.ToString
            loaddata()
            hitung()
        Else
            MsgBox("Gagal Hapus Barang")
        End If
    End Sub

    Private Sub butBATAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBATAL.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus " & noFak.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsJual
        Dim b As Boolean
        b = a.hapus(noFak.Text)

        'Dim x As New clsCCTV.clsSJ
        'Dim o As Boolean
        'o = x.hapus(
        If b = True Then
            MsgBox("Berhasil Hapus Faktur No. " & noFak.Text)
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Mengahus Faktur No. " & noFak.Text)
        End If
    End Sub

    Private Sub butSELESAI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSELESAI.Click
        If Total2.Text = 0 Then
            Exit Sub
        End If

        Dim aa As New clsCCTV.clsLangganan
        Dim b1 As Double = 0
        b1 = aa.piut(klgn)

        Dim piutt As Double = 0
        Dim b2 As DataTable
        b2 = aa.table(klgn, "%")
        If b2.Rows.Count = 1 Then
            piutt = b2.Rows(0)("LIMIT")
        End If

        If b1 + Grandtot2.Text > piutt Then
            MsgBox("Total Faktur Melebihi Limit")
            Exit Sub
        End If

        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan perubahan ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        If noSO.Text = "" Then
            Dim a As New clsCCTV.clsJual
            Dim b As Boolean
            b = a.setvalid(noFak.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text, CBbayar.Text, CBexpedisi.Text, CBbiayaexp.Text, ket2.Text)

            If b = True Then
                MsgBox("Berhasil Menyimpan Perubahan")
                PRINT()
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
            Else
                MsgBox("Gagal Menyimpan Perubahan")
            End If
        Else
            Me.Close()
        End If
    End Sub

    Sub PRINT()

        Dim ask As DialogResult

        ask = MessageBox.Show("Print Faktur Penjualan?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsJual
            Penjualan.tb = otc.table(noFak.Text)

            With Penjualan
                .tmpnofak = .tb.Tables("DATJUAL").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Penjualan.tb.Tables("DETJUAL").Rows.Count - 1

            ReDim Penjualan.tmpKode(jumlah)
            ReDim Penjualan.tmpNama(jumlah)
            ReDim Penjualan.tmpQty(jumlah)
            ReDim Penjualan.tmpSat(jumlah)
            ReDim Penjualan.tmpHrg(jumlah)
            ReDim Penjualan.tmpJmlh(jumlah)
            ReDim Penjualan.tmpPot1(jumlah)
            ReDim Penjualan.tmpPot2(jumlah)
            ReDim Penjualan.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Penjualan
                    .tmpKode(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("TOTAL").ToString

                End With
            Next
            With Penjualan
                arrke = 0
                printFont = New Font("Times New Roman", 10)
            End With

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Penjualan.faktur
            PrintDoc.Print()
        End If
        Me.Close()
    End Sub

    Private Sub kode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles kode.KeyDown
        If e.KeyCode = Keys.Enter Then
            If kode.Text = "" Then
                butINPUT.Focus()
            Else
                Dim a As New clsCCTV.clsBarang
                Dim b As DataTable
                b = a.table(kode.Text)
                If b.Rows.Count = 1 Then
                    Nama.Text = b.Rows(0)("N_BRG").ToString
                    Sat.Text = b.Rows(0)("SAT").ToString
                    Qty.Focus()
                Else
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub Disc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Disc.TextChanged

    End Sub

    Private Sub cbPPN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPN.CheckedChanged
        hitung()
    End Sub

    Private Sub DGVso_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVso.CellContentClick
        Dim baris As Integer
        baris = DGVso.CurrentRow.Index.ToString

        kode.Text = DGVso.Item("K_BRG", baris).Value.ToString
        Nama.Text = DGVso.Item("N_BRG", baris).Value.ToString
        Qty.Text = DGVso.Item("QTY_BRG", baris).Value.ToString
        If DGVso.Item("QTYSERIAL", baris).Value <> 0 Then
            Qty.Enabled = False
        Else
            Qty.Enabled = True
        End If
        Harga.Text = DGVso.Item("HARGA_BRG", baris).Value.ToString
        Sat.Text = DGVso.Item("SAT_BRG", baris).Value.ToString
        Jumlah.Text = DGVso.Item("JUMLAH_BRG", baris).Value.ToString
        Pot1.Text = DGVso.Item("POT1_BRG", baris).Value.ToString
        Pot2.Text = DGVso.Item("POT2_BRG", baris).Value.ToString
        Total.Text = DGVso.Item("TOTAL_BRG", baris).Value.ToString
    End Sub


    Private Sub DGVso_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVso.CellClick
        Dim baris As Integer
        baris = DGVso.CurrentRow.Index.ToString

        kode.Text = DGVso.Item("K_BRG", baris).Value.ToString
        Nama.Text = DGVso.Item("N_BRG", baris).Value.ToString
        Qty.Text = DGVso.Item("QTY_BRG", baris).Value.ToString
        If DGVso.Item("QTYSERIAL", baris).Value <> 0 Then
            Qty.Enabled = False
        Else
            Qty.Enabled = True
        End If
        Harga.Text = DGVso.Item("HARGA_BRG", baris).Value.ToString
        Sat.Text = DGVso.Item("SAT_BRG", baris).Value.ToString
        Jumlah.Text = DGVso.Item("JUMLAH_BRG", baris).Value.ToString
        Pot1.Text = DGVso.Item("POT1_BRG", baris).Value.ToString
        Pot2.Text = DGVso.Item("POT2_BRG", baris).Value.ToString
        Total.Text = DGVso.Item("TOTAL_BRG", baris).Value.ToString
    End Sub
End Class