﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SuratJalanLap

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub SuratJalanLap_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyData = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("PRINTULANGLAPORAN")
            If b = True Then
                PRINT()
            Else
                MsgBox("anda tdk di otorisasi")
            End If

        ElseIf e.KeyCode = Keys.F3 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("RETURSJ_INPUT")
            If b = True Then
                If Me.MdiChildren.Length > 0 Then
                    Dim childForm As Form = CType(ActiveMdiChild, Form)
                    childForm.Close()
                End If
                Dim frm As SuratJalanRetur
                frm = New SuratJalanRetur
                frm.MdiParent = frmMenu
                frm.Text = "Retur Surat Jalan"
                Dim row As Integer
                row = DGVheader.CurrentRow.Index.ToString
                frm.MdiParent = frmMenu
                frm.noSJ.Text = DGVheader.Item("NO_FAK", row).Value.ToString
                frm.noSO.Text = DGVheader.Item("NO_SO", row).Value.ToString
                frm.lgn.Text = DGVheader.Item("N_LGN", row).Value.ToString
                frm.k_lgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
                frm.Show()
                frm.Dock = DockStyle.Fill
                Me.Close()
            Else
                MsgBox("anda tdk di otorisasi")
            End If

        ElseIf e.KeyCode = Keys.F4 Then

            Dim a As New clsCCTV.clsPengguna
            Dim b1 As Boolean
            b1 = a.cek("DetailSerialInput")
            If b1 = False Then
                MsgBox("anda tdk di otorisasi")
                Exit Sub
            End If

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SuratJalanDetail
            frm = New SuratJalanDetail
            frm.MdiParent = frmMenu
            frm.Text = "Input Detail Serial Number"
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.noFak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.nlgn.Text = DGVheader.Item("N_LGN", row).Value.ToString
            frm.klgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()

        ElseIf e.KeyCode = Keys.F5 Then
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            SuratJalanDetail.tmpnofak = DGVheader.Item("NO_FAK", row).Value.ToString
            SuratJalanDetail.s = DGVheader.Item("NO_FAK", row).Value.ToString
            SuratJalanDetail.printSJ1()

        End If
    End Sub

    Sub PRINT()
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult
        Dim PrintDoc As New PrintDocument

        Dim otc As New clsCCTV.clsSJ
        TerimaBarang.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

        With TerimaBarang
            .tmpnofak = .tb.Tables("DATSJ").Rows(0)("NO_FAK").ToString
        End With

        'PDF
        ask = MessageBox.Show("Print Ulang Surat Jalan?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then

            Dim jumlah As Integer = TerimaBarang.tb.Tables("DETSJ").Rows.Count - 1

            ReDim TerimaBarang.tmpKode(jumlah)
            ReDim TerimaBarang.tmpNama(jumlah)
            ReDim TerimaBarang.tmpQty(jumlah)
            ReDim TerimaBarang.tmpSat(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With TerimaBarang
                    .tmpKode(ipo) = .tb.Tables("DETSJ").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETSJ").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETSJ").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETSJ").Rows(ipo)("SAT").ToString
                End With
            Next
            TerimaBarang.arrke = 0

            AddHandler PrintDoc.PrintPage, AddressOf TerimaBarang.SJ
            PrintDoc.Print()
        End If

        ask = MessageBox.Show("Print Alamat Kirim ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        pilihPrinter.Show()
        Exit Sub
    End Sub

    Private Sub SuratJalanLap_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SuratJalanLap_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBlgn.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim N As New clsCCTV.clsSJ
        Dim tb As DataSet

        Dim tmpsup As String = "%"
        If CBlgn.SelectedValue <> "0" Then
            tmpsup = CBlgn.SelectedValue
        End If
        tb = N.table(date1.Text, date2.Text, tmpsup)

        Dim i As Integer
        For i = 0 To tb.Tables("DETSJ2").Rows.Count - 1
        Next
        Console.WriteLine("i = " & i)

        Dim parentColumn As DataColumn = tb.Tables("DATSJ").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETSJ").Columns("NO_FAK")
        Dim childColumn2 As DataColumn = tb.Tables("DETSJ2").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETSJ", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim relDATBELIDETBELI2 As DataRelation
        relDATBELIDETBELI2 = New DataRelation("DETSJ2", parentColumn, childColumn2, False)
        tb.Relations.Add(relDATBELIDETBELI2)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource
        Dim detailbindingSource2 As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATSJ"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETSJ"

        detailbindingSource2.DataSource = masterbindingSource
        detailbindingSource2.DataMember = "DETSJ2"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVserial.DataSource = detailbindingSource2
        DGVheader.Focus()

    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("SJ_EDITHAPUS")
        If b = True Then
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SuratJalanEditHapus
            frm = New SuratJalanEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Surat Jalan"
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.noSJ.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.Tanggal.Text = FormatDateTime(DGVheader.Item("TANGGAL", row).Value.ToString, DateFormat.ShortDate)
            frm.CBlgn.Text = DGVheader.Item("N_LGN", row).Value.ToString
            'frm.CBsls.SelectedItem = DGVheader.Item("N_SLS", row).Value.ToString
            frm.noSO.Text = DGVheader.Item("NO_SO", row).Value.ToString
            frm.Keterangan.Text = DGVheader.Item("KET", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class