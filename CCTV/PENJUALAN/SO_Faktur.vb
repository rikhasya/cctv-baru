﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SO_Faktur

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub SO_Faktur_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        butSELESAI.Focus()

        DGVso.AutoGenerateColumns = False 'before databinding
        Dim a As New clsCCTV.clsSOJual
        Dim b As DataSet
        b = a.table(noSO.Text)
        DGVso.DataSource = b.Tables("DETSOJUAL")

        Dim i As Integer
        For i = 0 To DGVso.RowCount - 1
            DGVso.Item("JUMLAH_BRG", i).Value = CDbl(DGVso.Item("QTY_BRG", i).Value) * CDbl(DGVso.Item("HARGA_BRG", i).Value)
        Next

        hitung()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVso.RowCount - 1
            tot = tot + DGVso.Item("TOTAL_BRG", i).Value
        Next
        Total2.Text = FormatNumber(tot, 2)
        Disc.Text = FormatNumber(Disc.Text, 2)
        Grandtot.Text = FormatNumber(CDbl(Total2.Text) - CDbl(Disc.Text), 2)
        If cbPPN.Checked = True Then
            PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
        Else
            PPN.Text = "0.00"
        End If
        Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
    End Sub

    Private Sub butBATAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBATAL.Click
        Me.Close()
    End Sub

    Dim t As String

    Private Sub butSELESAI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSELESAI.Click
        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan Faktur ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If Windows.Forms.DialogResult.Yes Then
            Dim b As New clsCCTV.clsJual

            t = b.noProject()

            Dim table1 As New DataTable
            table1.Columns.Add("NO_FAK")
            table1.Columns.Add("K_BRG")
            table1.Columns.Add("N_BRG")
            table1.Columns.Add("QTY")
            table1.Columns.Add("SAT")
            table1.Columns.Add("HARGA")
            table1.Columns.Add("JUMLAH")
            table1.Columns.Add("POT1")
            table1.Columns.Add("POT2")
            table1.Columns.Add("TOTAL")

            Dim i As Integer
            Dim jumlah As Double = 0
            jumlah = CDbl(DGVso.Item("HARGA_BRG", i).Value.ToString) * CDbl(DGVso.Item("QTY_BRG", i).Value.ToString)

            For i = 0 To DGVso.Rows.Count - 1
                table1.Rows.Add(t, DGVso.Item("K_BRG", i).Value.ToString, DGVso.Item("N_BRG", i).Value.ToString, DGVso.Item("QTY_BRG", i).Value.ToString, DGVso.Item("SAT_BRG", i).Value.ToString, CDbl(DGVso.Item("HARGA_BRG", i).Value.ToString), CDbl(DGVso.Item("JUMLAH_BRG", i).Value.ToString), CDbl(DGVso.Item("POT1_BRG", i).Value.ToString), CDbl(DGVso.Item("POT2_BRG", i).Value.ToString), CDbl(DGVso.Item("TOTAL_BRG", i).Value.ToString))
            Next

            Dim a As New clsCCTV.clsJual
            Dim p As Boolean
            p = a.input(t, Tanggal.Text, kLgn.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text, TanggalJT.Text, kSls.Text, noSO.Text, CBbayar.Text, CBexpedisi.Text, CBbiayaexp.Text, ket2.Text, table1)

            If p = True Then
                MsgBox("Berhasil Input Faktur Penjualan")

                ask = MessageBox.Show("Print Faktur ?", "Konfirmasi", MessageBoxButtons.YesNo)
                If Windows.Forms.DialogResult.No Then
                    Me.Close()
                    frmMenu.MenuStrip1.Enabled = True
                    Exit Sub
                End If
                PRINT()
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
            Else
                MsgBox("Gagal Input Faktur Penjualan")
            End If
        Else
            Exit Sub
        End If
    End Sub

    Sub PRINT()
        
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Faktur Penjualan?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsJual
            Penjualan.tb = otc.table(t)

            With Penjualan
                .tmpnofak = .tb.Tables("DATJUAL").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Penjualan.tb.Tables("DETJUAL").Rows.Count - 1

            ReDim Penjualan.tmpKode(jumlah)
            ReDim Penjualan.tmpNama(jumlah)
            ReDim Penjualan.tmpQty(jumlah)
            ReDim Penjualan.tmpSat(jumlah)
            ReDim Penjualan.tmpHrg(jumlah)
            ReDim Penjualan.tmpJmlh(jumlah)
            ReDim Penjualan.tmpPot1(jumlah)
            ReDim Penjualan.tmpPot2(jumlah)
            ReDim Penjualan.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Penjualan
                    .tmpKode(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("TOTAL").ToString

                End With
            Next
            With Penjualan
                arrke = 0
                printFont = New Font("Times New Roman", 10)
            End With

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Penjualan.faktur
            PrintDoc.Print()
        End If
    End Sub

End Class