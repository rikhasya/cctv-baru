﻿Public Class DaftarKontak

    Public dari As String

    Private Sub butCARI_Click(sender As Object, e As EventArgs) Handles butCARI.Click

        If dari = "BELI" Or dari = "RBELI" Then

            Dim a As New clsCCTV.clsSupplier
            Dim b As DataTable
            If kode.Text.Length = 5 Then
                b = a.table(kode.Text, "%" & nama.Text & "%")
            Else
                b = a.table("%" & kode.Text, "%" & nama.Text & "%")
            End If

            DGVsupplier.Visible = True
            DGVsupplier.DataSource = b
        Else

            Dim a As New clsCCTV.clsLangganan
            Dim b As DataTable
            If kode.Text.Length = 5 Then
                b = a.table(kode.Text, "%" & nama.Text & "%")
            Else
                b = a.table(kode.Text & "%", "%" & nama.Text & "%")
            End If
            DGVcustomer.Visible = True
            DGVcustomer.DataSource = b
        End If
        
    End Sub

    Private Sub DaftarKontak_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub DGVcustomer_DoubleClick(sender As Object, e As EventArgs) Handles DGVcustomer.DoubleClick
        Dim baris As Integer
        baris = DGVcustomer.CurrentRow.Index.ToString

        If dari = "SOJL" Then
            Dim frm As Penjualan = CType(frmMenu.MdiChildren(0), Form)
            frm.kodelgn.Text = DGVcustomer.Item("K_LGN", baris).Value.ToString
            frm.namalgn.Text = DGVcustomer.Item("N_LGN", baris).Value.ToString
            frm.CBsales.Focus()
            Me.Close()

        ElseIf dari = "RJUAL" Then
            Dim frm As Penjualan_Retur = CType(frmMenu.MdiChildren(0), Form)
            frm.kodelgn.Text = DGVcustomer.Item("K_LGN", baris).Value.ToString
            frm.namalgn.Text = DGVcustomer.Item("N_LGN", baris).Value.ToString
            frm.noJual.Focus()
            Me.Close()

        ElseIf dari = "SO" Then
            Dim frm As SO = CType(frmMenu.MdiChildren(0), Form)
            frm.kodelgn.Text = DGVcustomer.Item("K_LGN", baris).Value.ToString
            frm.namalgn.Text = DGVcustomer.Item("N_LGN", baris).Value.ToString
            frm.CBsales.Focus()
            Me.Close()
        End If
    End Sub

    Private Sub DGVsupplier_DoubleClick(sender As Object, e As EventArgs) Handles DGVsupplier.DoubleClick
        Dim baris As Integer
        baris = DGVsupplier.CurrentRow.Index.ToString

        If dari = "BELI" Then
            Dim frm As Pembelian = CType(frmMenu.MdiChildren(0), Form)
            frm.kodesup.Text = DGVsupplier.Item("K_SUP", baris).Value.ToString
            frm.namasup.Text = DGVsupplier.Item("N_SUP", baris).Value.ToString
            frm.NoSup.Focus()
            Me.Close()

        ElseIf dari = "RBELI" Then
            Dim frm As Pembelian_Retur = CType(frmMenu.MdiChildren(0), Form)
            frm.kodesup.Text = DGVsupplier.Item("K_SUP", baris).Value.ToString
            frm.namasup.Text = DGVsupplier.Item("N_SUP", baris).Value.ToString
            frm.noBeli.Focus()
            Me.Close()
        End If
    End Sub
End Class
