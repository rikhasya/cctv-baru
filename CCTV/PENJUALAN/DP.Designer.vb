﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.NoSO = New System.Windows.Forms.TextBox()
        Me.CBacc = New System.Windows.Forms.ComboBox()
        Me.NoGiro = New System.Windows.Forms.TextBox()
        Me.langganan = New System.Windows.Forms.TextBox()
        Me.kLgn = New System.Windows.Forms.Label()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.keterangan = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.butBAT = New System.Windows.Forms.Button()
        Me.butINP = New System.Windows.Forms.Button()
        Me.bayarDP = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.total = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tanggal"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "No SO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(133, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Account"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(276, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "No Giro"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(157, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Langganan"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(34, 37)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(83, 20)
        Me.date1.TabIndex = 5
        '
        'NoSO
        '
        Me.NoSO.Enabled = False
        Me.NoSO.Location = New System.Drawing.Point(34, 82)
        Me.NoSO.Name = "NoSO"
        Me.NoSO.Size = New System.Drawing.Size(100, 20)
        Me.NoSO.TabIndex = 6
        '
        'CBacc
        '
        Me.CBacc.FormattingEnabled = True
        Me.CBacc.Location = New System.Drawing.Point(136, 37)
        Me.CBacc.Name = "CBacc"
        Me.CBacc.Size = New System.Drawing.Size(121, 21)
        Me.CBacc.TabIndex = 0
        '
        'NoGiro
        '
        Me.NoGiro.Location = New System.Drawing.Point(279, 38)
        Me.NoGiro.Name = "NoGiro"
        Me.NoGiro.Size = New System.Drawing.Size(100, 20)
        Me.NoGiro.TabIndex = 0
        '
        'langganan
        '
        Me.langganan.Enabled = False
        Me.langganan.Location = New System.Drawing.Point(160, 82)
        Me.langganan.Name = "langganan"
        Me.langganan.Size = New System.Drawing.Size(176, 20)
        Me.langganan.TabIndex = 9
        '
        'kLgn
        '
        Me.kLgn.AutoSize = True
        Me.kLgn.Location = New System.Drawing.Point(224, 64)
        Me.kLgn.Name = "kLgn"
        Me.kLgn.Size = New System.Drawing.Size(31, 13)
        Me.kLgn.TabIndex = 10
        Me.kLgn.Text = "kode"
        Me.kLgn.Visible = False
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(402, 38)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(83, 20)
        Me.date2.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(399, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Tanggal JT"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(537, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(277, 82)
        Me.Panel1.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Bodoni MT Black", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(119, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 24)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "DP"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Bodoni MT Black", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(86, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 24)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "TERIMA"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel2.Controls.Add(Me.keterangan)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.butBAT)
        Me.Panel2.Controls.Add(Me.butINP)
        Me.Panel2.Controls.Add(Me.bayarDP)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.total)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Location = New System.Drawing.Point(34, 118)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(780, 88)
        Me.Panel2.TabIndex = 1
        '
        'keterangan
        '
        Me.keterangan.Location = New System.Drawing.Point(252, 25)
        Me.keterangan.Multiline = True
        Me.keterangan.Name = "keterangan"
        Me.keterangan.Size = New System.Drawing.Size(294, 50)
        Me.keterangan.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(249, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Keterangan"
        '
        'butBAT
        '
        Me.butBAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBAT.Location = New System.Drawing.Point(676, 25)
        Me.butBAT.Name = "butBAT"
        Me.butBAT.Size = New System.Drawing.Size(75, 29)
        Me.butBAT.TabIndex = 3
        Me.butBAT.Text = "BATAL"
        Me.butBAT.UseVisualStyleBackColor = True
        '
        'butINP
        '
        Me.butINP.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butINP.Location = New System.Drawing.Point(566, 25)
        Me.butINP.Name = "butINP"
        Me.butINP.Size = New System.Drawing.Size(93, 29)
        Me.butINP.TabIndex = 2
        Me.butINP.Text = "INPUT DP"
        Me.butINP.UseVisualStyleBackColor = True
        '
        'bayarDP
        '
        Me.bayarDP.Location = New System.Drawing.Point(135, 25)
        Me.bayarDP.Name = "bayarDP"
        Me.bayarDP.Size = New System.Drawing.Size(100, 20)
        Me.bayarDP.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(132, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Bayar DP"
        '
        'total
        '
        Me.total.Enabled = False
        Me.total.Location = New System.Drawing.Point(19, 25)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(100, 20)
        Me.total.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(16, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Total Bayar"
        '
        'DP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(842, 217)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.date2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.kLgn)
        Me.Controls.Add(Me.langganan)
        Me.Controls.Add(Me.NoGiro)
        Me.Controls.Add(Me.CBacc)
        Me.Controls.Add(Me.NoSO)
        Me.Controls.Add(Me.date1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.Name = "DP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "INPUT-DP"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents NoSO As System.Windows.Forms.TextBox
    Friend WithEvents CBacc As System.Windows.Forms.ComboBox
    Friend WithEvents NoGiro As System.Windows.Forms.TextBox
    Friend WithEvents langganan As System.Windows.Forms.TextBox
    Friend WithEvents kLgn As System.Windows.Forms.Label
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents butBAT As System.Windows.Forms.Button
    Friend WithEvents butINP As System.Windows.Forms.Button
    Friend WithEvents bayarDP As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents total As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
