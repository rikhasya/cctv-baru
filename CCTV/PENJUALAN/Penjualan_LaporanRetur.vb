﻿Imports System.Drawing.Printing
Imports System.IO

Public Class Penjualan_LaporanRetur

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub Penjualan_LaporanRetur_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub Penjualan_LaporanRetur_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBlgn.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim a As New clsCCTV.clsReJual
        Dim tb As DataSet

        Dim tmpsup As String = "%"
        If CBlgn.SelectedValue <> "0" Then
            tmpsup = CBlgn.SelectedValue
        End If

        tb = a.table(date1.Text, date2.Text, tmpsup)

        Dim parentColumn As DataColumn = tb.Tables("DATREJUAL").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETREJUAL").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETREJUAL", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATREJUAL"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETREJUAL"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVheader.Focus()
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURJUAL_EDITHAPUS")
        If b = True Then
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            If DGVheader.Item("BAYAR", row).Value <> 0 Then
                MsgBox("Sudah Pernah Di Bayar")
                Exit Sub
            End If

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As Penjualan_ReturEditHapus
            frm = New Penjualan_ReturEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Retur Penjualan"
            frm.MdiParent = frmMenu
            frm.nofak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.Tanggal.Text = FormatDateTime(DGVheader.Item("TGL_FAK", row).Value.ToString, DateFormat.ShortDate)
            frm.langganan.Text = DGVheader.Item("N_LGN", row).Value.ToString
            frm.sales.Text = DGVheader.Item("N_SLS", row).Value.ToString
            frm.noJual.Text = DGVheader.Item("NO_JUAL", row).Value.ToString
            'frm.TanggalJT.Text = FormatDateTime(DGVheader.Item("TGL_JT", row).Value.ToString, DateFormat.ShortDate)
            frm.Total2.Text = DGVheader.Item("TOTAL1", row).Value.ToString
            frm.Disc.Text = DGVheader.Item("DISC", row).Value.ToString
            frm.Grandtot.Text = DGVheader.Item("GRANDTOT", row).Value.ToString
            frm.Keterangan.Text = DGVheader.Item("KET", row).Value.ToString

            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        If e.KeyCode = Keys.F2 Then
            'Dim a As New clsCCTV.clsPengguna
            'Dim b As Boolean
            'b = a.cek("SERVICE_INPUT")
            'If b = True Then
            '    If Me.MdiChildren.Length > 0 Then
            '        Dim childForm As Form = CType(ActiveMdiChild, Form)
            '        childForm.Close()
            '    End If
            '    Dim frm As ServiceInput
            '    frm = New ServiceInput
            '    frm.MdiParent = frmMenu
            '    frm.Text = "Input Service"
            '    Dim row As Integer
            '    row = DGVheader.CurrentRow.Index.ToString
            '    frm.MdiParent = frmMenu
            '    frm.noRetur.Text = DGVheader.Item("NO_FAK", row).Value.ToString

            '    frm.Show()
            '    frm.Dock = DockStyle.Fill
            '    Me.Close()
            'Else
            '    MsgBox("anda tdk di otorisasi")
            'End If
        ElseIf e.KeyCode = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("PRINTULANGLAPORAN")
            If b = True Then
                PRINT()
            Else
                MsgBox("anda tdk di otorisasi")
            End If
            End If
    End Sub

    Sub PRINT()
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Ulang Faktur Retur Penjualan?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsReJual
            Penjualan_Retur.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With Penjualan_Retur
                .tmpnofak = .tb.Tables("DATREJUAL").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Penjualan_Retur.tb.Tables("DETREJUAL").Rows.Count - 1

            ReDim Penjualan_Retur.tmpKode(jumlah)
            ReDim Penjualan_Retur.tmpNama(jumlah)
            ReDim Penjualan_Retur.tmpQty(jumlah)
            ReDim Penjualan_Retur.tmpSat(jumlah)
            ReDim Penjualan_Retur.tmpHrg(jumlah)
            ReDim Penjualan_Retur.tmpJmlh(jumlah)
            ReDim Penjualan_Retur.tmpPot1(jumlah)
            ReDim Penjualan_Retur.tmpPot2(jumlah)
            ReDim Penjualan_Retur.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Penjualan_Retur
                    .tmpKode(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETREJUAL").Rows(ipo)("TOTAL").ToString

                End With
            Next
            Penjualan_Retur.arrke = 0

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Penjualan_Retur.faktur
            PrintDoc.Print()
            Exit Sub
        End If
    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class