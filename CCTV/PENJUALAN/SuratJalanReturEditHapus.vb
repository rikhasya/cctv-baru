﻿Public Class SuratJalanReturEditHapus

    Private Sub butHAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHAP.Click
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus faktur " & noSJ.Text & " ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim a As New clsCCTV.clsRSJ
        Dim b As Boolean
        b = a.hapus(noSJ.Text)
        If b = True Then
            MsgBox("Berhasil hapus faktur " & noSJ.Text)
            Me.Close()
        Else
            MsgBox("Gagal hapus faktur")
        End If
    End Sub

    Private Sub SuratJalanReturEditHapus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsRSJ
        Dim b As DataSet
        b = a.table(noSJ.Text)
        DGVdetail.DataSource = b.Tables("DETRSJ")
    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        Me.Close()
    End Sub
End Class