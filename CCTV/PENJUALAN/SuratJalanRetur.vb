﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SuratJalanRetur

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub SuratJalanRetur_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsSJ
        Dim b As DataSet
        b = a.table(noSJ.Text)
        DGVdetail.DataSource = b.Tables("DETSJ")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        Dim ask As DialogResult
        ask = MessageBox.Show("Input Retur Surat jalan ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim b As New clsCCTV.clsRSJ
        Dim t As String
        t = b.no()

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")

        Dim i As Integer
        For i = 0 To DGVdetail.Rows.Count - 1
            If DGVdetail.Item("QTY", i).Value.ToString <> 0 Then
                table1.Rows.Add(t, DGVdetail.Item("K_BRG", i).Value.ToString, DGVdetail.Item("N_BRG", i).Value.ToString, DGVdetail.Item("QTY", i).Value.ToString, DGVdetail.Item("SAT", i).Value.ToString)
            End If
        Next

        Dim a As New clsCCTV.clsRSJ
        Dim p As Boolean
        p = a.input(t, Tanggal.Text, k_lgn.Text, Keterangan.Text, noSO.Text, table1)

        If p = True Then
            MsgBox("Berhasil input retur surat jalan")
            ask = MessageBox.Show("Print Retur Surat Jalan?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.No Then
                Me.Close()
                frmMenu.MenuStrip1.Enabled = True
                Exit Sub

            End If

            Dim otc As New clsCCTV.clsRSJ
            tmpnofak = t
            tb = otc.table(tmpnofak)

            Dim jumlah As Integer = tb.Tables("DETRSJ").Rows.Count - 1

            ReDim tmpKode(jumlah)
            ReDim tmpNama(jumlah)
            ReDim tmpQty(jumlah)
            ReDim tmpSat(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah

                tmpKode(ipo) = tb.Tables("DETRSJ").Rows(ipo)("K_BRG").ToString
                tmpNama(ipo) = tb.Tables("DETRSJ").Rows(ipo)("N_BRG").ToString
                tmpQty(ipo) = tb.Tables("DETRSJ").Rows(ipo)("QTY").ToString
                tmpSat(ipo) = tb.Tables("DETRSJ").Rows(ipo)("SAT").ToString
            Next

            arrke = 0
            printFont = New Font("Times New Roman", 10)

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Me.SJ
            PrintDoc.Print()
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal input retur surat jalan")
            Exit Sub
        End If
    End Sub

    Public Sub SJ(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim count As Integer = 0
        Dim leftMargin As Single = ev.MarginBounds.Left
        Dim topMargin As Single = ev.MarginBounds.Top
        Dim line As String = Nothing

        'hitung line per halaman 
        'linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics)

        Dim ukuran As New Font("Times New Roman", 11, FontStyle.Regular)
        Dim ukJD As New Font("Times New Roman", 10, FontStyle.Bold)
        Dim ukMn As New Font("Times New Roman", 10, FontStyle.Regular)

        ev.Graphics.DrawString("RETUR SURAT JALAN", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 30, 15)
        ev.Graphics.DrawString("No : " & tmpnofak, New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 33, 35)

        ev.Graphics.DrawImage(My.Resources.logo_sm, 15, 65, 165, 50)

        'ev.Graphics.DrawString("SEJAHTERA MANDIRI", New Font(FontFamily.GenericSerif, 9, FontStyle.Bold), Brushes.Black, 25, 85)

        ev.Graphics.DrawString(tb.Tables("DATRSJ").Rows(0)("TGL_FAK"), New Font(FontFamily.GenericSerif, 9, FontStyle.Regular), Brushes.Black, 70, 60)

        ev.Graphics.DrawString("Kepada : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 500, 35)
        ev.Graphics.DrawString(tb.Tables("DATRSJ").Rows(0)("N_LGN"), New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 560, 35)

        Dim displayRectangleJ As New Rectangle(New Point(560, 55), New Size(350, 45))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleJ)
        Dim formatK As New StringFormat(StringFormatFlags.NoClip)
        formatK.LineAlignment = StringAlignment.Near ' = top
        formatK.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ev.Graphics.DrawString(tb.Tables("DATRSJ").Rows(0)("ALAMAT"), ukMn, Brushes.Black, RectangleF.op_Implicit(displayRectangleJ), formatK)

        ev.Graphics.DrawString("Dengan rincian barang sebagai berikut : ", New Font(FontFamily.GenericSerif, 10, FontStyle.Regular), Brushes.Black, 30, 130)

        ev.Graphics.DrawLine(Pens.Black, 25, 170, 500, 170)
        ev.Graphics.DrawLine(Pens.Black, 25, 150, 500, 150)
        ev.Graphics.DrawLine(Pens.Black, 25, 150, 25, 400) 'vertikal 
        ev.Graphics.DrawLine(Pens.Black, 45, 150, 45, 400) 'ok
        ev.Graphics.DrawString("Kode Barang", ukJD, Brushes.Black, 60, 150)
        ev.Graphics.DrawLine(Pens.Black, 160, 150, 160, 400)
        ev.Graphics.DrawString("Nama Barang", ukJD, Brushes.Black, 230, 150)
        ev.Graphics.DrawLine(Pens.Black, 390, 150, 390, 400)
        ev.Graphics.DrawString("Qty", ukJD, Brushes.Black, 400, 150)
        ev.Graphics.DrawLine(Pens.Black, 440, 150, 440, 400)
        ev.Graphics.DrawString("Satuan", ukJD, Brushes.Black, 445, 150)
        ev.Graphics.DrawLine(Pens.Black, 500, 150, 500, 400)

        While count < 12
            If arrke > UBound(tmpKode) Then
                line = Nothing
            Else
                line = "aa"
                ev.Graphics.DrawString(arrke + 1, ukMn, Brushes.Black, 30, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpKode(arrke), ukMn, Brushes.Black, 52, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpNama(arrke), ukMn, Brushes.Black, 165, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawString(tmpQty(arrke), ukMn, Brushes.Black, 430, 170 + ((count Mod 12) * 12), New StringFormat(LeftRightAlignment.Right))
                ev.Graphics.DrawString(tmpSat(arrke), ukMn, Brushes.Black, 450, 170 + ((count Mod 12) * 12))
                ev.Graphics.DrawLine(Pens.Black, 25, 400, 500, 400)
            End If

            If line Is Nothing Then
                Exit While
            End If
            count += 1
            arrke += 1
        End While

        Dim displayRectangleL As New Rectangle(New Point(550, 320), New Size(300, 70))
        ev.Graphics.DrawRectangle(Pens.Transparent, displayRectangleL)
        Dim formatL As New StringFormat(StringFormatFlags.NoClip)
        formatL.LineAlignment = StringAlignment.Near ' = top
        formatL.Alignment = StringAlignment.Near  ' horizontal alignment kanan
        ev.Graphics.DrawString("Ket : ", ukMn, Brushes.Black, 550, 300)
        ev.Graphics.DrawString(tb.Tables("DATRSJ").Rows(0)("KET"), ukMn, Brushes.Black, RectangleF.op_Implicit(displayRectangleL), formatL)

        ev.Graphics.DrawString("Penerima", ukMn, Brushes.Black, 60, 445)
        ev.Graphics.DrawString("Pengirim", ukMn, Brushes.Black, 460, 445)
        ev.Graphics.DrawString("SEJAHTERA MANDIRI", ukMn, Brushes.Black, 20, 460)
        ev.Graphics.DrawString("(................................)", ukMn, Brushes.Black, 20, 505)
        ev.Graphics.DrawString("(................................)", ukMn, Brushes.Black, 430, 505)

        ev.Graphics.DrawString("***Perhatian !!", New Font("Courier", 7, FontStyle.Bold), Brushes.Black, 250, 480)
        ev.Graphics.DrawString("Telitilah barang-barang yang akan diterima.***", New Font("Courier", 7, FontStyle.Italic), Brushes.Black, 180, 500)

        If line = "aa" And arrke <= UBound(tmpKode) Then
            hal = hal + 1
            ev.HasMorePages = True
        Else
            ev.HasMorePages = False
        End If
    End Sub
End Class