﻿Public Class DP

    Private Sub DP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub DP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsACC
        Dim b As DataTable
        b = a.table("%", "%")
        With CBacc
            .DisplayMember = "N_ACC"
            .ValueMember = "K_ACC"
            .DataSource = b
        End With
    End Sub

    Private Sub bayarDP_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles bayarDP.Leave
        If IsNumeric(bayarDP.Text) Then
            bayarDP.Text = FormatNumber(bayarDP.Text, 2)
        End If
    End Sub

    Private Sub bayarDP_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bayarDP.TabIndexChanged
        If IsNumeric(bayarDP.Text) Then
            bayarDP.Text = FormatNumber(bayarDP.Text, 2)
        End If
    End Sub

    Private Sub butINP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINP.Click
        Dim a As New clsCCTV.clsPiutHutang
        Dim t As String
        Dim t2 As String

        t = a.nokas("DP")
        t2 = a.nokas2("DP")

        Dim table1 As New DataTable
        table1.Columns.Add("NO_PV")
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("BAYAR")

        table1.Rows.Add(t, NoSO.Text, CDbl(bayarDP.Text))

        Dim b As Boolean
        b = a.inputkasDP(t, t2, CBacc.SelectedValue, NoGiro.Text, kLgn.Text, langganan.Text, date1.Text, date2.Text, CDbl(bayarDP.Text), keterangan.Text, clsCCTV.clsPengguna.CLUserID, table1)


        If b = True Then
            MsgBox("Berhasil input DP")
            Me.Close()
        Else
            MsgBox("Gagal input DP")
        End If

    End Sub

    Private Sub butBAT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBAT.Click
        Me.Close()
    End Sub

    Private Sub bayarDP_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bayarDP.TextChanged

    End Sub
End Class