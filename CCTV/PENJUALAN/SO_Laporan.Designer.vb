﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SO_Laporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CBlgn = New System.Windows.Forms.ComboBox()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HARGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JUMLAH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.POT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY_SJ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DGVheader = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CBstts = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TANGGAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TGL_JT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRANDTOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GGTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PEMBAYARAN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EKSPEDISI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.B_EKSPEDISI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_SLS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GIRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CBlgn
        '
        Me.CBlgn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBlgn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBlgn.FormattingEnabled = True
        Me.CBlgn.Location = New System.Drawing.Point(463, 10)
        Me.CBlgn.Name = "CBlgn"
        Me.CBlgn.Size = New System.Drawing.Size(180, 21)
        Me.CBlgn.TabIndex = 2
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToAddRows = False
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT, Me.HARGA, Me.JUMLAH, Me.POT1, Me.POT2, Me.TOTAL, Me.QTY_SJ, Me.LUNAS2})
        Me.DGVdetail.Location = New System.Drawing.Point(12, 391)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.ReadOnly = True
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(978, 250)
        Me.DGVdetail.TabIndex = 30
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle1
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'HARGA
        '
        Me.HARGA.DataPropertyName = "HARGA"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.HARGA.DefaultCellStyle = DataGridViewCellStyle2
        Me.HARGA.HeaderText = "Harga"
        Me.HARGA.Name = "HARGA"
        Me.HARGA.ReadOnly = True
        '
        'JUMLAH
        '
        Me.JUMLAH.DataPropertyName = "JUMLAH"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.JUMLAH.DefaultCellStyle = DataGridViewCellStyle3
        Me.JUMLAH.HeaderText = "Jumlah"
        Me.JUMLAH.Name = "JUMLAH"
        Me.JUMLAH.ReadOnly = True
        '
        'POT1
        '
        Me.POT1.DataPropertyName = "POT1"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.POT1.DefaultCellStyle = DataGridViewCellStyle4
        Me.POT1.HeaderText = "Pot 1"
        Me.POT1.Name = "POT1"
        Me.POT1.ReadOnly = True
        '
        'POT2
        '
        Me.POT2.DataPropertyName = "POT2"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        Me.POT2.DefaultCellStyle = DataGridViewCellStyle5
        Me.POT2.HeaderText = "Pot 2"
        Me.POT2.Name = "POT2"
        Me.POT2.ReadOnly = True
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle6
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'QTY_SJ
        '
        Me.QTY_SJ.DataPropertyName = "QTY_SJ"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.QTY_SJ.DefaultCellStyle = DataGridViewCellStyle7
        Me.QTY_SJ.HeaderText = "Qty Terkirim"
        Me.QTY_SJ.Name = "QTY_SJ"
        Me.QTY_SJ.ReadOnly = True
        '
        'LUNAS2
        '
        Me.LUNAS2.DataPropertyName = "LUNAS"
        Me.LUNAS2.FalseValue = "N"
        Me.LUNAS2.HeaderText = "Lunas"
        Me.LUNAS2.Name = "LUNAS2"
        Me.LUNAS2.ReadOnly = True
        Me.LUNAS2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LUNAS2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.LUNAS2.TrueValue = "Y"
        '
        'DGVheader
        '
        Me.DGVheader.AllowUserToAddRows = False
        Me.DGVheader.AllowUserToDeleteRows = False
        Me.DGVheader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVheader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.TANGGAL, Me.TGL_JT, Me.DATESTAMP, Me.STATUS, Me.N_LGN, Me.N_SLS, Me.TOTAL1, Me.DISC, Me.GRANDTOT, Me.PPN, Me.GGTOTAL, Me.KET, Me.KET2, Me.PEMBAYARAN, Me.EKSPEDISI, Me.B_EKSPEDISI, Me.ALAMAT, Me.TELP, Me.K_LGN, Me.K_SLS, Me.USERID, Me.VALID, Me.BAYAR, Me.GIRO, Me.LUNAS})
        Me.DGVheader.Location = New System.Drawing.Point(12, 107)
        Me.DGVheader.Name = "DGVheader"
        Me.DGVheader.ReadOnly = True
        Me.DGVheader.RowHeadersVisible = False
        Me.DGVheader.Size = New System.Drawing.Size(978, 263)
        Me.DGVheader.TabIndex = 29
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(248, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "s/d"
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(272, 11)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(88, 20)
        Me.date2.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.CBstts)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.CBlgn)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.date2)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-2, 38)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'CBstts
        '
        Me.CBstts.FormattingEnabled = True
        Me.CBstts.Items.AddRange(New Object() {"ALL", "SO", "SJ", "FK"})
        Me.CBstts.Location = New System.Drawing.Point(697, 11)
        Me.CBstts.Name = "CBstts"
        Me.CBstts.Size = New System.Drawing.Size(52, 21)
        Me.CBstts.TabIndex = 3
        Me.CBstts.Text = "ALL"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(654, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Status"
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(158, 11)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(106, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Tanggal"
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(775, 10)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 4
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(370, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Langganan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "* Doubleclick untuk edit/hapus laporan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(423, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 24)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Laporan SO"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 374)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Detail Laporan :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(201, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(138, 13)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "* F1 untuk input Surat Jalan"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(12, 86)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(978, 23)
        Me.Panel2.TabIndex = 33
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(478, 5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 13)
        Me.Label9.TabIndex = 34
        Me.Label9.Text = "* F3 untuk Input DP"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(349, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(116, 13)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "* F2 untuk Input Faktur"
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        Me.NO_FAK.Width = 70
        '
        'TANGGAL
        '
        Me.TANGGAL.DataPropertyName = "TGL_FAK"
        Me.TANGGAL.HeaderText = "Tanggal"
        Me.TANGGAL.Name = "TANGGAL"
        Me.TANGGAL.ReadOnly = True
        Me.TANGGAL.Width = 70
        '
        'TGL_JT
        '
        Me.TGL_JT.DataPropertyName = "TGL_JT"
        Me.TGL_JT.HeaderText = "Tanggal JT"
        Me.TGL_JT.Name = "TGL_JT"
        Me.TGL_JT.ReadOnly = True
        Me.TGL_JT.Width = 70
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "Datestamp"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'STATUS
        '
        Me.STATUS.DataPropertyName = "STATUS"
        Me.STATUS.HeaderText = "Status"
        Me.STATUS.Name = "STATUS"
        Me.STATUS.ReadOnly = True
        Me.STATUS.Width = 50
        '
        'N_LGN
        '
        Me.N_LGN.DataPropertyName = "N_LGN"
        Me.N_LGN.HeaderText = "Pelanggan"
        Me.N_LGN.Name = "N_LGN"
        Me.N_LGN.ReadOnly = True
        Me.N_LGN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'N_SLS
        '
        Me.N_SLS.DataPropertyName = "N_SLS"
        Me.N_SLS.HeaderText = "Sales"
        Me.N_SLS.Name = "N_SLS"
        Me.N_SLS.ReadOnly = True
        '
        'TOTAL1
        '
        Me.TOTAL1.DataPropertyName = "TOTAL"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        Me.TOTAL1.DefaultCellStyle = DataGridViewCellStyle8
        Me.TOTAL1.HeaderText = "Total"
        Me.TOTAL1.Name = "TOTAL1"
        Me.TOTAL1.ReadOnly = True
        '
        'DISC
        '
        Me.DISC.DataPropertyName = "DISC"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.DISC.DefaultCellStyle = DataGridViewCellStyle9
        Me.DISC.HeaderText = "Diskon"
        Me.DISC.Name = "DISC"
        Me.DISC.ReadOnly = True
        '
        'GRANDTOT
        '
        Me.GRANDTOT.DataPropertyName = "GTOTAL"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.GRANDTOT.DefaultCellStyle = DataGridViewCellStyle10
        Me.GRANDTOT.HeaderText = "Grand Total"
        Me.GRANDTOT.Name = "GRANDTOT"
        Me.GRANDTOT.ReadOnly = True
        '
        'PPN
        '
        Me.PPN.DataPropertyName = "PPN"
        Me.PPN.HeaderText = "PPN"
        Me.PPN.Name = "PPN"
        Me.PPN.ReadOnly = True
        '
        'GGTOTAL
        '
        Me.GGTOTAL.DataPropertyName = "GGTOTAL"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        Me.GGTOTAL.DefaultCellStyle = DataGridViewCellStyle11
        Me.GGTOTAL.HeaderText = "Grand Total"
        Me.GGTOTAL.Name = "GGTOTAL"
        Me.GGTOTAL.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan Print"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'KET2
        '
        Me.KET2.DataPropertyName = "KET2"
        Me.KET2.HeaderText = "Keterangan 2"
        Me.KET2.Name = "KET2"
        Me.KET2.ReadOnly = True
        '
        'PEMBAYARAN
        '
        Me.PEMBAYARAN.DataPropertyName = "PEMBAYARAN"
        Me.PEMBAYARAN.HeaderText = "Pembayaran"
        Me.PEMBAYARAN.Name = "PEMBAYARAN"
        Me.PEMBAYARAN.ReadOnly = True
        '
        'EKSPEDISI
        '
        Me.EKSPEDISI.DataPropertyName = "EKSPEDISI"
        Me.EKSPEDISI.HeaderText = "Expedisi"
        Me.EKSPEDISI.Name = "EKSPEDISI"
        Me.EKSPEDISI.ReadOnly = True
        '
        'B_EKSPEDISI
        '
        Me.B_EKSPEDISI.DataPropertyName = "B_EXPEDISI"
        Me.B_EKSPEDISI.HeaderText = "Biaya Expedisi"
        Me.B_EKSPEDISI.Name = "B_EKSPEDISI"
        Me.B_EKSPEDISI.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'K_LGN
        '
        Me.K_LGN.DataPropertyName = "K_LGN"
        Me.K_LGN.HeaderText = "Kode Supplier"
        Me.K_LGN.Name = "K_LGN"
        Me.K_LGN.ReadOnly = True
        Me.K_LGN.Visible = False
        '
        'K_SLS
        '
        Me.K_SLS.DataPropertyName = "K_SLS"
        Me.K_SLS.HeaderText = "Kode Sales"
        Me.K_SLS.Name = "K_SLS"
        Me.K_SLS.ReadOnly = True
        Me.K_SLS.Visible = False
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'BAYAR
        '
        Me.BAYAR.DataPropertyName = "BAYAR"
        Me.BAYAR.HeaderText = "Bayar"
        Me.BAYAR.Name = "BAYAR"
        Me.BAYAR.ReadOnly = True
        Me.BAYAR.Visible = False
        '
        'GIRO
        '
        Me.GIRO.DataPropertyName = "GIRO"
        Me.GIRO.HeaderText = "Giro"
        Me.GIRO.Name = "GIRO"
        Me.GIRO.ReadOnly = True
        Me.GIRO.Visible = False
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.FalseValue = "N"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        Me.LUNAS.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LUNAS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.LUNAS.TrueValue = "T"
        Me.LUNAS.Visible = False
        '
        'SO_Laporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.DGVheader)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "SO_Laporan"
        Me.Text = "SO_Laporan"
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CBlgn As System.Windows.Forms.ComboBox
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents DGVheader As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HARGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JUMLAH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents POT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY_SJ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CBstts As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TANGGAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TGL_JT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRANDTOT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GGTOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PEMBAYARAN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EKSPEDISI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents B_EKSPEDISI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_SLS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAYAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GIRO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
