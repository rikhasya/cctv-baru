﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SuratJalanLap
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DGVdetail = New System.Windows.Forms.DataGridView()
        Me.NO_FAK2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_BRG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CBlgn = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DGVheader = New System.Windows.Forms.DataGridView()
        Me.NO_FAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_SO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TANGGAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LUNAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.K_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGVserial = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVserial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(399, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(195, 24)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "Laporan Surat Jalan"
        '
        'DGVdetail
        '
        Me.DGVdetail.AllowUserToAddRows = False
        Me.DGVdetail.AllowUserToDeleteRows = False
        Me.DGVdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK2, Me.K_BRG, Me.N_BRG, Me.QTY, Me.SAT})
        Me.DGVdetail.Location = New System.Drawing.Point(11, 391)
        Me.DGVdetail.Name = "DGVdetail"
        Me.DGVdetail.ReadOnly = True
        Me.DGVdetail.RowHeadersVisible = False
        Me.DGVdetail.Size = New System.Drawing.Size(615, 250)
        Me.DGVdetail.TabIndex = 39
        '
        'NO_FAK2
        '
        Me.NO_FAK2.DataPropertyName = "NO_FAK"
        Me.NO_FAK2.HeaderText = "No Faktur"
        Me.NO_FAK2.Name = "NO_FAK2"
        Me.NO_FAK2.ReadOnly = True
        Me.NO_FAK2.Visible = False
        '
        'K_BRG
        '
        Me.K_BRG.DataPropertyName = "K_BRG"
        Me.K_BRG.HeaderText = "Kode Barang"
        Me.K_BRG.Name = "K_BRG"
        Me.K_BRG.ReadOnly = True
        '
        'N_BRG
        '
        Me.N_BRG.DataPropertyName = "N_BRG"
        Me.N_BRG.HeaderText = "Nama Barang"
        Me.N_BRG.Name = "N_BRG"
        Me.N_BRG.ReadOnly = True
        '
        'QTY
        '
        Me.QTY.DataPropertyName = "QTY"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        Me.QTY.DefaultCellStyle = DataGridViewCellStyle1
        Me.QTY.HeaderText = "Qty"
        Me.QTY.Name = "QTY"
        Me.QTY.ReadOnly = True
        '
        'SAT
        '
        Me.SAT.DataPropertyName = "SAT"
        Me.SAT.HeaderText = "Sat"
        Me.SAT.Name = "SAT"
        Me.SAT.ReadOnly = True
        '
        'CBlgn
        '
        Me.CBlgn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBlgn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBlgn.FormattingEnabled = True
        Me.CBlgn.Location = New System.Drawing.Point(536, 11)
        Me.CBlgn.Name = "CBlgn"
        Me.CBlgn.Size = New System.Drawing.Size(180, 21)
        Me.CBlgn.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(323, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "s/d"
        '
        'DGVheader
        '
        Me.DGVheader.AllowUserToAddRows = False
        Me.DGVheader.AllowUserToDeleteRows = False
        Me.DGVheader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVheader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NO_FAK, Me.NO_SO, Me.TANGGAL, Me.N_LGN, Me.ALAMAT, Me.TELP, Me.USERID, Me.LUNAS, Me.KET, Me.K_LGN, Me.VALID})
        Me.DGVheader.Location = New System.Drawing.Point(11, 107)
        Me.DGVheader.Name = "DGVheader"
        Me.DGVheader.ReadOnly = True
        Me.DGVheader.RowHeadersVisible = False
        Me.DGVheader.Size = New System.Drawing.Size(978, 263)
        Me.DGVheader.TabIndex = 38
        '
        'NO_FAK
        '
        Me.NO_FAK.DataPropertyName = "NO_FAK"
        Me.NO_FAK.HeaderText = "No Faktur"
        Me.NO_FAK.Name = "NO_FAK"
        Me.NO_FAK.ReadOnly = True
        '
        'NO_SO
        '
        Me.NO_SO.DataPropertyName = "NO_SO"
        Me.NO_SO.HeaderText = "No SO"
        Me.NO_SO.Name = "NO_SO"
        Me.NO_SO.ReadOnly = True
        '
        'TANGGAL
        '
        Me.TANGGAL.DataPropertyName = "TGL_FAK"
        Me.TANGGAL.HeaderText = "Tanggal"
        Me.TANGGAL.Name = "TANGGAL"
        Me.TANGGAL.ReadOnly = True
        '
        'N_LGN
        '
        Me.N_LGN.DataPropertyName = "N_LGN"
        Me.N_LGN.HeaderText = "Pelanggan"
        Me.N_LGN.Name = "N_LGN"
        Me.N_LGN.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'LUNAS
        '
        Me.LUNAS.DataPropertyName = "LUNAS"
        Me.LUNAS.HeaderText = "Lunas"
        Me.LUNAS.Name = "LUNAS"
        Me.LUNAS.ReadOnly = True
        '
        'KET
        '
        Me.KET.DataPropertyName = "KET"
        Me.KET.HeaderText = "Keterangan"
        Me.KET.Name = "KET"
        Me.KET.ReadOnly = True
        '
        'K_LGN
        '
        Me.K_LGN.DataPropertyName = "K_LGN"
        Me.K_LGN.HeaderText = "Kode Lgn"
        Me.K_LGN.Name = "K_LGN"
        Me.K_LGN.ReadOnly = True
        Me.K_LGN.Visible = False
        '
        'VALID
        '
        Me.VALID.DataPropertyName = "VALID"
        Me.VALID.HeaderText = "Valid"
        Me.VALID.Name = "VALID"
        Me.VALID.ReadOnly = True
        '
        'date2
        '
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(347, 11)
        Me.date2.Name = "date2"
        Me.date2.Size = New System.Drawing.Size(88, 20)
        Me.date2.TabIndex = 1
        '
        'date1
        '
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(233, 11)
        Me.date1.Name = "date1"
        Me.date1.Size = New System.Drawing.Size(88, 20)
        Me.date1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(181, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Tanggal"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.CBlgn)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.date2)
        Me.Panel1.Controls.Add(Me.date1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(-2, 38)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1006, 43)
        Me.Panel1.TabIndex = 0
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(720, 10)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 3
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(445, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama Langganan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "* Doubleclick untuk edit/hapus laporan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 375)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "Detail Laporan :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(219, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 13)
        Me.Label7.TabIndex = 40
        Me.Label7.Text = "**F1 untuk print ulang SJ"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(368, 91)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(134, 13)
        Me.Label8.TabIndex = 41
        Me.Label8.Text = "*** F3 untuk input Retur SJ"
        '
        'DGVserial
        '
        Me.DGVserial.AllowUserToAddRows = False
        Me.DGVserial.AllowUserToDeleteRows = False
        Me.DGVserial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVserial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.N_, Me.DataGridViewTextBoxColumn3})
        Me.DGVserial.Location = New System.Drawing.Point(632, 391)
        Me.DGVserial.Name = "DGVserial"
        Me.DGVserial.ReadOnly = True
        Me.DGVserial.RowHeadersVisible = False
        Me.DGVserial.Size = New System.Drawing.Size(358, 250)
        Me.DGVserial.TabIndex = 42
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(520, 91)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(194, 13)
        Me.Label9.TabIndex = 43
        Me.Label9.Text = "**** F4 untuk Input Detail Serial Number"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(730, 91)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(173, 13)
        Me.Label10.TabIndex = 44
        Me.Label10.Text = "* F5 untuk Print Ulang Detail Serial "
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "NO_FAK"
        Me.DataGridViewTextBoxColumn1.HeaderText = "No Faktur"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "K_BRG"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Kode Barang"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'N_
        '
        Me.N_.DataPropertyName = "N_BRG"
        Me.N_.HeaderText = "Nama Barang"
        Me.N_.Name = "N_"
        Me.N_.ReadOnly = True
        Me.N_.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "SERIAL"
        Me.DataGridViewTextBoxColumn3.HeaderText = "No Serial"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'SuratJalanLap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1002, 705)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.DGVserial)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.DGVdetail)
        Me.Controls.Add(Me.DGVheader)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "SuratJalanLap"
        Me.Text = "SuratJalanLap"
        CType(Me.DGVdetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVheader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVserial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DGVdetail As System.Windows.Forms.DataGridView
    Friend WithEvents CBlgn As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DGVheader As System.Windows.Forms.DataGridView
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_BRG As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents NO_FAK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_SO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TANGGAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LUNAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents K_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DGVserial As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
