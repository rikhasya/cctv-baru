﻿Public Class QtyNeg

    Private Sub QtyNeg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsBarang
        Dim b As DataTable
        b = a.qtyneg()
        DGVheader.DataSource = b
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim row As Integer
        row = DGVheader.CurrentRow.Index.ToString

        Dim a As New clsCCTV.clsBarang
        Dim b As DataTable
        b = a.mutasi(DGVheader.Item("K_BRG", row).Value.ToString)
        DGVdetail.DataSource = b
        NAMA.Text = DGVheader.Item("N_BRG", row).Value.ToString
    End Sub

    Private Sub DGVdetail_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGVdetail.CellFormatting
        Dim row As Integer
        row = DGVheader.CurrentRow.Index.ToString
        Dim nofak As String
        nofak = DGVheader.Item("NO_FAK", row).Value.ToString
        Dim I As Integer
        For I = 0 To DGVdetail.Rows.Count - 1
            If DGVdetail.Item("NO_FAK2", I).Value.ToString = nofak Then
                DGVdetail.Rows(I).DefaultCellStyle.BackColor = Color.Yellow
                nofak = DGVdetail.Item("NO_FAK2", I).Value.ToString
            End If
        Next
    End Sub
End Class