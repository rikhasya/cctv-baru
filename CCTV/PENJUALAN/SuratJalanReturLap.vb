﻿Imports System.Drawing.Printing
Imports System.IO

Public Class SuratJalanReturLap
    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub SuratJalanReturLap_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBlgn.SelectedValue = "0"

    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        Dim N As New clsCCTV.clsRSJ
        Dim tb As DataSet

        Dim tmpsup As String = "%"
        If CBlgn.SelectedValue <> "0" Then
            tmpsup = CBlgn.SelectedValue
        End If

        tb = N.table(date1.Text, date2.Text, tmpsup)

        Dim i As Integer
        For i = 0 To tb.Tables("DATRSJ").Rows.Count - 1
            Console.WriteLine("dat" & tb.Tables("DATRSJ").Rows(i)(0).ToString)
        Next

        For i = 0 To tb.Tables("DETRSJ").Rows.Count - 1
            Console.WriteLine("det" & tb.Tables("DETRSJ").Rows(i)(0).ToString)
        Next

        Dim parentColumn As DataColumn = tb.Tables("DATRSJ").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETRSJ").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETRSJ", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATRSJ"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETRSJ"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVheader.Focus()
    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("RETURSJ_EDITHAPUS")
        If b = True Then
            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SuratJalanReturEditHapus
            frm = New SuratJalanReturEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit / Hapus Retur Surat Jalan"
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.noSJ.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.noSO.Text = DGVheader.Item("NO_SO", row).Value.ToString
            frm.lgn.Text = DGVheader.Item("N_LGN", row).Value.ToString
            frm.k_lgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
            frm.Keterangan.Text = DGVheader.Item("KET", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Sub PRINT()
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult
        'PDF
        ask = MessageBox.Show("Print Ulang Retur Surat Jalan?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then
            Dim otc As New clsCCTV.clsRSJ
            SuratJalanRetur.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With SuratJalanRetur
                .tmpnofak = .tb.Tables("DATRSJ").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = TerimaBarang.tb.Tables("DETRSJ").Rows.Count - 1

            ReDim SuratJalanRetur.tmpKode(jumlah)
            ReDim SuratJalanRetur.tmpNama(jumlah)
            ReDim SuratJalanRetur.tmpQty(jumlah)
            ReDim SuratJalanRetur.tmpSat(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With SuratJalanRetur
                    .tmpKode(ipo) = .tb.Tables("DETRSJ").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETRSJ").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETRSJ").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETRSJ").Rows(ipo)("SAT").ToString
                End With
            Next
            SuratJalanRetur.arrke = 0

            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf SuratJalanRetur.SJ
            PrintDoc.Print()
        End If
    End Sub

    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("PRINTULANGLAPORAN")
        If b = True Then
            PRINT()
        Else
            MsgBox("anda tdk di otorisasi")
        End If
    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class