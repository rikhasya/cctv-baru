﻿Public Class SO

    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        e.Cancel = False
    End Sub

    Private Sub kode_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles kode.Leave
        If kode.Text = "" Then
            butINPUT.Focus()
        Else
            Dim a As New clsCCTV.clsBarang
            Dim b As DataTable
            b = a.table(kode.Text)
            If b.Rows.Count = 1 Then
                kode.Text = b.Rows(0)("K_BRG").ToString
                Nama.Text = b.Rows(0)("N_BRG").ToString
                Sat.Text = b.Rows(0)("SAT").ToString
                Harga.Text = b.Rows(0)("H_JUAL").ToString
                Qty.Focus()
            Else
                Exit Sub
            End If
        End If
    End Sub

#Region "errorprov"

    Private Sub CBsales_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBsales.Validated
        ErrorProvider1.SetError(CBsales, "")
    End Sub

    Private Sub CBsales_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles CBsales.Validating
        If Microsoft.VisualBasic.IsDBNull(CBsales.SelectedValue) Or IsNothing(CBsales.SelectedValue) Then
            e.Cancel = True
            CBsales.Select(0, CBsales.Text.Length)
            ErrorProvider1.SetError(CBsales, "koreksi nama sales")
        End If
    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CBsales.Focus()
        End If
    End Sub

    Private Sub Disc_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
        ErrorProvider1.SetError(Disc, "")
    End Sub

    Private Sub Disc_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not IsNumeric(Disc.Text) Or Disc.Text = "" Then
            e.Cancel = True
            Disc.Select(0, Disc.Text.Length)
            ErrorProvider1.SetError(Disc, "harap di isi")
        End If

    End Sub

    Private Sub Qty_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Qty.Validated
        ErrorProvider1.SetError(Qty, "")
    End Sub

    Private Sub Qty_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Qty.Validating
        If Not IsNumeric(Qty.Text) Or Qty.Text = "" Then
            e.Cancel = True
            Qty.Select(0, Qty.Text.Length)
            ErrorProvider1.SetError(Qty, "harap di isi, harus angka")
        End If
    End Sub

    Private Sub Harga_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Harga.Validated
        ErrorProvider1.SetError(Harga, "")
    End Sub

    Private Sub Harga_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Harga.Validating
        If Not IsNumeric(Harga.Text) Or Harga.Text = "" Then
            e.Cancel = True
            Harga.Select(0, Harga.Text.Length)
            ErrorProvider1.SetError(Harga, "harap di isi")
        End If
    End Sub

    Private Sub Pot1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot1.Validated
        ErrorProvider1.SetError(Pot1, "")
    End Sub

    Private Sub Pot1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot1.Validating
        If Not IsNumeric(Pot1.Text) Or Pot1.Text = "" Then
            e.Cancel = True
            Pot1.Select(0, Pot1.Text.Length)
            ErrorProvider1.SetError(Pot1, "harap di isi")
        End If
    End Sub

    Private Sub Pot2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Pot2.Validated
        ErrorProvider1.SetError(Pot2, "")
    End Sub

    Private Sub Pot2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Pot2.Validating
        If Not IsNumeric(Pot2.Text) Or Pot2.Text = "" Then
            e.Cancel = True
            Pot2.Select(0, Pot2.Text.Length)
            ErrorProvider1.SetError(Pot2, "harap di isi")
        End If
    End Sub

#End Region

    Private Sub SO_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            Cari_Brg.Show()
            Cari_Brg.dari.Text = "SO"

        ElseIf e.KeyCode = Keys.F3 Then
            DaftarKontak.Show()
            DaftarKontak.dari = "SO"
            DaftarKontak.kode.Focus()
        End If
    End Sub

    Private Sub SO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If

    End Sub

    Private Sub SO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
        Dim c As New clsCCTV.clsSales
        Dim d As DataTable
        d = c.table("%", "%")

        With CBsales
            .DisplayMember = "N_SLS"
            .ValueMember = "K_SLS"
            .DataSource = d
        End With

    End Sub

    Private Sub butINPUT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butINPUT.Click
        If namalgn.Text = "" Then
            MsgBox("Pilih Customer")
            Exit Sub
        ElseIf kode.Text = "" Then
            butSELESAI.Focus()
            Exit Sub
        End If
        Dim a As Boolean = True
        Dim b As Integer

        Panel1.Enabled = False
        frmMenu.MenuStrip1.Enabled = False

        For b = 0 To DGVso.RowCount - 1
            Console.WriteLine(kode.Text & ":" & DGVso.Item("K_BRG", b).Value)
            If kode.Text = DGVso.Item("K_BRG", b).Value.ToString Then
                a = False
            End If
        Next
        If a = False Then
            MsgBox("ada kode barang yg sama")
            Exit Sub
        End If

        With DGVso
            .Rows.Add(1)
            .Rows(DGVso.Rows.Count() - 1).Cells("K_BRG").Value = kode.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("N_BRG").Value = Nama.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("QTY_BRG").Value = Qty.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("SAT_BRG").Value = Sat.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("HARGA_BRG").Value = Harga.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("JUMLAH_BRG").Value = Jumlah.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("pot1_BRG").Value = Pot1.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("pot2_BRG").Value = Pot2.Text
            .Rows(DGVso.Rows.Count() - 1).Cells("TOTAL_BRG").Value = Total.Text
            butSELESAI.Visible = True
            kode.Focus()
        End With

        hitung()

        kode.Clear()
        Nama.Clear()
        Qty.Clear()
        Harga.Clear()
        Sat.Clear()
        Jumlah.Clear()
        Pot1.Clear()
        Pot2.Clear()
        Total.Clear()

    End Sub

    Private Sub Harga_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Harga.Leave, Qty.Leave, Pot1.Leave, Pot2.Leave

        If IsNumeric(Qty.Text) And IsNumeric(Harga.Text) And IsNumeric(Pot1.Text) And IsNumeric(Pot2.Text) Then

            Jumlah.Text = CDbl(Harga.Text) * CDbl(Qty.Text)
            Total.Text = CDbl(Jumlah.Text) * (100 - CDbl(Pot1.Text)) / 100 * (100 - CDbl(Pot2.Text)) / 100
            Harga.Text = FormatNumber(Harga.Text, 2)
            Jumlah.Text = FormatNumber(Jumlah.Text, 2)
            Total.Text = FormatNumber(Total.Text, 2)
            Total2.Text = FormatNumber(Total.Text, 2)
        End If

    End Sub

    Private Sub Total2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Total2.TextChanged
        hitung()
    End Sub

    Sub hitung()
        Dim tot As Double = 0
        Dim i As Integer
        For i = 0 To DGVso.RowCount - 1
            tot = tot + DGVso.Item("TOTAL_BRG", i).Value
        Next
        Try
            Total2.Text = FormatNumber(tot, 2)
            Disc.Text = FormatNumber(Disc.Text, 2)
            Grandtot.Text = CDbl(Total2.Text) - CDbl(Disc.Text)
            If cbPPN.Checked = True Then
                PPN.Text = FormatNumber(Grandtot.Text * (10 / 100), 2)
            Else
                PPN.Text = "0.00"
            End If
            Grandtot2.Text = FormatNumber(CDbl(Grandtot.Text) + CDbl(PPN.Text), 2)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DGVso_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVso.DoubleClick
        Dim gmn As DialogResult
        gmn = MessageBox.Show("Hapus ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If gmn = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim baris As Integer
        baris = DGVso.CurrentRow.Index.ToString

        kode.Text = DGVso.Item("K_BRG", baris).Value.ToString
        Nama.Text = DGVso.Item("N_BRG", baris).Value.ToString
        Qty.Text = DGVso.Item("QTY_BRG", baris).Value.ToString
        Harga.Text = DGVso.Item("HARGA_BRG", baris).Value.ToString
        Sat.Text = DGVso.Item("SAT_BRG", baris).Value.ToString
        Jumlah.Text = DGVso.Item("JUMLAH_BRG", baris).Value.ToString
        Pot1.Text = DGVso.Item("POT1_BRG", baris).Value.ToString
        Pot2.Text = DGVso.Item("POT2_BRG", baris).Value.ToString
        Total.Text = DGVso.Item("TOTAL_BRG", baris).Value.ToString

        DGVso.Rows.RemoveAt(baris)
        hitung()
    End Sub

    Private Sub butBATAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBATAL.Click
        Me.Close()
        frmMenu.MenuStrip1.Enabled = True
    End Sub

    Private Sub butSELESAI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSELESAI.Click
        If Total2.Text = 0 Then
            Exit Sub
        End If
        Dim ask As DialogResult
        ask = MessageBox.Show("Simpan data SO ?", "Lanjutkan?", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim b As New clsCCTV.clsSOJual
        Dim t As String
        t = b.no()

        Dim table1 As New DataTable
        table1.Columns.Add("NO_FAK")
        table1.Columns.Add("K_BRG")
        table1.Columns.Add("N_BRG")
        table1.Columns.Add("QTY")
        table1.Columns.Add("SAT")
        table1.Columns.Add("HARGA")
        table1.Columns.Add("JUMLAH")
        table1.Columns.Add("POT1")
        table1.Columns.Add("POT2")
        table1.Columns.Add("TOTAL")

        Dim i As Integer
        For i = 0 To DGVso.Rows.Count - 1
            table1.Rows.Add(t, DGVso.Item("K_BRG", i).Value.ToString, DGVso.Item("N_BRG", i).Value.ToString, DGVso.Item("QTY_BRG", i).Value.ToString, DGVso.Item("SAT_BRG", i).Value.ToString, CDbl(DGVso.Item("HARGA_BRG", i).Value.ToString), CDbl(DGVso.Item("JUMLAH_BRG", i).Value.ToString), CDbl(DGVso.Item("POT1_BRG", i).Value.ToString), CDbl(DGVso.Item("POT2_BRG", i).Value.ToString), CDbl(DGVso.Item("TOTAL_BRG", i).Value.ToString))
        Next

        Dim a As New clsCCTV.clsSOJual
        Dim p As Boolean
        p = a.input(t, Tanggal.Text, kodelgn.Text, Total2.Text, Disc.Text, Grandtot.Text, PPN.Text, Grandtot2.Text, Keterangan.Text, TanggalJT.Text, CBsales.SelectedValue, CBbayar.Text, CBexpedisi.Text, CBbiayaexp.Text, ket2.Text, table1)

        If p = True Then
            MsgBox("Berhasil Simpan Data SO")
            Me.Close()
            frmMenu.MenuStrip1.Enabled = True
        Else
            MsgBox("Gagal Menyimpan Data SO")
        End If

    End Sub

    Private Sub cbPPN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPPN.CheckedChanged
        hitung()
    End Sub

    Private Sub Disc_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc.Leave
        hitung()
    End Sub

    Private Sub CBlgn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class