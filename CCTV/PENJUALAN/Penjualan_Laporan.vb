﻿Imports System.Drawing.Printing
Imports System.IO

Public Class Penjualan_Laporan

    Public tmpKode(), tmpNama(), tmpQty(), tmpSat(), tmpHrg(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font
    Public hal As Integer = 1

    Private Sub Penjualan_Laporan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If TypeOf Me.ActiveControl Is TextBox Then
                Dim tb As TextBox = DirectCast(Me.ActiveControl, TextBox)
                If tb.Multiline AndAlso tb.AcceptsReturn Then
                    e.Handled = False
                    Exit Sub
                End If
            End If
            e.Handled = True
            Dim oform As Form = Me.FindForm
            oform.SelectNextControl(oform.ActiveControl, True, True, True, True)
            oform.ActiveControl.Focus()
        End If
    End Sub

    Private Sub Penjualan_Laporan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As New clsCCTV.clsLangganan
        Dim b As DataTable
        b = a.table("%", "%")
        Dim row As DataRow
        row = b.NewRow
        row("N_LGN") = "ALL"
        row("K_LGN") = "0"
        b.Rows.Add(row)
        With CBlgn
            .DisplayMember = "N_LGN"
            .ValueMember = "K_LGN"
            .DataSource = b
        End With
        CBlgn.SelectedValue = "0"

        Dim c As New clsCCTV.clsSales
        Dim d As DataTable
        d = c.table("%", "%")
        Dim row2 As DataRow
        row2 = d.NewRow
        row2("N_SLS") = "ALL"
        row2("K_SLS") = "0"
        d.Rows.Add(row2)
        With CBsales
            .DisplayMember = "N_SLS"
            .ValueMember = "K_SLS"
            .DataSource = d
        End With
        CBsales.SelectedValue = "0"
    End Sub

    Private Sub butCARI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCARI.Click
        DGVheader.AutoGenerateColumns = False 'before databinding

        Dim a As New clsCCTV.clsJual
        Dim tb As DataSet

        Dim tmpsup As String = "%"
        If CBlgn.SelectedValue <> "0" Then
            tmpsup = CBlgn.SelectedValue
        End If

        Dim tmpsls As String = "%"
        If CBsales.SelectedValue <> "0" Then
            tmpsls = CBsales.SelectedValue
        End If

        tb = a.table(date1.Text, date2.Text, tmpsup, tmpsls)

        Dim i As Integer
        For i = 0 To tb.Tables("DETJUAL2").Rows.Count - 1
            Console.WriteLine(tb.Tables("DETJUAL2").Rows(i)("SERIAL"))
        Next
        Console.WriteLine("i = " & i)

        Dim parentColumn As DataColumn = tb.Tables("DATJUAL").Columns("NO_FAK")
        Dim childColumn As DataColumn = tb.Tables("DETJUAL").Columns("NO_FAK")
        Dim childColumn2 As DataColumn = tb.Tables("DETJUAL2").Columns("NO_FAK")

        Dim relDATBELIDETBELI As DataRelation
        relDATBELIDETBELI = New DataRelation("DETJUAL", parentColumn, childColumn, False)
        tb.Relations.Add(relDATBELIDETBELI)

        Dim relDATBELIDETBELI2 As DataRelation
        relDATBELIDETBELI2 = New DataRelation("DETJUAL2", parentColumn, childColumn2, False)
        tb.Relations.Add(relDATBELIDETBELI2)

        Dim masterbindingSource As New BindingSource
        Dim detailbindingSource As New BindingSource
        Dim detailbindingSource2 As New BindingSource

        masterbindingSource.DataSource = tb
        masterbindingSource.DataMember = "DATJUAL"

        detailbindingSource.DataSource = masterbindingSource
        detailbindingSource.DataMember = "DETJUAL"

        detailbindingSource2.DataSource = masterbindingSource
        detailbindingSource2.DataMember = "DETJUAL2"

        DGVheader.DataSource = masterbindingSource
        DGVdetail.DataSource = detailbindingSource
        DGVserial.DataSource = detailbindingSource2
        DGVheader.Focus()

        Dim c As New clsCCTV.clsPengguna
        Dim d As Boolean
        d = c.cek("LapJual_Harga")
        If d = False Then
            DGVheader.Columns.Item("TOTAL1").Visible = False
            DGVheader.Columns.Item("DISC").Visible = False
            DGVheader.Columns.Item("GRANDTOT").Visible = False
            DGVheader.Columns.Item("PPN").Visible = False
            DGVheader.Columns.Item("GGTOTAL").Visible = False

            DGVdetail.Columns.Item("HARGA").Visible = False
            DGVdetail.Columns.Item("JUMLAH").Visible = False
            DGVdetail.Columns.Item("POT1").Visible = False
            DGVdetail.Columns.Item("POT2").Visible = False
            DGVdetail.Columns.Item("TOTAL").Visible = False
        End If

    End Sub

    Private Sub DGVheader_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVheader.DoubleClick
        Dim a As New clsCCTV.clsPengguna
        Dim b As Boolean
        b = a.cek("JUAL_EDITHAPUS")
        If b = True Then
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            If DGVheader.Item("BAYAR", row).Value <> 0 Then
                MsgBox("Sudah Pernah Di Bayar")
                Exit Sub
            End If

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As PenjualanEditHapus
            frm = New PenjualanEditHapus
            frm.MdiParent = frmMenu
            frm.Text = "Edit Hapus Penjualan"
            frm.MdiParent = frmMenu
            frm.noFak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.Tanggal.Text = FormatDateTime(DGVheader.Item("TANGGAL", row).Value.ToString, DateFormat.ShortDate)
            frm.langganan.Text = DGVheader.Item("N_LGN", row).Value.ToString
            frm.klgn = DGVheader.Item("K_LGN", row).Value.ToString
            frm.sales.Text = DGVheader.Item("N_SLS", row).Value.ToString
            frm.noSO.Text = DGVheader.Item("NO_SO", row).Value.ToString
            frm.TanggalJT.Text = FormatDateTime(DGVheader.Item("TGL_JT", row).Value.ToString, DateFormat.ShortDate)
            frm.Total2.Text = DGVheader.Item("TOTAL1", row).Value.ToString
            frm.Disc.Text = DGVheader.Item("DISC", row).Value.ToString
            frm.Grandtot.Text = DGVheader.Item("GRANDTOT", row).Value.ToString
            frm.Keterangan.Text = DGVheader.Item("KET", row).Value.ToString
            frm.CBbayar.Text = DGVheader.Item("PEMBAYARAN", row).Value.ToString
            frm.CBbiayaexp.Text = DGVheader.Item("B_EKSPEDISI", row).Value.ToString
            frm.CBexpedisi.Text = DGVheader.Item("EKSPEDISI", row).Value.ToString
            frm.ket2.Text = DGVheader.Item("KET2", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()
        Else
            MsgBox("anda tdk di otorisasi")
        End If

    End Sub

    Private Sub DGVheader_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVheader.KeyDown
        If e.KeyData = Keys.F1 Then
            Dim a As New clsCCTV.clsPengguna
            Dim b As Boolean
            b = a.cek("PRINTULANGLAPORAN")
            If b = True Then
                PRINT()
            Else
                MsgBox("anda tdk di otorisasi")
            End If

        ElseIf e.KeyCode = Keys.F2 Then

            Dim a As New clsCCTV.clsPengguna
            Dim b1 As Boolean
            b1 = a.cek("DetailSerialInput")
            If b1 = False Then
                MsgBox("anda tdk di otorisasi")
                Exit Sub
            End If

            If Me.MdiChildren.Length > 0 Then
                Dim childForm As Form = CType(ActiveMdiChild, Form)
                childForm.Close()
            End If
            Dim frm As SuratJalanDetail
            frm = New SuratJalanDetail
            frm.MdiParent = frmMenu
            frm.Text = "Input Detail Serial Number"
            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            frm.MdiParent = frmMenu
            frm.noFak.Text = DGVheader.Item("NO_FAK", row).Value.ToString
            frm.nlgn.Text = DGVheader.Item("N_LGN", row).Value.ToString
            frm.klgn.Text = DGVheader.Item("K_LGN", row).Value.ToString
            frm.Show()
            frm.Dock = DockStyle.Fill
            Me.Close()

        ElseIf e.KeyCode = Keys.F3 Then

            Dim row As Integer
            row = DGVheader.CurrentRow.Index.ToString
            Dim S As String = DGVheader.Item("NO_FAK", row).Value.ToString
            S = S.Substring(0, 2)
            If S <> "JR" Then
                Exit Sub
            End If
            SuratJalanDetail.tmpnofak = DGVheader.Item("NO_FAK", row).Value.ToString
            SuratJalanDetail.s = DGVheader.Item("NO_FAK", row).Value.ToString
            SuratJalanDetail.printSJ1()

        End If
    End Sub

    Sub PRINT()
        Penjualan.arrke = 0
        Dim baris As Integer
        baris = DGVheader.CurrentRow.Index.ToString
        Dim ask As DialogResult

        Dim otc As New clsCCTV.clsJual
        Dim pr As Boolean
        pr = otc.print(DGVheader.Item("NO_FAK", baris).Value.ToString)

        ask = MessageBox.Show("Print Faktur Penjualan (to Printer) ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then

            If pr = False Then
                MsgBox("Tidak Diizinkan")
                Exit Sub
            End If

            Penjualan.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With Penjualan
                .tmpnofak = .tb.Tables("DATJUAL").Rows(0)("NO_FAK").ToString
            End With

            Dim jumlah As Integer = Penjualan.tb.Tables("DETJUAL").Rows.Count - 1

            ReDim Penjualan.tmpKode(jumlah)
            ReDim Penjualan.tmpNama(jumlah)
            ReDim Penjualan.tmpQty(jumlah)
            ReDim Penjualan.tmpSat(jumlah)
            ReDim Penjualan.tmpHrg(jumlah)
            ReDim Penjualan.tmpJmlh(jumlah)
            ReDim Penjualan.tmpPot1(jumlah)
            ReDim Penjualan.tmpPot2(jumlah)
            ReDim Penjualan.tmpTot(jumlah)

            Dim ipo As Integer
            For ipo = 0 To jumlah
                With Penjualan
                    .tmpKode(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("K_BRG").ToString
                    .tmpNama(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("N_BRG").ToString
                    .tmpQty(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("QTY").ToString
                    .tmpSat(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("SAT").ToString
                    .tmpHrg(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("HARGA").ToString
                    .tmpJmlh(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("JUMLAH").ToString
                    .tmpPot1(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT1").ToString
                    .tmpPot2(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT2").ToString
                    .tmpTot(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("TOTAL").ToString

                End With
            Next

            Penjualan.arrke = 0
            Penjualan.untuk = "FAKTUR PENJUALAN"
            Dim PrintDoc As New PrintDocument
            AddHandler PrintDoc.PrintPage, AddressOf Penjualan.faktur
            PrintDoc.Print()
        End If

        ask = MessageBox.Show("Print Ulang Faktur Penjualan (to PDF) ?", "Konfirmasi", MessageBoxButtons.YesNo)
        If ask = Windows.Forms.DialogResult.Yes Then

            If pr = False Then
                MsgBox("Tidak Diizinkan")
                Exit Sub
            End If
            printpdf.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

            With printpdf
                .tmpnofak = .tb.Tables("DATJUAL").Rows(0)("NO_FAK").ToString
            End With
            printpdf.untuk = "FAKTUR PENJUALAN"
            printpdf.print()
        End If

        Dim s As String = DGVheader.Item("NO_FAK", baris).Value.ToString
        s = s.Substring(0, 2)
        If s = "JR" Then

            ask = MessageBox.Show("Print Surat Jalan (to Printer) ?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.Yes Then

                If pr = False Then
                    MsgBox("Tidak Diizinkan")
                    Exit Sub
                End If

                Penjualan.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

                With Penjualan
                    .tmpnofak = .tb.Tables("DATJUAL").Rows(0)("NO_FAK").ToString
                End With

                Dim jumlah As Integer = Penjualan.tb.Tables("DETJUAL").Rows.Count - 1

                ReDim Penjualan.tmpKode(jumlah)
                ReDim Penjualan.tmpNama(jumlah)
                ReDim Penjualan.tmpQty(jumlah)
                ReDim Penjualan.tmpSat(jumlah)
                ReDim Penjualan.tmpHrg(jumlah)
                ReDim Penjualan.tmpJmlh(jumlah)
                ReDim Penjualan.tmpPot1(jumlah)
                ReDim Penjualan.tmpPot2(jumlah)
                ReDim Penjualan.tmpTot(jumlah)

                Dim ipo As Integer
                For ipo = 0 To jumlah
                    With Penjualan
                        .tmpKode(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("K_BRG").ToString
                        .tmpNama(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("N_BRG").ToString
                        .tmpQty(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("QTY").ToString
                        .tmpSat(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("SAT").ToString
                        .tmpHrg(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("HARGA").ToString
                        .tmpJmlh(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("JUMLAH").ToString
                        .tmpPot1(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT1").ToString
                        .tmpPot2(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("POT2").ToString
                        .tmpTot(ipo) = .tb.Tables("DETJUAL").Rows(ipo)("TOTAL").ToString

                    End With
                Next
                Penjualan.arrke = 0
                Penjualan.untuk = "SURAT JALAN"
                Dim PrintDoc As New PrintDocument
                AddHandler PrintDoc.PrintPage, AddressOf Penjualan.faktur
                PrintDoc.Print()
            End If


            ask = MessageBox.Show("Print Ulang Surat Jalan (to PDF) ?", "Konfirmasi", MessageBoxButtons.YesNo)
            If ask = Windows.Forms.DialogResult.No Then
                Exit Sub
            Else
                If pr = False Then
                    MsgBox("Tidak Diizinkan")
                    Exit Sub
                End If
                printpdf.tb = otc.table(DGVheader.Item("NO_FAK", baris).Value.ToString)

                With printpdf
                    .tmpnofak = .tb.Tables("DATJUAL").Rows(0)("NO_FAK").ToString
                End With
                printpdf.untuk = "SURAT JALAN"
                printpdf.print()
                Exit Sub
            End If
        Else
            MsgBox("Penjualan Project print Surat Jalan dari Laporan Surat Jalan")
            Exit Sub
        End If

    End Sub

    Private Sub CBlgn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBlgn.KeyDown
        If e.KeyCode = Keys.Enter Then
            butCARI.Focus()
        End If
    End Sub

End Class