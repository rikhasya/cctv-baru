﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DaftarKontak
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.butCARI = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.kode = New System.Windows.Forms.TextBox()
        Me.DGVcustomer = New System.Windows.Forms.DataGridView()
        Me.K_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_LGN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALAMAT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELP2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FAX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUST_LAMA = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.USERID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATESTAMP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVsupplier = New System.Windows.Forms.DataGridView()
        Me.K_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.N_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVcustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVsupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.butCARI)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.nama)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.kode)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(2, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(671, 43)
        Me.Panel1.TabIndex = 0
        '
        'butCARI
        '
        Me.butCARI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCARI.ForeColor = System.Drawing.Color.Black
        Me.butCARI.Location = New System.Drawing.Point(426, 11)
        Me.butCARI.Name = "butCARI"
        Me.butCARI.Size = New System.Drawing.Size(75, 23)
        Me.butCARI.TabIndex = 2
        Me.butCARI.Text = "CARI"
        Me.butCARI.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Kode"
        '
        'nama
        '
        Me.nama.Location = New System.Drawing.Point(232, 13)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(163, 20)
        Me.nama.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(191, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nama"
        '
        'kode
        '
        Me.kode.Location = New System.Drawing.Point(49, 13)
        Me.kode.Name = "kode"
        Me.kode.Size = New System.Drawing.Size(119, 20)
        Me.kode.TabIndex = 0
        '
        'DGVcustomer
        '
        Me.DGVcustomer.AllowUserToAddRows = False
        Me.DGVcustomer.AllowUserToDeleteRows = False
        Me.DGVcustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVcustomer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_LGN, Me.N_LGN, Me.ALAMAT, Me.TELP, Me.TELP2, Me.FAX, Me.CUST_LAMA, Me.USERID, Me.DATESTAMP})
        Me.DGVcustomer.Location = New System.Drawing.Point(12, 61)
        Me.DGVcustomer.Name = "DGVcustomer"
        Me.DGVcustomer.ReadOnly = True
        Me.DGVcustomer.RowHeadersVisible = False
        Me.DGVcustomer.Size = New System.Drawing.Size(652, 361)
        Me.DGVcustomer.TabIndex = 20
        Me.DGVcustomer.Visible = False
        '
        'K_LGN
        '
        Me.K_LGN.DataPropertyName = "K_LGN"
        Me.K_LGN.HeaderText = "Kode Customer"
        Me.K_LGN.Name = "K_LGN"
        Me.K_LGN.ReadOnly = True
        '
        'N_LGN
        '
        Me.N_LGN.DataPropertyName = "N_LGN"
        Me.N_LGN.HeaderText = "Nama Customer"
        Me.N_LGN.Name = "N_LGN"
        Me.N_LGN.ReadOnly = True
        '
        'ALAMAT
        '
        Me.ALAMAT.DataPropertyName = "ALAMAT"
        Me.ALAMAT.HeaderText = "Alamat"
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        '
        'TELP
        '
        Me.TELP.DataPropertyName = "TELP"
        Me.TELP.HeaderText = "Telp"
        Me.TELP.Name = "TELP"
        Me.TELP.ReadOnly = True
        '
        'TELP2
        '
        Me.TELP2.DataPropertyName = "TELP2"
        Me.TELP2.HeaderText = "Telp (2)"
        Me.TELP2.Name = "TELP2"
        Me.TELP2.ReadOnly = True
        '
        'FAX
        '
        Me.FAX.DataPropertyName = "FAX"
        Me.FAX.HeaderText = "Fax"
        Me.FAX.Name = "FAX"
        Me.FAX.ReadOnly = True
        '
        'CUST_LAMA
        '
        Me.CUST_LAMA.DataPropertyName = "CUST_LAMA"
        Me.CUST_LAMA.FalseValue = "0"
        Me.CUST_LAMA.HeaderText = "Izin Putang"
        Me.CUST_LAMA.Name = "CUST_LAMA"
        Me.CUST_LAMA.ReadOnly = True
        Me.CUST_LAMA.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CUST_LAMA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CUST_LAMA.TrueValue = "1"
        '
        'USERID
        '
        Me.USERID.DataPropertyName = "USERID"
        Me.USERID.HeaderText = "UserID"
        Me.USERID.Name = "USERID"
        Me.USERID.ReadOnly = True
        '
        'DATESTAMP
        '
        Me.DATESTAMP.DataPropertyName = "DATESTAMP"
        Me.DATESTAMP.HeaderText = "DATESATMP"
        Me.DATESTAMP.Name = "DATESTAMP"
        Me.DATESTAMP.ReadOnly = True
        '
        'DGVsupplier
        '
        Me.DGVsupplier.AllowUserToAddRows = False
        Me.DGVsupplier.AllowUserToDeleteRows = False
        Me.DGVsupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVsupplier.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.K_SUP, Me.N_SUP, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.DGVsupplier.Location = New System.Drawing.Point(12, 61)
        Me.DGVsupplier.Name = "DGVsupplier"
        Me.DGVsupplier.ReadOnly = True
        Me.DGVsupplier.RowHeadersVisible = False
        Me.DGVsupplier.Size = New System.Drawing.Size(652, 361)
        Me.DGVsupplier.TabIndex = 23
        Me.DGVsupplier.Visible = False
        '
        'K_SUP
        '
        Me.K_SUP.DataPropertyName = "K_SUP"
        Me.K_SUP.HeaderText = "Kode Supplier"
        Me.K_SUP.Name = "K_SUP"
        Me.K_SUP.ReadOnly = True
        '
        'N_SUP
        '
        Me.N_SUP.DataPropertyName = "N_SUP"
        Me.N_SUP.HeaderText = "Nama Supplier"
        Me.N_SUP.Name = "N_SUP"
        Me.N_SUP.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "ALAMAT"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Alamat"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "TELP"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Telp"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "TELP2"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Telp (2)"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "FAX"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Fax"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DaftarKontak
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(676, 434)
        Me.Controls.Add(Me.DGVsupplier)
        Me.Controls.Add(Me.DGVcustomer)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "DaftarKontak"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DaftarKontak"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVcustomer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVsupplier, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents butCARI As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kode As System.Windows.Forms.TextBox
    Friend WithEvents DGVcustomer As System.Windows.Forms.DataGridView
    Friend WithEvents K_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_LGN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALAMAT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELP2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FAX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUST_LAMA As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents USERID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATESTAMP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DGVsupplier As System.Windows.Forms.DataGridView
    Friend WithEvents K_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents N_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
